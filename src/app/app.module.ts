import {BrowserModule} from '@angular/platform-browser';
import {NgModule, ErrorHandler} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {APP_BASE_HREF} from '@angular/common';
import {NgHttpLoaderModule} from 'ng-http-loader/ng-http-loader.module';

import {AppRoutingModule} from './app-routing.module';
import {ComponentsModule} from './pages/components/components.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import {environment} from '../environments/environment';

import {ApiService} from './pages/services/api.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MenuService} from './pages/services/menu.service';
import {AlertsService} from './pages/services/alerts.service';
import {GetStartedService} from './pages/services/get-started.service';
import {DEFAULT_TIMEOUT, defaultTimeout, TimeoutInterceptor} from './pages/services/TimeoutInterceptor';
import {GlobalErrorHandler, RollBarService, rollBarFactory} from './pages/services/global-error-handler';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,

    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ComponentsModule,
    NgbModule.forRoot(),
    NgHttpLoaderModule,
  ],
  providers: [
    GetStartedService,
    ApiService,
    {provide: APP_BASE_HREF, useValue: `${environment.basePath}`},
    MenuService,
    AlertsService,
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true},
    {provide: DEFAULT_TIMEOUT, useValue: defaultTimeout},
    {provide: RollBarService, useFactory: rollBarFactory}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
