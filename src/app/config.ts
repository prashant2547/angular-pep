export const LOG_OUT_URL = 'logout';
export const API_URL = 'http://192.168.41.60:5000/';
export const WARNING_MSG = 'Something went wrong!';

// Getting started API Constants
export const GETTING_STARTED_API_URL = 'Homepage';
export const GETTING_STARTED_TIMEZONE_URL = 'Homepage/set_timezone';
export const DASHBOARD_GET_STARTED_STEPS = 'Homepage/steps_dashboard';
export const RESEND_CONFIRMATION = 'userinfo/resendConfirmationLink';
export const UPDATE_ACTIVATION_EMAIL_ADDRESS = 'userinfo/updateEmailAddress';
export const CHECK_ACCOUNT_STATUS = 'userinfo/checkAccountStatus';
export const DELETE_ACCOUNT = 'userinfo/deleteAccountNotification';
export const PROFILE = 'profile';

// Webhooks API Constants
export const WEBHOOK_GLOBALDATA = 'webhook/getWebhook';
export const WEBHOOK_GLOBALDATA_VERIFY = 'webhook/verifyWebhook';
export const WEBHOOK_GLOBALDATA_SUBMIT = 'webhook/saveWebhook';
export const WEBHOOK_CUSTOMEDATA = 'webhook/webhooks_customdata';
export const WEBHOOK_CUSTOMEDATA_VERIFY = 'webhook/verifyWebhook';
export const WEBHOOK_CUSTOMEDATA_SUBMIT = 'webhook/saveWebhook';

// Domain Setting API Constants
export const DOMAIN_DATA = 'domain';
export const DOMAIN_ADD = 'domain/add_domain';
export const VERIFY_DOMAIN = 'domain/verify_domain';
export const VERIFY_CNAME_DOMAIN = 'Domain/verify_cname_domain';
export const VERIFY_SPF = 'domain/verifyspf';
export const VERIFY_DKIM = 'domain/verifydkim';
export const VERIFY_CNAME = 'domain/verify_cname';
export const SAVE_ENVELOP = 'domain/save_envelope';
export const DOMAIN_DELETE = 'domain/deleteDomain';
export const CHECK_APPROVAL = 'domain/checkapproval';
export const SEND_APPROVAL = 'domain/sendapproval';
export const REJECTED_DOMAIN_REDIRECTION =
  'https://docs.pepipost.com/docs/what-to-do-if-your-sender-domain-is-rejected';

// API for getting suppression email addresses
export const GET_SUPPRESSION_EMAIL_ADDRESS = 'Suppression/get_suppression_email_addresses';
export const GET_SUPPRESSION_DOMAIN = 'Suppression/get_suppression_domain';
export const MAKE_BLACKLIST_EMAIL = 'Suppression/blacklist_email';
export const MAKE_WHITELIST_EMAIL = 'Suppression/whitelist_email';
export const MAKE_BLACKLIST_DOMAIN = 'Suppression/blacklist_domain';
export const MAKE_WHITELIST_DOMAIN = 'Suppression/whitelist_domain';
export const DOWNLOAD_SUPPRESSION_EMAIL = 'Suppression/download_suppression_email_addresses';
export const DOWNLOAD_SUPPRESSION_DOMAIN = 'Suppression/download_suppression_domain';

// Billing API Constatnts
export const ADD_NEW_CARD = 'billing/add_new_card';
export const ADD_NEW_CARD_CHARGE_PENDING_BILL = 'billing/add_new_card_carge_pending';
export const ADD_NEW_CARD_AND_MAKE_PRIMARY = 'billing/add_new_card_and_make_primary';
export const ADD_NEWCARD_CHARGE_AND_MAKE_PRIMARY = 'billing/add_newcard_charge_and_make_primary';
export const NEW_CHARGE_PRIMARY_CARD = 'billing/charge_from_primary_card';
export const SEND_CUSTOM_PRICE_REQUEST = 'billing/send_custom_price_request';
export const BILLING_HISTORY = 'billing/billing_history';
export const DOWNLOAD_INVOICE = 'billing/download_invoice';
export const GET_APPLIED_COUPON_CODE = 'billing/get_applied_coupon_code';
export const CHECK_PRPMO_CODE = 'billing/check_promo_code';
export const GET_EMAIL_CREDITS = 'billing/get_available_email_credits';
export const GET_NEW_SLAB_EMAIL_CREDITS = 'billing/get_available_email_credits_dashboard';
export const GET_AVAILABLE_EMAIL_CREDITS = 'billing/get_available_open_credits';
export const GET_POSTPAID_BILLING_DETAILS = 'billing/postpaid_billing_details';
export const UPDATE_AUTO_RENEW = 'billing/update_auto_renew_values';
export const VALIDATE_AUTO_RENEW = 'billing/validate_auto_renew_values';
export const GET_REDEEM_HISTORY = 'billing/get_redeem_history';
export const ENABLE_AUTO_REDEEM = 'billing/enable_auto_redeem';
export const UPDATE_AUTO_RENEW_STATUS = 'billing/update_auto_renew_status';
export const REDEEM_OPEN_CREDITS = 'billing/redeem_open_credits';
export const PREVIEW_INVOICE = 'billing/preview_invoice';
export const RETRIVE_CREDIT_CARD_DETAILS = 'billing/retriving_credit_card';
export const SAVE_BILLING_DETAILS = 'billing/save_billing_deatils';
export const GET_BILLING_DETAILS = 'billing/get_billing_details';
export const APPLY_COUPON = 'billing/apply_coupon';
export const MAKE_PRIMARY_CARD = 'billing/make_primary_card';
export const MAKE_PRIMARY_CARD_CHARGE_UNPAID_BILL = 'billing/make_primary_card_charge_unpaidbill';
export const DELETE_CARD = 'billing/delete_card';
export const GET_SLAB_PLAN = 'billing/get_plan_slab';
export const GET_CURRENT_PLAN = 'billing/get_current_plan';
export const MAKE_PAYMENT = 'billing/make_payment';
export const MAKE_PAYMENT_EXISTING_CARD = 'billing/make_payment_existing_card';
export const GET_AUTORENEW_DETAILS = 'billing/get_recurring_details';
export const GET_SPECIAL_PRICING = 'billing/get_special_pricing';
export const GET_PLAN_TYPE = 'billing/get_plan_type';
export const CHECK_PEPI_CLIENT = 'billing/check_pepipost_client';
export const FETCH_CREDIT_MANAGEMENT_GRAPH_DATA = 'billing/get_credit_management_graph_data';
export const CHECK_UNLIMITED_CODE_APPLIED = 'billing/check_unlimited_code_applied';
export const DOMAIN_APPROVE_CHECK = 'billing/check_domain_approved';
export const CHECK_SUBUSER = 'billing/check_subuser';
export const CHECK_FALCON_PREPAID = 'billing/check_falcon_prepaid';
export const GET_TRANSACTION_DATA = 'billing/get_transaction_data';
export const GET_ALL_INVOICE_DATA = 'billing/get_invoice_data';
export const GET_POSTPAID_REDEEM_HISTORY = 'billing/get_postpaid_redeem_history';
export const GET_UPGRADE_PLAN_DETAILS = 'billing/get_all_plan_details';
export const UPDATE_DOWNGRADE_REQUEST = 'billing/update_downgrade_request';

// Warmup slap API Constants
export const WARMUPSLAB_DETAILS = 'userinfo/warmupSlab/';
export const READ_NOTIFICATION = 'userinfo/markasread';
export const GET_ALL_NOTIFICATION = 'userinfo/getNotifications';
export const INCREASE_SLAB_REQUEST = 'userinfo/increaseSlabRequest';
export const GET_HEADER_NOTIFICATION = 'userinfo/getHeaderNotifications';


// Mail Setting
export const LIST_MAIL_SETTINGS = 'Mailsettings/list_settings';
export const UPDATE_MAIL_SETTINGS = 'Mailsettings/updateSettingValue';
export const UPDATE_UNSUBSCIBE_DETAILS = 'Mailsettings/updateunsubscribe';
export const TOGGLE_MAIL_SETTINGS = 'Mailsettings/updateSettings';


// Integration Setting
export const GET_SMTP_INFO = 'Integrations/get_smtp_info';
export const GET_API_INFO = 'Integrations/get_api_key';
export const GET_SMTP_API_KEY = 'Integrations/get_smtp_api_key';
export const TEST_MAIL = 'Integrations/testmail';
export const RESET_SMTP_PASSWORD = 'Integrations/changeSMTP';
export const SET_SMTP_PASSWORD = 'Integrations/setSMTPPass';

// Profile constants
export const GET_PROFILE_INFO = 'Profile/get_profile_info';
export const GET_COUNTRY_LIST = 'Profile/get_country_list';
export const UPDATE_PROFILE_INFO = 'Profile/update_profile_info';
export const CHANGE_PASSWORD = 'Profile/change_password';
export const CHANGE_EMAIL = 'Profile/change_email';
export const GET_EMAIL_DETAILS = 'Profile/get_email_details';
export const RESEND_EMAIL_VERIFICATION = 'Profile/resend_link';

// Mail Alerts
export const LIST_MAIL_ALERTS = 'Mailalert/list_mail_alerts';
export const ADD_MAIL_ALERTS = 'Mailalert/add_mail_alert';
export const DELETE_ALERTS = 'Mailalert/deletealert';

// Domain
export const DOMAIN_LISTING = 'Domain';
export const ADD_DOMAIN = 'domain/add_domain';
export const ARCHIVE_DOMAIN = 'domain/deleteDomain';
export const FASTRACK_DOMAIN_APPROVAL = 'domain/query_email';
export const GET_DOMAIN_TRACKING_DETAILS = 'domain/get_domainwise_envelope_setting';
export const SET_DOMAIN_TRACKING_DETAILS = 'domain/set_domainwise_envelope_setting';
export const GET_DOMAIN_SUMMARY = 'domain/get_domain_summary';
export const TEST_MAIL_DOMAIN = 'domain/testmail';

// Analytics
export const LIST_TAG_NAMES = 'analytics/getAllTags';
export const MAIL_SUMMARY_TAGS = 'analytics/getTagWiseSummary';
export const MAIL_SUMMARY_TAGS_CSV = 'analytics/getTagWiseSummaryCSV';
export const ANALYTICS_GRAPH = 'analytics/getTagWiseSummary';
export const DELETE_TAG = 'analytics/deleteTag';

// 1000 emails for
export const DEFAULT_MAIL_COUNT = 1000;
export const DEFAULT_EMAIL_PRICE = 0.20;


//
export const GET_LIVEFEED_INFO = 'livefeed/fetchtablefeed';
export const DOWNLOAD_LIVE_REPORT = 'livefeed/downloadreport';
export const SHARE_LIVE_REPORT = 'livefeed/sharereport';
export const SEND_LIVE_FEED = 'livefeed/sendemailreport';

// SubUser
export const GET_ALL_SUBUSERS = 'subuser/getAllSubusers';
export const CREATE_SUBUSER = 'subuser/create';
export const MODIFY_SUBUSER = 'subuser/modify';

// template
export const GET_ALL_TEMPLATES = 'template/getAllTemplate';
export const PREVIEW_TEMPLATE = 'template/previewTemplate';
export const SAVE_TEMPLATE = 'template/saveTemplate';
export const DELETE_TEMPLATE = 'template/deleteTemplate';


// Dashboard
export const DASHBOARD_HISTORY_GRAPH = 'dashboard/fetchDashboardData';
export const GET_CLIENT_ID = 'dashboard/fetchClientId';

export const NO_DATA_FOUND = 'There are no results for your current search criteria.';
export const NONE_FOUND = '<i>None found</i>';
export const UNDEFINED_TEXT = `<i>Not Defined</i>`;
export const DOMAIN_URL_ERROR = 'Please enter a valid URL.';

// Sub Accounts
export const LIST_SUBACCOUNT_DETAILS = 'SubAccounts/list';
export const CREATE_SUBACCOUNT = 'SubAccounts/create';
export const DELETE_SUBACCOUNT = 'SubAccounts/delete';
export const UPDATE_SUBACCOUNT_STATUS = 'SubAccounts/update';
export const UPDATE_SUBACCOUNT_PASSWORD = 'Profile/change_password_subaccount';
export const GET_DOMAIN_LIST = 'SubAccounts/getDomainList';
export const UPDATE_DOMAIN_STATUS = 'Domain/allowManageDomain';
export const ADD_REMOVE_DOMAIN_SUBACCOOUNT = 'Domain/addDomain_SubAccount';
export const SEND_REQUEST_FOR_DEDICATED_IP = 'billing/request_dedicatedIp';
// export const DOWNLOAD_USAGE_SUMMARY = 'billing/downloadUsageSummary';
export const USAGE_SUMMARY = 'Billing/usage_summary';
export const GTE_SUBACCOUNT_BILLING_DETAILS = 'Billing/getSubAccountBilling';
export const CHANGE_CREDIT_TYPE = 'Billing/changeCreditType';
export const UPDATE_RECURRING = 'Billing/updateRecurring';
export const MANAGE_CREDIT = 'Billing/MangeCreditsSubaccounts';
export const ALL_SUBACCOUNT_CREDIT_HISTORY = 'Billing/CreditHistory';
export const CHECK_DOMAIN_ADDITION_PERMISSION = 'Domain/checkDomainPermission';
export const LIST_SUBACCOUNT = 'analytics/getSubAccountsList';
export const DOWNLOAD_ACCOUNT_LOG = 'Billing/CreditHistoryDownload';
export const DOWNLOAD_USAGE_SUMMARY = 'Billing/usage_summary_download';
