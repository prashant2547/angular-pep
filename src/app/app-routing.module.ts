import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [{
  path: 'getting-started',
  loadChildren: 'app/pages/getting-started/getting-started.module#GettingStartedModule'
}, {
  path: 'settings',
  loadChildren: 'app/pages/settings/settings.module#SettingsModule'
}, {
  path: 'dashboard',
  loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule'
}, {
  path: 'live',
  loadChildren: 'app/pages/live/live.module#LiveModule'
}, {
  path: 'analytics',
  loadChildren: 'app/pages/analytics/analytics.module#AnalyticsModule'
}, {
  path: 'suppression',
  loadChildren: 'app/pages/suppression/suppression.module#SuppressionModule'
}, {
  path: 'billings',
  loadChildren: 'app/pages/billings/billings.module#BillingsModule'
}, {
  path: 'notification',
  loadChildren: 'app/pages/notification/notification.module#NotificationModule'
}, {
  path: 'profile',
  loadChildren: 'app/pages/profile/profile.module#ProfileModule'
}, {
  path: 'help',
  loadChildren: 'app/pages/help/help.module#HelpModule'
}, {
  path: '',
  redirectTo: 'dashboard',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    enableTracing: false,
  })],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
