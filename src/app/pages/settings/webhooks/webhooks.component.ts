import {Component, OnInit} from '@angular/core';
import {Response} from '@angular/http';
import {
  DOMAIN_URL_ERROR,
  WARNING_MSG,
  WEBHOOK_CUSTOMEDATA_SUBMIT,
  WEBHOOK_CUSTOMEDATA_VERIFY,
  WEBHOOK_GLOBALDATA,
  WEBHOOK_GLOBALDATA_SUBMIT,
  WEBHOOK_GLOBALDATA_VERIFY
} from '../../../config';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {ApiService} from '../../services/api.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {API} from '../../components/forms/form-validation';

@Component({
  selector: 'app-webhooks',
  templateUrl: './webhooks.component.html',
  styleUrls: ['./webhooks.component.scss']
})
export class WebhooksComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'What are Webhooks?',
      helpLink: 'https://docs.pepipost.com/docs/what-are-webhooks'
    }, {
      helpText: 'What are the different Webhooks format?',
      helpLink: 'https://docs.pepipost.com/docs/what-are-the-different-webhooks-format'
    }, {
      helpText: 'How to test and debug Webhooks?',
      helpLink: 'https://docs.pepipost.com/docs/how-to-test-and-debug-webhooks'
    }, {
      helpText: 'What happens if my Webhook URL is down or can\'t accept requests?',
      helpLink: 'https://docs.pepipost.com/docs/what-happens-if-my-webhook-url-is-down-or-cant-accept-requests'
    },
  ];

  data: any;
  globaldata: any = '';
  customedata: any = '';
  success = false;
  error = false;
  respClass = '';
  global_webhook_urlmsg = '';
  global_webhook_url1msg = '';
  global_webhook_url2msg = '';
  global_webhook_url3msg = '';
  global_webhook_url4msg = '';
  global_webhook_url5msg = '';
  global_webhook_url6msg = '';
  global_webhook_url7msg = '';
  global_webhook_url8msg = '';
  globalType = '';
  globalMsg = '';
  customType = '';
  customMsg = '';
  type: string;
  globalAPI = '';
  globalForm: FormGroup;
  customForm: FormGroup;
  domainUrlError = DOMAIN_URL_ERROR;

  constructor(private apiService: ApiService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.globalForm = new FormGroup({
      global_webhook_url: new FormControl('', [
        Validators.required,
        Validators.pattern(API)
      ]),
    });
    this.customForm = this.fb.group({
      global_webhook_url1: ['', [Validators.pattern(API)]],
      global_webhook_url2: ['', [Validators.pattern(API)]],
      global_webhook_url3: ['', [Validators.pattern(API)]],
      global_webhook_url4: ['', [Validators.pattern(API)]],
      global_webhook_url5: ['', [Validators.pattern(API)]],
      global_webhook_url6: ['', [Validators.pattern(API)]],
      global_webhook_url7: ['', [Validators.pattern(API)]],
      global_webhook_url8: ['', [Validators.pattern(API)]],
    });
    // to load global data // working
    this.fetchdata();
  }

  fetchdata() {
    this.apiService.post(WEBHOOK_GLOBALDATA, '')
      .subscribe(result => {
        console.log((result));
        if (result['status'] === 'success') {
          if (result['data']['global_url'] === 1) { // custome data
            this.globaldata = result['data']['url'];
            this.customedata = [];
          } else if (result['data']['global_url'] === 0) {
            this.customedata = result['data']['url'];
            this.globaldata = [];
          } else {
            this.globaldata = [];
            this.customedata = [];
          }
        } else {
          this.globalMsg = this.customMsg = result['data']['message'];
          this.globalType = this.customType = 'danger';
        }
      }, error => {
        this.globalMsg = this.customMsg = WARNING_MSG;
        this.globalType = this.customType = 'warning';
      });
  }

  // Code to handle Global API webhooks settings //working
  cancelGlobalApi(): void {
    this.refreshData();
  }


  verifyGlobalApiDetails(optionvalue: any, optionname: any) {
    this.clearMessageData();
    console.log(optionname);
    this.apiService.post(WEBHOOK_GLOBALDATA_VERIFY, ({'url': optionvalue}))
      .subscribe((result) => {
        console.log(result);
        if (result['status'] === 'success') {
          this.respClass = 'info';
          this[optionname] = result['message'];
        } else {
          this.respClass = 'danger';
          console.log(this[optionname]);
          this[optionname] = result['error_info']['message'];
        }
      }, error => {
        this[optionname] = WARNING_MSG;
      });
    return false;
  }

// Save global api data // done working
  saveGlobalApiDetails(optionvalue: any): void {
    this.clearMessageData();
    this.apiService.post(WEBHOOK_GLOBALDATA_SUBMIT, ({
      'webhook_url': 'GLOBAL',
      'global_webhook_url': optionvalue
    }))
      .subscribe((result) => {
        console.log(result);
        console.log(result['message']);
        if (result['status'] === 'success') {
          this.globalMsg = result['message'];
          this.globalType = 'info';
        } else {
          this.globalMsg = result['error_info']['message'];
          this.globalType = 'danger';
        }
      }, error => {
        this.globalMsg = WARNING_MSG;
        this.globalType = 'warning';
      });
    console.log(this.globalMsg);
    console.log(this.globalType);
  }


// Code to handle Custome API webhooks settings //working
  cancelCustomApi(): void {
    this.refreshData();
  }

  verifyCustomeApiDetails(optionvalue: any, optionname: any): void {
    this.clearMessageData();
    this.apiService.post(WEBHOOK_CUSTOMEDATA_VERIFY, ({'url': optionvalue}))
      .subscribe((result) => {
        console.log(result);
        if (result['status'] === 'success') {
          this.respClass = 'info';
          this[optionname] = result['message'];
        } else {
          this.respClass = 'danger';
          this[optionname] = result['error_info']['message'];
        }
      });
  }

// Save cutom api data // done working
  saveCustomeApiDetails(): void {
    this.clearMessageData();
    this.apiService.post(WEBHOOK_CUSTOMEDATA_SUBMIT, ({
      'webhook_url': 'CUSTOM',
      ...this.customedata
    }))
      .subscribe((result) => {
        console.log(result);
        if (result['status'] === 'success') {
          this.customMsg = result['message'];
          this.customType = 'info';
        } else {
          this.customMsg = result['error_info']['message'];
          this.customType = 'danger';
        }
      }, error => {
        this.customMsg = WARNING_MSG;
        this.customType = 'warning';
      });
  }

  refreshData() {
    this.clearMessageData();
    this.fetchdata();
  }

  clearMessageData() {
    this.global_webhook_urlmsg = this.global_webhook_url1msg = this.global_webhook_url2msg =
      this.global_webhook_url3msg = this.global_webhook_url4msg =
        this.global_webhook_url5msg = this.global_webhook_url6msg =
          this.global_webhook_url7msg = this.global_webhook_url8msg =
            this.globalAPI = this.globalMsg = this.globalType = this.customMsg = this.customType = '';
  }
}
