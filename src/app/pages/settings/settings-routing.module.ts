import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SettingsComponent} from './settings.component';
import {DomainComponent} from './domain/domain.component';
import {IntegrationComponent} from './integration/integration.component';
import {MailSettingsComponent} from './mail-settings/mail-settings.component';
import {WebhooksComponent} from './webhooks/webhooks.component';
import {ManageTagsComponent} from './manage-tags/manage-tags.component';
import {TemplateComponent} from './template/template.component';
import {SubUserManagementComponent} from './sub-user-management/sub-user-management.component';
import {DomainDetailsComponent} from './domain/domain-details/domain-details.component';
import {TemplateListComponent} from './template/template-list/template-list.component';
import {TemplatePreviewComponent} from './template/template-preview/template-preview.component';
import {TemplateEditComponent} from './template/template-edit/template-edit.component';
import {WarmUpComponent} from './warmup/warmup.component';
import {MailAlertsComponent} from '../profile/mail-alerts/mail-alerts.component';
import {SubAccountsComponent} from './sub-accounts/sub-accounts.component';

const routes: Routes = [{
  path: '',
  component: SettingsComponent,
  children: [{
    path: 'domain',
    component: DomainComponent,
    pathMatch: 'full',
  }, {
    path: 'domain/:id',
    component: DomainDetailsComponent,
    pathMatch: 'full',
  }, {
    path: 'integration',
    component: IntegrationComponent
  }, {
    path: 'mail-settings',
    component: MailSettingsComponent
  }, {
    path: 'webhooks',
    component: WebhooksComponent
  }, {
    path: 'manage-tags',
    component: ManageTagsComponent
  }, {
    path: 'sub-accounts',
    component: SubAccountsComponent
  }, {
    path: 'template',
    component: TemplateComponent,
    children: [{
      path: '',
      component: TemplateListComponent
    }, {
      path: 'preview',
      component: TemplateListComponent,
      pathMatch: 'full'
    }, {
      path: 'preview/:id',
      component: TemplatePreviewComponent
    }, {
      path: 'edit/:id',
      component: TemplateEditComponent
    }]
  }, {
    path: 'sub-user-management',
    component: SubUserManagementComponent
  }, {
    path: 'warmup',
    component: WarmUpComponent
  }, {
    path: 'mail-alerts',
    component: MailAlertsComponent
  }, {
    path: '',
    redirectTo: 'domain',
    pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SettingsRoutingModule {
}
