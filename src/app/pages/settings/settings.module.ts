import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SettingsRoutingModule} from './settings-routing.module';
import {SettingsComponent} from './settings.component';
import {DomainComponent} from './domain/domain.component';
import {IntegrationComponent} from './integration/integration.component';
import {ManageTagsComponent} from './manage-tags/manage-tags.component';
import {SubUserManagementComponent} from './sub-user-management/sub-user-management.component';
import {MailSettingsComponent} from './mail-settings/mail-settings.component';
import {WebhooksComponent} from './webhooks/webhooks.component';
import {TemplateComponent} from './template/template.component';
import {CKEditorModule} from 'ng2-ckeditor';
import {ComponentsModule} from '../components/components.module';
import {ClipboardModule} from 'ngx-clipboard';
import {DomainDetailsComponent} from './domain/domain-details/domain-details.component';
import {ScrollbarModule} from 'ngx-scrollbar';
import {ArchiveComponent} from './template/archive/archive.component';
import {TemplateListComponent} from './template/template-list/template-list.component';
import {TemplatePreviewComponent} from './template/template-preview/template-preview.component';
import {TemplateEditComponent} from './template/template-edit/template-edit.component';
import {CodemirrorModule} from 'ng2-codemirror';
import {WarmUpComponent} from './warmup/warmup.component';
import {MailAlertsComponent} from '../profile/mail-alerts/mail-alerts.component';
import {SubAccountsComponent} from './sub-accounts/sub-accounts.component';
// Import your library
import {DualListBoxModule} from 'ng2-dual-list-box';
import {Daterangepicker} from 'ng2-daterangepicker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SettingsRoutingModule,
    ComponentsModule,
    CKEditorModule,
    ClipboardModule,
    ScrollbarModule,
    CodemirrorModule,
    DualListBoxModule.forRoot(),
    Daterangepicker,
  ],
  declarations: [
    SettingsComponent,
    DomainComponent,
    IntegrationComponent,
    MailSettingsComponent,
    WebhooksComponent,
    ManageTagsComponent,
    TemplateComponent,
    SubUserManagementComponent,
    DomainDetailsComponent,
    ArchiveComponent,
    TemplateListComponent,
    TemplatePreviewComponent,
    TemplateEditComponent,
    WarmUpComponent,
    MailAlertsComponent,
    SubAccountsComponent,
  ]
})
export class SettingsModule {
}
