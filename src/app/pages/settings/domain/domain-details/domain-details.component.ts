import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  ARCHIVE_DOMAIN,
  CHECK_APPROVAL,
  GET_DOMAIN_SUMMARY,
  GET_DOMAIN_TRACKING_DETAILS, NONE_FOUND,
  SAVE_ENVELOP,
  SEND_APPROVAL,
  SET_DOMAIN_TRACKING_DETAILS,
  VERIFY_CNAME,
  VERIFY_CNAME_DOMAIN,
  VERIFY_DKIM,
  VERIFY_DOMAIN,
  VERIFY_SPF,
  WARNING_MSG
} from '../../../../config';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../../../services/api.service';
import {Location} from '@angular/common';
import {GetStartedService} from '../../../services/get-started.service';

@Component({
  selector: 'app-domain-details',
  templateUrl: './domain-details.component.html',
  styleUrls: ['./domain-details.component.scss']
})
export class DomainDetailsComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: ' What is SPF?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-spf-and-dkim-and-why-these-are-important-to-have'
    }, {
      helpText: 'What is DKIM?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-spf-and-dkim-and-why-these-are-important-to-have'
    }, {
      helpText: 'What is Custom envelope and why it is required?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-custom-envelope'
    }, {
      helpText: 'What will happen if I delete a domain?',
      helpLink: 'https://docs.pepipost.com/docs/what-will-happen-if-i-delete-a-domain'
    },
  ];

  spf = 'SPF/DKIM';
  editSpf = false;
  url: '';
  domainId: number;
  cnameDomainData: any;
  spfDomainData: any;
  dkimDomainData: any;
  customDomainData: any;
  requiredText = 'SPF & DKIM';
  domain = '';
  domainName = '';
  checkType = false;
  typeName = 'spf_dkim';
  showArchivePop: boolean;
  trackingData: any = '';
  trackingMsg = '';
  trackingType = '';
  domainSummaryMsg = '';
  domainSummaryType = '';
  evelopeData = '';
  customEnvelopStatus = 0;
  cnameApprovalValue = 0;
  isSpfCopied: boolean;
  isDkimCopied: boolean;
  isSpfDkimCustomCopied: boolean;
  isCnameCopied: boolean;
  // var for archive display message
  archiveMsg = '';
  archiveType = '';
  stepList = [];
  stepCount = 0;
  spfSubValue = '';
  requiredValError = '';
  requiredValErrorCname = '';
  hideCname = true;
  is_subDomain = 0;
  dnsStatusMessage = 'We couldn\'t find this record when we checked your DNS records. Please check you\'ve entered it correctly with your DNS provider. Sometimes it might take 24-48 hrs of time to reflect the update the DNS records.';
  domainActionFlag = false;

  constructor(private location: Location,
              private route: ActivatedRoute,
              private http: HttpClient,
              private apiService: ApiService,
              private getStartedService: GetStartedService,
              private router: Router) {
  }

  ngOnInit() {
    this.domainId = +this.route.snapshot.paramMap.get('id');
    const param = this.router.parseUrl(this.router.url);
    this.domain = param.queryParams.domain;
    this.domainName = this.domain;
    // in case of domain hide drop down for CNAME
    const len = this.findDomainSubDomain(this.domainName, '\\.');
    this.hideCname = len > 1 ? false : true;
    this.is_subDomain = len > 1 ? 1 : 0;


    this.getDomainStatusDetails();
    this.getDomainTrackingDetails();

    // get started
    this.getStartedService.getSteps().subscribe(result => {
      this.stepList = result['stepList'] || [];
      this.stepCount = result['stepCount'] || 0;
    });
  }
  // display seleted text on single mouse click
  selectText(containerid) {
    const range = document.createRange();
    range.selectNode(document.getElementById(containerid));
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(range);
  }
  onStepClick(evt) {
    this.getStartedService.goToStep(evt);
  }

  // this function will give you  SPF,DKIM,CNAME envelop data,
  // based on the data current and required value of spf are modified.
  getDomainStatusDetails() {

    this.apiService.post(GET_DOMAIN_SUMMARY, {
      'domid': this.domainId,
      'domain': this.domain,
      'type': this.typeName,
      'is_subDomain': this.is_subDomain
    })
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.domainActionFlag = result['data']['subaccount_flag'] === 1 ? true : false;
            // cname data
            if ('cname' in result['data']) {
              this.cnameDomainData = result['data']['cname'];
              this.cnameDomainData['current_value'] = this.cnameDomainData['current_value'] === '' ? NONE_FOUND
                : this.cnameDomainData['current_value'];

              this.cnameApprovalValue = result['data']['cname']['is_verified'];
              // to display selected drop down
              this.spf = result['data']['cname']['is_verified'] || this.typeName === 'cname' ? 'CNAME' : 'SPF/DKIM';
              // to send selected drop down value to API
              this.typeName = this.cnameApprovalValue || this.typeName === 'cname' ? 'cname' : 'spf_dkim';

              this.checkType = this.spf.toLowerCase() === 'cname' ? true : false;
              this.requiredValErrorCname = result['data']['cname']['is_matched'] !== 1 ? 'Current value should be same as required value' : '';
            }
            // spf data
            this.spfDomainData = result['data']['spf'];
            // get size of object or array
            const dataSize = this.getObjectSize(this.spfDomainData['current_value']);
            if (dataSize <= 2) {
              // for blank
              this.requiredValError = +result['data']['spf']['spf_matched'] === 0 ? 'Copy the record and place it in your DNS settings.' : '';
              const spfDomainCurVal = this.spfDomainData['current_value'];
              let spfDomainCurValStr;

              spfDomainCurValStr = this.mergeObjToString(spfDomainCurVal);
              // overwrite string data to current value
              this.spfDomainData['current_value'] = spfDomainCurValStr ? spfDomainCurValStr : NONE_FOUND;
            } else if (dataSize > 2) {
              this.spfDomainData['current_value'] = this.mergeObjToString(this.spfDomainData['current_value'], '<br/>');
              this.requiredValError = 'Please merge all spf record into one.';
            } else {
              this.requiredValError = 'Current value should contain include: ppspf.com.';
              this.spfDomainData['current_value'] = NONE_FOUND;
            }


            // dkim data
            this.dkimDomainData = result['data']['dkim'];
            this.dkimDomainData['current_value'] = this.dkimDomainData['current_value'] === '' ? NONE_FOUND
              : this.dkimDomainData['current_value'];

          } else {
            this.domainSummaryMsg = result['error_info']['message'];
            this.domainSummaryType = 'danger';
          }
        }
      );
  }

  getDomainTrackingDetails() {
    this.domainName = 'delivery';
    this.apiService.post(GET_DOMAIN_TRACKING_DETAILS, {'domid': this.domainId})
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.trackingData = result['data'];
          this.evelopeData = (this.trackingData['envdom'].split(this.domain)[0]).slice(0, -1) || 'delivery';
          this.verifyCname(this.evelopeData);
        } else {
          this.trackingMsg = result['error_info']['message'];
          this.trackingType = 'danger';
        }
      });
    return (this.evelopeData);
  }

  openEmailPopup() {
    console.log('email popup');
    return false;
  }

  downloadDomainDetails() {
    console.log('download domain details');
    return false;
  }

  onArchiveClick() {
    this.showArchivePop = !this.showArchivePop;
    return false;
  }

  onCancelArchiveClick() {
    this.showArchivePop = false;
  }

  // This function will show , hide the table data based on the mentioned condition
  displayData(evt) {
    this.typeName = evt.target.value.toLowerCase();
    this.checkType = this.typeName === 'cname' ? true : false;
    this.requiredText = this.typeName === 'cname' ? 'CNAME' : '(SPF & DKIM)';
  }

  /**
   * for updating domain tracking details
   * @param value will be true or false
   * @param trackType will be type of domain tracking
   */
  updateTrackingDetails(value, trackType) {
    this.apiService.post(SET_DOMAIN_TRACKING_DETAILS,
      {'domid': this.domainId, 'tracking': trackType, 'value': value === true ? 1 : 0})
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.trackingMsg = result['message'];
            this.trackingType = 'info';
          } else {
            this.trackingMsg = result['error_info']['message'];
            this.trackingType = 'danger';
          }
        }
      );
  }

  // on archive click delete domain and redirect to domain list page
  onArchiveConfirm() {
    this.apiService.post(ARCHIVE_DOMAIN, {'domid': this.domainId})
      .subscribe((result) => {
          if (result['status'] === 'success') {
            // this.location.back();
            this.router.navigate(['/settings/domain/']);
          } else {
            this.archiveMsg = result['error_info']['message'];
            this.archiveType = 'danger';
          }
        }
      );
  }

  // based on the drop down value whether SPF/DKIM or CNAME respective function get called to verify the same
  checkDnsRecord(envDomain) {
    this.getDomainStatusDetails();
    this.evelopeData !== 'delivery' ? this.verifyCname(envDomain) : this.verifyCname(envDomain);
  }

  verifySpf() {
    this.apiService.post(VERIFY_SPF,
      {
        'domid': this.domainId,
        'domain': this.domain
      })
      .subscribe((result) => {
          if (result['status'] === 'success') {
          } else {
          }
        }
      );
  }

  verifyDkim() {
    this.apiService.post(VERIFY_DKIM,
      {
        'domid': this.domainId,
        'domain': this.domain
      })
      .subscribe((result) => {
          if (result['status'] === 'success') {
          } else {
          }
        }
      );
  }

  verifyCustom() {
    console.log('in verify custom');
  }

  verifyCnameDomain() {
    this.apiService.post(VERIFY_CNAME_DOMAIN,
      {
        'domain': this.domain,
        'domid': this.domainId
      })
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.cnameDomainData = result['data']['cname'];
            this.cnameApprovalValue = result['data']['cname']['is_verified'];
            this.requiredValErrorCname = this.cnameApprovalValue !== 1 ? 'Current value and required value should be same' : '';
          }
        }
      );
  }

  verifyCname(envDomain = '') {
    if (envDomain === '') {
      envDomain = this.evelopeData;
    }
    this.apiService.post(VERIFY_CNAME,
      {
        'setting_name': 'ltut',
        'setting_value': envDomain,
        'domain': this.domain,
        'domid': this.domainId
      })
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.customDomainData = result['data']['custom_envelope'];
            this.customEnvelopStatus = result['data']['custom_envelope']['is_verified'];
            this.customDomainData['current_value'] = this.customDomainData['current_value'] === '' ? NONE_FOUND
              : this.customDomainData['current_value'];
          }
        }
      );
  }

  checkApproval() {
    this.apiService.post(CHECK_APPROVAL,
      {
        'domain': this.domain,
        'domid': this.domainId
      })
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.sendApproval();
          }
        }
      );
  }

  sendApproval() {
    this.apiService.post(SEND_APPROVAL,
      {
        'domain': this.domain,
        'domid': this.domainId
      })
      .subscribe((result) => {
          console.log(result);
        }
      );
  }

  save_envelope(envDomain) {
    this.apiService.post(SAVE_ENVELOP,
      {
        'setting_value': envDomain + '.' + this.domain,
        'domain': this.domain,
        'domid': this.domainId
      })
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.customEnvelopStatus = 1;
          } else {
            this.customEnvelopStatus = 0;
          }
        }
      );
  }

  domainNameDisp(label) {
    this.domainName = label;
  }

  /**
   * This function will merge the Array or object into single string
   * @param source
   * @param {string} join this parameter will be concatenation parameter between string
   * @returns {string}
   */
  mergeObjToString(source: any, join: string = ''): string {
    let str = '';
    if (source instanceof Array) {
      for (const row of source) {
        str += row + join;
      }
    } else {
      for (const row of Object.keys(source)) {
        str += source[row] + join;
      }
    }
    return str;
  }

  /**
   * this fcuntion will return the size of passed parameter
   * @param source can either array or object
   * @returns {number}
   */
  getObjectSize(source: any): number {
    console.log(source instanceof Array);
    return (source instanceof Array) ? source.length : Object.keys(source).length;
  }

  /**
   * function to get required value to be displayed into SPF record
   * @param source will be either array or object
   * @returns {string}
   */
  getRequiredvalue(source: any) {
    let requiredValue = '';
    if (source instanceof Array) {
      for (const row of source) {
        requiredValue += row.substring(6, row.indexOf('~'));
      }
    } else {
      for (const row of Object.keys(source)) {
        requiredValue += source[row].substring(7, source[row].indexOf('~'));
      }
    }
    requiredValue = 'v=spf1 a mx ' + requiredValue + '~all';
    return requiredValue;
  }

  getRequiredErrorForCname(current_value, required_value) {
    if (current_value.indexOf(required_value) === -1) {
      this.requiredValErrorCname = 'Current value should contain include: pepipost.net ' +
        'for the refer required value and do the verification';
    } else {
      this.requiredValErrorCname = '';
    }
  }

  editSpfClick() {
    this.editSpf = true;
    return false;
  }

  /**
   * Find wether the given value is domain or sub domain
   * @param domainName
   * @param letter
   * @returns {number}
   */
  findDomainSubDomain(domainName, letter) {
    return (domainName.match(RegExp(letter, 'g')) || []).length;
  }


}
