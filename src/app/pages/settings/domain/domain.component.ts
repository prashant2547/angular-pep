import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TableDataSource} from '../../services/table.data-source';
import {Http} from '@angular/http';
import {
  ADD_DOMAIN, API_URL, CHECK_DOMAIN_ADDITION_PERMISSION, DOMAIN_LISTING, FASTRACK_DOMAIN_APPROVAL, GET_SMTP_INFO, NO_DATA_FOUND,
  REJECTED_DOMAIN_REDIRECTION, TEST_MAIL,
  TEST_MAIL_DOMAIN,
  WARNING_MSG
} from '../../../config';
import {TableDomainStatusComponent} from '../../components/table/table-domain-status/table-domain-status.component';
import {DOMAIN_ACTIVE, DOMAIN_REJECTED, DOMAIN_UNDER_APPROVAL} from '../../components/table/table-domain-status/table-domain-status';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomNullValidation, DOMAIN, EMAIL, FileValidation} from '../../components/forms/form-validation';
import {ApiService} from '../../services/api.service';
import {environment} from '../../../../environments/environment';
import {GetStartedService} from '../../services/get-started.service';
import {TableSettingsTextComponent} from '../../components/table/table-settings-text/table-settings-text.component';

@Component({
  selector: 'app-domain',
  templateUrl: './domain.component.html',
  styleUrls: ['./domain.component.scss']
})
export class DomainComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'What is Sender Domain?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-a-sending-domain-how-to-set-up-sending-domains'
    }, {
      helpText: ' What is SPF?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-spf-and-dkim-and-why-these-are-important-to-have'
    }, {
      helpText: 'What is DKIM?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-spf-and-dkim-and-why-these-are-important-to-have'
    }, {
      helpText: 'Know about different Domain Statuses',
      helpLink: 'https://docs.pepipost.com/docs/know-about-different-domain-statuses'
    }
  ];

  // var to show hide no data found window
  nodataFlag = true;
  tableFlag = true;

  // Refer documentation of ng2-smart-table for settings
  domainListSource: any;
  initialAddDomain: boolean;
  domainsInfo = 'All sending domains will need a clear web presence. ' +
    '<a href=\'https://docs.pepipost.com/docs/requirements-for-sending-domains\' target=\'_blank\'>' +
    'Learn about review requirements.</a>';

  // popup vars
  fastTrackRow: any;
  testMailRow: any;
  // Five step for fastrack form var
  step1Form: FormGroup;
  step2Form: FormGroup;
  step3Form: FormGroup;
  step4Form: FormGroup;
  step5Form: FormGroup;
  verifyDomainForm: FormGroup;
  testMailForm: FormGroup;
  // for the custom required field validation
  customValidation: CustomNullValidation;
  customerror = '';
  // for file validation includind size and type of file
  fileValidation: FileValidation;
  fileErrorList: string[] = [];
  // fastrack popup window
  @ViewChild('fastTrackPopup') fastTrackPopup;
  @ViewChild('addDomainPopup') addDomainPopup;
  @ViewChild('testMailPopup') testMailPopup;
  fastTrackCurStep: number;
  hideBack = false;
  popRef: NgbActiveModal;

  otherSubscibtionSource = '';
  subscibtionSource = '';
  dispOtherMailType = false;
  dispOtherSubscribtionSource = true;
  // checkBoxValue = [];
  checkBoxValue = new Set();
  checkBoxValueSize: number;
  data: Object[] = [];
  file: File;
  filename = '';
  fileMsg = '';
  dom_id;

  fastTrackMsg = '';
  fastTrackType = '';
  addDomainMsg = '';
  addDomainType = '';

  smtpdata = '';
  smtptype = '';
  smtpmsg = '';
  testMailType = '';
  testMailMsg = '';

  domainFlag = true;
  domainAdditionFlag = true;

  stepList = [];
  stepCount = 0;
  // https://akveo.github.io/ng2-smart-table/#/documentation
  domainListSettings = {
    attr: {
      class: 'table'
    },
    columns: {
      domain: {
        title: 'Domain name',
        sort: false
      },
      status: {
        title: 'Status',
        sort: false,
        type: 'custom',
        renderComponent: TableDomainStatusComponent,
        onComponentInitFunction: (instance) => {
          instance.icon1Click.subscribe(row => {
            switch (row.domain_status) {
              case DOMAIN_UNDER_APPROVAL :
                this.fastTrackCurStep = 1;
                this.fastTrackRow = row;
                this.open(this.fastTrackPopup);
                break;
              case DOMAIN_ACTIVE:
                this.testMailRow = row;
                this.fetchSmtpData();
                this.open(this.testMailPopup);
                break;
              case DOMAIN_REJECTED:
                // redirect to given link
                window.open(REJECTED_DOMAIN_REDIRECTION);
                break;
              default:
                // console.log('row.domain_status -> ', row.domain_status);
                this.router.navigate([row.domid], {relativeTo: this.route, queryParams: {domain: row.domain}});
                console.log(row);
                break;
            }
          });
        }
      },
      action: {
        title: 'Action',
        sort: false,
        type: 'custom',
        renderComponent: TableSettingsTextComponent,
        onComponentInitFunction: (instance) => {
          instance.icon1Click.subscribe(row => {
            this.router.navigate([row.domid], {relativeTo: this.route, queryParams: {domain: row.domain}});
          });
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      showSelectPage: true,
      selectOptions: [10, 20, 30],
      perPage: 10
    }
  };

  constructor(private router: Router,
              private route: ActivatedRoute,
              private http: Http,
              private modalService: NgbModal,
              private fb: FormBuilder,
              private apiService: ApiService,
              private getStartedService: GetStartedService) {

  }

  ngOnInit() {
    this.fetchDomainAdditionPermission();
    // fetch domain details
    this.fetchDomainDetails();
    // file validation
    this.customValidation = new CustomNullValidation();
    this.fileValidation = new FileValidation(1024 * 512, ['doc', 'docx', 'pdf', 'html', 'png', 'jpg', 'jpeg', 'zip']);
    this.customerror = this.customValidation.required(this.filename);
    console.log(this.customerror.length);
    this.step1Form = this.fb.group({
      fastTrackFile: [''],
    });

    this.step2Form = new FormGroup({
      mailType: new FormControl(['',
        Validators.required
      ]),
    });
    this.step3Form = this.fb.group({
      noOfEmail: ['', [Validators.required]],
    });
    this.step4Form = this.fb.group({
      subscribtionSource: ['', [Validators.required]],
    });
    this.step5Form = this.fb.group({
      sendingList: ['', [Validators.required]],
    });

    // form validation
    this.verifyDomainForm = new FormGroup({
      domain_name: new FormControl('', [
        Validators.required,
        Validators.pattern(DOMAIN)
      ]),
    });
    this.testMailForm = new FormGroup({
      to: new FormControl('', [
        Validators.required,
        Validators.pattern(EMAIL)
      ]),
    });
    // get started
    this.getStartedService.getSteps().subscribe(result => {
      this.stepList = result['stepList'] || [];
      this.stepCount = result['stepCount'] || 0;
    });
  }

  fetchDomainAdditionPermission() {
    this.apiService.get(CHECK_DOMAIN_ADDITION_PERMISSION)
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.domainAdditionFlag = result['data']['allow_domain_addition'] === 1 ? true : false;
          }
        }
      );
  }

  // fetch domain details
  fetchDomainDetails() {
    this.domainListSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${DOMAIN_LISTING}`,
      // endPoint: `${API_URL}${DOMAIN_LISTING}`,
      dataKey: 'domain_list',
    });
    // to show hide of no data fond window
    this.domainListSource.onGetData.subscribe((res) => {
      this.nodataFlag = +res.data.domain_list.total_domain_added > 0;
      this.tableFlag = !this.nodataFlag;
    });
  }

  onSearch(query: string = '') {
    this.domainListSource.setFilter([
      {
        field: 'domain',
        search: query
      },
    ]);
  }

  onStepClick(evt) {
    this.getStartedService.goToStep(evt);
  }

  addDomainPopUp() {
    this.open(this.addDomainPopup);
  }

  /**
   * On upload file , on change event will accept the event and willl store the file object data.
   * @param event
   */
  onFileChange(event) {
    if (event.target.files && event.target.files.length > 0) {
      this.file = event.target.files[0];
      this.filename = this.file.name;
      this.fileErrorList = this.fileValidation.validate(this.file);
      this.fileMsg = this.fileErrorList.join('<br>');
      this.customerror = '';
    } else {
      this.customerror = this.customValidation.required(this.filename);
    }
  }

  prevPage() {
    this.fastTrackCurStep--;
  }

  nextPage(formdata = [], otherSubscriptionValue = '') {
    Object.assign(this.data, formdata);
    if (this.fastTrackCurStep < 6) {
      this.fastTrackCurStep++;
    } else {
      if (this.fastTrackCurStep === 4) {
        this.otherSubscibtionSource = otherSubscriptionValue;
      }
      const formData = new FormData();
      formData.append('browsed_file', this.file);
      formData.append('sub_nu', this.data['noOfEmail']);
      formData.append('sign_up',
        this.subscibtionSource.toLowerCase() === 'others' ? this.otherSubscibtionSource : this.subscibtionSource);
      formData.append('comment', this.data['sendingList']);
      formData.append('domain', this.fastTrackRow['domain']);
      formData.append('email_type', ('chk=' + Array.from(this.checkBoxValue).join('&chk=')));
      this.apiService.post(FASTRACK_DOMAIN_APPROVAL, formData)
        .subscribe((result) => {
            if (result['status'] === 'success') {
              this.hideBack = true;
              this.popRef.close(); // can be useed if wanted to close the popup window
              this.fastTrackMsg = result['message'];
              this.fastTrackType = 'info';
            } else {
              this.fastTrackMsg = result['error_info']['message'];
              this.fastTrackType = 'danger';
            }
          },
          error => {
            this.fastTrackMsg = WARNING_MSG;
            this.fastTrackType = 'warning';
          }
        );
    }
  }

  onchangeCheckBoxValue(event) {
    console.log('event', event);
    this.dispOtherMailType = (event.target.checked === true && event.target.value.toLowerCase() === 'others') ? true : false;
    if (event.target.checked === true) {
      this.checkBoxValue.add(event.target.value);
    } else {
      this.checkBoxValue.delete(event.target.value);

    }
    console.log(this.checkBoxValue.size);
    console.log(this.checkBoxValue);
    this.checkBoxValueSize = this.checkBoxValue.size;
  }

  addDomain(formData) {
    this.apiService.post(ADD_DOMAIN, formData)
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.dom_id = result['data']['domid'];
            this.router.navigate([this.dom_id], {relativeTo: this.route, queryParams: {domain: formData['domain_name']}});
            this.popRef.close();
          } else {
            // this.dom_id = result['data']['domaid'];
            // uncomment afterwards after testing
            this.addDomainMsg = decodeURIComponent(result['error_info']['message']);
            this.addDomainType = 'danger';
          }
        }, error => {
          console.log(error);
          this.addDomainMsg = WARNING_MSG;
          this.addDomainType = 'warning';
        }
      );

  }

  /**
   * This function will display the text area , if the subscribtion option is 'others'
   * @param event
   */
  onSubscribtionOther(event) {
    console.log(event.target.value);
    const sourceArr = ['From Contact Us form available on my business website', 'From "subscription for newsletter" form on website',
      'From Signup form on my business website', 'Others'];
    event.target.value === '4' ? this.dispOtherSubscribtionSource = false : this.dispOtherSubscribtionSource = true;
    this.subscibtionSource = sourceArr[event.target.value - 1];
  }

  /**
   * Open popup when click on chcnage profile password
   */
  open(content) {
    this.popRef = this.modalService.open(content);
    // for resetting error msg text
    this.hideBack = false;
    this.fastTrackMsg = '';
  }

  /**
   * Used to send test email
   * @param emaild
   */
  sendMail(emaild, fromEmail) {
    this.apiService.post(TEST_MAIL_DOMAIN, {
      'from': fromEmail,
      'to': emaild,
    })
      .subscribe(result => {
          console.log((result));
          if (result['status'] === 'success') {
            this.testMailMsg = result['message'];
            this.testMailType = 'info';
          } else {
            this.testMailType = 'danger';
            this.testMailMsg = result['error_info']['message'];
          }
        }
      );

  }

  /**
   *  Fetching SMTP details. on load check if request is for changing password popup will be display to change password
   */
  fetchSmtpData() {
    this.apiService.get(GET_SMTP_INFO)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.smtpdata = result['data'];
          } else {
            this.smtptype = 'warning';
            this.smtpmsg = result['error_info']['message'];
          }
        }, error => {
          this.smtptype = 'warning';
          this.smtpmsg = WARNING_MSG;
        }
      );

  }
}
