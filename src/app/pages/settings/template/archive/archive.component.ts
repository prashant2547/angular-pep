import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss']
})
export class ArchiveComponent implements OnInit {

  templateList = ['assets/images/template01.jpg', 'assets/images/template01.jpg', 'assets/images/template01.jpg'];

  constructor(private location: Location) { }

  ngOnInit() {
  }

  backClick() {
    this.location.back();
  }

  editTemplateClick() {
    console.log('edit click');
    return false;
  }

  trashTemplateClick() {
    console.log('trash click');
    return false;
  }

  archiveTemplateClick() {
    console.log('archive click');
    return false;
  }

  previewTemplateClick() {
    console.log('preview click');
    return false;
  }

  getPage() {
    return 1;
  }

  getLast() {
    return 10;
  }

  prevPage() {
    return false;
  }

  nextPage() {
    return false;
  }

}
