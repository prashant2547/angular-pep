import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../services/api.service';
import {DELETE_TEMPLATE, GET_ALL_TEMPLATES} from '../../../../config';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TemplateSearchPipe} from '../../../components/pipes/template-search.pipe';

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.scss']
})
export class TemplateListComponent implements OnInit {

  // templateList = ['assets/images/template01.jpg', 'assets/images/template01.jpg', 'assets/images/template01.jpg'];
  templateList = [];
  editTemplateFrm: FormGroup;
  deleteTempType: string;
  deleteTempMsg: string;
  deleteTempObj: any;
  confirmPopRef: NgbActiveModal;
  searchText: string;

  @ViewChild('confirmPopup') confirmPopup;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private apiService: ApiService,
              private fb: FormBuilder) {

  }

  showListTemplates = true;

  ngOnInit() {
    this.editTemplateFrm = this.fb.group({
      tempName: ['', [Validators.required]],
      // tempId         : ['', [Validators.required]],
      templateFile: ['', [Validators.required]],
      templateContent: ['', [Validators.required]]

    });
    this.apiService.post(GET_ALL_TEMPLATES)
      .subscribe((result) => {
        if (result['status'] === 'success') {
          if (result['data']['template'].length > 0) {
            this.showListTemplates = true;
            this.templateList = result['data']['template'];
          } else {
            this.showListTemplates = false;
          }
        } else {
          // console.log(result['data']['template'].length)
        }
      });
  }

  archiveListClick() {
    this.router.navigate(['archive'], {relativeTo: this.route});
  }

  addTemplateClick() {
    // console.log('add template clicked');
    this.router.navigate(['edit/new'], {relativeTo: this.route});
    return false;
  }

  editTemplateClick(tmplid) {
    // alert(tmplid);return false;
    this.router.navigate(['edit', tmplid], {relativeTo: this.route});
    return false;
  }

  // on template delete icon click
  // opens confirm popup for deleting template
  trashTemplateClick(templateObj) {
    this.deleteTempObj = templateObj;
    this.confirmPopRef = this.modalService.open(this.confirmPopup);
    return false;
  }

  // deletes template from confirm popup
  deleteTemplateClick() {
    this.apiService.get(DELETE_TEMPLATE + '/format/json?template_id=' + this.deleteTempObj.tmplid)
      .subscribe(result => {
        if (result['status'].toLowerCase() === 'success') {
          this.templateList = this.templateList.filter(template => {
            return template.tmplid !== this.deleteTempObj.tmplid;
          });
          this.deleteTempType = 'info';
          this.deleteTempMsg = result['message'];
        } else {
          this.deleteTempType = 'danger';
          this.deleteTempMsg = result['error_info']['message'];
        }

        this.confirmPopRef.close();
      });
  }

  previewTemplateClick(tmplid) {
    this.router.navigate(['preview', tmplid], {relativeTo: this.route});
    return false;
  }

  getPage() {
    return 1;
  }

  getLast() {
    return 10;
  }

  prevPage() {
    return false;
  }

  nextPage() {
    return false;
  }
}
