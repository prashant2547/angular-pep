import {AfterViewInit, Component, ElementRef, OnInit, SecurityContext, ViewChild} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {DomSanitizer} from '@angular/platform-browser';
import {PREVIEW_TEMPLATE} from '../../../../config';


@Component({
  selector: 'app-template-preview',
  templateUrl: './template-preview.component.html',
  styleUrls: ['./template-preview.component.scss']
})
export class TemplatePreviewComponent implements OnInit, AfterViewInit {
  templateId: number;
  templateName: string;
  templateHtml: any;
  viewTemplate: any;
  @ViewChild('previewFrame') previewFrame: ElementRef;
  preview: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private sanitizer: DomSanitizer,
              private apiService: ApiService) {
  }

  ngOnInit() {
    this.viewTemplate = this.getDetails();
    this.apiService.post(PREVIEW_TEMPLATE, {template_id: this.viewTemplate})
      .subscribe((result) => {

        if (result['status'] === 'success') {
          this.templateName = result['data']['name'];
          this.templateHtml = result['data']['content'];
          this.callIframe();
        } else {
          alert('keep on same page');
          return false;
        }
      });
  }

  ngAfterViewInit(): void {

  }

  callIframe() {
    this.preview = this.previewFrame.nativeElement.contentDocument || this.previewFrame.nativeElement.contentWindow.document;
    this.preview.open();
    this.preview.write(this.sanitizer.sanitize(SecurityContext.URL, this.templateHtml));
    this.preview.close();
  }

  getDetails() {
    return this.templateId = +this.route.snapshot.paramMap.get('id');
  }

  editTemplateClick(tmplid) {
    this.router.navigate(['../../edit/', tmplid], {relativeTo: this.route});
    return false;
  }

  goBack() {
    this.location.back();
    return false;
  }
}
