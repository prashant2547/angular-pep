import {AfterViewInit, Component, ElementRef, OnInit, SecurityContext, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import '../../../../../../node_modules/codemirror/mode/htmlmixed/htmlmixed.js';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../../../services/api.service';
import {API_URL, PREVIEW_TEMPLATE, SAVE_TEMPLATE} from '../../../../config';
import {ALPHA_NUMERIC, FileValidation} from '../../../components/forms/form-validation';
import {FormBuilder, FormGroup, Validator, Validators} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-template-edit',
  templateUrl: './template-edit.component.html',
  styleUrls: ['./template-edit.component.scss']
})
export class TemplateEditComponent implements OnInit, AfterViewInit {
  templateId: number;
  config = {
    lineNumbers: true,
    mode: 'htmlmixed',
    viewportMargin: Infinity
  };
  content = '';
  delay: any;
  codeMirrorInstance: any;
  previewFrame: any;
  isUpload: boolean;
  templateName = '';
  editTemplateForm: FormGroup;

  htmlFile: File;
  htmlFileName = '';
  htmlFileValidation: FileValidation;
  htmlFileErrorList: string[] = [];
  htmlFileErrorMsg = '';
  hideFileError = true;
  uploadErrType = '';
  uploadErrMsg = '';

  @ViewChild('preview') preview: ElementRef;
  @ViewChild('editor') editor;

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private location: Location,
              private fb: FormBuilder,
              private sanitizer: DomSanitizer,
              private router: Router,
              private apiService: ApiService) {
  }

  ngAfterViewInit() {
    this.codeMirrorInstance = this.editor.instance;
    this.previewFrame = this.preview.nativeElement.contentDocument || this.preview.nativeElement.contentWindow.document;
    this.codeMirrorInstance.on('change', () => {
      clearTimeout(this.delay);
      this.delay = setTimeout(this.updatePreview.bind(this), 300);
    });
  }

  updatePreview() {
    this.previewFrame.open();
    this.previewFrame.write(this.content);
    this.previewFrame.close();

    this.editTemplateForm.controls['codeMirrorContent'].setValue(this.content);

    this.updateFileError();
  }

  updateFileError() {
    this.hideFileError = this.content !== '' || this.htmlFile !== null;
  }

  ngOnInit() {
    this.getDetails();

    // html htmlFile validation
    this.htmlFileValidation = new FileValidation(1024 * 512, ['html', 'htm']);

    this.editTemplateForm = this.fb.group({
      templateName: ['', [Validators.required, Validators.pattern(ALPHA_NUMERIC), Validators.maxLength(25)]],
      templateFile: ['', [Validators.required]],
      codeMirrorContent: ['', [Validators.required]]
    });

    this.editTemplateForm.get('templateFile').valueChanges.subscribe(value => {
      const validator = value ? [] : [Validators.required];
      this.editTemplateForm.get('codeMirrorContent').setValidators(validator);
      this.editTemplateForm.get('codeMirrorContent').updateValueAndValidity({
        onlySelf: true, emitEvent: false
      });
    });

    this.editTemplateForm.get('codeMirrorContent').valueChanges.subscribe(value => {
      const validator = value ? [] : [Validators.required];
      this.editTemplateForm.get('templateFile').setValidators(validator);
      this.editTemplateForm.get('templateFile').updateValueAndValidity({
        onlySelf: true, emitEvent: false
      });
    });
  }

  getTemplate() {
    this.apiService.post(PREVIEW_TEMPLATE, {'template_id': this.templateId})
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.templateName = result['data']['name'];
          this.content = result['data']['content'];
        } else {
          alert('keep on same page');
          return false;
        }

      });
  }

  getDetails() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id === 'new') {
      this.isUpload = true;
    } else {
      this.templateId = +id;
      this.getTemplate();
    }
  }

  onFileChange(event) {
    this.htmlFile = null;
    if (event.target.files && event.target.files.length > 0) {
      this.htmlFile = event.target.files[0];
      this.htmlFileName = this.htmlFile.name;
      this.htmlFileErrorList = this.htmlFileValidation.validate(this.htmlFile);
      this.htmlFileErrorMsg = this.htmlFileErrorList.join('<br>');
      const reader: FileReader = new FileReader();
      reader.readAsText(this.htmlFile);
      reader.onload = (e) => {
        this.content = reader.result;
      };
    } else {
      this.htmlFile = null;
      this.htmlFileName = '';
    }

    this.updateFileError();
    this.editTemplateForm.controls['templateFile'].setValue(this.htmlFile ? this.htmlFile.name : '');
  }

  saveClick() {
    // use form data to send data in case of uploading any kind of file
    const formData = new FormData();
    if (this.htmlFileName) {
      formData.append('upload_template_file', this.htmlFile);
    }
    if (this.content) {
      console.log('this.codeMirrorInstance.getValue()', this.codeMirrorInstance.getValue());
      formData.append('template_content', this.codeMirrorInstance.getValue());
    }
    if (!this.isUpload) {
      formData.append('template_id', this.templateId.toString());
    }
    formData.append('template_name', this.templateName);
    this.apiService.post(SAVE_TEMPLATE, formData)
    .subscribe((result) => {
        if (result.status.toLowerCase() === 'success') {
          this.uploadErrType = 'info';
          this.uploadErrMsg = result['message'];
          this.router.navigate(['../../'], {relativeTo: this.route});
        } else {
          this.uploadErrType = 'danger';
          this.uploadErrMsg = result['error_info']['message'];
        }
      }
    );

  }

  goBack() {
    this.location.back();
    return false;
  }
}
