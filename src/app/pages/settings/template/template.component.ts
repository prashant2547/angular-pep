import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-template',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class TemplateComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
}
