import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {
  GET_API_INFO,
  GET_SMTP_INFO,
  TEST_MAIL,
  RESET_SMTP_PASSWORD, SET_SMTP_PASSWORD, WARNING_MSG, GET_SMTP_API_KEY, CHECK_ACCOUNT_STATUS,
} from '../../../config';
import {ApiService} from '../../services/api.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EMAIL, PASSWORD, PasswordPolicy} from '../../components/forms/form-validation';
import {Router} from '@angular/router';

@Component({
  selector: 'app-integration',
  templateUrl: './integration.component.html',
  styleUrls: ['./integration.component.scss']
})
export class IntegrationComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'Should I integrate with SMTP or API?',
      helpLink: 'https://docs.pepipost.com/docs/should-i-integrate-with-smtp-or-api'
    }, {
      helpText: 'How to do SMTP Integration?',
      helpLink: 'https://docs.pepipost.com/docs/how-to-do-smtp-integration'
    }, {
      helpText: 'What is the difference between “From” and “Sender”',
      helpLink: 'https://docs.pepipost.com/docs/what-is-the-difference-between-from-and-sender'
    }, {
      helpText: 'How to integrate with different mail server?',
      helpLink: 'https://docs.pepipost.com/docs/how-to-integrate-with-different-mail-server'
    }, {
      helpText: 'How to Integrate your Mail Clients',
      helpLink: 'https://docs.pepipost.com/docs/how-to-integrate-your-mail-clients'
    }, {
      helpText: 'How to pass Unique Arguments in each SMTP emails?',
      helpLink: 'https://docs.pepipost.com/docs/how-to-pass-unique-arguments-in-each-smtp-email'
    }, {
      helpText: 'How to view Message headers?',
      helpLink: 'https://docs.pepipost.com/docs/how-to-view-message-headers'
    },
  ];
  id = '';
  // variable to stored onload fetched data of API and SMTP
  apidata = '';
  smtpdata = '';
  // variable to stored error or sucess and message of API and SMTP
  apitype = '';
  smtptype = '';
  apimsg = '';
  smtpmsg = '';
  data = '';
  // variable to store domain list from api
  domainList: any;
  dispDomainDropDown = true;
  noDomainData = false;
  // selected domain value while sending test email
  selectedDomain = '';
  // variable to display Copy and show button on load and on "show click" display copy button
  apiCopyBtn = false;
  apiShowBtn = true;
  smtpShowBtn = true;
  smtpCopyBtn = false;
  showchangePaswdPop: boolean;
  passwdType = '';
  passwdMsg = '';
  testMailType = '';
  testMailMsg = '';
  showSmtpPwdType = '';
  showSmtpPwdMsg = '';
  showApiKeyType = '';
  showApiKeyMsg = '';
  verifyChangePwdForm: FormGroup;
  testMailForm: FormGroup;
  showSmtpPwdForm: FormGroup;
  showAPIKeyForm: FormGroup;
  popRef: NgbActiveModal;
  isApiCopied: boolean;
  // validation for password policy
  passwordPolicy: PasswordPolicy;
  passwordDataObject = [];
  passwordData = '';
  passwordFlag = 0;
  accountStatus = 0;

  @ViewChild('changePwdPopup') changePwdPopup;
  @ViewChild('testMailPopup') testMailPopup;
  @ViewChild('apiKeyPopup') apiKeyPopup;
  @ViewChild('smtpPwdPopup') smtpPwdPopup;
  @ViewChild('tabset') tabset;

  constructor(private apiService: ApiService,
              private modalService: NgbModal,
              private router: Router) {
  }

  ngOnInit() {
    this.passwordPolicy = new PasswordPolicy();
    this.fetchApiData();
    this.fetchSmtpData();
    this.fetchWithoutActivatUserDetails();
  }

  fetchWithoutActivatUserDetails() {
    this.apiService.get(CHECK_ACCOUNT_STATUS)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.accountStatus = result['data']['status'];
          }
        }
      );
  }

  //
  // ngAfterViewInit() {
  //   const param = this.router.parseUrl(this.router.url);
  //   this.id = param.queryParams.id;
  //
  //   if (this.id === '1') {
  //     // this.testMail();
  //     this.tabset.select('smtp-info');
  //     this.open(this.testMailPopup);
  //     // console.log(this.id);
  //     // console.log(this.testMailPopup);
  //   }
  //   // param.queryParams.id === '1' ? this.testMail() : '';
  // }

  // to get api key
  /**
   * To fetch API key on inital loas as well as show on lick of "SHOW"
   * @param {string} pwd
   */
  fetchApiData(pwd = '*') {
    this.apiService.post(GET_API_INFO, ({'password': pwd}))
      .subscribe(result => {
          console.log((result));
          if (result['status'] === 'success') {
            if (pwd !== '*') {
              this.apiCopyBtn = true;
              this.apiShowBtn = false;
              this.popRef.close();
            }
            this.apidata = result['data'];
          } else {
            this.showApiKeyType = 'danger';
            this.showApiKeyMsg = result['error_info']['message'];
          }
        }
      );
  }

  /**
   *  Fetching SMTP details. on load check if request is for changing password popup will be display to change password
   */
  fetchSmtpData() {

    const param = this.router.parseUrl(this.router.url);
    this.id = param.queryParams.id;
    this.apiService.get(GET_SMTP_INFO)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.smtpdata = result['data'];
            // From domain will be dynamic based on API result.
            this.domainList = result['data']['from_domains'];
            if (this.domainList.length !== 0) {
              if (this.domainList.length === 1) {
                // if array contains only one record then domain will be display as label
                this.dispDomainDropDown = true;
              } else {
                // by default 1st will be selected from drop down
                this.selectedDomain = this.domainList[0];
                this.dispDomainDropDown = false;
              }
            } else {
              // if no domain data from api , then no domain approved
              this.noDomainData = true;
            }
            // end dropdown logic
            this.showchangePaswdPop = this.smtpdata['show_form'];
            if (this.showchangePaswdPop && this.id !== '1') {
              this.tabset.select('smtp-info');
              this.passwordDataObject = this.passwordPolicy.validatePassword('');
              this.passwordData = this.passwordDataObject[0];
              this.passwordFlag = this.passwordDataObject[1];
              this.open(this.changePwdPopup);
            }
            if (this.id === '1') {
              this.tabset.select('smtp-info');
              this.open(this.testMailPopup);
            }
          } else {
            this.apitype = this.smtptype = 'warning';
            this.apimsg = this.smtpmsg = WARNING_MSG;
          }
        }, error => {
          this.apitype = this.smtptype = 'warning';
          this.apimsg = this.smtpmsg = WARNING_MSG;
        }
      );

  }

  /**
   * This function is used to validate password polciy
   * @param ev
   */
  checkPassword(ev) {
    this.passwordDataObject = this.passwordPolicy.validatePassword(ev.target.value);
    this.passwordData = this.passwordDataObject[0];
    this.passwordFlag = this.passwordDataObject[1];
  }

  /**
   *  To send test mail open popup window to enter details
   */
  testMail() {
    this.open(this.testMailPopup);
    return false;
  }

// get selected domain name
  getDomainName(e) {
    this.selectedDomain = e.target.value;
  }

  /**
   * Used to send test email
   * @param emaild
   */
  sendMail(emaild, fromEmail) {
    console.log(this.selectedDomain);
    // if single domain then from array direct else from drodown
    this.selectedDomain = this.selectedDomain ? this.selectedDomain : this.domainList;
    this.apiService.post(TEST_MAIL, {
      'login': this.smtpdata['smtpuser'],
      'password': this.smtpdata['smtpapikey'],
      'port': '25',
      'from': fromEmail + '@' + this.selectedDomain,
      'to': emaild,
      'server': this.smtpdata['panel_name']
    })
      .subscribe(result => {
          console.log((result));
          if (result['status'] === 'success') {
            this.testMailMsg = result['message'];
            this.testMailType = 'info';
          } else {
            this.testMailType = 'danger';
            this.testMailMsg = result['error_info']['message'];
          }
        }, error => {
          this.testMailType = 'warning';
          this.testMailMsg = WARNING_MSG;
        }
      );

  }

  /**
   * To sahow API key , Open popup to enter password
   */
  showAPIKey() {
    this.open(this.apiKeyPopup);
  }

  /**
   * to show smtp password open popup to enter password
   */
  showSmtpApikey() {
    this.open(this.smtpPwdPopup);
  }

  /**
   * To get SMTP API key on click on SHOW
   */
  getSmtpApikey(smtpApiPwd) {
    console.log(smtpApiPwd);
    this.apiService.post(GET_SMTP_API_KEY, {'password': smtpApiPwd})
      .subscribe(result => {
          console.log((result));
          if (result['status'] === 'success') {
            this.popRef.close();
            this.smtpdata['smtpapikey'] = result['data'];
            this.smtpShowBtn = false;
            this.smtpCopyBtn = true;

          } else {
            this.showSmtpPwdType = 'danger';
            this.showSmtpPwdMsg = result['error_info']['message'];
          }
        }, error => {
          this.showSmtpPwdType = 'warning';
          this.showSmtpPwdMsg = WARNING_MSG;
        }
      );
  }

  /**
   * Calling API to send email to change password will only send email
   */
  resetSMTPPassword() {
    this.apiService.post(RESET_SMTP_PASSWORD, '')
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.smtptype = 'info';
            this.smtpmsg = result['message'];
          } else {
            this.smtptype = 'danger';
            this.smtpmsg = result['error_info']['message'];
          }
        }, error => {
          this.smtptype = 'warning';
          this.smtpmsg = WARNING_MSG;
        }
      );
    return false;
  }

  /**
   * Change SMTP Password
   * @param pwdData
   */
  setSMTPPassword(pwdData) {

    this.apiService.post(SET_SMTP_PASSWORD, {'changed_password': pwdData})
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.passwdType = 'info';
            this.passwdMsg = result['message'];
          } else {
            this.passwdType = 'danger';
            this.passwdMsg = result['error_info']['message'];
          }
        }, error => {
          this.passwdType = 'warning';
          this.passwdMsg = WARNING_MSG;
        }
      );
  }

  redirectoDomain() {
    this.popRef.close();
    this.router.navigate(['/settings/domain/']);
    return false;
  }

  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.passwdMsg = this.testMailMsg = this.showSmtpPwdMsg = this.showApiKeyMsg = '';
    this.verifyChangePwdForm = new FormGroup({
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(PASSWORD)
      ]),
      confmPassword: new FormControl('', [
        Validators.required,
        // Validators.pattern(PASSWORD)
      ]),
    });
    this.testMailForm = new FormGroup({
      to: new FormControl('', [
        Validators.required,
        Validators.pattern(EMAIL)
      ]),
      from: new FormControl('', [
        Validators.required,
        // Validators.pattern(EMAIL)
      ]),
    });
    this.showSmtpPwdForm = new FormGroup({
      password: new FormControl('', [
        Validators.required,
        // Validators.min(8)
        // Validators.pattern(PASSWORD)
      ]),
    });
    this.showAPIKeyForm = new FormGroup({
      password: new FormControl('', [
        Validators.required,
        // Validators.min(8)
        // Validators.pattern(PASSWORD)
      ]),
    });
    this.popRef = this.modalService.open(content);
  }
}
