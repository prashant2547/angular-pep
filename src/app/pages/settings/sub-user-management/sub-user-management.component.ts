import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CREATE_SUBUSER, GET_ALL_SUBUSERS, MODIFY_SUBUSER, NO_DATA_FOUND} from '../../../config';
import {ApiService} from '../../services/api.service';
import {TableSettingsDeleteComponent} from '../../components/table/table-settings-delete/table-settings-delete.component';
import {TableDataSource} from '../../services/table.data-source';
import {environment} from '../../../../environments/environment';
import {EMAIL, ONLY_DIGIT, USERNAME, COMPANY_TEXT } from '../../components/forms/form-validation';
import {TableSubuserToggleTextComponent} from '../../components/table/table-subuser-toggle-text/table-subuser-toggle-text.component';

@Component({
  selector: 'app-sub-user-management',
  templateUrl: './sub-user-management.component.html',
  styleUrls: ['./sub-user-management.component.scss']
})
export class SubUserManagementComponent implements OnInit {
  createSubUserForm: FormGroup;
  createSubUsercreditAllocationForm: FormGroup;
  createSubUsercreditAllocationForm1: FormGroup;
  // Refer documentation of ng2-smart-table for settings
  // https://akveo.github.io/ng2-smart-table/#/documentation
  subuserFlag = false;
  subuserSource: any;
  subuserSuccess: string;
  allocationSuccess: string;
  allocationErrorType: string;
  subuserErrorEmail: string;
  subuserErrorUsername: string;
  subuserAllocation4 = true;
  showSubusername: string;
  showTotalmails: string;
  subuserMsg: any;
  subuserType: any;

  subUser = {
    add: {
      addButtonContent: '<span class="btn btn-primary">ADD</span>',
      createButtonContent: '<i class="icon icon-check-mark"></i>',
      cancelButtonContent: '<i class="icon icon-close"></i>',
      confirmCreate: true
    },
    delete: {
      deleteButtonContent: '<i class="icon icon-trash"></i>',
      confirmDelete: true
    },
    attr: {
      class: 'table'
    },
    columns: {
      contact_email: {
        title: 'Email address',
        sort: false,
      },
      username: {
        title: 'User name',
        sort: false
      },
      credits_remaining: {
        title: 'Credits remaining',
        sort: false,
      },
      company: {
        title: 'Company',
        sort: false
      },
      creation_date: {
        title: 'Creation date',
        sort: false
      },
      status: {
        title: 'Status',
        sort: false,
        type: 'custom',
        renderComponent: TableSubuserToggleTextComponent,
        onComponentInitFunction: (instance) => {
          instance.clicked.subscribe(row => {
            // row.status = !row.status;
            // alert(row.status);
            if (+row.status === 0) {
              this.apiService.post(MODIFY_SUBUSER, ({
                'username': row.username,
                'action': 'enable'
              }))
                .subscribe((result) => {
                  if (result['status'] === 'success') {
                    this.subuserSource.refresh();
                    console.log('enabled');
                  } else {
                    alert(result['error_info']['message']);
                  }

                });
            } else {
              this.apiService.post(MODIFY_SUBUSER, ({
                'username': row.username,
                'action': 'disable'
              }))
                .subscribe((result) => {
                  if (result['status'] === 'success') {
                    this.subuserSource.refresh();
                  } else {
                    alert(result['error_info']['message']);
                  }
                });
            }
          });
        }
      },
      actions: {
        title: 'Actions',
        sort: false,
        type: 'custom',
        renderComponent: TableSettingsDeleteComponent,
        onComponentInitFunction: (instance) => {
          instance.settingClick.subscribe(row => {
            this.selCreditAllocation = '1';
            this.subuserAllocation4 = true;
            this.showSubusername = row.username;
            this.showTotalmails = row.total_mails;
          });
          instance.deleteClick.subscribe(row => {
            this.apiService.post(MODIFY_SUBUSER, ({
              'username': row.username,
              'action': 'delete'
            }))
              .subscribe((result) => {
                if (result['status'] === 'success') {
                  this.subuserSource.refresh();
                } else {
                  alert(result['error_info']['message']);
                }
              });
          });
        }
      }
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    }
  };
  clientType: any;
  selectedPages: number;
  pagesOptions = [10, 20, 30, 40];
  selCreditAllocation = '1';
  creditMenu = [{
    id: 1,
    label: 'Unlimited',
  }, {
    id: 2,
    label: 'Recurring'
  }, {
    id: 3,
    label: 'Non-recurring'
  }];

  isFirstTime = true;

  constructor(private http: Http,
              private apiService: ApiService,
              private fb: FormBuilder) {

  }

  ngOnInit() {
    this.selCreditAllocation = '0';
    this.apiService.get(GET_ALL_SUBUSERS)
      .subscribe(result => {
          this.clientType = result['data']['all_subuser']['client_type'];
          if (+this.clientType === 2) {
            this.creditMenu = this.creditMenu.filter(function (el) {
              return el.id !== 1;
            });
            this.selCreditAllocation = '2';
          }
          if (result['data']) {
            this.subuserFlag = true;
            this.subuserAllocation4 = false;
          } else {
            this.subuserFlag = false;
          }

        }
      );
    this.createSubUserForm = this.fb.group({
      subuserEmail: ['', [Validators.pattern(EMAIL)]],
      subuserUsername: ['', [Validators.pattern(USERNAME)]],
      subuserCompany: ['', [Validators.pattern(COMPANY_TEXT)]],
    });
    this.createSubUsercreditAllocationForm = this.fb.group({
      creditAllocation: ['', [Validators.required]],
      subuserPeriod: ['Daily', [Validators.required]],
      subuserCredit: ['', [Validators.pattern(ONLY_DIGIT)]],
    });
    this.createSubUsercreditAllocationForm1 = this.fb.group({
      creditAllocation1: ['', [Validators.required]],
      subuserPeriod1: ['Add', [Validators.required]],
      subuserCredit1: ['', [Validators.pattern(ONLY_DIGIT)]],
    });
    this.subuserSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${GET_ALL_SUBUSERS}`,
      dataKey: 'all_subuser'
    });

    this.subuserSource.onDataLoaded.subscribe((res) => {
      // show no data for first time only
      if (!this.isFirstTime) {
        return;
      }
      this.isFirstTime = false;
      // check if response is undefined
      if (res) {
        // if got response and check if table has rows
        this.subuserFlag = res.length > 0;
      } else {
        // if response is undefined, then show no data found
        this.subuserFlag = false;
      }

      this.subuserAllocation4 = !this.subuserFlag;
    });
    this.selectedPages = this.pagesOptions[0];
  }

  onCreditAllocChange(evt) {
    console.log('this.selCreditType', this.selCreditAllocation);
    console.log('selected allocation', evt);
  }

  createSubuser(company, username, email) {
    this.apiService.post(CREATE_SUBUSER, {'company': company, 'username': username, 'emailid': email})
      .subscribe(result => {
        if (result['status'] === 'success') {
          // this.subuserSuccess = result['message'];
          this.subuserMsg = result['message'];
          this.subuserType = 'info';
          this.createSubUserForm.reset();
          this.subuserSource.refresh();
        } else {
          if (result['error_info']['error_code'] === 1006) {
            // this.subuserErrorEmail = result['error_info']['message'];
            this.subuserMsg = result['error_info']['message'];
            this.subuserType = 'danger';
          } else {
            // this.subuserErrorUsername = result['error_info']['message'];
            this.subuserMsg = result['error_info']['message'];
            this.subuserType = 'danger';
          }
        }
      });
  }

  subuserCreditAlloc(alloc, per, cre) {
    // Non Recurring
    if (+alloc === 3) {
      // alert(cre);
      if (per.toLowerCase() === 'add') {
        per = 'add';
      } else {
        per = 'subtract';
      }
      this.apiService.post(MODIFY_SUBUSER, ({
        'action': per,
        'username': this.showSubusername,
        'credits': cre,
        'flag': 2
      }))
        .subscribe((result) => {
          if (result['status'] === 'success') {
            console.log(result['message']);
            // this.createSubUsercreditAllocationForm1.reset();
            this.allocationSuccess = result['message'];
            this.allocationErrorType = 'success';
          } else {
            this.allocationSuccess = result['error_info']['message'];
            this.allocationErrorType = 'danger';
          }
        });
    } else if (+alloc === 2) {
      // Recurring
      this.apiService.post(MODIFY_SUBUSER, ({
        'period': per,
        'action': 'config',
        'username': this.showSubusername,
        'credits': cre,
        'flag': 1
      }))
        .subscribe((result) => {
          if (result['status'] === 'success') {
            console.log(result['message']);
            // this.createSubUsercreditAllocationForm.reset();
            this.allocationSuccess = result['message'];
            this.allocationErrorType = 'success';
          } else {
            this.allocationSuccess = result['error_info']['message'];
            this.allocationErrorType = 'danger';
          }
        });
    }

  }

  subuserCancel() {
    this.createSubUserForm.reset();
    this.subuserSuccess = '';
    this.subuserErrorEmail = '';
    this.subuserErrorUsername = '';

  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      console.log(event);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to create?')) {
      event.newData['name'] += ' + added in code';
      event.confirm.resolve(event.newData);
      console.log(event);
    } else {
      event.confirm.reject();
    }
  }

  showAddSubuser() {
    this.subuserFlag = true;
    this.subuserAllocation4 = false;
  }

  cancelAllocation() {
    this.subuserAllocation4 = false;
    this.showSubusername = '';
  }
}
