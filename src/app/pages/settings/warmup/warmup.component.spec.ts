import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarmUpComponent } from './warmup.component';

describe('WarmUpComponent', () => {
  let component: WarmUpComponent;
  let fixture: ComponentFixture<WarmUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarmUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarmUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
