import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {INCREASE_SLAB_REQUEST, WARMUPSLAB_DETAILS, WARNING_MSG} from '../../../config';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomNullValidation, FileValidation} from '../../components/forms/form-validation';
import {ActivatedRoute, Route, Router} from '@angular/router';

@Component({
  selector: 'app-warmup',
  templateUrl: './warmup.component.html',
  styleUrls: ['./warmup.component.scss']
})
export class WarmUpComponent implements OnInit {

  constructor(private apiService: ApiService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  warmupData: any;
  // to list select dropdown with slabs from database/API
  warmupDataList: any;
  activeWarmupDetails: any;
  domainFlag = 1;
  warmupForm: FormGroup;
  warmupFileValidation: FileValidation;
  // for the custom required field validation
  customValidation: CustomNullValidation;
  file: File;
  filename = '';
  fileMsg = '';
  customerror = '';
  // for file validation includind size and type of file
  size: any; // size of uploaded file
  csvRecordsArray: any; // csv file record array
  warmupFileErrorList: string[] = [];
  warmupErrorType = '';
  warmupErrorMsg = '';
  warmupType = '';
  warmupMsg = '';

// data to display helpful links
  helpSectionData = [
    {helpText: 'What is warm up?', helpLink: 'https://pepipost.com/blog/warmup/'},
  ];


  ngOnInit() {
    // Warmup form validation
    this.warmupForm = new FormGroup({
      slab: new FormControl('', [Validators.required]),
      warmupFile: new FormControl('', [
        Validators.required,
      ]),
      comment: new FormControl(''),
    });
    // custom file validation
    this.warmupFileValidation = new FileValidation(1024 * 512, ['doc', 'docx', 'pdf', 'html', 'png', 'jpg', 'jpeg', 'zip']);

    this.fetchWarmUpDetails();

  }

// fetch warup initail details displayed on the lock
  fetchWarmUpDetails() {
    this.apiService.post(WARMUPSLAB_DETAILS)
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.activeWarmupDetails = result['data']['clientWarmupSlab'];
            this.warmupData = result['data']['warmupSlabs'];
            this.warmupDataList = this.warmupData.filter((warmupdata) => {
              return ( warmupdata.slab_id !== this.activeWarmupDetails.slab_id
                && warmupdata.volume > this.activeWarmupDetails.total_volume);
            });
            this.domainFlag = result['data']['domain_flag'];
          } else {
            this.warmupErrorType = 'danger';
            this.warmupErrorMsg = result['error_info']['message'];
          }
        },
        error => {
          this.warmupErrorType = 'warning';
          this.warmupErrorMsg = WARNING_MSG;
        }
      );
  }

  /**
   * On upload file , on change event will accept the event and willl store the file object data.
   * @param event
   */
  onFileChange(event) {
    if (event.target.files && event.target.files.length > 0) {
      this.file = event.target.files[0];
      this.filename = this.file.name;
      this.warmupFileErrorList = this.warmupFileValidation.validate(this.file);
      this.fileMsg = this.warmupFileErrorList.join('<br>');
      if (this.fileMsg === '' && this.warmupFileErrorList.length === 0) {
        const reader: FileReader = new FileReader();
        reader.readAsText(this.file);
        reader.onload = (e) => {
          const csv: string = reader.result;
          this.csvRecordsArray = csv.split(/\r\n|\n/);
          this.size = this.csvRecordsArray.length;
        };
      }
      this.customerror = '';
    } else {
      this.customerror = this.customValidation.required(this.filename);
    }
  }

  /**
   * this function will change the existing warmup details
   * @param formData
   */
  changeWarmup(formDetails) {
    console.log('formadat', formDetails);
    console.log('file', this.file);
    console.log('active warmup data', this.activeWarmupDetails);
    // use formdata to send data in case of uploading any kind of file
    const formData = new FormData();
    formData.append('attachment_file', this.file);
    formData.append('current_slab', this.activeWarmupDetails.total_volume);
    formData.append('selected_slab', formDetails.slab);
    formData.append('comment', formDetails.comment);
    this.apiService.post(INCREASE_SLAB_REQUEST, formData)
      .subscribe((result) => {
          console.log(result);
          if (result['status'] === 'success') {
            this.warmupMsg = result['message'];
            this.warmupType = 'info';
          } else {
            this.warmupMsg = result['error_info']['message'];
            this.warmupType = 'danger';
          }
        },
        error => {
          this.warmupMsg = WARNING_MSG;
          this.warmupType = 'warning';
        }
      );
  }

// redirect to domain if domain is not added or not verified
  redirectToDomain() {
    this.router.navigate(['../domain'], {relativeTo: this.route});
    return false;
  }
}
