import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EMAIL, ONLY_DIGIT, PASSWORD, PasswordPolicy, USERNAME} from '../../components/forms/form-validation';
import {
  CHANGE_EMAIL, GET_EMAIL_DETAILS, GET_PROFILE_INFO, GETTING_STARTED_API_URL, LIST_SUBACCOUNT_DETAILS, NO_DATA_FOUND,
  RESEND_EMAIL_VERIFICATION,
  UPDATE_PROFILE_INFO,
  CREATE_SUBACCOUNT,
  WARNING_MSG, UPDATE_SUBACCOUNT_STATUS, DASHBOARD_HISTORY_GRAPH, GET_DOMAIN_LIST, UPDATE_DOMAIN_STATUS, ADD_REMOVE_DOMAIN_SUBACCOOUNT,
  UPDATE_SUBACCOUNT_PASSWORD, GTE_SUBACCOUNT_BILLING_DETAILS, CHANGE_CREDIT_TYPE, UPDATE_RECURRING, MANAGE_CREDIT, DELETE_SUBACCOUNT
} from '../../../config';
import {TableSubadminStatusComponent} from '../../components/table/table-subadmin-status/table-subadmin-status.component';
import {Http} from '@angular/http';
import {TableDataSource} from '../../services/table.data-source';
import {EmailChange, Profile} from '../../profile/profile-details/profile-model';
import {Timezone} from '../../components/forms/timezone/timezone';
import {DaterangepickerConfig} from 'ng2-daterangepicker';
import * as moment from 'moment';
import {TableEditOptionComponent} from '../../components/table/table-edit-option/table-edit-option.component';
import {environment} from '../../../../environments/environment';
import {Recurring} from './sub-account-model';
import {addCommasToNum} from '../../components/utils/number-utils';

@Component({
  selector: 'app-sub-accounts',
  templateUrl: './sub-accounts.component.html',
  styleUrls: ['./sub-accounts.component.scss']
})
export class SubAccountsComponent implements OnInit {
  start_date: any;
  end_date: any;
  // var to store user summary data
  userSummaryData: any;
  spamcount: number;

  // var to store biiling details
  billingDetails: any;
  recurringDetails: Recurring;

  // var to show hide password on sub sccount creation
  showPasswordFlag = false;

  // var to store domain list
  arrDomainList: any;
  arrMasterDomainList = [];
  arrSubAccDomainList = [];
  domainFlag: any;
  showListBox = false;
  isDomainAddAllow = false;
  // data to display helpful links
  helpSectionDataForTable = [
    {
      helpText: 'What is subaccount? What are the benefits?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/what-is-subaccount-what-are-the-benefits',
    }, {
      helpText: 'How many Subaccounts can I add to my account?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/how-many-subaccounts-can-i-add-to-my-account',
    }, {
      helpText: 'How to create Subaccount?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/how-to-create-subaccount',
    }, {
      helpText: 'Can I add multiple users by using same email address?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/can-i-add-multiple-users-by-using-same-email-address',
    },
  ];
  helpSectionDataForAccountSettings = [
    {
      helpText: 'How do I allocate email credits to Subaccount?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/how-do-i-allocate-email-credits-to-subaccount'
    }, {
      helpText: ' Will credit balance of Subaccount be carry forwarded to next month?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/will-credit-balance-of-subaccount-be-carry-forwarded-to-next-month'
    }, {
      helpText: 'How does billing work for Subaccounts?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/how-does-billing-work-for-subaccounts'
    }, {
      helpText: 'How does domain control work for subaccount?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/how-does-domain-control-work-for-subaccount'
    }, {
      helpText: 'What happen when I disable a subaccount?',
      helpLink: 'https://docs.pepipost.com/v1.0/docs/what-happen-when-i-disable-a-subaccount'
    },
  ];
  // var to show hide no data found window
  nodataFlag = true;
  tableFlag = true;
  editFlag = true;

  // Selected user data from table
  selectedClientData: any;

  // Popup for change password
  popRef: NgbActiveModal;
  @ViewChild('subaccountCreationPopup') subaccountCreationPopup;
  @ViewChild('addSubtractCreditPopup') addSubtractCreditPopup;
  @ViewChild('changePwdPopup') changePwdPopup;
  @ViewChild('confirmDisablePopup') confirmDisablePopup;
  @ViewChild('confirmDeletePopup') confirmDeletePopup;
  // Form validation objects
  crateSubaccountsForm: FormGroup;
  crateSubaccountsPasswordForm: FormGroup;
  verifyChangePwdForm: FormGroup;
  profileEditForm: FormGroup;
  editEmailForm: FormGroup;
  creditTypeFrm: FormGroup;
  addSubCreditFrom: FormGroup;
  recurringFrm: FormGroup;

  checkedPwdValue = true;

  // For password validation
  smtpPwdStatus = false;
  passwordDataObject = [];
  passwordData = '';
  passwordFlag = 0;
  passwordPolicy: PasswordPolicy;

  // vars for Profile edit
  emailData: EmailChange;
  profileDetails: Profile;
  editDetails: boolean;
  editEmail = false;
  verified: boolean;
  // error message type and there message to be dispalyed
  createSubAccMsg = '';
  createSubAccType = '';
  changeCreditMsg = '';
  changeCreditType = '';
  subDomainStatusMsg = '';
  subDomainStatusType = '';
  recurringMsg = '';
  recurringType = '';
  addDeleteType = '';
  addDeleteMsg = '';
  msg: string;
  type: string;
  subAccStatusType: string;
  subAccStatusMsg: string;
  subDomainType: string;
  subDomainMsg: string;
  passwordType: string;
  passwordMsg: string;
  resendType: string;
  resendMsg: string;
  editProfileType: string;
  editProfileMsg: string;

  // for timezone
  timezones: Timezone[];
  timezoneSelected: string;

  subAccountsdataSource: any;
  // Show hide credit management and recurring
  showRecurring = true;
  showNonRecurring = true;
  showUnlimited = true;
  isFreezed = true;
  isRecurringFreezed = true;
  creditType = 'add';
  freeClient = false;
  disaplyRecurringCredit = 0;
  disaplyPeriod = 'daily';

  // showRecurring = false;
  // showNonRecurring = false;
  // showUnlimited = false;
  // isFreezed = false;
  // isRecurringFreezed = false;


  // Credit management varables
  selCreditType = '';
  disaplyCreditTyep = '';
  isRecurringEnabled = false;
  manageRecurringDisabled = false;
  manageCreditDisabledText = '';
  creditDispRemaining: any;
  creditRemaining: any;
  isAccountEnabled = false;
  errorEnabling = false;
  hideOption = true;

  availableText = 'Whitelabel Domain';
  selectedText = 'Allocated Domain';
  /**
   *Setting column data of Tag wise smart table
   * @type {
   * {attr: {class: string};
   * columns: {clientid: {
   * title: string; sort: boolean
   * }};
   * actions: {
   * add: boolean; edit: boolean; delete: boolean
   * };
   * hideSubHeader: boolean; noDataMessage: string; pager: {perPage: number}}}
   */
  subAccountsSettings = {
    attr: {
      class: 'table'
    },
    delete: {
      deleteButtonContent: '<i class="icon icon-trash"></i>',
      confirmDelete: true
    },
    columns: {
      username: {
        title: 'Username',
        sort: false,
        type: 'custom',
        renderComponent: TableSubadminStatusComponent,
      },
      email: {
        title: 'Email Address',
        sort: false
      },
      bounce_rate: {
        title: 'Bounce Rate',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.bounce_rate);
        }
      },
      complaint_rate: {
        title: 'Complaint Rate',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.complaint_rate);
        }
      },
      credit_remaining: {
        title: 'Credit Remaining',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.credit_remaining);
        }
      },
      actions: {
        title: 'Actions',
        sort: false,
        type: 'custom',
        renderComponent: TableEditOptionComponent,
        onComponentInitFunction: (instance) => {
          instance.editClick.subscribe(row => {
            this.creditDispRemaining = this.creditRemaining = +(row.credit_remaining);
            this.subAccStatusMsg = '';
            this.subDomainStatusMsg = '';
            this.subDomainMsg = '';
            this.editProfileMsg = '';
            this.passwordMsg = '';
            this.editFlag = false;
            this.tableFlag = true;

            this.selectedClientData = row;

            // this.isAccountEnabled = row.verified === '1' ? true : false;
            this.isAccountEnabled = row.statusid === 1 ? false : true;
            // call api to fetch setting values of Account and Profile

            this.onCancelClick();
            this.cancelEmailEdit();
            this.fetchProfileData();
            this.fetchAccountSettingsDetails();
          });
        },
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };
  eventLog = '';
  dateInput = {
    start: moment(),
    end: moment().add(1, 'month')
  };

  mainInput = {
    start: moment(),
    end: moment().add(1, 'month')
  };
  filterValues = [];
  selectedDateRange = [moment(this.dateInput.start).format('YYYY-MM-DD'),
    moment(this.dateInput.end).format('YYYY-MM-DD')];

  constructor(private http: Http,
              private apiService: ApiService,
              private modalService: NgbModal,
              private fb: FormBuilder,
              private dateRangePickerOptions: DaterangepickerConfig) {
    this.dateRangePickerOptions.settings = {
      minDate: moment(),
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
      ranges: {
        'Next Month': [moment(), moment().add(1, 'month')],
        'Next 3 Months': [moment(), moment().add(3, 'month')],
        'Next 6 Months': [moment(), moment().add(6, 'month')],
        'Next 12 Months': [moment(), moment().add(12, 'month')],
      }
    };
  }

  ngOnInit() {
    // To get Current month dates
    this.getCurrentMonthDates();
    this.passwordPolicy = new PasswordPolicy();
    this.fetchSubaccountDetails();
    // this.getCreditValue(this.selCreditType);


    // Form validation for profile information on edit
    this.profileEditForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      city: ['', Validators.required]
    });
    // Form validation for credit type
    this.creditTypeFrm = this.fb.group({
      creditType: ['', Validators.required],
    });
    // Form validation for adding subtracting Credits
    this.addSubCreditFrom = new FormGroup({
      addSubCredit: new FormControl(''),
      credit: new FormControl('', [Validators.required, Validators.pattern(ONLY_DIGIT)]),
      credit_remaining: new FormControl('', []),
    });
    this.recurringFrm = new FormGroup({
      recurring_credit: new FormControl('', [Validators.required, Validators.pattern(ONLY_DIGIT)]),
      period: new FormControl('', [Validators.required]),
    });
    // Form validation for Email address on edit
    this.editEmailForm = this.fb.group({
      currentemail: ['', [Validators.required, Validators.pattern(EMAIL)]]
    });
  }

  // Function to get current month dates.
  getCurrentMonthDates() {
    let date = new Date();
    let y = date.getFullYear();
    let m = date.getMonth();
    let firstDay = new Date(y, m, 1);
    let lastDay = new Date(y, m + 1, 0);
    this.start_date = moment(firstDay).format('YYYY-MM-DD');
    this.end_date = moment(lastDay).format('YYYY-MM-DD');
  }

  selectedDate(value: any, dateInput: any) {
    this.dateInput.start = value.start;
    this.dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = [
        moment(dateInput.start).format('YYYYMMDD'),
        moment(dateInput.end).format('YYYYMMDD')
      ];
    }
  }

  applyDate(e: any) {
    this.eventLog += '\nEvent Fired: ' + e.event.type;
    // console.log('\nEvent Fired: ', e.event);
  }


  fetchSubaccountDetails() {
    this.subAccountsdataSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${LIST_SUBACCOUNT_DETAILS}`,
      // endPoint: 'http://localhost:5000/subaccount',
      dataKey: 'subaccount'
    });
    // to show hide of no data fond window
    this.subAccountsdataSource.onGetData.subscribe((res) => {
      this.nodataFlag = +res.data.subaccount.total_user_count > 0;
      this.tableFlag = !this.nodataFlag;
    });
  }

  onSearch(query: string) {
    this.filterValues = [];
    this.filterValues.push({field: 'username', search: query});
    this.subAccountsdataSource.setFilter(this.filterValues, false);
  }

////////////////////////////////////////////////// Subaccount Creation //////////////////////////////////////////////////////////////
  showHidePaasword(ev) {
    this.showPasswordFlag = ev.target.checked;
    this.checkedPwdValue = !(this.showPasswordFlag);
    if (this.showPasswordFlag) {
      this.crateSubaccountsPasswordForm.reset();
      this.passwordDataObject = this.passwordPolicy.validatePassword('');
      this.passwordData = this.passwordDataObject[0];
      this.passwordFlag = this.passwordDataObject[1];
    }
  }

  createSubaccountPopup() {
    this.checkedPwdValue = true;
    this.passwordDataObject = this.passwordPolicy.validatePassword('');
    this.passwordData = this.passwordDataObject[0];
    this.passwordFlag = this.passwordDataObject[1];
    this.showPasswordFlag = false;
    this.createSubAccMsg = '';
    this.open(this.subaccountCreationPopup);
  }

  // Function to create sub accounts
  createSubaccont(formData, passwordFormData) {
    this.apiService.post(CREATE_SUBACCOUNT, ({
      'username': formData.username,
      'email': formData.email,
      'setpassword': (this.showPasswordFlag === true ? 1 : 0),
      'password': passwordFormData.pass
    }))
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.nodataFlag = true;
          this.tableFlag = false;
          this.editFlag = true;
          this.popRef.close();
          this.fetchSubaccountDetails();
        } else {
          this.createSubAccType = 'danger';
          this.createSubAccMsg = result['error_info']['message'];
        }
      }, error => {
        this.createSubAccType = 'warning';
        this.createSubAccMsg = WARNING_MSG;
      });
  }

//////////////////////////////////////////////////// Account Settings ///////////////////////////////////////////////////////////////
  backtoListSubAccounts() {
    this.editFlag = true;
    this.tableFlag = false;
    this.fetchSubaccountDetails();
  }

  // Function to fetch account settings tab details
  fetchAccountSettingsDetails() {
    // contain all api calls
    this.fetchSummaryData();
    this.fetchBillingDetails();
    this.fetchDomainList();
  }

  fetchBillingDetails() {
    this.apiService.get(GTE_SUBACCOUNT_BILLING_DETAILS + '?subaccount_username=' + this.selectedClientData['username'])
      .subscribe(result => {
        if (result['status'] === 'success') {
          this.billingDetails = result['data'];
          // Credit details
          this.recurringDetails = result['data']['recurring'];

          this.isRecurringEnabled = result['data']['recurring']['status'] === 1 ? true : false;
          this.dispToggleStatus(this.isRecurringEnabled);

          this.dateInput.start = this.recurringDetails['start_date'] || this.selectedDateRange[0];
          this.dateInput.end = this.recurringDetails['end_date'] || this.selectedDateRange[1];
          // this.dateInput = {
          //   start: this.recurringDetails['start_date'] || this.selectedDateRange[0],
          //   end: this.recurringDetails['end_date'] || this.selectedDateRange[1]
          // };

          this.isRecurringFreezed = this.isRecurringEnabled;
          this.creditRemaining = result['data']['credit_remaining'];
          // get Drop down values
          this.getCreditType(result['data']['master_client_type'], result['data']['martech_client'], result['data']['free']);
          // To set recurring form details
          this.recurringFrm.setValue({
            recurring_credit: this.recurringDetails.recurring_credit || 0,
            period: this.recurringDetails.period || 'daily',
          });
          this.disaplyRecurringCredit = this.recurringDetails.recurring_credit || 0;
          this.disaplyPeriod = this.recurringDetails.period || 'daily';
          // To show selected credit type
          this.isFreezed = this.billingDetails !== '' ? true : false;
          if (this.hideOption = result['data']['martech_client'].toLowerCase() === 'pepipost'
              && result['data']['free']) {
            this.freeClient = true;
          }
          else if (this.hideOption = result['data']['martech_client'].toLowerCase() === 'falconide'
              && result['data']['master_client_type'].toLowerCase() === 'prepaid') {
            this.freeClient = true;
          }
          else {
            this.freeClient = false;
          }


          this.getCreditValue(result['data']['credit_type']);
        }
      });
  }

  // Function to get list of credit type in the drop down
  getCreditType(client_type, martech_client, free) {
    if (client_type === 'prepaid') {
      this.hideOption = true;
    } else if (martech_client === 'pepipost' && free) {
      this.hideOption = true;
    } else {
      this.hideOption = false;
    }
  }

  cancelBilling() {
    this.fetchBillingDetails();
  }

  saveRecurringDetails(formData) {
    this.apiService.post(UPDATE_RECURRING, {
      'subaccount_username': this.selectedClientData['username'],
      'status': this.isRecurringEnabled === true ? 1 : 0,
      'recurring': {
        'start_date': moment(this.dateInput.start).format('YYYY-MM-DD'),
        'end_date': moment(this.dateInput.end).format('YYYY-MM-DD'),
        'period': formData.period,
        'recurring_credit': formData.recurring_credit
      }
    }).subscribe(result => {
      if (result['status'] === 'success') {
        this.recurringMsg = result['message'];
        this.recurringType = 'info';
        this.isRecurringFreezed = true;
      } else {
        this.recurringMsg = result['error_info']['message'];
        this.recurringType = 'danger';
      }
    }, error => {
      this.recurringMsg = WARNING_MSG;
      this.recurringType = 'warning';
    });
  }

  // Function to fetch summary data on account tab.
  fetchSummaryData() {
    this.apiService.post(DASHBOARD_HISTORY_GRAPH, {
      'end_date': this.end_date,
      'start_date': this.start_date,
      'subaccount_username': this.selectedClientData.username
    }).subscribe(result => {
        if (result['status'] === 'success') {
          this.userSummaryData = result['data']['count'];
          this.spamcount = this.userSummaryData['Spams Reports'];


        }
      }
    );
  }

  // Function to fetch the exiting domain list of master and assigned domain list to sub account
  fetchDomainList() {
    // get refresh the domain list make array blank
    this.isDomainAddAllow = false;
    this.showListBox = false;
    this.arrMasterDomainList = [];
    this.arrSubAccDomainList = [];

    this.apiService.post(GET_DOMAIN_LIST, {'subaccount_username': this.selectedClientData.username})
      .subscribe(result => {
        if (result['status'] === 'success') {
          for (let i = 0; i < result['data']['domainlist']['domain_client'].length; i++) {
            this.arrMasterDomainList.push({
              id: result['data']['domainlist']['domain_client'][i],
              name: result['data']['domainlist']['domain_client'][i]
            });
          }
          for (let i = 0; i < result['data']['domainlist']['domain_subaccount'].length; i++) {
            this.arrSubAccDomainList.push(
              result['data']['domainlist']['domain_subaccount'][i], result['data']['domainlist']['domain_subaccount'][i]
            );
          }
          this.isDomainAddAllow = result['data']['allow_domain_addition'] === 1 ? true : false;
        }
        this.showListBox = true;
      });
  }

  // Function to get selected Item list from existing list
  onAvailableItemSelected(ev) {
    this.domainFlag = 'add';
    this.arrDomainList = ev;
  }

  // Function to get selected Item list from selected list
  onSelectedItemsSelected(ev) {
    this.domainFlag = 'remove';
    this.arrDomainList = ev;
  }

  // Function to get Domain list after hitting add or remove button
  getDomainList() {
    this.subDomainStatusMsg = '';
    this.subAccStatusMsg = '';
    this.subDomainMsg = '';
    this.apiService.post(ADD_REMOVE_DOMAIN_SUBACCOOUNT, {
      'subaccount_username': this.selectedClientData.username,
      'domain': this.arrDomainList,
      'action': this.domainFlag
    })
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.subDomainType = 'info';
            this.subDomainMsg = result['message'];
          } else {
            this.subDomainType = 'danger';
            this.subDomainMsg = result['error_info']['message'];
            this.showListBox = false;
          }
          this.fetchDomainList();
        }, error => {
          this.subAccStatusType = 'warning';
          this.subAccStatusMsg = WARNING_MSG;
        }
      );
  }

  // Function to save credit type or change credit type
  saveCreditType(selCreditType) {
    this.changeCreditMsg = '';
    // this.showNonRecurring = this.showUnlimited = true;
    this.apiService.post(CHANGE_CREDIT_TYPE, {
      'subaccount_username': this.selectedClientData['username'],
      'credit_type': selCreditType
    }).subscribe(result => {
      if (result['status'] === 'success') {
        this.changeCreditType = 'info';
        this.changeCreditMsg = result['message'];
        this.isFreezed = true;
      } else {
        this.changeCreditType = 'danger';
        this.changeCreditMsg = result['error_info']['message'];
      }
    }, error => {
      this.changeCreditType = 'warning';
      this.changeCreditMsg = WARNING_MSG;
    });

  }

  changeCredtiType() {
    this.isFreezed = !this.isFreezed;
  }

  changeRecurringDetails() {
    this.isRecurringFreezed = !this.isRecurringFreezed;
  }

  dispToggleStatus(creditStatus) {
    this.isRecurringEnabled = creditStatus;
    this.manageRecurringDisabled = (creditStatus === true) ? false : true;
    this.manageCreditDisabledText = (creditStatus === true) ? '' : 'Please enable Manage Credit in case of any operations';

  }

  // To enable disable credit status
  toggleRecurringStatus(creditStatus) {
    this.dispToggleStatus(creditStatus);
    this.apiService.post(UPDATE_RECURRING, {
      'subaccount_username': this.selectedClientData['username'],
      'status': this.isRecurringEnabled === true ? 1 : 0,
    }).subscribe(result => {
      if (result['status'] === 'success') {
        this.recurringMsg = result['message'];
        this.recurringType = 'info';
      } else {
        this.recurringMsg = result['error_info']['message'];
        this.recurringType = 'danger';
      }
    }, error => {
      this.recurringMsg = WARNING_MSG;
      this.recurringType = 'warning';
    });

  }

// open popup of Add Subtract
  openAddSubtractPopup() {
    this.addDeleteMsg = '';
    this.creditDispRemaining = this.creditRemaining;
    this.addSubCreditFrom.reset();
    this.creditType = 'add';
    this.open(this.addSubtractCreditPopup);
    return false;
  }

  calculateCredit(credit) {
    this.getRemainingVal(this.creditType, credit);

  }

  // Function to add subtrct credit of users
  addDeleteCredit(formData) {
    this.apiService.post(MANAGE_CREDIT, {
      'subaccount_username': this.selectedClientData['username'],
      'action': this.creditType,
      'credits': formData.credit,
    }).subscribe(result => {
      if (result['status'] === 'success') {
        this.addDeleteType = 'info';
        this.addDeleteMsg = result['message'];
        this.fetchBillingDetails();
      } else {
        this.addDeleteType = 'danger';
        this.addDeleteMsg = result['error_info']['message'];
      }
    }, error => {
      this.addDeleteType = 'warning';
      this.addDeleteMsg = WARNING_MSG;
    });
  }

  getRemainingVal(type = 'add', credit) {
    this.creditType = type;
    if (type.toLowerCase() === 'add') {
      this.creditDispRemaining = +(this.creditRemaining + (+credit));
    } else if (type.toLowerCase() === 'subtract' && (+this.creditRemaining) !== 0) {
      this.creditDispRemaining = +(this.creditRemaining - (+credit));
    } else {
      this.creditDispRemaining = this.creditRemaining;
    }
  }

  // To get Type of credit
  getCreditValue(creditType) {
    this.changeCreditMsg = '';
    this.selCreditType = creditType;
    this.disaplyCreditTyep = this.selCreditType === 'fixed' ? 'One-time' : 'Unlimited';
    switch (this.selCreditType) {
      case '':
        this.showUnlimited = true;
        this.showNonRecurring = true;
        this.showRecurring = true;
        break;
      case 'fixed':
        this.showNonRecurring = false;
        this.showRecurring = true;
        this.showUnlimited = true;
        break;
      case 'unlimited':
        this.showUnlimited = false;
        this.showNonRecurring = true;
        this.showRecurring = true;
        break;
      default:
        this.showRecurring = false;
        this.showNonRecurring = false;
        this.showUnlimited = false;
        break;
    }
  }


  allowDomainAdd(ev) {
    this.subDomainStatusMsg = '';
    this.subAccStatusMsg = '';
    this.subDomainMsg = '';
    this.apiService.post(UPDATE_DOMAIN_STATUS, {
      'subaccount_username': this.selectedClientData.username,
      'status': ev.target.checked === true ? 1 : 0
    })
      .subscribe(result => {
        if (result['status'] === 'success') {
          this.subDomainStatusType = 'info';
          this.subDomainStatusMsg = result['message'];
        } else {
          this.subDomainStatusType = 'danger';
          this.subDomainStatusMsg = result['error_info']['message'];
        }
      }, error => {
        this.subDomainStatusType = 'warning';
        this.subDomainStatusMsg = WARNING_MSG;
      });
  }

  // Function to open delete sub account popup
  onDeleteConfirm() {
    this.open(this.confirmDeletePopup);
  }

  // To enable disable account
  toggleAccountStatus(accountEnabled) {
    if (!accountEnabled) {
      this.updateAccountStatus(accountEnabled);
      return false;
    } else {
      this.open(this.confirmDisablePopup);
    }
  }

  disableSubaccount() {
    this.updateAccountStatus(true);
  }

  // Function to update the status of account eg: Enabled or Disabled
  updateAccountStatus(accountEnabled) {
    this.errorEnabling = false;
    this.subAccStatusMsg = '';
    this.apiService.post(UPDATE_SUBACCOUNT_STATUS, {
      'status': accountEnabled === true ? 0 : 1,
      'subaccount_username': this.selectedClientData.username
    })
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.subAccStatusType = 'info';
            this.subAccStatusMsg = result['message'];
            // if account is disabled then close opened popup and redirect to table with refreshed data.
            if (accountEnabled === true) {
              this.popRef.close();
              this.tableFlag = false;
              this.editFlag = true;
              this.fetchSubaccountDetails();
            }
          } else {
            this.subAccStatusType = 'danger';
            this.subAccStatusMsg = result['error_info']['message'];
            this.errorEnabling = true;
            this.isAccountEnabled = true;
          }
        }, error => {
          this.subAccStatusType = 'warning';
          this.subAccStatusMsg = WARNING_MSG;
        }
      );
  }


  // Delete sub account
  deleteSubaccount() {
    this.subAccStatusMsg = '';
    this.apiService.post(DELETE_SUBACCOUNT, {
      'subaccount_username': this.selectedClientData.username
    })
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.popRef.close();
            this.tableFlag = false;
            this.editFlag = true;
            this.fetchSubaccountDetails();
          } else {
            this.subAccStatusType = 'danger';
            this.subAccStatusMsg = result['error_info']['message'];
          }
        }, error => {
          this.subAccStatusType = 'warning';
          this.subAccStatusMsg = WARNING_MSG;
        }
      );

  }

//////////////////////////////////////////////////// Profile Settings ///////////////////////////////////////////////////////////////

  /**
   * Fetch profile data on load
   */
  fetchProfileData() {
    this.fetchEmailDetails();
    this.apiService.get(GET_PROFILE_INFO + '?subaccount_username=' + this.selectedClientData['username'])
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.profileDetails = result['data'];
            // API has different name for info and post
            this.profileDetails.timezone_change = this.profileDetails.timezone_value;
          } else {
            this.type = 'danger';
            this.msg = result['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );

  }

  // On time selection get the change value
  onTimezoneSelect(evt) {
    // store selected timezone value for posting
    this.timezoneSelected = evt;
  }

  /**
   * On edit profile display edit form and fetch timezone data
   */
  editDetailsClick() {
    // set values to be showed on edit form page by getting details from API data
    this.profileEditForm.setValue({
      first_name: this.profileDetails.first_name,
      last_name: this.profileDetails.last_name,
      city: this.profileDetails.city
    });
    // shows edit details page
    this.editDetails = true;
    // to fetch time zones
    this.fetchTimezones();
  }

  /**
   * Edit and save Profile data
   */
  saveProfileData() {
    this.editProfileMsg = '';
    this.resendMsg = '';
    // set previously stored timezone selected value for posting
    this.profileDetails.timezone_change = this.timezoneSelected || this.profileDetails.timezone_value || this.timezones[0]['timezone'];
    // for updating select menu of timezone
    this.profileDetails.timezone_value = this.timezoneSelected || this.profileDetails.timezone_value || this.timezones[0]['timezone'];
    // merging form value to profile details for posting
    const subUserName = {subaccount_username: this.selectedClientData.username};
    this.profileDetails = Object.assign(this.profileDetails, this.profileEditForm.value, subUserName);
    this.apiService.post(UPDATE_PROFILE_INFO, this.profileDetails)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.editProfileType = 'info';
            this.editProfileMsg = result['message'];
            this.editDetails = false;
          } else {
            this.editProfileType = 'error';
            this.editProfileMsg = result['error_info']['message'];
          }
        }, error => {
          this.editProfileType = 'warning';
          this.editProfileMsg = WARNING_MSG;
        }
      );
  }

  /**
   * On click on cancel button hide edit form and display read only profile form
   */
  onCancelClick() {
    this.editDetails = false;
    this.editProfileType = '';
    this.recurringMsg = '';
    this.resendType = '';

  }

  /**
   * To edit email address show email address edit form
   */
  editEmailAddress() {
    this.editEmail = true;
    this.editProfileMsg = '';
    this.resendType = '';
    this.editEmailForm.setValue({
      currentemail: this.emailData.currentemail
    });
    return false;
  }

  /**
   * To set new email address or edit email address
   * @param email
   */
  setEmailAddress() {
    this.editProfileMsg = '';
    this.resendMsg = '';
    this.emailData.resend_email = this.editEmailForm.value['currentemail'];
    this.apiService.post(CHANGE_EMAIL, {
      'email': this.editEmailForm.value['currentemail'],
      'subaccount_username': this.selectedClientData['username']
    })
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.resendType = 'info';
            this.resendMsg = result['message'];
            this.editEmail = false;
          } else {
            this.resendType = 'danger';
            this.resendMsg = result['error_info']['message'];
          }
        },
        error => {
          this.resendType = 'warning';
          this.resendMsg = WARNING_MSG;
        });
    return false;
  }

  cancelEmailEdit() {
    this.editEmail = false;
    this.resendType = '';
    return false;
  }

  /**
   * Open popup when click on chcnage profile password
   */
  changePassword() {
    this.passwordData = '';
    this.passwordMsg = '';
    this.smtpPwdStatus = false;
    this.open(this.changePwdPopup);
    this.passwordDataObject = this.passwordPolicy.validatePassword('');
    this.passwordData = this.passwordDataObject[0];
    this.passwordFlag = this.passwordDataObject[1];
    return false;
  }

  /**
   * This function is used to validate password polciy
   * @param ev
   */
  checkPassword(ev) {
    this.passwordDataObject = this.passwordPolicy.validatePassword(ev.target.value);
    this.passwordData = this.passwordDataObject[0];
    this.passwordFlag = this.passwordDataObject[1];
  }

  getChangeSmtpPwdStatus(ev) {
    this.smtpPwdStatus = ev.target.checked;
  }

  /**
   * Set New Login password
   * @param old_pass
   * @param new_pass
   */
  setPassword(smtpPwdFlag, new_pass) {
    this.apiService.post(UPDATE_SUBACCOUNT_PASSWORD,
      {'subaccount_username': this.selectedClientData['username'], 'new_pass': new_pass, set_smtp_pass: smtpPwdFlag === true ? 1 : 0}
    )
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.passwordType = 'info';
            this.passwordMsg = result['message'];
          } else {
            this.passwordType = 'danger';
            this.passwordMsg = result['error_info']['message'];
          }
        }, error => {
          this.passwordType = 'warning';
          this.passwordMsg = WARNING_MSG;
        }
      );
  }

  /**
   * Fetch timezone data to be displayed in the dropdown on edit form
   */
  fetchTimezones() {
    this.apiService.post(GETTING_STARTED_API_URL)
      .subscribe(result => {
          if (result['message'] === 'success') {
            this.timezones = result['data']['timezone'];
          }
        },
      );
  }


  /**
   *  To Fetch email details
   */
  fetchEmailDetails() {
    this.apiService.get(GET_EMAIL_DETAILS + '?subaccount_username=' + this.selectedClientData['username'])
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.emailData = result['data'];
            this.verified = this.emailData.is_verified === 1 ? false : true;
          }
        }
      );
  }

  /**
   * To resend email verification link on the mail
   * @param newemail
   */
  resendEmailVerification(newemail) {
    this.apiService.post(RESEND_EMAIL_VERIFICATION,
      {'email': newemail, 'subaccount_usernam': this.selectedClientData['username']}
    )
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.resendType = 'info';
            this.resendMsg = result['message'];
          } else {
            this.resendType = 'danger';
            this.resendMsg = result['error_info']['message'];
          }
        },
        error => {
          this.resendType = 'warning';
          this.resendMsg = WARNING_MSG;
        });
    return false;
  }

  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.verifyChangePwdForm = new FormGroup({
      // old_pass: new FormControl('', [Validators.required]),
      allowSmtpPwdChange: new FormControl(''),
      new_pass: new FormControl('', [
        Validators.required,
        Validators.pattern(PASSWORD)
      ]),
      confirm_pass: new FormControl('', [
        Validators.required,
        // Validators.pattern(PASSWORD)
      ]),
    });
    this.crateSubaccountsForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern(USERNAME)]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAIL)]),
    });
    this.crateSubaccountsPasswordForm = new FormGroup({
      pass: new FormControl('', [Validators.required, Validators.pattern(PASSWORD)]),
      confmPassword: new FormControl('', [Validators.required, Validators.pattern(PASSWORD)]),
    });

    this.popRef = this.modalService.open(content);
  }


}
