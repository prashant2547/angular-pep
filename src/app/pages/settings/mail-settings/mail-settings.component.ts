import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {
  UPDATE_MAIL_SETTINGS,
  LIST_MAIL_SETTINGS,
  TOGGLE_MAIL_SETTINGS,
  WARNING_MSG,
  UPDATE_UNSUBSCIBE_DETAILS
} from '../../../config';
import '../../components/ckeditor.loader';
import 'ckeditor';
import {API, EMAIL, FileValidation, TAG} from '../../components/forms/form-validation';
import {MailSettingModel} from './mail-setting-model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment.prod';
import {CKEditorComponent} from 'ng2-ckeditor';

@Component({
  selector: 'app-mail-settings',
  templateUrl: './mail-settings.component.html',
  styleUrls: ['./mail-settings.component.scss']
})
export class MailSettingsComponent implements OnInit {
  content: any;
  @ViewChild('personalisedFile1') fileInput: ElementRef;
  @ViewChild(CKEditorComponent) ckEditor: CKEditorComponent;

  constructor(private apiService: ApiService, private fb: FormBuilder, private http: HttpClient) {
  }

  // Declaration of variables
  isBounce = false;
  bccForm: FormGroup;
  unsubscribeForm: FormGroup;
  msg = '';
  classValue = '';
  bounce = '';
  bcc = '';
  titleAlert = 'Please enter valid email';
  bouncedata: any;
  bccdata = '';
  decodedContent = '';
  unsubscribedata: MailSettingModel;
  bccCheckboxValue = false;
  unsubscribeCheckboxValue = false;
  dispUnsubTog = false;
  unsubType = '';
  unsubMsg = '';
  showpersonalisedFile = false;
  file: File;
  filename = '';
  fileMsg = '';
  imageFileValidation: FileValidation;
  imgFileErrorList: string[] = [];
  basepathPreview = environment.api_url;

  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'What is bounce forward? And, how does it works?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-bounce-notification-and-how-does-it-works'
    }, {
      helpText: 'What is BCC Mailing?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-bcc-mailing'
    }, {
      helpText: 'What is Customize unsubscribe page/link and why it is required?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-customize-unsubscribe-pagelink-and-why-it-is-required'
    },
  ];

// on load execution to display data
  ngOnInit() {
    this.fetchdata();

    // image validation
    this.imageFileValidation = new FileValidation(1024 * 512, ['jpeg', 'jpg', 'png']);
    // form validation for bcc form
    this.bccForm = new FormGroup({
      bcc: new FormControl('', [
        Validators.required,
        Validators.pattern(EMAIL)
      ]),
    });
    // form validation for unsubscribe form
    this.unsubscribeForm = this.fb.group({
      unsubtag: ['', [Validators.pattern(TAG)]],
      ulandpage: ['', [Validators.pattern(API)]],
      unsub_logo: [''],
      personalisedFile: ['', Validators.maxLength(0)],

    });
  }

  // To fetch existing setting value working
  fetchdata() {
    this.apiService.post(LIST_MAIL_SETTINGS)
      .subscribe((result) => {
        // bounce data
        this.bouncedata = result['data']['bounce_email'] ? result['data']['bounce_email'] : '';
        this.isBounce = this.bouncedata !== '' ? true : false;
        // bcc data
        if ('bcc' in result['data']['setting_value']) {
          this.bccdata = result['data']['setting_value']['bcc']['value'];
          this.bccCheckboxValue = +(result['data']['setting_value']['bcc']['status']) === 1 ? true : false;
        }

        // unsubscribe data
        if ('unsubscribe' in result['data']['setting_value']) {
          this.unsubscribedata = typeof (result['data']['setting_value']['unsubscribe']['value']) !== 'string' ?
            result['data']['setting_value']['unsubscribe']['value']
            : JSON.parse(result['data']['setting_value']['unsubscribe']['value']);
          this.unsubscribedata['status'] = result['data']['setting_value']['unsubscribe']['status'];
          this.unsubscribeCheckboxValue = this.dispUnsubTog = this.unsubscribedata['status'] == 1 ? true : false;
        }
        this.unsubscribedata['unsub_logo'] = result['data']['unsub_logo'];
        console.log(result['data']['unsub_logo']);

        this.showpersonalisedFile = this.unsubscribedata['unsub_logo'] === 'default' ? false : true;


        // decoding html data
        this.decodedContent = (decodeURIComponent(this.unsubscribedata.unsubcontent));

        // setting form values on initial load
        this.unsubscribeForm.patchValue({
          unsubcontent: this.unsubscribedata.unsubcontent,
          unsubtag: decodeURIComponent(this.unsubscribedata.unsubtag),
          ulandpage: (this.unsubscribedata.ulandpage),
          unsub_logo: this.unsubscribedata.unsub_logo,
          personalisedFile: (this.unsubscribedata.personalisedFile)
        });
      });
  }

// to toggle bounce notification button if data is not blank and button is true , saveMailSetting get called
  toggleBounce() {
    this.isBounce !== false && this.bouncedata !== '' ?
      this.saveMailSetting('bounce', '', 1) : '';
    this.isBounce = !this.isBounce;
  }

  /**
   * Toggle setting values and saving the flag into datatbase
   * @param e
   * @param optionvalue
   * @param settingOptionVal
   */
  toggleSettingValue(e, optionvalue, settingOptionVal) {
    settingOptionVal = e;
    this.apiService.post(TOGGLE_MAIL_SETTINGS, ({
      'updateSettingname': optionvalue,
      'updateSettingvalue': settingOptionVal === true ? 1 : 0
    }))
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this[optionvalue] = result['message'];
            this.classValue = 'info';
            settingOptionVal === true ? this.bccCheckboxValue = true : this.bccCheckboxValue = false;
            this.fetchdata();
          } else {
            this[optionvalue] = result['error_info']['message'];
            this.classValue = 'danger';
          }
        },
        error => {
          this[optionvalue] = WARNING_MSG;
          this.classValue = 'warning';
        }
      );
  }

  /**
   * Save bounce and bcc mail-settings details
   * @param {string} setting_name
   * @param {string} settingvalue
   */
  saveMailSetting(setting_name: string, settingvalue: string, disableMessageOnBlankSave: number = 0) {
    this.apiService.post(UPDATE_MAIL_SETTINGS, ({'setting_name': setting_name, 'setting_value': settingvalue}))
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this[setting_name] = disableMessageOnBlankSave === 0 ? result['message'] : '';
            this.classValue = 'info';
            this.bouncedata = disableMessageOnBlankSave === 1 ? '' : this.bouncedata;
          } else {
            this.msg = disableMessageOnBlankSave === 0 ? result['error_info']['message'] : '';
            this.classValue = 'danger';
          }
        }, error => {
          this.msg = WARNING_MSG;
          this.classValue = 'warning';
        }
      );
  }

  // show hide unsubsidised view setting
  dispUnsubscribeDetails() {
    this.dispUnsubTog = !this.dispUnsubTog;
    return false;
  }

  /**
   * On upload file , on change event will accept the event and willl store the file object data.
   * @param event
   */
  onFileChange(event) {
    if (event.target.files && event.target.files.length > 0) {
      this.file = event.target.files[0];
      this.filename = this.file.name;
      this.imgFileErrorList = this.imageFileValidation.validate(this.file);
      this.fileMsg = this.imgFileErrorList.join('<br>');
    }
  }


  onCkChange() {
    this.decodedContent = this.ckEditor.instance.getData();
  }

  /**
   * save unsubscribe details
   * @param unsubscribeData
   */
  saveUnsubscribeData(unsubscribeData: any) {
    // use formdata to send data in case of uploading any kind of file
    const formData = new FormData();
    formData.append('browsed_file', this.file);
    formData.append('unsub_link', unsubscribeData.ulandpage);
    formData.append('unsub_content', this.decodedContent);
    formData.append('unsub_tag', unsubscribeData.unsubtag);
    formData.append('logo', unsubscribeData.unsub_logo);

    this.apiService.post(UPDATE_UNSUBSCIBE_DETAILS, formData)
      .subscribe((result) => {
          console.log(result);
          if (result['status'] === 'success') {
            this.unsubMsg = result['message'];
            this.unsubType = 'info';
            this.fetchdata();
          } else {
            this.unsubMsg = result['error_info']['message'];
            this.unsubType = 'danger';
          }
        },
        error => {
          this.unsubMsg = WARNING_MSG;
          this.unsubType = 'warning';
        }
      );
  }

  /**
   * will accept the event and based on value set and display the default or personalised radio button value
   * @param e
   */
  showpersonalised(e) {
    e.target.value === 'default' ? this.showpersonalisedFile = false : this.showpersonalisedFile = true;
    if (e.target.value === 'default') {
      this.unsubscribeForm.patchValue({
        personalisedFile: ''
      });
      this.imgFileErrorList = [];
    }
    this.fileMsg = '';
  }
}
