import {Component, OnInit, ViewChild} from '@angular/core';
import {TableDataSource} from '../../services/table.data-source';
import {environment} from '../../../../environments/environment';
import {LIST_TAG_NAMES, WARNING_MSG, DELETE_TAG, NO_DATA_FOUND} from '../../../config';
import {Http} from '@angular/http';
import {ApiService} from '../../services/api.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-manage-tags',
  templateUrl: './manage-tags.component.html',
  styleUrls: ['./manage-tags.component.scss']
})
export class ManageTagsComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'How to use Tags in email?',
      helpLink: 'https://docs.pepipost.com/docs/how-to-use-tags-in-pepipost'
    }
  ];

  dataSourcetagwise: any;
  dataSourceArchive: any;
  selectedPages: number;
  redeemed: boolean;
  pagesOptions = [10, 20, 30, 40];
  dispTagwiseValue: boolean;
  dispArchiveValue: boolean;
  tagHeadeLine = 'Archive';
  dataToDisp = 'archive';
  tagid: any;

  // var for no data found window
  searchField = '';
  dataLength = 0;
  manageTagFlag = true;
  manageTagFlagTable = true;
  isFirstTime = true;

  @ViewChild('confirmPopup') confirmPopup;
  tableTagId: number;
  confirmPopRef: NgbActiveModal;

  /**
   *Setting column data of Tag wise smart table
   * @type {{attr: {class: string}; columns: {clientid: {title: string; sort: boolean}}; actions: {add: boolean; edit: boolean; delete: boolean}; hideSubHeader: boolean; noDataMessage: string; pager: {perPage: number}}}
   */
  settingsTagwise = {
    attr: {
      class: 'table'
    },
    delete: {
      deleteButtonContent: '<i class="icon icon-trash"></i>',
      confirmDelete: true
    },
    columns: {
      tag: {
        title: 'Tag Name',
        sort: false
      },
      adddate: {
        title: 'Date',
        sort: false
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: true,
      position: 'right'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };
  /**
   * Setting column data of Archive Tag wise smart table
   * @type {{attr: {class: string}; columns: {clientid: {title: string; sort: boolean}}; actions: {add: boolean; edit: boolean; delete: boolean}; hideSubHeader: boolean; noDataMessage: string; pager: {perPage: number}}}
   */
  settingsArchive = {
    attr: {
      class: 'table'
    },
    columns: {
      clientid: {
        title: 'archivetable',
        sort: true
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  ngOnInit() {
    this.fetchTagwiseData(this.tagHeadeLine);
  }

  onChange(evt) {
    this.dataSourcetagwise.setPaging(this.dataSourcetagwise.pagingConf.page, +evt, true);
  }

  onSearch(query: string = '') {
    this.dataSourcetagwise.setFilter([
      {
        field: 'tag',
        search: query
      },
    ]);
    this.searchField = this.dataSourcetagwise.getFilter()['filters'][0]['search'];
  }

  constructor(private http: Http, private apiService: ApiService, private modalService: NgbModal) {
  }

  /**
   * Fetch tagwise data
   * @param tagHeadeLine
   */
  fetchTagwiseData(tagHeadeLine) {
    this.dispTagwiseValue = false;
    this.dispArchiveValue = true;
    this.tagHeadeLine = tagHeadeLine;
    this.dataSourcetagwise = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${LIST_TAG_NAMES}`,
      // endPoint: `http://192.168.77.101/sachinapi/index.php/analytics/getAllTags`,
      dataKey: 'tag_list'
    });
    // to hide and show no data found window
    this.dataSourcetagwise.onGetData.subscribe((res) => {
      this.manageTagFlag = +res.data.tag_list.total_manage_tags > 0;
      this.manageTagFlagTable = !this.manageTagFlag;
    });
    this.selectedPages = this.pagesOptions[0];
  }

  // Fetch archive data
  /**
   * Fetch archive tag-wise data
   * @param tagHeadeLine
   */
  fetchTagwiseArchiveData(tagHeadeLine) {
    this.dispTagwiseValue = true;
    this.dispArchiveValue = false;
    this.tagHeadeLine = tagHeadeLine;
    this.dataSourceArchive = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${'archive'}`,
      dataKey: 'alerts_list'
    });
    this.selectedPages = this.pagesOptions[0];
  }

  /**
   * On load show tagwise data and onclick display archive data
   * @param dataToDisp
   */
  fetchData(dataToDisp) {
    if (dataToDisp.toLowerCase() === 'tagwise') {
      this.dataToDisp = 'archive';
      this.fetchTagwiseData('Archive');
    } else {
      this.dataToDisp = 'tagwise';
      this.fetchTagwiseArchiveData('Back');
    }
  }

  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.confirmPopRef = this.modalService.open(content);
  }

  /**
   * To get confirmation on delete and call open modal window
   * @param event
   */
  onDeleteConfirm(event) {
    this.tagid = event.data['tagid'];
    console.log(this.tagid);
    this.open(this.confirmPopup);
  }

  /**
   * Deletes row from table with provided alert id
   * @param {number} tagid Id of the alert
   */
  deleteTag(tagid: number) {
    console.log('delete tag id :', tagid);
    this.confirmPopRef.close();
    this.fetchTagwiseData('');
    this.apiService.get(DELETE_TAG + '/format/json?tagid=' + tagid)
      .subscribe((result) => {
        console.log(result);
        if (result['status'] === 'success') {
          this.fetchTagwiseData(this.tagHeadeLine);
        } else {
          alert('Failed to delete tag');
          this.fetchTagwiseData(this.tagHeadeLine);
        }
      }, error => {
        alert('Failed to delete tag');
        this.fetchTagwiseData(this.tagHeadeLine);
      });
  }

}

