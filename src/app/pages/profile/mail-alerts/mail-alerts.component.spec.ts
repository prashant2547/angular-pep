import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailAlertsComponent } from './mail-alerts.component';

describe('MailAlertsComponent', () => {
  let component: MailAlertsComponent;
  let fixture: ComponentFixture<MailAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
