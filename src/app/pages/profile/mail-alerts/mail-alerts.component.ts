import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ADD_MAIL_ALERTS, DELETE_ALERTS, LIST_MAIL_ALERTS, NO_DATA_FOUND, WARNING_MSG} from '../../../config';
import {TableDataSource} from '../../services/table.data-source';
import {environment} from '../../../../environments/environment';
import {ApiService} from '../../services/api.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Http} from '@angular/http';
import {EMAIL, MULTI_EMAIL, THRESHOLD} from '../../components/forms/form-validation';
import {TableEditDeleteComponent} from '../../components/table/table-edit-delete/table-edit-delete.component';


@Component({
  selector: 'app-mail-alerts',
  templateUrl: './mail-alerts.component.html',
  styleUrls: ['./mail-alerts.component.scss']
})
export class MailAlertsComponent implements OnInit {
  // var to show hide no data found window
  nodataFlag = true;
  tableFlag = true;
  tableDisplayFlag = true;

  msg = '';
  type = '';
  selectType: string;
  typeData: MailAlertsComponent;
  mailAlertForm: FormGroup;
  editMailAlertForm: FormGroup;
  alertsSource: any;
  selectedPages: number;
  redeemed: boolean;
  pagesOptions = [10, 20, 30, 40];
  frequencyData = ['Daily', 'Weekly', 'Monthly'];
  frequencySelected = this.frequencyData[0];
  // var for populate on popup once clicked on edit.
  edit_selectType: string;
  edit_thresholdSelected = '';
  edit_frequencySelected = '';
  edit_emailSelected = '';
  editmsg = '';
  edit_dispThreshlod = true;

  dispThreshlod = true;
  customThresholdError = '';
  mailAlertFlag = true;

  /**
   *  You can use ViewChild to get the first element or the directive matching the selector from the
   * view DOM. If the view DOM changes, and a new child matches the selector,
   * the property will be updated.
   */
  @ViewChild('confirmPopup') confirmPopup;
  @ViewChild('editMailAlertPopup') editMailAlertPopup;
  tableAlertId: number;
  confirmPopRef: NgbActiveModal;

  settings = {
    attr: {
      class: 'table'
    },
    delete: {
      deleteButtonContent: '<i class="icon icon-trash"></i>',
      confirmDelete: true
    },
    columns: {
      alert_type: {
        title: 'Alert Type',
        sort: false
      },
      perc: {
        title: 'Threshold %',
        sort: false
      },
      frequency: {
        title: 'Frequency',
        sort: false
      },
      email: {
        title: 'Email Address',
        sort: false
      },
      actions: {
        title: 'Actions',
        sort: false,
        type: 'custom',
        renderComponent: TableEditDeleteComponent,
        onComponentInitFunction: (instance) => {
          instance.deleteClick.subscribe(row => {
            this.onDeleteConfirm(row);
          });
          instance.editClick.subscribe(row => {
            this.onEditConfirm(row);
          });
        }
      },

    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      showSelectPage: false,
      perPage: 10
    }
  };

  constructor(private http: Http, private apiService: ApiService, private modalService: NgbModal) {
  }

  ngOnInit() {
    // form validation
    this.mailAlertForm = new FormGroup({
      alert_type: new FormControl('', [
        Validators.required,
      ]),
      alert_threshold: new FormControl('', [
        Validators.pattern(THRESHOLD)
      ]),
      alert_frequency: new FormControl('', [
        Validators.required,
      ]),
      alert_email: new FormControl('', [
        Validators.required,
        // Validators.pattern(MULTI_EMAIL)
      ])
    });
    this.editMailAlertForm = new FormGroup({
      edit_alert_type: new FormControl('', [
        Validators.required,
      ]),
      edit_alert_threshold: new FormControl('', [
        Validators.pattern(THRESHOLD)
      ]),
      edit_alert_frequency: new FormControl('', [
        Validators.required,
      ]),
      edit_alert_email: new FormControl('', [
        Validators.required,
      ])
    });
    this.fetchData();
    this.fetchTypeData();
  }

  onChange(evt) {
    this.alertsSource.setPaging(this.alertsSource.pagingConf.page, +evt, true);
  }

  /**
   * To fetch data which will displayed in the Mail alert table
   **/
  fetchData() {
    this.alertsSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${LIST_MAIL_ALERTS}`,
      dataKey: 'alerts_list'
    });

    // to hide and show no data found window
    this.alertsSource.onGetData.subscribe((res) => {
      this.nodataFlag = +res.data.alerts_list.total_mail_alerts > 0;
      this.tableFlag = !this.nodataFlag;
      this.tableDisplayFlag = !this.nodataFlag;
      this.msg = '';
    });
    this.selectedPages = this.pagesOptions[0];
  }

  /**
   * To fetch mailing alert type
   */
  fetchTypeData() {
    this.apiService.get(LIST_MAIL_ALERTS)
      .subscribe((result) => {
        if (result['status'] === 'success') {

          this.typeData = result['data']['alert_types'];
          this.selectType = this.typeData[0].alertid;
        } else {
          this.type = 'info';
          this.msg = result['error_info']['message'];
        }
      }, error => {
        this.type = 'warning';
        this.msg = WARNING_MSG;
      });
  }

  /**
   * To get confirmation on delete and call open modal window
   * @param event
   */
  onDeleteConfirm(event) {
    this.tableAlertId = event['alertid'];
    this.open(this.confirmPopup);
  }

  editFrequncy(ev) {
    this.edit_frequencySelected = ev.target.value;
  }

  /**
   * Once user click on edit , the details are set to populate on popup
   * @param data
   */
  onEditConfirm(data) {
    this.editmsg = '';
    this.edit_selectType = data['alertid'];
    this.edit_thresholdSelected = data['perc'];
    this.edit_frequencySelected = data['frequency'];
    this.edit_emailSelected = data['email'];
    // on load of popup chek if alert type is daily the hide threhold
    this.showHideEditThresholdOnLoad(this.edit_selectType);
    // open popup
    this.open(this.editMailAlertPopup);
  }

  /**
   * To add mail alert
   * @param data
   */
  addAlert(data, editFlag = 0) {
    // this.validateThreshold(data['alert_type']);
    // console.log(data['alert_threshold'].match('^[1-9][0-9]?$|^100$'));
    // if (data['alert_type'] !== '7' || data['alert_type'] !== '8') {
    //   if (data['alert_threshold'] === '') {
    //     this.customThresholdError = 'cant not be empty';
    //     console.log(this.customThresholdError );
    //     return false;
    //   } else if (data['alert_threshold'].match('^[1-9][0-9]?$|^100$')) {
    //     this.customThresholdError = 'cant not bsde empty';
    //     console.log(this.customThresholdError );
    //     return false;
    //   }else{
    //     this.customThresholdError = '';
    //   }
    //
    // }
    this.apiService.post(ADD_MAIL_ALERTS, (data))
      .subscribe((result) => {
        if (result['status'] === 'success') {
          if (editFlag) {
            this.editmsg = result['message'];
          } else {
            this.msg = result['message'];
          }
          this.type = 'info';
          this.mailAlertForm.controls['alert_email'].reset('');
          this.mailAlertForm.controls['alert_threshold'].reset('');
          this.alertsSource.refresh();
          this.nodataFlag = true;
          this.tableFlag = !this.nodataFlag;
          this.tableDisplayFlag = this.nodataFlag;
        } else {
          if (editFlag) {
            this.editmsg = result['error_info']['message'];
          } else {
            this.msg = result['error_info']['message'];
          }
          this.type = 'danger';
        }
      }, error => {
        this.type = 'warning';
        this.msg = WARNING_MSG;
      });
  }

  /**
   * This function is used to update the alert details
   * @param data
   */
  updateMailAlert(data) {
    const editData = {
      'alert_type': data['edit_alert_type'], 'alert_threshold': data['edit_alert_threshold'],
      'alert_frequency': data['edit_alert_frequency'], 'alert_email': data['edit_alert_email']
    };
    // same function is used to update the details because , at API end insert/update works
    this.addAlert(editData, 1);
  }

  /**
   * Deletes row from table with provided alert id
   * @param {number} alertId Id of the alert
   */
  deleteMailAlert(alertId: number) {
    this.confirmPopRef.close();
    this.apiService.post(DELETE_ALERTS, ({'delete_id': alertId}))
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.type = 'info';
          this.msg = result['message'];
          this.alertsSource.refresh();
        } else {
          this.type = 'danger';
          this.msg = result['error_info']['message'];
        }
      }, error => {
        this.type = 'warning';
        this.msg = WARNING_MSG;
      });
  }

  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.confirmPopRef = this.modalService.open(content);
  }
// alert type is daily then hide threhold
  showHideThreshold(ev, edit = '') {
    this.dispThreshlod = true;
    if (ev.target.value === '7' || ev.target.value === '8') {
      this.dispThreshlod = false;
    } else {
      this.dispThreshlod = true;
    }
  }
// on load of popup chek if alert type is daily then hide threhold
  showHideEditThresholdOnLoad(alertid) {
    if (alertid === '7' || alertid === '8') {
      this.edit_dispThreshlod = false;

    } else {
      this.edit_dispThreshlod = true;
    }
  }

  validateThreshold(threshold) {
    if (threshold !== '7' || threshold !== '8') {
      this.customThresholdError = 'Please enter valid threshold';
      console.log('in error');
      return false;
    }
  }

  showAddAlerts() {
    this.nodataFlag = true;
    this.tableFlag = false;
  }
}
