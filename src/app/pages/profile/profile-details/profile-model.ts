export class Profile {
  member_since? = '';
  credits? = '';
  first_name = '';
  last_name = '';
  city = '';
  state = '';
  country = '';
  email? = '';
  timezone_change? = '';
  timezone_value? = '';
}

export class EmailChange {
  email? = '';
  is_verified: number;
  resend_email = '';
  currentemail = '';
}
