import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EMAIL, PASSWORD} from '../../components/forms/form-validation';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../../services/api.service';
import {
  CHANGE_EMAIL,
  CHANGE_PASSWORD, DELETE_ACCOUNT, GET_ALL_SUBUSERS,
  GET_EMAIL_DETAILS,
  GET_PROFILE_INFO,
  GETTING_STARTED_API_URL,
  RESEND_EMAIL_VERIFICATION,
  UPDATE_PROFILE_INFO,
  WARNING_MSG
} from '../../../config';
import {Timezone} from '../../components/forms/timezone/timezone';
import {Profile} from './profile-model';
import {EmailChange} from './profile-model';
import {PasswordPolicy} from '../../components/forms/form-validation';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit {
  editDetails: boolean;
  editEmail = false;
  // error message type and there message to be dispalyed
  type: string;
  msg: string;
  passwordType: string;
  passwordMsg: string;
  resendType: string;
  resendMsg: string;
  editProfileType: string;
  editProfileMsg: string;
  accountDeleteMsg: string;
  accountDeleteType: string;
  // Store profile data and email data
  emailData: EmailChange;
  profileDetails: Profile;
  // Form validation for password change, profile edit info, edit email
  verifyChangePwdForm: FormGroup;
  profileEditForm: FormGroup;
  editEmailForm: FormGroup;
  // Popup for change password
  popRef: NgbActiveModal;
  verified: boolean;
  // for timezone
  timezones: Timezone[];
  timezoneSelected: string;
  passwordDataObject = [];
  passwordData = '';
  passwordFlag = 0;
  subuserCount = 0;

  passwordPolicy: PasswordPolicy;

  @ViewChild('changePwdPopup') changePwdPopup;
  @ViewChild('deleteAccountPopup') deleteAccountPopup;

  constructor(private apiService: ApiService,
              private modalService: NgbModal,
              private fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit() {

    this.passwordPolicy = new PasswordPolicy();
    // To fetch profile data
    this.fetchProfileData();
    this.fetchEmailDetails();
    this.getSubaccountCount();


    // Form validation for profile information on edit
    this.profileEditForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      city: ['', Validators.required]
    });

    // Form validation for Email address on edit
    this.editEmailForm = this.fb.group({
      currentemail: ['', [Validators.required, Validators.pattern(EMAIL)]]
    });
  }

  // On time selection get the change value
  onTimezoneSelect(evt) {
    // store selected timezone value for posting
    this.timezoneSelected = evt;
  }

  /**
   * Fetch profile data on load
   */
  fetchProfileData() {
    this.apiService.get(GET_PROFILE_INFO)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.profileDetails = result['data'];
            // API has different name for info and post
            this.profileDetails.timezone_change = this.profileDetails.timezone_value;
            // If timezone is not set redirect to set timezone page.
            if (!this.profileDetails.timezone_change) {
              this.router.navigate(['/getting-started/welcome']);
            }
          } else {
            this.type = 'danger';
            this.msg = result['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

  /**
   * On edit profile display edit form and fetch timezone data
   */
  editDetailsClick() {
    // set values to be showed on edit form page by getting details from API data
    this.profileEditForm.setValue({
      first_name: this.profileDetails.first_name,
      last_name: this.profileDetails.last_name,
      city: this.profileDetails.city
    });
    // shows edit details page
    this.editDetails = true;
    // to fetch time zones
    this.fetchTimezones();
  }

  /**
   * Edit and save Profile data
   */
  saveProfileData() {
    this.editProfileMsg = '';
    this.resendMsg = '';
    // set previously stored timezone selected value for posting
    this.profileDetails.timezone_change = this.timezoneSelected || this.profileDetails.timezone_value;
    // for updating select menu of timezone
    this.profileDetails.timezone_value = this.timezoneSelected || this.profileDetails.timezone_value;
    // merging form value to profile details for posting
    this.profileDetails = Object.assign(this.profileDetails, this.profileEditForm.value);
    console.log(this.profileDetails);
    this.apiService.post(UPDATE_PROFILE_INFO, this.profileDetails)
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.editProfileType = 'info';
            this.editProfileMsg = result['message'];
            this.editDetails = false;
          } else {
            this.editProfileType = 'danger';
            this.editProfileMsg = result['error_info']['message'];
          }
        }, error => {
          this.editProfileType = 'warning';
          this.editProfileMsg = WARNING_MSG;
        }
      );
  }

  /**
   * On click on cancel button hide edit form and display read only profile form
   */
  onCancelClick() {
    this.editDetails = false;
    this.editProfileType = '';
    this.resendType = '';
  }

  /**
   * To edit email address show email address edit form
   */
  editEmailAddress() {
    this.editEmail = true;
    this.editProfileMsg = '';
    this.resendType = '';
    console.log(this.emailData);
    this.editEmailForm.setValue({
      currentemail: this.emailData.currentemail
    });
    return false;
  }

  /**
   * To set new email address or edit email address
   * @param email
   */
  setEmailAddress() {
    this.editProfileMsg = '';
    this.resendMsg = '';
    this.emailData.resend_email = this.editEmailForm.value['currentemail'];
    this.apiService.post(CHANGE_EMAIL, {'email': this.editEmailForm.value['currentemail']})
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.resendType = 'info';
            this.resendMsg = result['message'];
            this.editEmail = false;
          } else {
            this.resendType = 'danger';
            this.resendMsg = result['error_info']['message'];
          }
        },
        error => {
          this.resendType = 'warning';
          this.resendMsg = WARNING_MSG;
        });
    return false;
  }

  cancelEmailEdit() {
    this.editEmail = false;
    this.resendType = '';
    return false;
  }

  /**
   * Open popup when click on chcnage profile password
   */
  changePassword() {
    this.passwordData = '';
    this.passwordMsg = '';
    this.open(this.changePwdPopup);
    this.passwordDataObject = this.passwordPolicy.validatePassword('');
    this.passwordData = this.passwordDataObject[0];
    this.passwordFlag = this.passwordDataObject[1];
    return false;
  }

  /**
   * function to open delete request popup
   */
  deleteAccountRequest() {
    this.accountDeleteMsg = '';
    this.open(this.deleteAccountPopup);
    return false;
  }


  /**
   * function to send delete request
   */
  deleteAccount() {
    this.apiService.get(DELETE_ACCOUNT)
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.accountDeleteType = 'info';
            this.accountDeleteMsg = result['message'];
          } else {
            this.accountDeleteType = 'danger';
            this.accountDeleteMsg = result['error_info']['message'];
          }
        }, error => {
          this.accountDeleteType = 'warning';
          this.accountDeleteMsg = WARNING_MSG;
        }
      );
  }

  // function to get total count of subusers
  getSubaccountCount() {
    this.apiService.get(GET_ALL_SUBUSERS)
      .subscribe(result => {
          if (result['status']) {
            this.subuserCount = result['data']['all_subuser']['data'].length;
          } else {
            this.subuserCount = 0;
          }
        }
      );
  }

  /**
   * This function is used to validate password polciy
   * @param ev
   */
  checkPassword(ev) {
    this.passwordDataObject = this.passwordPolicy.validatePassword(ev.target.value);
    this.passwordData = this.passwordDataObject[0];
    this.passwordFlag = this.passwordDataObject[1];
  }

  /**
   * Set New Login password
   * @param old_pass
   * @param new_pass
   */
  setPassword(old_pass, new_pass) {

    console.log(this.passwordMsg);
    console.log(old_pass, new_pass);
    this.apiService.post(CHANGE_PASSWORD, {'old_pass': old_pass, 'new_pass': new_pass})
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.passwordType = 'info';
            this.passwordMsg = result['message'];
          } else {
            this.passwordType = 'danger';
            this.passwordMsg = result['error_info']['message'];
          }
        }, error => {
          this.passwordType = 'warning';
          this.passwordMsg = WARNING_MSG;
        }
      );
  }


  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.verifyChangePwdForm = new FormGroup({
      old_pass: new FormControl('', [Validators.required]),
      new_pass: new FormControl('', [
        Validators.required,
        Validators.pattern(PASSWORD)
      ]),
      confirm_pass: new FormControl('', [
        Validators.required,
        // Validators.pattern(PASSWORD)
      ]),
    });

    this.popRef = this.modalService.open(content);
  }

  /**
   * Fetch timezone data to be displayed in the dropdown on edit form
   */
  fetchTimezones() {
    this.apiService.post(GETTING_STARTED_API_URL)
      .subscribe(result => {
          if (result['message'] === 'success') {
            this.timezones = result['data']['timezone'];
          }
        },
      );
  }


  /**
   *  To Fetch email details
   */
  fetchEmailDetails() {
    this.apiService.get(GET_EMAIL_DETAILS)
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.emailData = result['data'];
            this.verified = this.emailData.is_verified === 1 ? false : true;
            console.log(this.emailData.is_verified);
          }
        }
      );
  }

  /**
   * To resend email verification link on the mail
   * @param newemail
   */
  resendEmailVerification(newemail) {
    this.editProfileMsg = '';
    this.apiService.post(RESEND_EMAIL_VERIFICATION, {'email': newemail})
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.resendType = 'info';
            this.resendMsg = result['message'];
          } else {
            this.resendType = 'danger';
            this.resendMsg = result['error_info']['message'];
          }
        },
        error => {
          this.resendType = 'warning';
          this.resendMsg = WARNING_MSG;
        });
    return false;
  }
}
