import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './profile.component';
import {SystemLogComponent} from './system-log/system-log.component';
import {ProfileDetailsComponent} from './profile-details/profile-details.component';

const routes: Routes = [{
  path: '',
  component: ProfileComponent,
  children: [{
    path: 'details',
    component: ProfileDetailsComponent
  }, {
    path: 'system-log',
    component: SystemLogComponent
  }, {
    path: '',
    redirectTo: 'details'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {
}
