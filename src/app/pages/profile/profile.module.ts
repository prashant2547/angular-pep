import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfileRoutingModule} from './profile-routing.module';
import {ComponentsModule} from '../components/components.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SystemLogComponent} from './system-log/system-log.component';
import {ProfileComponent} from './profile.component';
import {ProfileDetailsComponent} from './profile-details/profile-details.component';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
  ],
  declarations: [
    SystemLogComponent,
    ProfileComponent,
    ProfileDetailsComponent
  ]
})
export class ProfileModule {
}
