/**
 * Converts number to string of k, m, b
 * For example
 * 25000 > 25k
 * 25000000 > 25m
 * 2500000000 > 25b
 * @param {number} num Number to convert
 * @param {number} fixed Precision after point
 * @returns {string} Converted string
 */
export function abbreviateNumber(num: number, fixed = 1): string {
  if (num === null) {
    return null;
  } // terminate early
  if (num === 0) {
    return '0';
  } // terminate early
  fixed = (!fixed || fixed < 0) ? 0 : fixed; // number of decimal places to show
  const b: any = (num).toPrecision(2).split('e'), // get power
    k: any = b.length === 1 ? 0 : Math.floor(Math.min(b[1].slice(1), 14) / 3), // floor at decimals, ceiling at trillions
    c: any = k < 1 ? num.toFixed(+fixed) : (num / Math.pow(10, k * 3) ).toFixed(1 + fixed), // divide by power
    d: any = c < 0 ? c : Math.abs(c), // enforce -0 is 0
    e: any = d + ['', 'K', 'M', 'B', 'T'][k]; // append power
  return e;
}

export function addCommasToNum(num: string | number, locale: string = 'en-US'): string {
  // store actual value whether it is a number or any text like unlimited
  const textData: string = '' + num;
  // try to convert text to number
  const convertNum = +num;
  if (isNaN(convertNum)) {
    // if text is not number like unlimited, return it
    return textData;
  } else {
    // if text is number, then convert it to comma separated
    return convertNum.toLocaleString(locale);
  }
}
