export function saveData(data, fileName) {
  const a = document.createElement('a');
  document.body.appendChild(a);
  // let json = JSON.stringify(data), blob = new Blob([json], {type: "octet/stream"}),
  const blob = new Blob([data], {type: 'octet/stream'});
  const url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = fileName;
  a.click();
  window.URL.revokeObjectURL(url);
}
