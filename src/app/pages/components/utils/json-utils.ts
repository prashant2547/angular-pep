import {environment} from '../../../../environments/environment';

export function resToJson(res: any) {
  let jsonData = {};
  try {
    jsonData = res.json();
  } catch (e) {
    if (!environment.production) {
      console.info(e);
    }
  }
  return jsonData;
}
