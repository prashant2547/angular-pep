import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSubaccountStatusComponent } from './table-subaccount-status.component';

describe('TableSubaccountStatusComponent', () => {
  let component: TableSubaccountStatusComponent;
  let fixture: ComponentFixture<TableSubaccountStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSubaccountStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSubaccountStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
