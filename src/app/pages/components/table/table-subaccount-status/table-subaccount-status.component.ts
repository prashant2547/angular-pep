import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-table-subaccount-status',
  template: `<i class="{{statusClass}}" ngbTooltip="{{tooltipText}}"></i>`,
  styleUrls: []
})
export class TableSubaccountStatusComponent implements OnInit {
  @Input() rowData: any;
  statusClass = '';
  tooltipText = '';

  constructor() {
  }

  ngOnInit() {
    switch (this.rowData.statusid) {
      case  '1':
        this.statusClass = 'status-icon icon-info active';
        this.tooltipText = 'Account is Active';
        break;
      case  '2':
        this.statusClass = 'status-icon icon-success';
        this.tooltipText = 'Account is Inactive';
        break;
      case  '3':
        this.statusClass = 'status-icon icon-incorrect inactive';
        this.tooltipText = 'Account is Deleted';
        break;
      case  'default':
        this.statusClass = 'icon-info';
        this.tooltipText = 'Account is Active';
        break;

    }
  }

}
