import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCurrentPlanChangeComponent } from './table-current-plan-change.component';

describe('TableCurrentPlanChangeComponent', () => {
  let component: TableCurrentPlanChangeComponent;
  let fixture: ComponentFixture<TableCurrentPlanChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableCurrentPlanChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCurrentPlanChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
