import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table';

@Component({
  selector: 'app-table-current-plan-change',
  template: `<div class="status-title"><button class="btn btn-primary" [disabled]="disable_change_plan" (click)="onButtonClick()">Upgrade Plan</button></div>`,
  styles: []
})
export class TableCurrentPlanChangeComponent implements ViewCell, OnInit {
  statusClass: string;
  renderValue: string;
  disable_change_plan = false;


  @Input() value: string | number;
  @Input() rowData: any;

  @Output() buttonClick: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.statusClass = this.value.toString().toLowerCase() === 'active' ? 'active' : 'not-verified';
    this.renderValue = this.value.toString();
    // alert(this.rowData.plan_price);
    if (this.rowData.unpaid_bill) {
      this.disable_change_plan = true;
    }

    if (!this.rowData.domain_approved) {
      this.disable_change_plan = true;
    }


  }
  onButtonClick() {
    // if(this.rowData)
    this.buttonClick.emit(this.rowData);
    return false;
  }
}

