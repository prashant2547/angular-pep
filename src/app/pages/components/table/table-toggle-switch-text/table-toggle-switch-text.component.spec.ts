import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableToggleSwitchTextComponent } from './table-toggle-switch-text.component';

describe('TableToggleSwitchTextComponent', () => {
  let component: TableToggleSwitchTextComponent;
  let fixture: ComponentFixture<TableToggleSwitchTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableToggleSwitchTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableToggleSwitchTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
