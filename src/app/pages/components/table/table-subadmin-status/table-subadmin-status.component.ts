import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-table-subadmin-status',
  template: `
    <i class="status-icon " ngClass="{{classValue}}" ngbTooltip="{{tooltipText}}"></i>
    <span *ngIf="rowData.username">{{rowData?.username}}</span>
    <span *ngIf="!rowData.username"><i>Not Defined</i></span>
  `,
  styles: []
})
export class TableSubadminStatusComponent implements OnInit {
  @Input() rowData: any;
  classValue: string;
  tooltipText: string;

  constructor() {
  }

  ngOnInit() {
    if (+this.rowData.statusid === 1) {
      this.classValue = 'icon-correct active';
      this.tooltipText = '';
    } else {
      this.classValue = 'icon-success';
      this.tooltipText = 'Account is disabled';
    }
  }
}
