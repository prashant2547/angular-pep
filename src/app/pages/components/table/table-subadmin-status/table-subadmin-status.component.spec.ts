import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSubadminStatusComponent } from './table-subadmin-status.component';

describe('TableSubadminStatusComponent', () => {
  let component: TableSubadminStatusComponent;
  let fixture: ComponentFixture<TableSubadminStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSubadminStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSubadminStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
