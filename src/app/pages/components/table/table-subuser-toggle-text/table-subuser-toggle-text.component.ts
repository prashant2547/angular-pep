import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table/components/cell/cell-view-mode/view-cell';

@Component({
  selector: 'app-table-subuser-toggle-text',
  template: `
    <app-ui-switch-text class="custom"
                        [color]="'#c90047'"
                        [defaultBoColor]="'#ffffff'"
                        [defaultBgColor]="'#b5b5b5'"
                        [labelOff]="'Disabled'"
                        (change)="onClick()"
                        [checked]="renderValue"
                        [labelOn]="'Enabled'"></app-ui-switch-text>`,
  styles: []
})
export class TableSubuserToggleTextComponent implements ViewCell, OnInit {

  renderValue: boolean;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() clicked: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = !!(+this.rowData.status && +this.rowData.statusid);
  }

  onClick() {
    this.clicked.emit(this.rowData);
  }

}
