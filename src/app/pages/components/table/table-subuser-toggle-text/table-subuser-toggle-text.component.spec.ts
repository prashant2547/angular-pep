import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TableSubuserToggleTextComponent} from './table-subuser-toggle-text.component';

describe('TableSubuserToggleTextComponent', () => {
  let component: TableSubuserToggleTextComponent;
  let fixture: ComponentFixture<TableSubuserToggleTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableSubuserToggleTextComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSubuserToggleTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
