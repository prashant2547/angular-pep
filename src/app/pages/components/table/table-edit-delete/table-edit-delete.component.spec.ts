import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableEditDeleteComponent } from './table-edit-delete.component';

describe('TableEditDeleteComponent', () => {
  let component: TableEditDeleteComponent;
  let fixture: ComponentFixture<TableEditDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableEditDeleteComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableEditDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
