import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table/components/cell/cell-view-mode/view-cell';

@Component({
  selector: 'app-table-edit-delete',
  template: `
    <a href="javascript:" class="text-link" (click)="onEditClick()">
      <i class="icon icon-edit"></i></a>
    <span class="link-divider">|</span>
    <a href="javascript:" class="text-link" (click)="onTrashClick()"><i class="icon icon-trash"></i></a>
  `,
  styles: []
})
export class TableEditDeleteComponent implements ViewCell, OnInit {

  renderValue: boolean;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() editClick: EventEmitter<any> = new EventEmitter();
  @Output() deleteClick: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = !!this.value;
    // console.log('=============>' + this.rowData.client_type);
  }


  onEditClick() {
    this.editClick.emit(this.rowData);
  }

  onTrashClick() {
    console.log(this.rowData);
    this.deleteClick.emit(this.rowData);
  }
}
