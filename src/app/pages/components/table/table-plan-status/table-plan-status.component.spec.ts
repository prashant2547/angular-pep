import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePlanStatusComponent } from './table-plan-status.component';

describe('TablePlanStatusComponent', () => {
  let component: TablePlanStatusComponent;
  let fixture: ComponentFixture<TablePlanStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablePlanStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablePlanStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
