import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table/components/cell/cell-view-mode/view-cell';

@Component({
  selector: 'app-table-plan-status',
  template: `
    <!--div class="status-title"><span class="status-circle" ngClass="{{statusClass}}"></span>{{statusText}}</div -->
    <div>
      <a class="{{cssForStatus}}" href="#"  (click)="onIcon1Click()">
        <span class="btn change-plan-btn-1">
          <span class="" ngClass="{{statusClass}}"></span>
          {{statusText}}
        </span>
      </a>
      <!--i *ngIf="statusLinkMode" class="icon-success" ngbTooltip="This domain can be managed only on the old Pepipost dashboard"></i-->
    </div>
  `,
  styleUrls: ['./table-plan-status.component.scss']
})
export class TablePlanStatusComponent implements ViewCell, OnInit {

  // renderValue: boolean;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() icon1Click: EventEmitter<any> = new EventEmitter();

  statusText: string;
  cssForStatus: string;
  statusClass: string;

  ngOnInit() {
    if (this.rowData.status.toLowerCase() === 'currentplan') {
      this.statusText = 'Current Plan';
      this.cssForStatus = 'status-link disabled currentplan';
      this.statusClass = 'active';
    } else if (this.rowData.status.toLowerCase() === 'upgrade') {
      this.statusText = 'Upgrade';
      this.cssForStatus = 'status-link text-link upgrade';
      this.statusClass = 'under-approval';
    } else if (this.rowData.status.toLowerCase() === 'downgrade') {
      this.statusText = 'Downgrade';
      this.cssForStatus = 'status-link text-link downgrade';
      this.statusClass = 'rejected';
    }
    // if (this.rowData.plan_price === 0) {
    //   this.statusText = 'Downgrade Not Available';
    //   this.cssForStatus = 'status-link disabled_1';
    //   this.statusClass = 'rejected';
    // }
  }

  onIcon1Click() {
    this.icon1Click.emit(this.rowData);
    return false;
  }
}

