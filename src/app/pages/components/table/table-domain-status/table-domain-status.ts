export const DOMAIN_NOT_VERIFIED = 'Not verified';
export const DOMAIN_UNDER_APPROVAL = 'Under Approval';
export const DOMAIN_ACTIVE = 'Active';
export const DOMAIN_REJECTED = 'Rejected';
