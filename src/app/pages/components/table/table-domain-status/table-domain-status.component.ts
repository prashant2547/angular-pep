import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table/components/cell/cell-view-mode/view-cell';
import {DOMAIN_ACTIVE, DOMAIN_NOT_VERIFIED, DOMAIN_REJECTED, DOMAIN_UNDER_APPROVAL} from './table-domain-status';

@Component({
  selector: 'app-table-domain-status',
  template: `
    <div class="status-title"><span class="status-circle" ngClass="{{statusClass}}"></span>{{statusText}}</div>
    <div class="status-info">
      <a class="status-link text-link" href="#" (click)="onIcon1Click()" [class.disabled]="statusLinkMode">{{statusLinkText}}</a>
      <i *ngIf="statusLinkMode" class="icon-success" ngbTooltip="This domain can be managed only on the old Pepipost dashboard"></i>
    </div>
  `,
  styleUrls: ['./table-domain-status.component.scss']
})
export class TableDomainStatusComponent implements ViewCell, OnInit {

  // renderValue: boolean;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() icon1Click: EventEmitter<any> = new EventEmitter();

  statusText: string;
  statusLinkText: string;
  statusLinkMode: boolean;
  statusClass: string;

  ngOnInit() {
    if (+this.rowData.status === 3) {
      this.statusText = DOMAIN_ACTIVE;
      this.statusLinkText = 'Test mail to get started';
      this.statusClass = 'active';
    } else if (+this.rowData.status === 1) {
      this.statusText = DOMAIN_UNDER_APPROVAL;
      this.statusLinkText = 'Fast Track the process';
      this.statusClass = 'under-approval';
    } else if (+this.rowData.status === 2) {
      this.statusText = DOMAIN_REJECTED;
      this.statusLinkText = 'Learn more';
      this.statusClass = 'rejected';
    } else if (+this.rowData.status === 0) {
      if (+this.rowData.active !== 1 && +this.rowData.mode !== 1) {
        this.statusText = DOMAIN_NOT_VERIFIED;
        this.statusLinkText = 'Verify SPF and DKIM';
        this.statusClass = 'not-verified';
      }else if (+this.rowData.mode !== 1) {
        this.statusText = DOMAIN_NOT_VERIFIED;
        this.statusLinkText = 'Verify DKIM';
        this.statusClass = 'not-verified';
      }else if (+this.rowData.active === 0) {
        this.statusText = DOMAIN_NOT_VERIFIED;
        this.statusLinkText = 'Verify SPF';
        this.statusClass = 'not-verified';
      }
    } else { // (+this.rowData.active !== 1)
      this.statusText = DOMAIN_NOT_VERIFIED;
      this.statusLinkText = 'Verify SPF and DKIM';
      this.statusClass = 'not-verified';
    }
    this.statusLinkMode = this.rowData.action.toLowerCase() === 'true' ? true : false;
  }

  onIcon1Click() {
    this.rowData.domain_status = this.statusText;
    this.icon1Click.emit(this.rowData);
    return false;
  }
}
