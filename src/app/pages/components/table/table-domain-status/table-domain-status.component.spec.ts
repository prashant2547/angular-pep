import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDomainStatusComponent } from './table-domain-status.component';

describe('TableDomainStatusComponent', () => {
  let component: TableDomainStatusComponent;
  let fixture: ComponentFixture<TableDomainStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableDomainStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDomainStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
