import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table';

// Ref https://akveo.github.io/ng2-smart-table/#/examples/custom-editors-viewers
@Component({
  selector: 'app-download-icon',
  template: `
    <a class="text-link" (click)="onClick()"><i class="icon icon-download"></i></a>
  `,
  styles: []
})
export class DownloadIconComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() clicked: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString();
  }

  onClick() {
    this.clicked.emit(this.rowData);
  }

}
