import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSettingsTextComponent } from './table-settings-text.component';

describe('TableSettingsTextComponent', () => {
  let component: TableSettingsTextComponent;
  let fixture: ComponentFixture<TableSettingsTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSettingsTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSettingsTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
