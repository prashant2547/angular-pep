import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table/components/cell/cell-view-mode/view-cell';

@Component({
  selector: 'app-table-settings-text',
  template: `<a class="text-link font-weight-bold" href="#" (click)="onIcon1Click()" [class.disabled]="showActionMode">View Settings </a>
  <i *ngIf="showActionMode" class="icon-success" ngbTooltip="This domain can be managed only on the old Pepipost dashboard"></i>`,
  styles: []
})
export class TableSettingsTextComponent implements ViewCell, OnInit {

  // renderValue: boolean;
  showActionMode: boolean;
  @Input() value: string | number;
  @Input() rowData: any;

  @Output() icon1Click: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    console.log(typeof this.rowData['action']);
    this.showActionMode = this.rowData['action'].toLowerCase() === 'true' ? true : false;
  }

  onIcon1Click() {
    this.icon1Click.emit(this.rowData);
    return false;
  }
}
