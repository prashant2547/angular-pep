import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CouponCodeStateComponent } from './coupon-code-state.component';

describe('CouponCodeStateComponent', () => {
  let component: CouponCodeStateComponent;
  let fixture: ComponentFixture<CouponCodeStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CouponCodeStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CouponCodeStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
