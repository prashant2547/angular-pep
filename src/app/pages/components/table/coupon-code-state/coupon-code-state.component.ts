import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table';

@Component({
  selector: 'app-coupon-code-state',
  template: `<div class="status-title"><span class="status-circle" ngClass="{{statusClass}}"></span>{{renderValue}}</div>`,
  styles: []
})
export class CouponCodeStateComponent implements ViewCell, OnInit {
  statusClass: string;
  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() clicked: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.statusClass = this.value.toString().toLowerCase() === 'active' ? 'active' : 'not-verified';
    this.renderValue = this.value.toString();
  }
}
