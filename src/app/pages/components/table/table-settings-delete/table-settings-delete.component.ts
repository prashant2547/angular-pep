import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table/components/cell/cell-view-mode/view-cell';

@Component({
  selector: 'app-table-settings-delete',
  template: `
    <a *ngIf="rowData.client_type" href="javascript:" class="text-link" (click)="onSettingClick()">
      <i class="icon icon-settings"></i></a>
    <span *ngIf="rowData.client_type" class="link-divider">|</span>
    <a  href="javascript:" class="text-link" (click)="onTrashClick()"><i class="icon icon-trash"></i></a>
  `,
  styles: []
})
export class TableSettingsDeleteComponent implements ViewCell, OnInit {

  renderValue: boolean;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() settingClick: EventEmitter<any> = new EventEmitter();
  @Output() deleteClick: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = !!this.value;
    // console.log('=============>' + this.rowData.client_type);
  }


  onSettingClick() {
    this.settingClick.emit(this.rowData);
  }

  onTrashClick() {
    this.deleteClick.emit(this.rowData);
  }
}
