import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSettingsDeleteComponent } from './table-settings-delete.component';

describe('TableSettingsDeleteComponent', () => {
  let component: TableSettingsDeleteComponent;
  let fixture: ComponentFixture<TableSettingsDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSettingsDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSettingsDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
