import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSettingsIconComponent } from './table-settings-icon.component';

describe('TableSettingsIconComponent', () => {
  let component: TableSettingsIconComponent;
  let fixture: ComponentFixture<TableSettingsIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSettingsIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSettingsIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
