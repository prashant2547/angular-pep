import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from '../../ng2-smart-table/components/cell/cell-view-mode/view-cell';

@Component({
  selector: 'app-table-settings-icon',
  template: `
    <a class="text-link" href="#" (click)="onIcon1Click()"><i class="icon icon-settings"></i></a>
  `,
  styles: []
})
export class TableSettingsIconComponent implements ViewCell, OnInit {

  // renderValue: boolean;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() icon1Click: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
  }

  onIcon1Click() {
    this.icon1Click.emit(this.rowData);
    return false;
  }
}
