import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-table-edit-option',
  template: '<a href="javascript:" class="text-link" (click)="onEditClick()"><i class="icon icon-edit"></i></a>',
  styleUrls: []
})
export class TableEditOptionComponent implements OnInit {
  @Input() rowData: any;
  @Output() editClick: EventEmitter<any> = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }

  onEditClick() {
    this.editClick.emit(this.rowData);
  }

}
