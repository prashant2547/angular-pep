import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableEditOptionComponent } from './table-edit-option.component';

describe('TableEditOptionComponent', () => {
  let component: TableEditOptionComponent;
  let fixture: ComponentFixture<TableEditOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableEditOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableEditOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
