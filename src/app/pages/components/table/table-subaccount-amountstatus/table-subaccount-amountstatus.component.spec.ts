import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSubaccountAmountstatusComponent } from './table-subaccount-amountstatus.component';

describe('TableSubaccountAmountstatusComponent', () => {
  let component: TableSubaccountAmountstatusComponent;
  let fixture: ComponentFixture<TableSubaccountAmountstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSubaccountAmountstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSubaccountAmountstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
