import {Component, Input, OnInit} from '@angular/core';
import {addCommasToNum} from '../../utils/number-utils';

@Component({
  selector: 'app-table-subaccount-amountstatus',
  template: `<i class="{{statusClass}}"></i>  <span>&nbsp;&nbsp;{{rowData.credit}}</span>
  `,
  styleUrls: []
})
export class TableSubaccountAmountstatusComponent implements OnInit {
  @Input() rowData: any;
  statusClass = '';

  constructor() {
  }

  ngOnInit() {
    if ((this.rowData.credit).toLowerCase() === 'unlimited') {
      this.statusClass = '';
      this.rowData.credit = '-';
    } else {
      if (+this.rowData.credit > 0) {
        this.statusClass = 'status-icon fa fa-plus correct active';
        this.rowData.credit = addCommasToNum(this.rowData.credit);
      } else {
        this.statusClass = 'status-icon fa fa-minus incorrect inactive';
        this.rowData.credit = addCommasToNum(this.rowData.credit.substring(1));
      }
    }
  }

}
