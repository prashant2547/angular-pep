import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimezoneSelectMenuComponent } from './timezone-select-menu.component';

describe('TimezoneSelectMenuComponent', () => {
  let component: TimezoneSelectMenuComponent;
  let fixture: ComponentFixture<TimezoneSelectMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimezoneSelectMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimezoneSelectMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
