import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Timezone} from '../timezone/timezone';

@Component({
  selector: 'app-timezone-select-menu',
  templateUrl: './timezone-select-menu.component.html',
  styleUrls: ['./timezone-select-menu.component.scss']
})
export class TimezoneSelectMenuComponent implements OnInit {
  private _timezones: Timezone[];

  selectedTimezone: string;

  @Input() classes: string;
  @Output() onSelect = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  onSelectionChange(evt) {
    this.onSelect.emit(evt);
  }

  @Input()
  set selectedZone(val: string) {
    this.selectedTimezone = val || (this._timezones && this._timezones[0].timezone);
  }

  @Input()
  set timezones(value: Timezone[]) {
    this._timezones = value;
    // if not set any timezone from selectedZone, set timezone
    this.selectedTimezone = this.selectedTimezone || (this._timezones && this._timezones[0].timezone);
  }

  get timezones(): Timezone[] {
    return this._timezones;
  }

}
