/**
 * Form validation regex
 * @type {string}
 */
export const MULTI_EMAIL = '^[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$';
export const EMAIL = '^([a-zA-Z0-9_\\-\\.]+)' +
  '@((\\[[0-9]{1,24}\\.[0-9]{1,24}\\.[0-9]{1,24}\\.)' +
  '|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,24}|[0-9]{1,24})(\\]?)$';
// export const PASSWORD = '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{5,20}'; //(changes 5 from 6)
export const PASSWORD = '^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%])(?!.*\\])\\S{8,20}$';// not allow to add slash
// export const PASSWORD = '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%])(?!.*[\\\\\\/]).{5,20}';// not allow to add slash
export const THRESHOLD = '^[1-9][0-9]?$|^100$';
export const ONLY_DIGIT = '^[0-9]*$';
// export const DOMAIN = '^[a-zA-Z0-9][a-zA-Z0-9-]{0,}[a-zA-Z0-9](?:\\.[a-zA-Z]{2,})+$';
export const DOMAIN = '^[a-zA-Z0-9][a-zA-Z0-9-]{0,}[a-zA-Z0-9](?:\\.[-a-zA-Z]{2,})+$';
// export const DOMAIN = '^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\\.[a-zA-Z]{2,})+$';
export const API = '^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)' +
  '[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,24}(:[0-9]{1,24})?(\\/.*)?$';
// export const API  = '[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+'
// export const TAG = '\\[%(.*?)\\%]';
export const TAG = '^[0-9a-zA-Z]+$';
export const USERNAME = '^([a-zA-Z0-9]){3,34}$';
// export const COMPANY_TEXT = '^([a-zA-Z0-9\-\s]){3,34}$';
export const COMPANY_TEXT = '^([ a-zA-Z0-9\-\s\._-]){3,34}$';
export const ALPHA_NUMERIC = '[a-zA-Z0-9\-_]+';

// constant regex for the password policy validation
const UPPERCASE_ALPHABATES = '[A-Z]+';
const LOWERCASE_ALPHABATES = '[a-z]+';
const SPECIAL_CHARCATER = '[ !@#$%^&*()_+\\-=\\[\\]{};\':"|,.<>?]';
// const SPECIAL_CHARCATER = '[ !@#$%^&*()_+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]';
const DIGIT = '[0-9]+';
const SLASH = '[(?!\\\\\\/]';

export class FileValidation {
  /**
   * Validates params for file object
   * @param {number} size Size of the file in bytes
   * @param {string[]} extensions File extension array in lowercase
   */
  constructor(private size: number,
              private extensions: string[]) {
  }

  /**
   * Validates given file object
   * @param {File} file
   * @returns {Array} List of errors
   */
  validate(file: File): string[] {
    const fileExtension = this.getExtension(file.name)[0].toLowerCase();
    const fileSize = file.size;
    const errorList = [];
    if (this.extensions.indexOf(fileExtension) === -1) {
      errorList.push('Please upload valid file. (allowed_extensions ' + this.extensions.join('|') + ')');
    }
    if (fileSize > this.size) {
      errorList.push('Exceeded max file size, maximum allowed size is ' +
        (this.size / 1024 ) + 'Kb');
    }
    return errorList;
  }

  getExtension(filename: string) {
    return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
  }
}

export class CustomNullValidation {
  constructor() {
  }

  required(data) {
    if (data.length === 0) {
      return ('Field is required');
    }
  }
}

// class to validate password policy
export class PasswordPolicy {
  constructor() {
  }

  validatePassword(password) {
    // store validation span value
    let passwordPolicyDisp = '';
    // store disable flag value of submit button
    let passwordFlag = 0;

    if (!password.match(PASSWORD)) {
      passwordPolicyDisp = '<ul class="validation-list">';
      // check for the length
      if (password.length < 8) {
        passwordPolicyDisp += '<li>Min 8 characters  </li>';
        passwordFlag = 1;
      } else {
        passwordPolicyDisp += '<li class="fade-label">Min 8 characters  </li>';
      }
      // check for the Upper case
      if ((!password.match(UPPERCASE_ALPHABATES))) {
        passwordPolicyDisp += '<li>One uppercase character  </li>';
        passwordFlag = 1;
      } else {
        passwordPolicyDisp += '<li class="fade-label">One uppercase character  </li>';
      }
      // check for the Lower case
      if ((!password.match(LOWERCASE_ALPHABATES))) {
        passwordPolicyDisp += '<li>One lowercase  character </li>';
        passwordFlag = 1;
      } else {
        passwordPolicyDisp += '<li class="fade-label">One lowercase  character  </li>';
      }
      // check for the Special case
      if ((!password.match(SPECIAL_CHARCATER))) {
        passwordPolicyDisp += '<li>One special character  </li>';
        passwordFlag = 1;
      } else {
        passwordPolicyDisp += '<li class="fade-label">One special character  </li>';
      }
      // check for the Number
      if ((!password.match(DIGIT))) {
        passwordPolicyDisp += '<li>One number  </li>';
        passwordFlag = 1;
      } else {
        passwordPolicyDisp += '<li class="fade-label">One number  </li>';
      }

    } else {
      passwordPolicyDisp = '<div class="alert alert-info">Yay! Your password is secure and you are all set.</div>';
      // passwordPolicyDisp = '<li class="form-text text-info">Yay! Your password is secure and you are all set.</li>';
      passwordFlag = 0;
    }
    passwordPolicyDisp += '</ul>';
    return [passwordPolicyDisp, passwordFlag];
  }
}

