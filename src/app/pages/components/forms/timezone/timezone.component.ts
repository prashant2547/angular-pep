import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Timezone} from './timezone';

@Component({
  selector: 'app-timezone',
  templateUrl: './timezone.component.html',
  styleUrls: ['./timezone.component.scss']
})
export class TimezoneComponent implements OnInit {
  private selectedTimezone: string;

  @Input() timezones: Timezone[];
  @Input() selectedZone: string;
  @Input() title: string;
  @Output() onContinueClick = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  onTimezoneSelect(evt) {
    this.selectedTimezone = evt;
  }

  onContinueBtnClick() {
    this.onContinueClick.emit(this.selectedTimezone);
  }
}
