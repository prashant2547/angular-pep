export class Timezone {
  timezone: string;
  utc?: string;
  utcm?: string;
}
