import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiSwitchTextComponent } from './ui-switch-text.component';

@NgModule({
    imports: [CommonModule],
    declarations: [UiSwitchTextComponent],
    exports: [UiSwitchTextComponent]
})
export class UiSwitchTextModule {
}
