import {Component, EventEmitter, forwardRef, HostListener, Input, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

const UI_SWITCH_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => UiSwitchTextComponent),
  multi: true
};

@Component({
  selector: 'app-ui-switch-text',
  templateUrl: 'ui-switch-text.component.html',
  styleUrls: ['ui-switch-text.component.scss'],
  providers: [UI_SWITCH_CONTROL_VALUE_ACCESSOR]
})
export class UiSwitchTextComponent implements ControlValueAccessor {
  @Input() labelOn: string = '';
  @Input() labelOff: string = '';
  @Input() size: string = 'medium';
  @Output() change = new EventEmitter<boolean>();
  @Input() color: string = '#c90047';
  @Input() switchOffColor: string = '';
  @Input() switchColor: string = '#fff';
  @Input() defaultBgColor: string = '#b5b5b5';
  @Input() defaultBoColor: string = '#fff';

  private onTouchedCallback = (v: any) => {
  };
  private onChangeCallback = (v: any) => {
  };

  private _checked: boolean;

  get checked() {
    return this._checked;
  }

  @Input()
  set checked(v: boolean) {
    this._checked = v !== false;
  }

  private _reverse: boolean;

  get reverse() {
    return this._reverse;
  }

  @Input()
  set reverse(v: boolean) {
    this._reverse = v !== false;
  }

  private _disabled: boolean;

  get disabled() {
    return this._disabled;
  }

  @Input()
  set disabled(v: boolean) {
    this._disabled = v !== false;
  }

  getColor(flag: string = '') {
    if (flag === 'borderColor') {
      return this.defaultBoColor;
    }
    if (flag === 'switchColor') {
      if (this.reverse) {
        return !this.checked ? this.switchColor : this.switchOffColor || this.switchColor;
      }
      return this.checked ? this.switchColor : this.switchOffColor || this.switchColor;
    }
    if (this.reverse) {
      return !this.checked ? this.color : this.defaultBgColor;
    }
    return this.checked ? this.color : this.defaultBgColor;
  }

  @HostListener('click')
  onToggle() {
    if (this.disabled) {
      return;
    }
    this.checked = !this.checked;
    this.change.emit(this.checked);
    this.onChangeCallback(this.checked);
    this.onTouchedCallback(this.checked);
  }

  writeValue(obj: any): void {
    if (obj !== this.checked) {
      this.checked = !!obj;
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
}
