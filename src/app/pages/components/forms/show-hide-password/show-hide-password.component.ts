import { Component, ContentChild } from '@angular/core';

@Component({
  selector: 'app-show-hide-password',
  template: `
    <ng-content></ng-content>
    <a (click)="toggleShow()" class="show-hide-password" [class.active]="show"><span class="show-hide-txt">Show &nbsp; Hide</span></a>
  `,
  styles: []
})
export class ShowHidePasswordComponent {

  show = false;

  @ContentChild('showhideinput') input;

  constructor() { }

  toggleShow() {
    this.show = !this.show;
    if (this.show) {
      this.input.nativeElement.type = 'text';
    } else {
      this.input.nativeElement.type = 'password';
    }
  }

}
