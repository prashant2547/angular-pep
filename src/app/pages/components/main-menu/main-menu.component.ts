import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MenuService} from '../../services/menu.service';
import {Subscription} from 'rxjs/Subscription';
import {DOCUMENT} from '@angular/common';
import {
  CHECK_ACCOUNT_STATUS,
  CHECK_FALCON_PREPAID, CHECK_PEPI_CLIENT, CHECK_SUBUSER, CHECK_UNLIMITED_CODE_APPLIED, GET_PLAN_TYPE,
  WARMUPSLAB_DETAILS
} from '../../../config';
import {ApiService} from '../../services/api.service';
import {AlertsService} from '../../services/alerts.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit, OnDestroy {
  showMenu = true;
  accountStatus: any;
  menuSubscription: Subscription;
  refreshPageSub: Subscription;
  hideBillingMenu: boolean;
  hideCreditManagement: boolean;
  hideCreditManagement1 = false;
  falconide = false;
  type = '';
  msg = '';
  hideWarmup = true;
  hideBilling: boolean;
  hideSubUserMenu: boolean;
  hideSubUserMenu1: boolean;
  // falconPrepaid: boolean;
  falconPrepaid = true;
  hideOldBillingMenu = false;
  postpaidClient = false;
  subaccount: any;
  newMater: any;

  constructor(private menuService: MenuService,
              private apiService: ApiService,
              private alertsService: AlertsService,
              @Inject(DOCUMENT) private document: any) {
  }

  ngOnInit() {
    this.menuSubscription = this.menuService.falconPrepaid$.subscribe(bool => {
      this.falconPrepaid = bool;

      this.checkFalconPrepaidClient();
    });

    this.menuSubscription = this.menuService.showMenu$.subscribe(bool => this.showMenu = bool);
    this.refreshPageSub = this.alertsService.refreshPage$.subscribe(bool => this.refreshMenu());
    /*
        this.menuSubscription = this.menuService.falconPrepaid$.subscribe(bool => {
          this.falconPrepaid = bool;
        });
        this.menuSubscription = this.menuService.showMenu$.subscribe(bool => this.showMenu = bool);
        this.refreshPageSub = this.alertsService.refreshPage$.subscribe(bool => this.refreshMenu());
    */

    this.init();

  }

  init() {
    this.getPlanType();
    this.checkPepipostClient();
    this.checkUnlimitedCodeApplied();
    this.fetchWarmUpDetails();
    this.checkSubuser();
    this.fetchWithoutActivatUserDetails();
  }

  refreshMenu() {
    this.init();
    // this.ngOnInit();
  }

  ngOnDestroy(): void {
    this.menuSubscription.unsubscribe();
  }

  fetchWithoutActivatUserDetails() {
    this.apiService.get(CHECK_ACCOUNT_STATUS)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.accountStatus = result['data']['status'];
          }
        }
      );
  }

  checkFalconPrepaidClient() {
    if (this.falconPrepaid) {
      this.hideBillingMenu = true;
      this.hideCreditManagement1 = true;
    } else {
      this.hideCreditManagement1 = false;
    }
  }

  getPlanType() {
    this.apiService.post(GET_PLAN_TYPE, '')
      .subscribe(result => {
          if (String(result['status']).toLowerCase() === 'success') {
            this.falconide = (result['data']['martech_client'] !== 3) ? true : false;
            if (result['data'] !== 'null') {
              if (result['data']['falconide_postpaid'] === 1) {
                //  this.hideBillingMenu = false;
                this.hideBillingMenu = true;
              }

              if (result['data']['new_pricing'] === 1) {
                this.hideOldBillingMenu = true;
              } else {
                this.hideOldBillingMenu = false;
              }

              if (result['data']['client_type'] === 'postpaid') {
                this.postpaidClient = true;
                this.hideCreditManagement = true;
              } else {
                this.hideCreditManagement = false;
              }
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }


  checkPepipostClient() {
    this.apiService.post(CHECK_PEPI_CLIENT, '')
      .subscribe(result => {
          if (String(result['status']).toLowerCase() === 'success') {
            if (result['data'] !== 'null') {
              if (result['data']['martech_client'] === 'pepipost') {
                this.hideBillingMenu = true;
              }
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  checkUnlimitedCodeApplied() {
    this.apiService.post(CHECK_UNLIMITED_CODE_APPLIED, '')
      .subscribe(result => {
          if (String(result['status']).toLowerCase() === 'success') {
            if (result['data']['unlimited_plan'] === 1) {
              // console.log('Unlimited code applied');
              this.hideCreditManagement = true;
            } else {
              this.hideCreditManagement = false;
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  fetchWarmUpDetails() {
    this.apiService.post(WARMUPSLAB_DETAILS)
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.hideWarmup = Object.keys(result['data']['clientWarmupSlab']).length > 0 ? false : true;
          }
        },
      );
  }

  checkSubuser() {
    this.apiService.post(CHECK_SUBUSER, '')
      .subscribe(result => {
          // let result = {
          //   'status': 'success',
          //   'message': '2 domain`s found',
          //   'data': {
          //     'sub_user': 0,
          //     'sub_account': 0,
          //     'sub_account_master': 0,
          //     'forever_free_plan': 0
          //   }
          // };
          if (String(result['status']).toLowerCase() === 'success') {
            if (result['data']['sub_user'] > 0) {
              // this.subaccount = true;
              this.hideSubUserMenu1 = true;
              this.hideBillingMenu = true;
              this.hideCreditManagement = true;
              if (this.postpaidClient === true) {
                this.hideBillingMenu = false;
              }
            } else {
              // for parent account
              // this.subaccount = false;
              this.hideBilling = false;
              this.hideSubUserMenu1 = false;
              // this.hideCreditManagement1 = false;
              this.hideCreditManagement = false;
            }
            // Conditions for subaccount
            if (result['data']['subaccount']) {
              this.subaccount = true;
            } else {
              if (result['data']['subaccount_menu'] > 0) {
                this.newMater = true;
              } else {
                this.newMater = false;
              }
              this.subaccount = false;
            }
            // Check for the forever free plan
            if (result['data']['forever_free_plan'] > 0) {
              //add sub account flag dont dispaly sub account to free plan users
              // this.subaccount = true;
              this.hideSubUserMenu = true;
            } else {
              // this.subaccount = false;
              this.hideSubUserMenu = false;
            }

          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }
}
