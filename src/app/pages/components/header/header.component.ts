import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {AlertsService} from '../../services/alerts.service';
import {
  GET_ALL_NOTIFICATION, GET_AVAILABLE_EMAIL_CREDITS, GET_EMAIL_CREDITS, GET_HEADER_NOTIFICATION, LOG_OUT_URL,
  READ_NOTIFICATION, RESEND_CONFIRMATION, UPDATE_ACTIVATION_EMAIL_ADDRESS
} from '../../../config';
import {ApiService} from '../../services/api.service';
import {MenuService} from '../../services/menu.service';
import {environment} from '../../../../environments/environment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EMAIL} from '../forms/form-validation';
import {empty} from 'rxjs/Observer';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  emailExist = '';
  showActivation = false;
  activationMsg = '';
  activationEmail = '';
  activationEmailTemp = '';
  activationUpdateEmail = '';

  showEmail = false;
  showOnResend = false;
  alertText = '';
  alertSubscription: Subscription;
  refreshPageSub: Subscription;
  creditSubscription: Subscription;
  menuSubscription: Subscription;
  email = '';
  notifications: any;
  unreadCount = 0;
  credits = 0;
  username = '';
  newUserFlag = true;
  parentUserName = '';
  updateEmailFrm: FormGroup;

  constructor(private router: Router,
              private alertsService: AlertsService,
              private menuService: MenuService,
              private apiService: ApiService,
              private fb: FormBuilder) {
    this.alertSubscription = this.alertsService.p0Alert$
      .subscribe(text => this.alertText = text);
    this.refreshPageSub = this.alertsService.refreshPage$
      .subscribe(bool => this.refreshHeader());
    this.creditSubscription = this.menuService.credits$
      .subscribe(value => this.credits = value);

    this.menuSubscription = menuService.parentUserName$
      .subscribe(value => this.parentUserName = value);
  }

  ngOnInit() {
    // Form validation for Email address on edit
    this.updateEmailFrm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(EMAIL)]]
    });
    this.getCredit();
    this.getP0Notification();
    this.apiService.post(GET_ALL_NOTIFICATION)
      .subscribe((result) => {
          if (result['status'].toLowerCase() === 'success') {
            this.notifications = result['data']['notifications'];
            this.unreadCount = result['data']['unread_count'];
            // email for ther profile picture
            this.email = result['data']['emailid'];
            this.username = result['data']['username'];
            // hide switch to old panel for ew user
            this.newUserFlag = result['data']['is_new_user'];

            // (<any>window).ga('set', 'dimension1', this.username);
          } else {
          }
        }
      );
  }

  refreshHeader() {
    this.ngOnInit();
  }

  notificationClick() {
    this.readAllNotification();
    this.router.navigate(['/notification']);
    return false;
  }

  readAllNotification() {
    this.apiService.post(READ_NOTIFICATION)
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.unreadCount = 0;
          }
        },
      );
  }

  closeClick() {
    this.alertsService.hideP0Alert();
  }

  closeActivationClick() {
    this.showActivation = false;
  }

  ngOnDestroy() {
    this.alertSubscription.unsubscribe();
    this.refreshPageSub.unsubscribe();
    this.creditSubscription.unsubscribe();
  }

  logout() {
    window.location.href = `${environment.api_url}${ LOG_OUT_URL}`;
    return false;
  }

  getCredit() {
    this.menuService.refreshCredits();
  }

  getP0Notification() {
    this.apiService.get(GET_HEADER_NOTIFICATION)
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.alertsService.showP0Alert(result['data']['p0']);
            // TODO: need to check p1 alerts , not displaying after disabling the error from ApiService
            // this.alertsService.showP1Alert(result['data']['p1']);
            if (result['data']['p2']['msg']) {
              this.showActivation = true;
              this.activationMsg = result['data']['p2']['msg'];
              this.activationEmail = result['data']['p2']['emailid'];

            }
          }
        },
      );
  }

  // api call to resend account activation link
  resendActivationLink() {
    this.apiService.get(RESEND_CONFIRMATION)
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.showOnResend = true;
          this.showEmail = false;
          this.showActivation = false;
        }
      });
    return false;
  }

// function to cancel after activation resend link
  closeResendActivation() {
    this.showOnResend = false;
    this.showEmail = false;
    this.showActivation = true;
  }

// show block to update email
  updateEmail() {
    this.activationEmailTemp = this.activationEmail;
    this.activationEmail = '';
    this.emailExist = '';
    this.showEmail = true;
    return false;
  }

// function to save / update email address
  saveNewEmail(formdata) {
    this.apiService.get(UPDATE_ACTIVATION_EMAIL_ADDRESS + '?emailid=' + formdata.email)
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.showEmail = false;
          this.activationEmail = formdata.email;
          this.resendActivationLink();
        } else {
          this.emailExist = result['error_info']['message'];
        }
      });
  }

  cancelNewEmail() {
    this.activationEmail = this.activationEmailTemp;
    this.showEmail = false;
  }
}
