import {
  Component,
  Input,
  OnInit,
  ViewChild,
  ElementRef,
  EventEmitter,
  Output, AfterViewInit
} from '@angular/core';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';

import {Element as StripeElement, ElementOptions} from '../ngx-stripe/ngx-stripe';
import {StripeService} from '../ngx-stripe/ngx-stripe';
import {Elements, ElementsOptions} from '../ngx-stripe/ngx-stripe';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-stripe-card',
  templateUrl: './stripe-card.component.html',
  styleUrls: ['./stripe-card.component.scss']
})
export class StripeCardComponent implements OnInit, AfterViewInit {
  id: string;
  @ViewChild('stripeCardNumber') stripeCardNumber: ElementRef;
  @ViewChild('stripeCardExpiry') stripeCardExpiry: ElementRef;
  @ViewChild('stripeCardCvc') stripeCardCvc: ElementRef;

  cardOptions = {
    style: {
      base: {
        color: '#fff',
        fontWeight: 'normal',
        fontFamily: '"Open Sans","Helvetica Neue",Arial,Helvetica,Verdana,sans-serif',
        fontSize: '24px',
        fontSmoothing: 'antialiased',

        '::placeholder': {
          color: '#ffa6a6',
        },
        ':-webkit-autofill': {
          color: '#e39f48',
        },
      },
      invalid: {
        color: '#E25950',

        '::placeholder': {
          color: '#FFCCA5',
        },
      },
    },
    classes: {
      focus: 'focused',
      empty: 'empty',
      invalid: 'invalid',
    }
  };

  cvvOptions;
  stripeNumError: string;
  stripeExpError: string;
  cardType: string;

  stripeForm: FormGroup;

  cardBrandToClass = {
    'visa': 'visa',
    'mastercard': 'mastercard',
    'amex': 'american-express',
    'discover': 'discover',
    'diners': 'diners',
    'jcb': 'jcb'
  };

  @Output() onSubmit: EventEmitter<string> = new EventEmitter();
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @Output() statusChanges: EventEmitter<any> = new EventEmitter();
  @Output() onCard = new EventEmitter<StripeElement>();

  @ViewChild('card') private card: ElementRef;
  private element: StripeElement;

  @Input() showButtons = true;

  @Input()
  private set options(optionsIn: ElementOptions) {
    this.options$.next(optionsIn);
  }

  private options$ = new BehaviorSubject<ElementOptions>({});

  @Input()
  private set elementsOptions(optionsIn: ElementsOptions) {
    this.elementsOptions$.next(optionsIn);
  }

  private elementsOptions$ = new BehaviorSubject<ElementsOptions>({});

  constructor(private stripeService: StripeService,
              private fb: FormBuilder) {
    this.cvvOptions = JSON.parse(JSON.stringify(this.cardOptions));
    this.cvvOptions.style.base.fontSize = '20px';

    this.stripeForm = this.fb.group({
      name: ['', [Validators.required]]
    });

    this.stripeForm.statusChanges.subscribe(status => {
      this.statusChanges.emit(status);
    });
  }

  public ngOnInit() {

  }

  ngAfterViewInit(): void {
    const elements$: Observable<Elements> = this.elementsOptions$
      .asObservable()
      .switchMap((options) => {
        if (Object.keys(options).length > 0) {
          return this.stripeService.elements(options);
        }
        return this.stripeService.elements();
      });
    Observable
      .combineLatest(
        elements$,
        this.options$.filter((options) => Boolean(options))
      )
      .subscribe(([elements]) => {
        const cardNumber = elements.create('cardNumber', this.cardOptions);
        cardNumber.mount(this.stripeCardNumber.nativeElement);

        this.element = cardNumber;

        cardNumber.on('change', function (result) {
          if (result.brand) {
            this.cardType = this.cardBrandToClass[result.brand] || '';
          } else {
            this.cardType = '';
          }

          if (result.error) {
            this.stripeNumError = result.error.message;
          } else {
            this.stripeNumError = '';
          }
        }.bind(this));

        const cardExpiry = elements.create('cardExpiry', this.cvvOptions);
        cardExpiry.mount(this.stripeCardExpiry.nativeElement);

        cardExpiry.on('change', function (result) {
          if (result.error) {
            this.stripeExpError = result.error.message;
          } else {
            this.stripeExpError = '';
          }
        }.bind(this));

        const cardCvc = elements.create('cardCvc', this.cvvOptions);
        cardCvc.mount(this.stripeCardCvc.nativeElement);

        // this.registerElements([cardNumber, cardExpiry, cardCvc]);

        this.onCard.emit(this.element);
      });
  }

  public getCard(): StripeElement {
    return this.element;
  }

  saveNewCard() {
    this.onSubmit.emit(this.getName());
  }

  getName() {
    return this.stripeForm.get('name').value;
  }

  cancelAddNewCard() {
    this.onCancel.emit();
  }

  registerElements(elements, exampleName = 'stripe') {
    const example = this.card.nativeElement;
    const form = example.querySelector('form');
    const resetButton = example.querySelector('a.reset');
    const error = form.querySelector('.error');
    const errorMessage = error.querySelector('.message');

    function enableInputs() {
      Array.prototype.forEach.call(
        form.querySelectorAll(
          'input[type=\'text\'], input[type=\'email\'], input[type=\'tel\']'
        ),
        function (input) {
          input.removeAttribute('disabled');
        }
      );
    }

    function disableInputs() {
      Array.prototype.forEach.call(
        form.querySelectorAll(
          'input[type=\'text\'], input[type=\'email\'], input[type=\'tel\']'
        ),
        function (input) {
          input.setAttribute('disabled', 'true');
        }
      );
    }

    // Listen for errors from each Element, and show error messages in the UI.
    const savedErrors = {};
    elements.forEach(function (element, idx) {
      element.on('change', function (event) {
        if (event.error) {
          error.classList.add('visible');
          savedErrors[idx] = event.error.message;
          errorMessage.innerText = event.error.message;
        } else {
          savedErrors[idx] = null;

          // Loop over the saved errors and find the first one, if any.
          const nextError = Object.keys(savedErrors)
            .sort()
            .reduce(function (maybeFoundError, key) {
              return maybeFoundError || savedErrors[key];
            }, null);

          if (nextError) {
            // Now that they've fixed the current error, show another one.
            errorMessage.innerText = nextError;
          } else {
            // The user fixed the last error; no more errors.
            error.classList.remove('visible');
          }
        }
      });
    });

    // Listen on the form's 'submit' handler...
    form.addEventListener('submit', function (e) {
      e.preventDefault();

      // Show a loading screen...
      example.classList.add('submitting');

      // Disable all inputs.
      disableInputs();

      // Gather additional customer data we may have collected in our form.
      const name = form.querySelector('#' + exampleName + '-name');
      const additionalData = {
        name: name ? name.value : undefined
      };

      // Use Stripe.js to create a token. We only need to pass in one Element
      // from the Element group in order to create a token. We can also pass
      // in the additional customer data we collected in our form.
      this.stripService.createToken(elements[0], additionalData).then(function (result) {
        // Stop loading!
        example.classList.remove('submitting');

        if (result.token) {
          // If we received a token, show the token ID.
          example.querySelector('.token').innerText = result.token.id;
          example.classList.add('submitted');
        } else {
          // Otherwise, un-disable inputs.
          enableInputs();
        }
      });
    });

    resetButton.addEventListener('click', function (e) {
      e.preventDefault();
      // Resetting the form (instead of setting the value to `''` for each input)
      // helps us clear webkit autofill styles.
      form.reset();

      // Clear each Element.
      elements.forEach(function (element) {
        element.clear();
      });

      // Reset error state as well.
      error.classList.remove('visible');

      // Resetting the form does not un-disable inputs, so we need to do it separately:
      enableInputs();
      example.classList.remove('submitted');
    });
  }
}
