import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-help-section',
  templateUrl: './help-section.component.html',
  styleUrls: ['./help-section.component.scss']
})
export class HelpSectionComponent implements OnInit {
  constructor() {
  }
  public helpFulData: any;
  @Input('helpSectionData')
  set helpLinks(val) {
    this.helpFulData = val;
  }

  ngOnInit() {
  }

}
