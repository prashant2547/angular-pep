import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

import {DataSource} from '../../lib/data-source/data-source';

@Component({
  selector: 'ng2-smart-table-pager2',
  styleUrls: ['./pager.component.scss'],
  template: `
    <nav *ngIf="shouldShow()" class="ng2-smart-pagination-nav float-right">
      <div class="ng2-smart-pagination pagination">
        <div class="ng2-smart-page-item page-item" [ngClass]="{disabled: getPage() == 1}">
          <a class="ng2-smart-page-link page-link btn-shadow border-0" href="#"
             (click)="getPage() == 1 ? false : prevPage()" aria-label="Previous">
            <span aria-hidden="true" class="icon-arrow-prev"></span>
            <span class="sr-only">Previous</span>
          </a>
        </div>

        <div class="ng2-smart-page-item page-text">
          <span class="page-link">{{ getPage() }} of {{ getLast() }}</span>
        </div>

        <div class="ng2-smart-page-item page-item"
             [ngClass]="{disabled: getPage() == getLast()}">
          <a class="ng2-smart-page-link page-link btn-shadow border-0" href="#"
             (click)="getPage() == getLast() ? false : nextPage()" aria-label="Next">
            <span aria-hidden="true" class="icon-arrow-next"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </nav>
  `,
})
export class Pager2Component implements OnChanges {

  @Input() source: DataSource;

  @Output() changePage = new EventEmitter<any>();

  protected page: number;
  protected count: number = 0;
  protected perPage: number;

  protected dataChangedSub: Subscription;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.source) {
      if (!changes.source.firstChange) {
        this.dataChangedSub.unsubscribe();
      }
      this.dataChangedSub = this.source.onChanged().subscribe((dataChanges) => {
        this.page = this.source.getPaging().page;
        this.perPage = this.source.getPaging().perPage;
        this.count = this.source.count();
        if (this.isPageOutOfBounce()) {
          this.source.setPage(--this.page);
        }

        this.processPageChange(dataChanges);
      });
    }
  }

  /**
   * We change the page here depending on the action performed against data source
   * if a new element was added to the end of the table - then change the page to the last
   * if a new element was added to the beginning of the table - then to the first page
   * @param changes
   */
  processPageChange(changes: any) {
    if (changes['action'] === 'prepend') {
      this.source.setPage(1);
    }
    if (changes['action'] === 'append') {
      this.source.setPage(this.getLast());
    }
  }

  shouldShow(): boolean {
    return this.source.count() > this.perPage;
  }

  prevPage() {
    if (this.page > 1) {
      this.paginate(--this.page);
    }
    return false;
  }

  nextPage() {
    if (this.page < this.getLast()) {
      this.paginate(++this.page);
    }
    return false;
  }

  paginate(page: number): boolean {
    this.source.setPage(page);
    this.page = page;
    this.changePage.emit({page});
    return false;
  }

  getPage(): number {
    return this.page;
  }

  getLast(): number {
    return Math.ceil(this.count / this.perPage);
  }

  isPageOutOfBounce(): boolean {
    return (this.page * this.perPage) >= (this.count + this.perPage) && this.page > 1;
  }
}
