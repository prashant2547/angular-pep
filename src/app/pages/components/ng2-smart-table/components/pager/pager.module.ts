import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagerComponent } from './pager.component';
import { Pager2Component } from './pager2.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    PagerComponent,
    Pager2Component,
  ],
  exports: [
    PagerComponent,
    Pager2Component,
  ],
})
export class PagerModule { }
