export class Ng2SmartTableDynamicColumn {
  //clone function:
  clone(obj) {
    if (null === obj || 'object' !== typeof obj) {
      return obj;
    }
    let copy = obj.constructor();
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) {
        copy[attr] = obj[attr];
      }
    }
    return copy;
  }
}
