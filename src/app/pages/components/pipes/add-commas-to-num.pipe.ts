import { Pipe, PipeTransform } from '@angular/core';
import {addCommasToNum} from '../utils/number-utils';

@Pipe({
  name: 'addCommasToNum'
})
export class AddCommasToNumPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return addCommasToNum(value);
  }

}
