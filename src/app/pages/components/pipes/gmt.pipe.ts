import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'gmt'
})
export class GmtPipe implements PipeTransform {

  transform(value: any, args?: any): string {
    return '(GMT ' + (value) + ')';
    // return '(GMT ' + this.convertToTime(value) + ')';
  }

  convertToTime(val: any) {
    const time: number = parseFloat(val) || 0;
    let sign: string = time > 0 ? '+' : '-';
    if (time === 0) {
      sign = '';
    }
    return sign + this.addZero(time);
  }

  addZero(val: number): string {
    const t = Math.abs(val);
    if (t < 10) {
      return '0' + t.toFixed(2);
    }
    return t.toFixed(2);
  }

}
