import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'templateSearch'
})
@Injectable()
export class TemplateSearchPipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLowerCase();
    const filtered = items.filter(res => {
      const templateName = (res['name']);
      return templateName.toLowerCase().includes(searchText);
    });
    return filtered.length === 0 ? [-1] : filtered;
  }

}
