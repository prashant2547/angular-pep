import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertsService, P1Alert} from '../../../services/alerts.service';
import {Subscription} from 'rxjs/Subscription';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-page-alert',
  templateUrl: './page-alert.component.html',
  styleUrls: ['./page-alert.component.scss']
})
export class PageAlertComponent implements OnInit, OnDestroy {
  alert: P1Alert;
  alertSubscription: Subscription;
  isInit = true;

  constructor(private alertsService: AlertsService,
              private router: Router) {
    this.alertSubscription = this.alertsService.p1Alert$
      .subscribe(alert => this.alert = alert);
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.alert && !this.isInit) {
          this.alert = null;
        }
        this.isInit = false;
      }
    });
  }

  ngOnInit() {
  }

  closeClick() {
    this.alertsService.hideP1Alert();
  }

  ngOnDestroy(): void {
    this.alertSubscription.unsubscribe();
  }

}
