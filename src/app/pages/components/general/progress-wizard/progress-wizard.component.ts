import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-progress-wizard',
  templateUrl: './progress-wizard.component.html',
  styleUrls: ['./progress-wizard.component.scss']
})
export class ProgressWizardComponent implements OnInit {
  @Input() showNumbers: boolean;

  constructor() {
  }

  private _totalSteps: number;

  get totalSteps(): number {
    return this._totalSteps;
  }

  @Input()
  set totalSteps(val: number) {
    this._totalSteps = val;
  }

  private _currentStep: number;

  get currentStep(): number {
    return this._currentStep;
  }

  @Input()
  set currentStep(val: number) {
    this._currentStep = val;
  }

  ngOnInit() {
  }
}
