import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

interface Step {
  label: string;
  action: string;
}

@Component({
  selector: 'app-process-wizard-text',
  templateUrl: './process-wizard-text.component.html',
  styleUrls: ['./process-wizard-text.component.scss']
})
export class ProcessWizardTextComponent implements OnInit {
  totalSteps: number;
  @Output() stepClick = new EventEmitter<string>();

  @Input() showNumbers: boolean;

  constructor() {
  }

  private _currentStep: number;

  get currentStep(): number {
    return this._currentStep;
  }

  @Input()
  set currentStep(val: number) {
    this._currentStep = val;
  }

  private _stepList: Step[];

  get stepList() {
    return this._stepList;
  }

  @Input()
  set stepList(val: Step[]) {
    this.totalSteps = val.length;
    this._stepList = val;
  }

  ngOnInit() {
  }

  onStepClick(index) {
    this.stepClick.emit(this._stepList[index].action);
    return false;
  }
}
