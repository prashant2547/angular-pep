import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessWizardTextComponent } from './process-wizard-text.component';

describe('ProcessWizardTextComponent', () => {
  let component: ProcessWizardTextComponent;
  let fixture: ComponentFixture<ProcessWizardTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessWizardTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessWizardTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
