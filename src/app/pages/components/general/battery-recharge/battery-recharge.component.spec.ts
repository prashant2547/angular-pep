import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BatteryRechargeComponent} from './battery-recharge.component';

describe('BatteryRechargeComponent', () => {
  let component: BatteryRechargeComponent;
  let fixture: ComponentFixture<BatteryRechargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BatteryRechargeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatteryRechargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
