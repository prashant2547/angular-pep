import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-battery-recharge',
  templateUrl: './battery-recharge.component.html',
  styleUrls: ['./battery-recharge.component.scss']
})
/**
 * Animated Battery Component
 */
export class BatteryRechargeComponent implements OnInit {
  // number of credits to show below battery
  availableCredits: number;
  // total credits used to calculate battery percent
  totalCredits: number;
  // used to adjust height of battery bar
  batteryPercent: number;
  // do we have sufficient credits, to show battery green?
  isSufficient: boolean;
  // set plugged true, to plug power to battery
  plugged: boolean;
  // required value for battery to show green
  sufficientPercent = 0.7;

  // get values from parameters
  @Input() earnedCredits: number;
  @Input() textAvailable: string;
  @Input() textEarned: string;
  @Input() textEarnedSub: string;

  constructor() {

  }

  // set redeemed tru to play battery filling animation and update numbers
  @Input('redeemed')
  set setRedeem(value: boolean) {
    // ignore if battery is already full or earned credits are zero
    // if (this.batteryPercent >= 1 || this.earnedCredits === 0) {
    if (this.earnedCredits === 0) {
      return;
    }
    // plug power to buttery
    this.plugged = value;
    if (value) {
      setTimeout(() => {
        this.availableCredits += this.earnedCredits;
        this.earnedCredits = 0;
        this.batteryPercent = 1;
        this.isSufficient = true;
      }, 500);
    }
  }

  @Input('availableCredits')
  set setAvailableCredits(value: number) {
    this.availableCredits = value;
    this.updateBattery();
  }

  @Input('totalCredits')
  set setTotalCredits(value: number) {
    this.totalCredits = value;
    this.updateBattery();
  }

  ngOnInit() {

  }

  updateBattery() {
    if (this.totalCredits > 0 && this.availableCredits > 0 &&
      this.totalCredits >= this.availableCredits) {
      this.batteryPercent = this.availableCredits / this.totalCredits;
      if (this.batteryPercent > this.sufficientPercent) {
        this.isSufficient = true;
      }
    }
  }
}
