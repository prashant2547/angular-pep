import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UiSwitchTextModule} from './forms/toggle-switch-with-text/index';
import {Ng2SmartTableModule} from './ng2-smart-table';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Ng2OdometerModule} from './ng2-odometer';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {GravatarDirective} from './directives/gravatar.directive';

import {GmtPipe} from './pipes/gmt.pipe';
import {TimesPipe} from './pipes/times.pipe';

import {TimezoneComponent} from './forms/timezone/timezone.component';
import {BatteryRechargeComponent} from './general/battery-recharge/battery-recharge.component';
import {HeaderComponent} from './header/header.component';
import {DownloadIconComponent} from './table/download-icon/download-icon.component';
import {TableToggleSwitchTextComponent} from './table/table-toggle-switch-text/table-toggle-switch-text.component';
import {TableSettingsDeleteComponent} from './table/table-settings-delete/table-settings-delete.component';

import {D3LineComponent} from './charts/d3/d3-line.component';
import {ProgressWizardComponent} from './general/progress-wizard/progress-wizard.component';
import {ProcessWizardTextComponent} from './general/process-wizard-text/process-wizard-text.component';
import {TimezoneSelectMenuComponent} from './forms/timezone-select-menu/timezone-select-menu.component';
import {SimplePieComponent} from './charts/d3/pie/simple-pie/simple-pie.component';
import {TableSubuserToggleTextComponent} from './table/table-subuser-toggle-text/table-subuser-toggle-text.component';
import {TableSettingsIconComponent} from './table/table-settings-icon/table-settings-icon.component';
import {TableDomainStatusComponent} from './table/table-domain-status/table-domain-status.component';
import {RouterModule} from '@angular/router';
import {TableStatusComponent} from './table/table-status/table-status.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {MainMenuComponent} from './main-menu/main-menu.component';
import {SelectModule} from 'ng2-select';
import {PageAlertComponent} from './general/page-alert/page-alert.component';
import {FromNowPipe} from './pipes/from-now.pipe';
import {StripeCardComponent} from './payments/stripe-card/stripe-card.component';
import {AlertComponent} from './forms/alert/alert.component';
import {TableSettingsTextComponent} from './table/table-settings-text/table-settings-text.component';
import {CouponCodeStateComponent} from './table/coupon-code-state/coupon-code-state.component';
import {TemplateSearchPipe} from './pipes/template-search.pipe';
import {AddCommasToNumPipe} from './pipes/add-commas-to-num.pipe';
import {TableEditDeleteComponent} from './table/table-edit-delete/table-edit-delete.component';
import {TablePlanStatusComponent} from './table/table-plan-status/table-plan-status.component';
import {TableCurrentPlanChangeComponent} from './table/table-current-plan-change/table-current-plan-change.component';
import {ShowHidePasswordComponent} from './forms/show-hide-password/show-hide-password.component';
import {HelpSectionComponent} from './help-section/help-section.component';
import {TableSubadminStatusComponent} from './table/table-subadmin-status/table-subadmin-status.component';
import {TableSubaccountStatusComponent} from './table/table-subaccount-status/table-subaccount-status.component';
import { TableSubaccountAmountstatusComponent } from './table/table-subaccount-amountstatus/table-subaccount-amountstatus.component';
import { TableEditOptionComponent } from './table/table-edit-option/table-edit-option.component';

const formComponents = [
  TimezoneComponent,
  TimezoneSelectMenuComponent,
  StripeCardComponent,
  ShowHidePasswordComponent,
];

const customPipes = [
  GmtPipe,
  TimesPipe,
  FromNowPipe,
  TemplateSearchPipe,
  AddCommasToNumPipe
];

const generalComponents = [
  BatteryRechargeComponent,
  ProgressWizardComponent,
  ProcessWizardTextComponent,
  AlertComponent,
  PageAlertComponent,
  HelpSectionComponent,
];

const tableComponents = [
  DownloadIconComponent,
  TableToggleSwitchTextComponent,
  TableSettingsDeleteComponent,
  TableSubuserToggleTextComponent,
  TableEditDeleteComponent,
  TableSettingsIconComponent,
  TableDomainStatusComponent,
  TableStatusComponent,
  TablePlanStatusComponent,
  TableSettingsTextComponent,
  CouponCodeStateComponent,
  TableCurrentPlanChangeComponent,
  TableSubadminStatusComponent,
  TableSubaccountStatusComponent,
  TableSubaccountAmountstatusComponent,
  TableEditOptionComponent,
];

const directives = [
  GravatarDirective,
];

const charts = [
  D3LineComponent,
  SimplePieComponent,
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    UiSwitchTextModule,
    Ng2SmartTableModule,
    NgxChartsModule,
    NgbModule.forRoot(),
    Ng2OdometerModule,
    NgbModule,
    Ng2OdometerModule,
    SelectModule,
  ],
  entryComponents: [
    ...tableComponents,
  ],
  declarations: [
    HeaderComponent,
    MainMenuComponent,
    ...formComponents,
    ...customPipes,
    ...generalComponents,
    ...tableComponents,
    ...directives,
    ...charts,
    TemplateSearchPipe,
    AddCommasToNumPipe,


  ],
  exports: [
    NgbModule,
    Ng2OdometerModule,
    Ng2SmartTableModule,
    NgxChartsModule,
    UiSwitchTextModule,
    HeaderComponent,
    MainMenuComponent,
    SelectModule,
    ...formComponents,
    ...customPipes,
    ...generalComponents,
    ...tableComponents,
    ...directives,
    ...charts,
  ]
})
export class ComponentsModule {
}
