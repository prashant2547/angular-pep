import {Component, ElementRef} from '@angular/core';

/**
 * This graph is for TESTING only
 */
@Component({
  selector: 'app-d3-line',
  template: `
    <ngx-charts-line-chart
      (window:resize)="onResize()"
      [view]="view"
      [autoScale]="'true'"
      [results]="multi"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [legend]="showLegend"
      [showXAxisLabel]="showXAxisLabel"
      [showYAxisLabel]="showYAxisLabel"
      [xAxisLabel]="xAxisLabel"
      [yAxisLabel]="yAxisLabel">
    </ngx-charts-line-chart>
  `,
})
export class D3LineComponent {
  multi = [
    {
      'name': 'Norfolk Island',
      'series': [
        {
          'value': 4669,
          'name': '2016-09-17T00:08:28.733Z'
        },
        {
          'value': 4577,
          'name': '2016-09-17T21:53:36.544Z'
        },
        {
          'value': 4227,
          'name': '2016-09-15T22:32:31.949Z'
        },
        {
          'value': 5843,
          'name': '2016-09-21T13:48:57.291Z'
        },
        {
          'value': 4189,
          'name': '2016-09-12T19:33:26.632Z'
        }
      ]
    },
    {
      'name': 'Jordan',
      'series': [
        {
          'value': 4976,
          'name': '2016-09-17T00:08:28.733Z'
        },
        {
          'value': 5067,
          'name': '2016-09-17T21:53:36.544Z'
        },
        {
          'value': 5681,
          'name': '2016-09-15T22:32:31.949Z'
        },
        {
          'value': 5580,
          'name': '2016-09-21T13:48:57.291Z'
        },
        {
          'value': 3932,
          'name': '2016-09-12T19:33:26.632Z'
        }
      ]
    },
    {
      'name': 'British Indian Ocean Territory',
      'series': [
        {
          'value': 5536,
          'name': '2016-09-17T00:08:28.733Z'
        },
        {
          'value': 6536,
          'name': '2016-09-17T21:53:36.544Z'
        },
        {
          'value': 3172,
          'name': '2016-09-15T22:32:31.949Z'
        },
        {
          'value': 5169,
          'name': '2016-09-21T13:48:57.291Z'
        },
        {
          'value': 4491,
          'name': '2016-09-12T19:33:26.632Z'
        }
      ]
    },
    {
      'name': 'Rwanda',
      'series': [
        {
          'value': 3091,
          'name': '2016-09-17T00:08:28.733Z'
        },
        {
          'value': 5883,
          'name': '2016-09-17T21:53:36.544Z'
        },
        {
          'value': 3018,
          'name': '2016-09-15T22:32:31.949Z'
        },
        {
          'value': 3340,
          'name': '2016-09-21T13:48:57.291Z'
        },
        {
          'value': 5716,
          'name': '2016-09-12T19:33:26.632Z'
        }
      ]
    },
    {
      'name': 'Mali',
      'series': [
        {
          'value': 2636,
          'name': '2016-09-17T00:08:28.733Z'
        },
        {
          'value': 4998,
          'name': '2016-09-17T21:53:36.544Z'
        },
        {
          'value': 2131,
          'name': '2016-09-15T22:32:31.949Z'
        },
        {
          'value': 2773,
          'name': '2016-09-21T13:48:57.291Z'
        },
        {
          'value': 6107,
          'name': '2016-09-12T19:33:26.632Z'
        }
      ]
    }
  ];
  view = [];
  showLegend = true;
  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'Current Requests';
  showYAxisLabel = true;
  yAxisLabel = 'No. of Emails';

  constructor(private el: ElementRef) {
  }

  setGraphSize() {
    const nativeEl: any = this.el.nativeElement;
    if (nativeEl && nativeEl.parentElement) {
      this.view = [nativeEl.parentElement.offsetWidth, 280];
    }
  }

  onResize() {
    this.setGraphSize();
  }

}
