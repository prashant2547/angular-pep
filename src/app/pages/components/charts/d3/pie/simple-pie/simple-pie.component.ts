import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import * as d3 from 'd3';
import {abbreviateNumber} from '../../../../utils/number-utils';

@Component({
  selector: 'app-simple-pie',
  templateUrl: './simple-pie.component.html',
  styleUrls: ['./simple-pie.component.scss']
})

// References:
// https://keathmilligan.net/create-a-reusable-chart-component-with-angular-and-d3-js/
// https://stackoverflow.com/questions/31703396/d3-pie-chart-element-popout
export class SimplePieComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('pie', {read: ElementRef}) pieContainer;

  dataSet = [
    {'legendLabel': 'Unused', 'credits': null},
    {'legendLabel': 'Used', 'credits': null}
  ];

  private chartSvg: any;
  private pie: any;
  private arc: any;
  private arcs: any;

  constructor() {
  }

  /**
   * Set Unused and Used credits for the pie chart
   * @param {number[]} credits Unused and Used credit list
   */
  @Input()
  set credits(credits: number[]) {
    if (!!(credits && credits.length < 2)) {
      return;
    }
    this.dataSet[0].credits = +credits[0];
    this.dataSet[1].credits = +credits[1];
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.createPie();
  }

  ngOnChanges() {
    if (this.chartSvg) {
      this.updatePie();
    }
  }

  createPie() {
    if (!this.isValidPieValues()) {
      return;
    }

    const element = this.pieContainer.nativeElement;

    const canvasWidth = 230,
      canvasHeight = 160,
      legendWidth = 45,
      legendRectSize = 18,
      legendSpacing = 4,
      innerRadius = 0,
      sliceOutDistance = 10,
      color = ['#62b9ff', '#e8e8e8'],
      textColors = ['#ffffff', '#464545'],
      outerRadius = 60;

    // create the SVG element inside the element;
    const rootSvg = d3.select(element).append('svg');
    this.chartSvg = rootSvg.data([this.dataSet]) // associate our data with the document
      .attr('width', canvasWidth) // set the width of the canvas
      .attr('height', canvasHeight) // set the height of the canvas
      .append('g') // make a group to hold our pie chart
      // relocate center of pie to left, middle
      .attr('transform',
        'translate(' + (outerRadius + sliceOutDistance) + ',' + (canvasHeight / 2) + ')');

    // This will create <path> elements for us using arc data...
    this.arc = d3.arc()
      .outerRadius(outerRadius)
      .innerRadius(innerRadius);

    this.pie = d3.pie() // this will create arc data for us given a list of values
      .value((d: any) => d.credits) // Binding each value to the pie
      .sort(d => null);

    // Select all <g> elements with class slice (there aren't any yet)
    this.arcs = this.chartSvg.selectAll('g.slice')
    // Associate the generated pie data (an array of arcs, each having startAngle,
    // endAngle and value properties)
      .data(this.pie(this.dataSet))
      // This will create <g> elements for every "extra" data element that should be associated
      // with a selection. The result is creating a <g> for every object in the data array
      .enter()
      // Create a group to hold each slice (we will have a <path> and a <text>
      // element associated with each slice)
      .append('g')
      .attr('class', 'slice') // allow us to style things in the slices (like text)
      .on('click', function () {
        d3.select(this)
          .transition()
          .duration(500)
          .attr('transform', function (d: any) {
            if (!d.data._expanded) {
              d.data._expanded = true;
              const a = d.startAngle + (d.endAngle - d.startAngle) / 2 - Math.PI / 2;
              const x = Math.cos(a) * sliceOutDistance;
              const y = Math.sin(a) * sliceOutDistance;
              return 'translate(' + x + ',' + y + ')';
            } else {
              d.data._expanded = false;
              return 'translate(0,0)';
            }
          });
      });

    this.arcs.append('path')
    // set the color for each slice to be chosen from the color function defined above
      .attr('fill', function (d, i) {
        return color[i];
      })
      // this creates the actual SVG path using the associated data (pie) with the arc drawing function
      .attr('d', this.arc);

    // Add a credits value to the larger arcs, translated to the arc centroid and rotated.
    // this.arcs.filter(function (d) {
    //   return d.endAngle - d.startAngle > .2;
    // })
    this.arcs.append('text')
      .attr('dy', '.35em')
      .attr('text-anchor', 'middle')
      .attr('transform', (d: any) => { // set the label's origin to the center of the arc
        // we have to make sure to set these before calling arc.centroid
        d.outerRadius = outerRadius; // Set Outer Coordinate
        d.innerRadius = outerRadius / 2; // Set Inner Coordinate
        return 'translate(' + this.arc.centroid(d) + ')';
        // return 'translate(' + arc.centroid(d) + ')rotate(' + angle(d) + ')';
      })
      .style('fill', (d, i) => {
        return textColors[i];
      })
      .style('font-weight', 'bold')
      .text(function (d: any) {
        return abbreviateNumber(d.data.credits);
      });

    const legendGroup = rootSvg.append('g')
      .attr('class', 'legend-group')
      .attr('transform', () => {
        const xPos = canvasWidth - legendWidth;
        const yPos = canvasHeight - (this.dataSet.length * (legendRectSize + legendSpacing));
        return 'translate(' + xPos + ',' + yPos + ')';
      });
    const legend = legendGroup.selectAll('.legend')
      .data(this.dataSet)
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', function (d, i) {
        const height = legendRectSize + legendSpacing;
        const horz = -2 * legendRectSize;
        const vert = i * height;
        return 'translate(' + horz + ',' + vert + ')';
      });

    legend.append('circle')
      .attr('cx', legendRectSize / 2)
      .attr('cy', legendRectSize / 2)
      .attr('r', legendRectSize / 3)
      // .attr('width', legendRectSize)
      // .attr('height', legendRectSize)
      .style('fill', (d, i) => color[i])
      .style('stroke', (d, i) => color[i]);

    legend.append('text')
      .attr('x', legendRectSize + legendSpacing)
      .attr('y', legendRectSize - legendSpacing)
      .style('fill', '#616161')
      .text(function (d: any) {
        return d.legendLabel;
      });

  }

  isValidPieValues() {
    return !(isNaN(+this.dataSet[0].credits) || isNaN(+this.dataSet[1].credits));
  }

  updatePie() {
    if (!this.isValidPieValues()) {
      return;
    }

    this.arcs = this.chartSvg.selectAll('g.slice')
    // Associate the generated pie data (an array of arcs, each having startAngle,
    // endAngle and value properties)
      .data(this.pie(this.dataSet))
      .select('path')
      .attr('d', this.arc); // redrawing the path

    this.arcs = this.chartSvg.selectAll('g.slice')
      .select('text')
      .attr('transform', (d: any) => { // set the label's origin to the center of the arc
        return 'translate(' + this.arc.centroid(d) + ')';
      })
      .text(function (d: any) {
        return abbreviateNumber(d.data.credits);
      });
  }

  // Computes the angle of an arc, converting from radians to degrees.
  angle(d) {
    const a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
    return a > 90 ? a - 180 : a;
  }
}
