import {Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as md5 from 'md5';

@Directive({
  selector: '[appGravatar]'
})
export class GravatarDirective implements OnInit, OnChanges {
  @Input('email') email: string;
  @Input('size') size = 16;
  @Input('fallback') fallback = 'mm';

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.setUrlGravatar(this.email);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['email']) {
      this.setUrlGravatar(changes['email'].currentValue);
    }
  }

  setUrlGravatar(email: string = 'test@example.com') {
    // Bug: getting null value even after setting default value
    email = email || 'test@example.com';
    this.elementRef.nativeElement.src = `//www.gravatar.com/avatar/${md5(email)}?s=${this.size}&d=${this.fallback}`;
  }
}
