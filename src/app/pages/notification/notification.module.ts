import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NotificationRoutingModule} from './notification-routing.module';
import {NotificationComponent} from './notification.component';
import {ComponentsModule} from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    NotificationRoutingModule,
    ComponentsModule,
  ],
  declarations: [NotificationComponent]
})
export class NotificationModule {
}
