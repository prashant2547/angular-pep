import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {GET_ALL_NOTIFICATION, READ_NOTIFICATION, WARNING_MSG} from '../../config';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  constructor(private apiService: ApiService) {
  }

  allNotificationData = [];
  unreadCount = 0;
  notificationType = '';
  notificationMsg = '';

  ngOnInit() {
    this.fetchAllNotification();
  }
// fetch all notification details
  fetchAllNotification() {
    this.apiService.post(GET_ALL_NOTIFICATION)
      .subscribe((result) => {
          console.log(result['data']['notifications']);
          if (result['status'] === 'success') {
            this.allNotificationData = result['data']['notifications'];
            this.unreadCount = result['data']['unread_count'];
          } else {
            this.notificationType = 'danger';
            this.notificationMsg = result['error_info']['message'];
          }
        },
        error => {
          this.notificationType = 'warning';
          this.notificationMsg = WARNING_MSG;
        }
      );
  }
}
