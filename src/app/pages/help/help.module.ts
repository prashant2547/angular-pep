import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HelpRoutingModule} from './help-routing.module';
import {HelpComponent} from './help.component';
import {ComponentsModule} from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    HelpRoutingModule,
    ComponentsModule,
  ],
  declarations: [HelpComponent]
})
export class HelpModule {
}
