import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {ComponentsModule} from '../components/components.module';
import {SelectModule} from 'ng2-select';
import {Daterangepicker} from 'ng2-daterangepicker';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ComponentsModule,
    SelectModule,
    Daterangepicker
  ],
  declarations: [
    DashboardComponent
  ]
})
export class DashboardModule {
}
