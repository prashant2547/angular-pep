import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, OnChanges} from '@angular/core';
import {DaterangepickerConfig} from 'ng2-daterangepicker';
import * as moment from 'moment';
import {
  CHECK_PEPI_CLIENT,
  CHECK_UNLIMITED_CODE_APPLIED,
  DEFAULT_EMAIL_PRICE,
  DEFAULT_MAIL_COUNT,
  GET_AVAILABLE_EMAIL_CREDITS,
  GET_EMAIL_CREDITS,
  GET_PLAN_TYPE,
  WARMUPSLAB_DETAILS,
  GET_CLIENT_ID,
  GET_SUPPRESSION_EMAIL_ADDRESS,
  CHECK_SUBUSER,
  GET_NEW_SLAB_EMAIL_CREDITS,
  CHECK_ACCOUNT_STATUS,
  GET_PROFILE_INFO
} from '../../config';
import {Router} from '@angular/router';
import {ApiService} from '../services/api.service';
import {abbreviateNumber} from '../components/utils/number-utils';
import {AlertsService} from '../services/alerts.service';
import {GetStartedService} from '../services/get-started.service';
import {HttpClient} from '@angular/common/http';
import * as io from 'socket.io-client';
import {environment} from '../../../environments/environment';
import {
  DASHBOARD_HISTORY_GRAPH,
} from '../../config';
import {Subscription} from 'rxjs/Subscription';
import {MenuService} from '../services/menu.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  accountStatus: any;
  // var to show hide no data found window
  nodataFlag = true;
  tableFlag = true;

  // pie chart variables
  unusedCredit: number;
  usedCredit: number;
  totalSaving: number;
  newTotalSaving: number;
  approxValue: any;
  newApproxValue: any;
  showPieTopText: number;
  showEmailCredits: string;
  pieCredits = [];
  pieCreditsNew = [];
  usedEmails: any;

  // warm up sidebar variables
  warmUpData: any;
  warmUpDataLength = 0;
  warmUpSidebarData = [];
  activeWarmUpDetailsLength = 0;
  activeWarmUpDetails: any;
  warmupFlag = true;
  warmUpErrorType = '';
  warmUpErrorMsg = '';
  stepList = [];
  stepCount = 0;
  hideAddCredits: boolean;
  subaccount: boolean;
  falconPrepaidClient: boolean;
  falconPrepaid = true;

  totalRecords = 1;
  showResendEmail = true;
  menuSubscription: Subscription;
  showPieNew = false;

  // graph variables
  historyData = [
    {
      'name': 'Requests',
      'series': []
    }, {
      'name': 'Delivered',
      'series': []
    }, {
      'name': 'Opened',
      'series': []
    }, {
      'name': 'Clicked',
      'series': []
    }, {
      'name': 'Unsubscribes',
      'series': []
    }, {
      'name': 'Bounces',
      'series': []
    }, {
      'name': 'Spam Reports',
      'series': []
    }, {
      'name': 'Invalid',
      'series': []
    }, {
      'name': 'Drops',
      'series': []
    }
  ];

  view = [];
  selectedDateRange = {};

  // socket
  socket = io(`${environment.socket_host}`);
  realtimeData = [{
    'name': 'Requests',
    'series': []
  }];
  intervalId;
  unlimitedCodeApplied: boolean;
  pepipostClient: boolean;
  type = '';
  msg = '';
  postPaidClient: boolean;
  paidClient: boolean;


  mainInput = {
    start: moment().subtract(7, 'days'),
    end: moment()
  };

  mainGraphInput = {
    'start_date': moment().subtract(7, 'days').format('YYYY-MM-DD'),
    'end_date': moment().format('YYYY-MM-DD')
  };


  // history variables
  history_delivered_percent = '0%';
  history_bounced_percent = '0%';
  history_dropped_percent = '0%';
  history_open_percent = '0%';
  history_clicks_percent = '0%';
  history_unsubscribe_percent = '0%';
  history_abuse_percent = '0%';
  history_request_count = '0';

  history_delivered_count = '0';
  history_bounced_count = '0';
  history_dropped_count = '0';
  history_open_count = '0';
  history_clicks_count = '0';
  history_unsubscribe_count = '0';
  history_abuse_count = '0';


  // realtime variables
  realtime_delivered_percent = '0%';
  realtime_bounced_percent = '0%';
  realtime_clicks_percent = '0%';
  realtime_open_percent = '0%';
  // newly added in 3.0
  realtime_dropped_percent = '0%';
  realtime_spam_percent = '0%';
  realtime_unsub_percent = '0%';


  realtime_request_count = '0';
  realtime_delivered_count = '0';
  realtime_bounced_count = '0';
  realtime_clicks_count = '0';
  realtime_open_count = '0';
  // newly added in 3.0
  realtime_dropped_count = '0';
  realtime_spam_count = '0';
  realtime_unsub_count = '0';

  uuid: any;

  eventLog = '';
  dateInput = {
    start: moment().subtract(7, 'days'),
    end: moment()
  };
  value: any = {};

  colorScheme = {
    domain: ['#d4336c', '#ffb141', '#81c7ff', '#fa4956', '#7349fa', '#51d599', '#fa49db', '#fa6f49']
  };

  colorSchemeUserSummary = {
    domain: ['#0874ba', '#26ca7f', '#8d8d8d']
  };

  resizeInterval: any;

  @ViewChild('flexContent') flexContent;
  // cache all graph elements
  graphElements: any[];

  constructor(private dateRangePickerOptions: DaterangepickerConfig,
              private apiService: ApiService,
              private alertsService: AlertsService,
              private getStartedService: GetStartedService,
              private el: ElementRef,
              private http: HttpClient,
              private menuService: MenuService,
              private router: Router) {
    this.menuSubscription = menuService.falconPrepaid$.subscribe(bool => this.falconPrepaid = bool);
    this.dateRangePickerOptions.settings = {
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(3, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
      }
    };
  }

  ngOnInit() {
    /**
     * Fetch profile data on load
     */
    this.apiService.get(GET_PROFILE_INFO)
      .subscribe(result => {
          if (result['status'] === 'success') {
            if (!result['data']['timezone_value']) {
              this.router.navigate(['/getting-started/welcome']);
            }
          }
        }
      );
    this.fetchGetStartSteps();
    this.fetchPieChartData();
    this.fetchWarmUpDetails();
    this.fetchGraphData(this.mainGraphInput);
    this.checkUnlimitedCodeApplied();
    this.checkPepipostClient();
    this.getPlanType();
    this.getSuppresionCount();
    this.checkSubuser();
    this.setGraphSize();
    this.checkFalconPrepaidClient();
    this.fetchNewSlabCredits();
    this.fetchWithoutActivatUserDetails();

    // Object.assign(this, {realtimeData});
    // this.intervalId = setInterval(() => {
    //   this.realtimeData = [...this.addRandomValue()];
    //  console.log(new Date + JSON.stringify(realtimeData));
    // }, 2000);

    let cid = '';
    this.uuid = this.generateUUID();

    this.apiService.get(GET_CLIENT_ID).subscribe((result) => {
      if (result['status'] === 'success') {
        cid = result['data'];
        const new_key = this.uuid + '_' + cid;
        this.socket.emit('polling', new_key);
        // console.log('realtime data is : ', JSON.stringify(this.realtimeData));
        Object.assign(this, [this.realtimeData]);
        this.socket.on(this.uuid, function (data) {
          Object.assign(this, [this.realtimeData]);
          this.realtimeData = [...this.updateRealTimeGraph(data)];
        }.bind(this));


      } else {
        console.log('Fetch clientid failed : ' + ( (result['error_info']['message'] ? result['error_info']['message'] : '') ? (result['error_info']['message'] ? result['error_info']['message'] : '') : ''));
      }
    });
  }

  ngOnChanges() {
    this.onResize();
  }

  fetchWithoutActivatUserDetails() {
    this.apiService.get(CHECK_ACCOUNT_STATUS)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.accountStatus = result['data']['status'];
          }
        }
      );
  }

  checkFalconPrepaidClient() {
    if (this.falconPrepaid === true) {
      // this.hideBillingMenu = true;
      // this.hideCreditManagement1 = true;
      this.falconPrepaidClient = true;
    }
  }

  updateRealTimeGraph(data) {

    this.realtimeData[0].series.push({'name': new Date, 'value': data.differ});

    if (this.realtimeData[0].series.length > 60) {
      this.realtimeData[0].series.splice(0, 1);
    }

    this.realtime_delivered_percent = this.calculate_percent(data.total_mails, data.sent) + '%';
    this.realtime_bounced_percent = this.calculate_percent(data.total_mails, data.bounce) + '%';
    this.realtime_clicks_percent = this.calculate_percent(data.total_mails, data.clicks) + '%';
    this.realtime_open_percent = this.calculate_percent(data.total_mails, data.open) + '%';
    // newly added in 3.0
    this.realtime_dropped_percent = this.calculate_percent(data.total_mails, data.dropped) + '%';
    this.realtime_spam_percent = this.calculate_percent(data.total_mails, data.spam) + '%';
    this.realtime_unsub_percent = this.calculate_percent(data.total_mails, data.unsub) + '%';

    this.realtime_request_count = data.total_mails;

    this.realtime_delivered_count = data.sent;
    this.realtime_bounced_count = data.bounce;
    this.realtime_clicks_count = data.clicks;
    this.realtime_open_count = data.open;
    // newly added in 3.0
    this.realtime_dropped_count = data.dropped;
    this.realtime_spam_count = data.spam;
    this.realtime_unsub_count = data.unsub;

    return this.realtimeData;
  }

  generateUUID() {
    let d = new Date().getTime();
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  calculate_percent(total, value) {
    if (total > 0 && value > 0) {
      return (Math.round((value / total) * 100) / 100);
    } else {
      return 0;
    }

  }

  applyDate(e: any) {
    this.eventLog += '\nEvent Fired: ' + e.event.type;
    // console.log('\nEvent Fired: ', e.event);
  }

  ngAfterViewInit(): void {
    this.setGraphSize();
  }

  ngOnDestroy(): void {
    this.socket.emit('leave', this.uuid);
    clearInterval(this.resizeInterval);
  }

  cacheGraphElements() {
    this.graphElements = this.flexContent.nativeElement.querySelectorAll('.chart-container');
  }

  fetchGraphData(historyDateRange) {
    const options = {headers: {'Content-Type': 'application/json'}};
    this.apiService
      .post(DASHBOARD_HISTORY_GRAPH, JSON.stringify(historyDateRange), options)
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.setGraphSize();
          this.historyData = result['data']['data'];
          // to show hide of no data fond window
          this.nodataFlag = +result['data']['total_history_counts'] > 0;
          this.tableFlag = !this.nodataFlag;

          this.updateHistoryLabels(result['data']['count']);
        } else {
          // console.log('History graph loading failed : ' +  ( (result['error_info']['message'] ? result['error_info']['message'] : '') ?  (result['error_info']['message'] ? result['error_info']['message'] : '') : ''));
        }
      });
  }

  updateHistoryLabels(result) {
    this.history_delivered_percent = result.Delivered_pr + '%';
    this.history_bounced_percent = result.Bounced_pr + '%';
    this.history_clicks_percent = result.Clicks_pr + '%';
    this.history_unsubscribe_percent = result.Unsub_pr + '%';
    this.history_abuse_percent = result.Spams_pr + '%';
    this.history_open_percent = result.Opens_pr + '%';
    this.history_dropped_percent = result.Dropped_pr + '%';
    this.history_request_count = result.Requests;

    this.history_delivered_count = result.Delivered;
    this.history_bounced_count = result.Bounces;
    this.history_clicks_count = result.Clicked;
    this.history_unsubscribe_count = result.Unsubscribes;
    this.history_abuse_count = result['Spams Reports'];
    this.history_open_count = result.Opened;
    this.history_dropped_count = result.Dropped;
  }

  selectedDate(value: any, dateInput: any) {
    this.dateInput.start = value.start;
    this.dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = {
        'start_date': moment(this.dateInput.start).format('YYYY-MM-DD'),
        'end_date': moment(this.dateInput.end).format('YYYY-MM-DD')
      };
      this.fetchGraphData(this.selectedDateRange);
    }
  }

  // Goes to Get Started current step
  onStepClick(evt) {
    this.getStartedService.goToStep(evt);
  }

  // gets data for get started widget in sidebar
  fetchGetStartSteps() {
    this.getStartedService.getSteps().subscribe(result => {
      this.stepList = result['stepList'] || [];
      this.stepCount = result['stepCount'] || 0;
      this.showResendEmail = this.stepCount === 0 ? false : true;
    });
  }

  // gets details for sidebar pie graph
  fetchPieChartData() {
    this.fetchCredits();
    this.fetchTotalSaving();
  }

  // gets credit for pie chart graph
  fetchCredits() {
    this.apiService.post(GET_EMAIL_CREDITS)
      .subscribe(result => {
        if (result['status'] && result['status'].toLowerCase() === 'success') {
          this.unusedCredit = result['data']['credits_available'];
          this.usedCredit = result['data']['total_available'] - this.unusedCredit;
          this.showPieTopText = result['data']['status'];
          this.showEmailCredits = abbreviateNumber(result['data']['email_credits']);
          this.pieCredits = [+this.unusedCredit, +this.usedCredit];
        }
      });
  }


  fetchNewSlabCredits() {
    this.apiService.post(GET_NEW_SLAB_EMAIL_CREDITS)
      .subscribe(result => {
        if (result['status'] && result['status'].toLowerCase() === 'success') {
          // this.unusedCredit = result['data']['credits_available'];
          // this.usedCredit = result['data']['total_available'] - this.unusedCredit;
          // this.showPieTopText = result['data']['status'];
          // this.showEmailCredits = abbreviateNumber(result['data']['email_credits']);

          this.newTotalSaving = result['data']['earned_open_emails'];

          this.newApproxValue = result['data']['open_free_discount'];
          // result['data']['total_email_sent'] = 500000000;

          if (+result['data']['total_email_sent'] > +result['data']['allocated_emails']) {
            this.pieCreditsNew = [0, +result['data']['total_email_sent']];
          } else {
            this.usedEmails = result['data']['allocated_emails'] - result['data']['total_email_sent'];
            this.pieCreditsNew = [+this.usedEmails, +result['data']['total_email_sent']];
            // this.pieCreditsNew = [+result['data']['allocated_emails'], +result['data']['total_email_sent']];
          }


          if (result['data']['base_plan'] === 2) {
            this.showPieNew = true;
          } else {
            this.showPieNew = false;
          }

        }
      });
  }


  // gets total credit for pie chart text
  fetchTotalSaving() {
    this.apiService.post(GET_AVAILABLE_EMAIL_CREDITS)
      .subscribe(result => {
        if (result['status'] && result['status'].toLowerCase() === 'success') {
          this.totalSaving = +result['data']['opens_available'];
          // this.totalSaving = 4000;
          this.approxValue = Number((DEFAULT_EMAIL_PRICE * this.totalSaving) / DEFAULT_MAIL_COUNT).toFixed(2);
        }
      });
  }

  // redirects to recharge page of billing
  addCredit() {
    this.router.navigate(['/billings/details/recharge']);
  }

  // redirects to recharge page of billing
  managePlan() {
    this.router.navigate(['/billings/postpaid-billing/payment']);
  }

  // fetch warm up initial details to be displayed on the lock
  fetchWarmUpDetails() {
    this.apiService.post(WARMUPSLAB_DETAILS)
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.activeWarmUpDetailsLength = Object.keys(result['data']['clientWarmupSlab']).length;
          if (this.activeWarmUpDetailsLength > 0) {
            this.warmupFlag = false;
            this.activeWarmUpDetails = result['data']['clientWarmupSlab'];
            this.warmUpData = result['data']['warmupSlabs'];
            this.warmUpDataLength = Object.keys(this.warmUpData).length;
            if (this.warmUpDataLength >= 3) {
              let position = 0;
              if (this.activeWarmUpDetailsLength > 0) {
                position = this.warmUpData.findIndex(el => el.slab_id === this.activeWarmUpDetails.slab_id) || 0;
              }
              if (position === 0) {
                this.warmUpSidebarData = [this.warmUpData[0], this.warmUpData[1], this.warmUpData[2]];
              } else if (position === this.warmUpDataLength - 1) {
                this.warmUpSidebarData = [this.warmUpData[position - 2],
                  this.warmUpData[position - 1],
                  this.warmUpData[position]];
              } else {
                this.warmUpSidebarData = [this.warmUpData[position - 1], this.warmUpData[position], this.warmUpData[position + 1]];
              }
            } else {
              this.warmUpSidebarData = this.warmUpData;
            }
          } else {
            this.warmupFlag = true;
          }
        } else {
          this.warmUpErrorType = 'danger';
          this.warmUpErrorMsg = (result['error_info']['message'] ? result['error_info']['message'] : '');
          this.warmupFlag = false;
        }
      });
  }

  // redirects to warm up to scale the warm up
  redirectToWarmUp() {
    this.router.navigate(['/settings/warmup']);
    return false;
  }

  // redirects to upgrade account static page
  redirectToUpgradeAccount() {
    this.router.navigate(['/getting-started/upgrade-account']);
  }

  onTabChange() {
    this.setGraphSize();
  }

  setGraphSize() {
    // check if graph containers are available
    if (!this.graphElements || (this.graphElements && this.graphElements.length === 0)) {
      // try to cache graph elements
      this.cacheGraphElements();

      // if graph elements are still not available, try to get after some time
      if (!this.graphElements || (this.graphElements && this.graphElements.length === 0)) {
        this.onResize();
        return;
      }
    }

    setTimeout(() => {
      // hide all graphs for getting correct width
      this.graphElements.forEach(element => {
        element.classList.add('graph-fix');
      });
      // 25px padding * 2 sides
      this.view = [this.flexContent.nativeElement.offsetWidth - (25 * 2), 280];
      // after getting correct width show all graphs
      this.graphElements.forEach(element => {
        element.classList.remove('graph-fix');
      });
    }, 100);
  }

  onResize() {
    // calling after 300 ms instead of every ms
    clearInterval(this.resizeInterval);
    this.resizeInterval = setInterval(() => {
      this.setGraphSize();
      clearInterval(this.resizeInterval);
    }, 300);
  }

  checkUnlimitedCodeApplied() {
    // this.unlimitedCodeApplied = true;
    this.apiService.post(CHECK_UNLIMITED_CODE_APPLIED, '')
      .subscribe(result => {
          if (result['status'] && result['status'].toLowerCase() === 'success') {
            if (result['data']['unlimited_plan'] === 1) {
              this.unlimitedCodeApplied = true;
            } else {
              this.unlimitedCodeApplied = false;
            }
          } else {
            this.type = 'danger';
            this.msg = (result['error_info']['message'] ? result['error_info']['message'] : '');
          }
        }
      );
  }

  checkPepipostClient() {
    this.apiService.post(CHECK_PEPI_CLIENT, '')
      .subscribe(result => {
          if (result['status'] && result['status'].toLowerCase() === 'success') {
            // console.log('get plan type');
            // console.log(result['data']);
            if (result['data'] !== 'null') {
              // this.primaryCard = result['data']['default_source'];
              // console.log('Client martech_client ==>' + result['data']['martech_client']);
              if (result['data']['martech_client'] === 'pepipost') {
                this.pepipostClient = true;
                // this.specialPrice = true;
                // this.getPostPaidDetails();
              } else {
                this.pepipostClient = false;
              }
            }
          } else {
            this.type = 'danger';
            this.msg = (result['error_info']['message'] ? result['error_info']['message'] : '');
          }
        }
      );
  }

  getPlanType() {
    this.apiService.post(GET_PLAN_TYPE, '')
      .subscribe(result => {
          if (result['status'] && result['status'].toLowerCase() === 'success') {
            if (result['data'] !== 'null') {
              if (result['data']['client_type'] === 'postpaid') {
                this.postPaidClient = true;
              } else {
                this.postPaidClient = false;
              }

              if (result['data']['paidclient'] === 1) {
                this.paidClient = true;
              } else {
                this.paidClient = false;
              }

            }
          } else {
            this.type = 'danger';
            this.msg = (result['error_info']['message'] ? result['error_info']['message'] : '');
          }
        }
      );
  }

  getSuppresionCount() {
    this.apiService.get(GET_SUPPRESSION_EMAIL_ADDRESS)
      .subscribe(result => {
        if (result['status'] && result['status'].toLowerCase() === 'success') {
          this.totalRecords = result['data']['suppressionEmail']['data'].length;
        }
      });
  }

  redirectToSuppression() {
    this.router.navigate(['/suppression']);
  }

  checkSubuser() {
    this.apiService.post(CHECK_SUBUSER, '')
      .subscribe(result => {
          if (String(result['status']).toLowerCase() === 'success') {
            // console.log('Unlimited code applied');
            // console.log(result['data']['unlimited_plan']);
            // console.log(result['data']['approval']);
            if (result['data']['sub_user'] > 0) {
              // console.log('Unlimited code applied');
              this.hideAddCredits = true;
            } else {
              this.hideAddCredits = false;
            }
            // Conditions for subaccount
            if (result['data']['subaccount']) {
              this.subaccount = true;
            } else {
              this.subaccount = false;
            }
          } else {
            this.type = 'danger';
            this.msg = (result['error_info']['message'] ? result['error_info']['message'] : '');
          }
        }
      );
  }

}
