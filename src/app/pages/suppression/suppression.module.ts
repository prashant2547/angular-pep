import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SuppressionRoutingModule} from './suppression-routing.module';
import {SuppressionComponent} from './suppression.component';
import {ComponentsModule} from '../components/components.module';
import {Ng2SmartTableModule} from '../components/ng2-smart-table';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Daterangepicker} from 'ng2-daterangepicker';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SuppressionRoutingModule,
    ComponentsModule,
    Ng2SmartTableModule,
    Daterangepicker,
    NgbModule.forRoot(),
  ],
  declarations: [SuppressionComponent]
})
export class SuppressionModule {
}
