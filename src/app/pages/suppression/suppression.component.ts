import {Component, OnInit, ViewChild} from '@angular/core';
import {TableDataSource} from '../services/table.data-source';
import {environment} from '../../../environments/environment';
import {
  DOWNLOAD_LIVE_REPORT, DOWNLOAD_SUPPRESSION_DOMAIN, DOWNLOAD_SUPPRESSION_EMAIL,
  GET_SUPPRESSION_DOMAIN, GET_SUPPRESSION_EMAIL_ADDRESS, MAKE_BLACKLIST_DOMAIN, MAKE_BLACKLIST_EMAIL, MAKE_WHITELIST_DOMAIN,
  MAKE_WHITELIST_EMAIL, NO_DATA_FOUND,
  WARNING_MSG
} from '../../config';
import {Http} from '@angular/http';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../services/api.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomNullValidation, DOMAIN, EMAIL, FileValidation, MULTI_EMAIL} from '../components/forms/form-validation';
import {count} from '@swimlane/ngx-charts';
import {DaterangePickerComponent, DaterangepickerConfig} from 'ng2-daterangepicker';
// import moment = require('moment');
import * as moment from 'moment';

@Component({
  selector: 'app-suppression',
  templateUrl: './suppression.component.html',
  styleUrls: ['./suppression.component.scss']
})
export class SuppressionComponent implements OnInit {
  tempDownloadReport = [];
  filterValues = [];
  dataSourceLivefeed: any;
  ischecked = true;
  emailSettings: any;
  domainSettings: any;
  emailDataSource: any;
  domainDataSource: any;
  selectedPages: number;
  emailWhitelistMsg: string;
  emailWhitelistType: string;
  domainWhitelistMsg: string;
  domainWhitelistType: string;
  email_search: any;
  domain_search: any;
  apiUrl = '';
  parameters: any;
  showStatus = false;
  suppressionType: string;
  suppressionMsg: string;
  dispDefaultSuppressionEmail = true;
  dispDefaultSuppressionEmail_hide = true;
  dispDefaultSuppression = false;
  dispSuppressionTable = true;
  dispSuppressionOperation = true;
  dispDefaultSuppressionDomain = true;
  dispDefaultSuppressionDomain_hide = true;

  @ViewChild('tabset') tabset;
  @ViewChild('tabsetOperation') tabsetOperation;
  @ViewChild('bulkEmailFile1') bulkEmailFile;
  @ViewChild('bulkDomainFile1') bulkDomainFile;
  tabSelectionId = '';

  suppressionStatus = 'blacklist';

  addRemoveEmailSuppressionForm: FormGroup;
  addRemoveDomainSuppressionForm: FormGroup;
  addRemoveEmailFileFrm: FormGroup;
  addRemoveDomainFileFrm: FormGroup;
  addRemoveEmailRadioFrm: FormGroup;
  addRemoveDomainRadioFrm: FormGroup;
  addRemoveEmailDomainSuppFileForm: FileValidation;

  file: File;
  filename = '';
  fileMsg = '';
  // for the custom required field validation
  customValidation: CustomNullValidation;
  customerror = '';
  // for file validation includind size and type of file
  fileValidation: FileValidation;

  csvFileErrorList: string[] = [];
  size: any; // size of uploaded file
  sizeData: any; // size parameter value going to be sent to api
  csvRecordsArray: any; // csv file record array
  blacklist_email: any; // parameter of email sent to be api
  whitelist_email: any; // parameter of email sent to be api
  blacklist_domain: any; // parameter of domain sent to be api
  whitelist_domain: any; // parameter of domain sent to be api

  totalRecords = 0;
  pagesOptions = [10, 20, 30, 40];
  confirmPopRef: NgbActiveModal;
  blacklistemail: string;
  blacklistdomain: string;


  mainInput = {
    start: moment().subtract(1, 'month'),
    end: moment()
  };

  selectedDateRange = [moment(this.mainInput.start).format('YYYY-MM-DD'),
    moment(this.mainInput.end).format('YYYY-MM-DD')];

  saveData = (function () {
    const a = document.createElement('a');
    document.body.appendChild(a);
    return function (data, fileName) {
      // let json = JSON.stringify(data), blob = new Blob([json], {type: "octet/stream"}),
      const blob = new Blob([data], {type: 'octet/stream'});
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    };
  }());

  @ViewChild('removeEmail') removeEmail;
  @ViewChild('removeDomain') removeDomain;
  @ViewChild(DaterangePickerComponent) picker: DaterangePickerComponent;

  constructor(private http: Http,
              private modalService: NgbModal,
              private daterangepickerOptions: DaterangepickerConfig,
              private apiService: ApiService) {
    this.daterangepickerOptions.settings = {
      locale: {
        format: 'YYYY-MM-DD'
      },
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [
          moment().subtract(1, 'month'),
          moment()
        ],
        'Last 3 Months': [
          moment().subtract(3, 'month'),
          moment()
        ],
        'Last 6 Months': [
          moment().subtract(6, 'month'),
          moment()
        ],
        'Last 12 Months': [
          moment().subtract(12, 'month'),
          moment()
        ]
      }
    };
  }

  ngOnInit() {
    console.log(this.ischecked);
    this.fetchEmailDetails();
    this.fetchDomainDetails();
    // file validation
    this.customValidation = new CustomNullValidation();
    this.fileValidation = new FileValidation(1024 * 512, ['csv']);
    this.customerror = this.customValidation.required(this.filename);
    // Add suppression form validation
    this.addRemoveEmailSuppressionForm = new FormGroup({
      emailAddress: new FormControl('', [
        Validators.required,
        Validators.pattern(MULTI_EMAIL)
      ]),

    });
    this.addRemoveDomainSuppressionForm = new FormGroup({
      domain: new FormControl('', [
        Validators.required,
        Validators.pattern(DOMAIN)
      ]),
    });

    this.addRemoveEmailFileFrm = new FormGroup({
      bulkEmailFile: new FormControl('', [
        Validators.required,
      ])
    });
    this.addRemoveDomainFileFrm = new FormGroup({
      bulkDomainFile: new FormControl('', [
        Validators.required,
      ])
    });
    this.addRemoveEmailRadioFrm = new FormGroup({
      emailStatus: new FormControl('')
    });
    this.addRemoveDomainRadioFrm = new FormGroup({
      domainStatus: new FormControl('')
    });
    // file validation
    this.addRemoveEmailDomainSuppFileForm = new FileValidation(1024 * 512, ['csv']);
  }

  // show hide functionality of table and manage suppression tab
  displaySuppression(back = '') {
    if (back) {
      this.tabset.select(this.tabsetOperation.activeId.replace('Table', 'Operation'));
      this.dispDefaultSuppression = true;
      this.dispSuppressionOperation = true;
      this.dispSuppressionTable = false;

    } else {
      this.tabset.select(this.tabsetOperation.activeId.replace('Table', 'Operation'));
      this.dispDefaultSuppression = true;
      this.dispSuppressionOperation = false;
      this.dispSuppressionTable = true;
    }
    this.addRemoveEmailSuppressionForm.reset();
    this.addRemoveDomainSuppressionForm.reset();
    this.suppressionMsg = '';
    this.makeFileBlank();

  }

  selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = [
        moment(dateInput.start).format('YYYY-MM-DD'),
        moment(dateInput.end).format('YYYY-MM-DD')
      ];
      // clear existing date ranges
      const tempFilterValues = this.filterValues
        .map((obj) => {
          if (obj.field !== 'start_date' && obj.field !== 'end_date') {
            return obj;
          }
        });
      this.filterValues = tempFilterValues.filter(n => n);
      // Add new date ranges
      this.setEmailFilter(this.email_search);
    }
  }

  selectedDomainDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = [
        moment(dateInput.start).format('YYYY-MM-DD'),
        moment(dateInput.end).format('YYYY-MM-DD')
      ];
      // clear existing date ranges
      const tempFilterValues = this.filterValues
        .map((obj) => {
          if (obj.field !== 'start_date' && obj.field !== 'end_date') {
            return obj;
          }
        });
      this.filterValues = tempFilterValues.filter(n => n);
      // Add new date ranges
      this.setDomainFilter(this.domain_search);
    }
  }


  downloadEmailSupp() {
    // const options = {   headers: {'Content-Type' : 'text/plain'},
    // responseType: 'text/plain',   withCredentials: false };
    this.tempDownloadReport = [];
    this.filterValues
      .push({field: 'report', search: 1});

    for (let index = 0; index < this.filterValues.length; index++) {
      const element = this.filterValues[index];
      const temp = {};
      temp[element.field] = element.search;
      this.tempDownloadReport.push(temp);
    }

    // console.log('here' + JSON.stringify(this.tempDownloadReport));

    this.apiService
      .post(DOWNLOAD_SUPPRESSION_EMAIL, ({'settings': this.tempDownloadReport}))
      .subscribe((result) => {
        // console.log('here ==>');
        this.saveData(result['data']['email_list']['data'], result['data']['email_list']['filename']);
        if (result['status'] === 'success') {
          //  console.log('Download started');
        } else {
          alert('Download failed : ' + result['error_info']['message']);
        }
      });
    return false;
  }


  downloadDomainSupp() {
    // const options = {   headers: {'Content-Type' : 'text/plain'},
    // responseType: 'text/plain',   withCredentials: false };
    this.tempDownloadReport = [];
    this.filterValues
      .push({field: 'report', search: 1});

    for (let index = 0; index < this.filterValues.length; index++) {
      const element = this.filterValues[index];
      const temp = {};
      temp[element.field] = element.search;
      this.tempDownloadReport.push(temp);
    }

    // console.log('here' + JSON.stringify(this.tempDownloadReport));

    this.apiService
      .post(DOWNLOAD_SUPPRESSION_DOMAIN, ({'settings': this.tempDownloadReport}))
      .subscribe((result) => {
        // console.log('here ==>');
        this.saveData(result['data']['domain_list']['data'], result['data']['domain_list']['filename']);
        if (result['status'] === 'success') {
          //  console.log('Download started');
        } else {
          alert('Download failed : ' + result['error_info']['message']);
        }
      });
    return false;
  }

// fetch email details to be display in the table
  fetchEmailDetails() {

    this.emailSettings = {
      attr: {
        class: 'table'
      },
      delete: {
        deleteButtonContent: '<a class="text-link-dark"><i class="icon-trash"></i> Whitelist it</a>',
        confirmDelete: true
      },
      columns: {
        email: {
          title: 'Email',
          sort: false
        },
        date: {
          title: 'Added On',
          sort: false
        },
        status: {
          title: 'Status',
          sort: false
        },
      }, actions: {
        add: false,
        edit: false,
        delete: true,
        position: 'right',
        columnTitle: 'Remove from Suppression'
      },
      hideSubHeader: true,
      noDataMessage: NO_DATA_FOUND,
      pager: {
        perPage: 10
      },
    };
    this.emailDataSource = new TableDataSource(this.http,
      {
        endPoint: `${environment.api_url}${GET_SUPPRESSION_EMAIL_ADDRESS}`,
        dataKey: 'suppressionEmail'
      });
    this.setEmailFilter();

    this.emailDataSource.onDataLoaded.subscribe((res) => {
      this.totalRecords = res.length;
    });

    this.emailDataSource.onGetData.subscribe((res) => {
      console.log('Total Rows ==>' + res.data.suppressionEmail.total_supp_emails);
      if ( +res.data.suppressionEmail.total_supp_emails === 0) {
        console.log('here');
        this.dispDefaultSuppressionEmail = false;
        this.dispDefaultSuppressionEmail_hide = true;
      } else {
        this.dispDefaultSuppressionEmail_hide = false;
        this.dispDefaultSuppressionEmail = true;
      }
    });

    // Flag , which will help to display oops window at initial if data is not present
    // this.totalRecords > 0 ? this.dispSuppressionTable = true : this.dispSuppressionTable = false;
    // this.totalRecords > 0 ? this.dispDefaultSuppression = false : this.dispDefaultSuppression = true;
    this.dispSuppressionTable = false;
    this.dispDefaultSuppression = true;

    this.selectedPages = this.pagesOptions[0];
  }

// fetch domain details to be display in the table
  fetchDomainDetails() {
    this.domainSettings = {
      attr: {
        class: 'table'
      },
      delete: {
        deleteButtonContent: '<a class="text-link-dark"><i class="icon-trash"></i> Whitelist it</a>',
        confirmDelete: true
      },
      columns: {
        domain: {
          title: 'Domain',
          sort: false
        },
        date: {
          title: 'Added On',
          sort: false
        },
        status: {
          title: 'Status',
          sort: false
        },
      }, actions: {
        add: false,
        edit: false,
        delete: true,
        position: 'right',
        columnTitle: 'Remove from Suppression'
      },
      hideSubHeader: true,
      noDataMessage: NO_DATA_FOUND,
      pager: {
        perPage: 10
      }
    };
    this.domainDataSource = new TableDataSource(this.http,
      {
        endPoint: `${environment.api_url}${GET_SUPPRESSION_DOMAIN}`,
        dataKey: 'suppressionDomain'
      });
    this.setDomainFilter();
    this.selectedPages = this.pagesOptions[0];

    this.domainDataSource.onGetData.subscribe((res) => {
      console.log('Total Rows ==>' + res.data.suppressionDomain.total_supp_domains);
      if (+res.data.suppressionDomain.total_supp_domains === 0) {
        console.log('here');
        this.dispDefaultSuppressionDomain = false;
        this.dispDefaultSuppressionDomain_hide = true;
      } else {
        this.dispDefaultSuppressionDomain = true;
        this.dispDefaultSuppressionDomain_hide = false;
      }
    });


  }

  onChangeEmailDetails(evt) {
    this.emailDataSource.setPaging(this.emailDataSource.pagingConf.page, +evt, true);
  }

  onChangeDomainDetails(evt) {
    this.domainDataSource.setPaging(this.domainDataSource.pagingConf.page, +evt, true);
  }

  // search box , search by domain
  onDomainSearch(query: string = '') {
    this.domain_search = query;
    this.setDomainFilter(query);
  }

// search box , search by email
  onEmailSearch(query) {
    this.email_search = query;
    this.setEmailFilter(query);
  }

  /**
   * to open popup window to ask whether to whitelist the selected email
   * @param e
   */
  removeBlacklistEmail(e) {
    this.emailWhitelistType = this.emailWhitelistMsg = '';
    this.blacklistemail = e.data['email'];
    this.open(this.removeEmail);
  }

  /**
   * make email whitelist
   * @param email
   */
  makeWhitelistEmail(email) {
    this.apiService.post(MAKE_WHITELIST_EMAIL, ({'whitelist_email': email, 'size': 'single'}))
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.emailWhitelistMsg = result['message'];
          this.emailWhitelistType = 'info';
          this.fetchEmailDetails();
        } else {
          this.emailWhitelistMsg = result['error_info']['message'];
          this.emailWhitelistType = 'danger';
        }
      }, error => {
        this.emailWhitelistMsg = WARNING_MSG;
        this.emailWhitelistType = 'warning';
      });
  }

  /**
   * to open popup window to ask whether to whitelist the selected email
   * @param e
   */
  removeBlacklistDomain(e) {
    this.domainWhitelistType = this.domainWhitelistMsg = '';
    this.blacklistdomain = e.data['domain'];
    this.open(this.removeDomain);
  }

  /**
   * make domain whitelist
   * @param email
   */
  makeWhitelistDomain(domain) {
    this.apiService.post(MAKE_WHITELIST_DOMAIN, ({'whitelist_domain': domain, 'size': 'single'}))
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.domainWhitelistMsg = result['message'];
          this.domainWhitelistType = 'info';
          this.fetchDomainDetails();
        } else {
          this.domainWhitelistMsg = result['error_info']['message'];
          this.domainWhitelistType = 'danger';
        }
      }, error => {
        this.domainWhitelistMsg = WARNING_MSG;
        this.domainWhitelistType = 'warning';
      });
  }


  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.confirmPopRef = this.modalService.open(content);
  }

// This function will store the status of suppression that is either blacklist or whitelist
  storeSuppressionStatus(e) {
    this.suppressionStatus = e.target.value;
    this.suppressionStatus.toLowerCase() === 'blacklist' ? this.showStatus = false : this.showStatus = true;
  }

  /**
   * this function will either blacklist or whitelist the given email or domain.
   * based on its type the respective API get called
   * For example for email : email API and for domain: domain API
   * @param formData
   * @param suppressionType
   * @param bulkUpload
   */
  addSuppression(formData, suppressionType, bulkUpload = false) {
    this.suppressionStatus = this.suppressionStatus !== '' ? this.suppressionStatus : formData.emailStatus;
    // for email part

    if (suppressionType.toLowerCase() === 'email') {
      this.tabSelectionId = 'emailSuppressionTable';
      if (this.suppressionStatus.toLowerCase() === 'blacklist') { // email blacklist
        this.apiUrl = MAKE_BLACKLIST_EMAIL;
        if (bulkUpload) {
          this.blacklist_email = this.csvRecordsArray;
          this.sizeData = this.size;
        } else {
          this.blacklist_email = formData.emailAddress;
          this.sizeData = 'single';
        }
        this.parameters = ({'blacklist_email': this.blacklist_email, 'size': this.sizeData});
      } else { // email whitelist
        this.apiUrl = MAKE_WHITELIST_EMAIL;
        if (bulkUpload) {
          this.whitelist_email = this.csvRecordsArray;
          this.sizeData = this.size;
        } else {
          this.whitelist_email = formData.emailAddress;
          this.sizeData = 'single';
        }
        this.parameters = ({'whitelist_email': this.whitelist_email, 'size': this.sizeData});
      }
    } else {
      // domain part
      this.tabSelectionId = 'domainSuppressionTable';
      if (this.suppressionStatus.toLowerCase() === 'blacklist') { // domain blacklist
        this.apiUrl = MAKE_BLACKLIST_DOMAIN;
        if (bulkUpload) {
          this.blacklist_domain = this.csvRecordsArray;
          this.sizeData = this.size;
        } else {
          this.blacklist_domain = formData.domain;
          this.sizeData = 'single';
        }
        this.parameters = ({'blacklist_domain': this.blacklist_domain, 'size': this.sizeData});

      } else { // domain whitelist
        this.apiUrl = MAKE_WHITELIST_DOMAIN;
        if (bulkUpload) {
          this.whitelist_domain = this.csvRecordsArray;
          this.sizeData = this.size;
        } else {
          this.whitelist_domain = formData.domain;
          this.sizeData = 'single';
        }
        this.parameters = ({'whitelist_domain': this.whitelist_domain, 'size': this.sizeData});
      }

    }
    this.apiService.post(this.apiUrl, this.parameters)
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.suppressionType = 'info';
          this.suppressionMsg = result['message'];
          // show hide of adding suppression window and displaying table
          this.tabsetOperation.select(this.tabSelectionId);
          this.fetchEmailDetails();
          this.fetchDomainDetails();
          this.dispSuppressionOperation = true;
          this.dispSuppressionTable = false;
          this.dispDefaultSuppression = true;
          this.addRemoveEmailSuppressionForm.reset();
          this.addRemoveDomainSuppressionForm.reset();
          this.suppressionMsg = '';
        } else {
          this.suppressionType = 'danger';
          this.suppressionMsg = result['message'];
        }
      }, error => {
        this.suppressionType = 'warning';
        this.suppressionMsg = WARNING_MSG;
      });
  }

  /**
   * On upload file , on change event will accept the event and will store the file object data.
   * @param event
   */
  onFileChange(event) {
    if (event.target.files && event.target.files.length > 0) {
      this.file = event.target.files[0];
      this.filename = this.file.name;
      this.csvFileErrorList = this.addRemoveEmailDomainSuppFileForm.validate(this.file);
      this.fileMsg = this.csvFileErrorList.join('<br>');
      if (this.fileMsg === '' && this.csvFileErrorList.length === 0) {
        const reader: FileReader = new FileReader();
        reader.readAsText(this.file);
        reader.onload = (e) => {
          const csv: string = reader.result;
          this.csvRecordsArray = csv.split(/\r\n|\n/);
          this.size = this.csvRecordsArray.length;
        };
      }
      this.customerror = '';
    } else {
      this.customerror = this.customValidation.required(this.filename);
    }
  }

  changeRadioVal() {
    console.log('sadfsadgasdfg');
    this.suppressionStatus = 'blacklist';
    this.makeFileBlank();
  }

// to make file object blank on click on 'Back' button
  makeFileBlank() {
    this.filename = '';
    this.customerror = 'error';
    if (this.bulkEmailFile) {
      this.bulkEmailFile.nativeElement.value = '';
    }
    if (this.bulkDomainFile) {
      this.bulkDomainFile.nativeElement.value = '';
    }
  }

  resetSearch() {
    this.mainInput = {
      start: moment().subtract(1, 'month'),
      end: moment()
    };

    this.selectedDateRange = [moment(this.mainInput.start).format('YYYY-MM-DD'),
      moment(this.mainInput.end).format('YYYY-MM-DD')];
    this.setEmailFilter();
    this.setDomainFilter();
  }

  // email rest filter
  setEmailFilter(query = '') {
    this.filterValues = [];
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.filterValues.push({field: 'email', search: query});
    this.emailDataSource.setFilter(this.filterValues, false);
  }

// domain rest filter
  setDomainFilter(query = '') {
    this.filterValues = [];
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.filterValues.push({field: 'domain', search: query});
    this.domainDataSource.setFilter(this.filterValues, false);
  }
}
