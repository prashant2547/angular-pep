import {Http} from '@angular/http';
import {RequestOptionsArgs} from '@angular/http/src/interfaces';
import {URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';
import {LocalDataSource} from '../components/ng2-smart-table';
import {getDeepFromObject} from '../components/ng2-smart-table/lib/helpers';
import {ServerSourceConf} from '../components/ng2-smart-table/lib/data-source/server/server-source.conf';
import {EventEmitter} from '@angular/core';
import {resToJson} from '../components/utils/json-utils';

/**
 * Copy of Server Data Source Class file from ng2-smart-table
 */
export class TableDataSource extends LocalDataSource {

  protected conf: ServerSourceConf;

  protected lastRequestCount = 0;

  // gets only rows array
  public onDataLoaded = new EventEmitter<Array<any>>();
  // returns whole api data
  public onGetData = new EventEmitter<Array<any>>();
  public getTotalRecords = new EventEmitter;

  constructor(protected http: Http, conf: ServerSourceConf | {} = {}) {
    super();

    this.conf = new ServerSourceConf(conf);

    if (!this.conf.endPoint) {
      throw new Error('At least endPoint must be specified as a configuration of the server data source.');
    }
  }

  count(): number {
    return this.lastRequestCount;
  }

  getElements(): Promise<any> {
    return this.requestElements().map(res => {
      this.lastRequestCount = this.extractTotalFromResponse(res);
      this.data = this.extractDataFromResponse(res);
      this.onDataLoaded.emit(this.data);
      this.onGetData.emit(res.json());
      this.getTotalRecords.emit(this.lastRequestCount);
      return this.data;
    }).toPromise();
  }

  /**
   *
   * Array of conf objects
   * [
   *  {field: string, search: string, filter: Function|null},
   * ]
   * @param conf
   * @param andOperator
   * @param doEmit
   * @returns {LocalDataSource}
   */
  setFilter(conf: Array<any>, andOperator = true, doEmit = true): LocalDataSource {
    // Fix issue where array didn't get blank on adding new data
    // reset filter array first
    this.filterConf = {
      filters: [],
      andOperator: true,
    };
    super.setFilter(conf, andOperator, doEmit);
    return this;
  }

  /**
   * Extracts array of data from server response
   * @param res
   * @returns {any}
   */
  protected extractDataFromResponse(res: any): Array<any> {
    const jsonData = resToJson(res);
    if (!jsonData['data'][this.conf.dataKey]['data']) {
      return [];
    }
    /*
     commenting following section becasue, it is cheking key inside data which we don't have.
    hence returning direct data
    */
    // console.log('rawData', rawData, rawData instanceof Array);
    // const data = !!this.conf.dataKey ? getDeepFromObject(rawData, this.conf.dataKey, []) : rawData;
    const data = jsonData['data'][this.conf.dataKey]['data'];
    if (data instanceof Array) {
      return data;
    }

    throw new Error(`Data must be an array.
    Please check that data extracted from the server response by the key '${this.conf.dataKey}' exists and is array.`);
  }

  /**
   * Extracts total rows count from the server response
   * Looks for the count in the heders first, then in the response body
   * @param res
   * @returns {any}
   */
  protected extractTotalFromResponse(res: any): number {
    // setting holder response as 'x-total-count' for Table total records.
    const jsonData = resToJson(res);
    if (jsonData['data'][this.conf.dataKey]['data']) {
      res.headers.set(this.conf.totalKey, jsonData['data'][this.conf.dataKey]['total_rows']);
    }
    if (res.headers.has(this.conf.totalKey)) {
      return +res.headers.get(this.conf.totalKey);
    } else {
      // setting data to specified key.
      let rawData = '';
      if (jsonData['data'][this.conf.dataKey]['data']) {
        rawData = jsonData['data'][this.conf.dataKey]['data'];
      }
      return getDeepFromObject(rawData, this.conf.totalKey, 0);
    }
  }

  protected requestElements(): Observable<any> {
    return this.http.get(this.conf.endPoint, this.createRequestOptions());
  }

  protected createRequestOptions(): RequestOptionsArgs {
    let requestOptions: RequestOptionsArgs = {};
    requestOptions.params = new URLSearchParams();

    requestOptions = this.addTableRequestOptions(requestOptions);
    requestOptions = this.addSortRequestOptions(requestOptions);
    requestOptions = this.addFilterRequestOptions(requestOptions);
    return this.addPagerRequestOptions(requestOptions);
  }

  protected addTableRequestOptions(requestOptions: RequestOptionsArgs): RequestOptionsArgs {
    const tableParams: URLSearchParams = <URLSearchParams>requestOptions.params;
    tableParams.set('table', this.conf.dataKey);
    return requestOptions;
  }

  protected addSortRequestOptions(requestOptions: RequestOptionsArgs): RequestOptionsArgs {
    const searchParams: URLSearchParams = <URLSearchParams>requestOptions.params;

    if (this.sortConf) {
      this.sortConf.forEach((fieldConf) => {
        searchParams.set(this.conf.sortFieldKey, fieldConf.field);
        searchParams.set(this.conf.sortDirKey, fieldConf.direction.toUpperCase());
      });
    }

    return requestOptions;
  }

  protected addFilterRequestOptions(requestOptions: RequestOptionsArgs): RequestOptionsArgs {
    const searchParams: URLSearchParams = <URLSearchParams>requestOptions.params;

    if (this.filterConf.filters) {
      this.filterConf.filters.forEach((fieldConf: any) => {
        if (fieldConf['search']) {
          searchParams.set(this.conf.filterFieldKey.replace('#field#', fieldConf['field']), fieldConf['search']);
        }
      });
    }

    return requestOptions;
  }

  protected addPagerRequestOptions(requestOptions: RequestOptionsArgs): RequestOptionsArgs {
    const searchParams: URLSearchParams = <URLSearchParams>requestOptions.params;

    if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
      searchParams.set(this.conf.pagerPageKey, this.pagingConf['page']);
      searchParams.set(this.conf.pagerLimitKey, this.pagingConf['perPage']);
    }

    return requestOptions;
  }
}
