import * as RollBar from 'rollbar';
import {ErrorHandler, Injectable, InjectionToken, Injector} from '@angular/core';
import {environment} from '../../../environments/environment';

const rollBarConfig = {
  accessToken: '75ce7f9267a8454185eb1d381b63437a',
  captureUncaught: true,
  captureUnhandledRejections: true,
};


@Injectable()
export class GlobalErrorHandler extends ErrorHandler {
  constructor(private injector: Injector) {
    super();
    window.console.log = function () { };
  }

  handleError(err) {
    if (environment.production) {
      // enable rollBar
      // const rollBar = this.injector.get(RollBarService);
      // rollBar.error(err.originalError || err);
    } else {
      super.handleError(err);
    }

  }
}


export function rollBarFactory() {
  // return new RollBar(rollBarConfig);
}

export const RollBarService = new InjectionToken<RollBar>('rollbar');
