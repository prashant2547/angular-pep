import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';

import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AlertsService} from './alerts.service';
import {Router} from '@angular/router';
import {LOG_OUT_URL} from '../../config';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient,
              private alertsService: AlertsService,
              private router: Router) {
  }

  /**
   * This function makes get call to api to given path and search params
   * @param path string
   * @param params URLSearchParams
   * @returns Observable
   */
  get(path: string, params: any = ''): Observable<any> {
    const targetUrl = `${environment.api_url}${path}`;
    return Observable.create(observer => {
      this.http.get(targetUrl, params).subscribe(response => {
          // check if session out
          if (+response['status'] === 0) {
            this.redirectToLogout(LOG_OUT_URL);
          }
          // return data to caller
          observer.next(response);
          observer.complete();
        },
        // TODO : need to check invalid response on top header
        //     res => {
        //   this.alertsService.showP1Alert({
        //     type: 'warning',
        //     value: (res.error_info && res.error_info.message) || 'Invalid response'
        //   });
        //   if (!environment.production) {
        //     console.info(res);
        //   }
        // }
      );
    });
  }

  /**
   * This function makes post call to api to given path and search params
   * @param path string
   * @param params URLSearchParams
   * @param options Header Option
   * @returns Observable
   */
  post(path: string, params: any = '', options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: 'body';
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  }): Observable<any> {
    const targetUrl = `${environment.api_url}${path}`;
    return Observable.create(observer => {
      this.http
        .post(targetUrl, params, options).subscribe(response => {
          // check if session out
          if (+response['status'] === 0) {
            this.redirectToLogout(LOG_OUT_URL);
          }
          // return data to caller
          observer.next(response);
          observer.complete();
        },
        //     res => {
        //   this.alertsService.showP1Alert({
        //     type: 'warning',
        //     value: (res.error_info && res.error_info.message) || 'Invalid response'
        //   });
        //   if (!environment.production) {
        //     console.info(res);
        //   }
        // }
      );
    });
  }

  redirectToLogout(url) {
    // redirect to logout page
    // Fix: https://stackoverflow.com/questions/34338440/how-to-redirect-to-an-external-url-in-angular2/34340883
    this.router.navigate(['/']).then(result => {
      window.location.href = `${environment.api_url}${ LOG_OUT_URL}`;
    });
  }
}
