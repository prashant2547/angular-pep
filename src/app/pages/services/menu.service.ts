import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {CHECK_FALCON_PREPAID, CHECK_SUBUSER, GET_EMAIL_CREDITS} from '../../config';
import {ApiService} from './api.service';

@Injectable()
export class MenuService {
  private _showMenu = new BehaviorSubject<boolean>(true);
  showMenu$ = this._showMenu.asObservable();

  private _falconPrepaid = new BehaviorSubject<boolean>(true);
  falconPrepaid$ = this._falconPrepaid.asObservable();

  private _subUser = new BehaviorSubject<boolean>(true);
  subUser$ = this._subUser.asObservable();

  private _parentUserName = new BehaviorSubject<string>('');
  parentUserName$ = this._parentUserName.asObservable();


  private _credits = new BehaviorSubject<number>(0);
  credits$ = this._credits.asObservable();

  constructor(private apiService: ApiService) {
  }

  hideMenu() {
    this._showMenu.next(false);
  }

  showMenu() {
    this._showMenu.next(true);
  }

  refreshCredits() {
    this.apiService.post(GET_EMAIL_CREDITS)
      .subscribe((result) => {
        if (String(result['status']).toLowerCase() === 'success') {
          this._credits.next(+parseFloat(result['data']['credits_available']).toFixed(0) || 0);
        }
      });

    this.apiService.post(CHECK_FALCON_PREPAID)
      .subscribe((result) => {
        if (String(result['status']).toLowerCase() === 'success') {
          if (result['data']['falcon_prepaid'] === 0) {
            this._falconPrepaid.next(false);
          } else {
            this._falconPrepaid.next(true);
          }
        }
      });

    this.apiService.post(CHECK_SUBUSER, '')
      .subscribe(result => {

          if (String(result['status']).toLowerCase() === 'success') {
            if (result['data']['sub_user'] > 0) {
              // console.log('Unlimited code applied');
              this._subUser.next(true);
              if (result['data']['parent'] !== '' || result['data']['parent'] !== null) {
                this._parentUserName.next(result['data']['parent']);
              } else {
                this._parentUserName.next('');
              }
            } else {
              this._subUser.next(false);
              this._parentUserName.next('');
            }
          }
        }
      );

  }
}
