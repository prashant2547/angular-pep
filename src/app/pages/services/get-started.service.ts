import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {DASHBOARD_GET_STARTED_STEPS} from '../../config';
import {colorSets} from '@swimlane/ngx-charts/release/utils';

@Injectable()
export class GetStartedService {
  // ACTIONS
  readonly ACTIVATE = 'account_activated';
  readonly ADD_DOMAIN = 'domain_added';
  readonly VERIFY = 'domain_verified';
  readonly SEND_EMAIL = 'email_sent';

  private stepList = [{
    action: this.ACTIVATE,
    label: 'Activate your account',
    desc: 'to get verification link , click on \'resend activation link\' in header.',
    active: false
  }, {
    action: this.ADD_DOMAIN,
    label: 'Add your sending domain',
    desc: 'so we’ll whitelabel this domain to send emails.',
    active: false
  }, {
    action: this.VERIFY,
    label: 'Authenticate your sending domain',
    desc: 'to enable Pepipost to send your emails.',
    active: false
  }, {
    action: this.SEND_EMAIL,
    label: 'Send your first email',
    desc: 'Yay! You’re all set to send. Test now.',
    active: false
  }];

  constructor(private apiService: ApiService,
              private route: Router) {
  }

  getSteps(): Observable<any> {
    return Observable.create(obs => {
      this.apiService.get(DASHBOARD_GET_STARTED_STEPS).subscribe(res => {
        // condition
        const stepStatus = res['data'];
        let stepCount = 0;
        // check step status has data
        if (stepStatus) {
          if (stepStatus.account_activated && +stepStatus.account_activated !== 0) {
            stepCount++;
            this.stepList[0].active = true;
          }
          if (stepStatus.domain_added && +stepStatus.domain_added !== 0) {
            stepCount++;
            this.stepList[1].active = true;
          }
          if (stepStatus.domain_verified && +stepStatus.domain_verified !== 0) {
            stepCount++;
            this.stepList[2].active = true;
          }
          if (stepStatus.email_sent && +stepStatus.email_sent !== 0) {
            stepCount++;
            this.stepList[3].active = true;
          }
        } else {
          // hides get started if undefined
          stepCount = 4;
        }

        // this.stepList[1].active = true;
        // this.stepList[3].active = true;
        // stepCount = 2;

        let obj = {stepCount: stepCount, stepList: []};
        if (stepCount === this.stepList.length) {
          obs.next(obj);
          obs.complete();
        } else {
          obj = {stepCount: stepCount, stepList: this.stepList};
          obs.next(obj);
          obs.complete();
        }
      });
    });
  }

  goToStep(action: string) {
    switch (action) {
      case this.ACTIVATE:
        // TODO: Resend email functionality
        // this.route.navigate(['/getting-started/resend-confirmation']);
        break;
      case this.ADD_DOMAIN:
        this.route.navigate(['/settings/domain']);
        break;
      case this.VERIFY:
        this.route.navigate(['/settings/domain']);
        break;
      case this.SEND_EMAIL:
        this.route.navigate(['/settings/integration'], {queryParams: {id: 1}});
        break;
      default:
        break;
    }
  }
}
