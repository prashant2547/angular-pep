import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

export interface P1Alert {
  type: string;
  value: string;
  custom?: any;
}

@Injectable()
export class AlertsService {
  // For updating areas which don't update with each page
  // Like Main Menu and Header
  private _refreshPage = new BehaviorSubject<boolean>(false);
  refreshPage$ = this._refreshPage.asObservable();

  // Priority 0 Alert - Very High Priority, site wide alert
  private _p0Alert = new BehaviorSubject<string>('');
  p0Alert$ = this._p0Alert.asObservable();

  // Priority 1 Alert - High Priority, page level
  private _p1Alert = new BehaviorSubject<P1Alert>(null);
  p1Alert$ = this._p1Alert.asObservable();

  constructor() { }

  /**
   * Show P0 alert with provided string
   * @param {string} value
   */
  showP0Alert(value: string) {
    this._p0Alert.next(value);
  }

  /**
   * Hides P0 alert
   */
  hideP0Alert() {
    this._p0Alert.next('');
  }

  /**
   * Shows alert with provided type on page
   * @param {P1Alert} alert
   */
  showP1Alert(alert: P1Alert) {
    this._p1Alert.next(alert);
  }

  /**
   * Hides P1 alert
   */
  hideP1Alert() {
    this._p1Alert.next(null);
  }

  refreshPage() {
    this._refreshPage.next(!this._refreshPage.value);
  }
}
