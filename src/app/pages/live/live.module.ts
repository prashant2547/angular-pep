import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LiveRoutingModule} from './live-routing.module';
import {LiveComponent} from './live.component';
import {ComponentsModule} from '../components/components.module';
import {Daterangepicker} from 'ng2-daterangepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    LiveRoutingModule,
    ComponentsModule,
    FormsModule,
    Daterangepicker,
    ReactiveFormsModule
  ],
  declarations: [LiveComponent]
})
export class LiveModule {}