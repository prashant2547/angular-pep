import {Component, OnInit, ViewChild} from '@angular/core';
import {TableDataSource} from '../services/table.data-source';
import {
  DOWNLOAD_LIVE_REPORT,
  SHARE_LIVE_REPORT,
  WARNING_MSG,
  SEND_LIVE_FEED,
  GET_LIVEFEED_INFO, NO_DATA_FOUND
} from '../../config';
import {Http} from '@angular/http';
import {DaterangePickerComponent, DaterangepickerConfig} from 'ng2-daterangepicker';
import {Router} from '@angular/router';
import {ApiService} from '../services/api.service';
import * as moment from 'moment';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EMAIL} from '../components/forms/form-validation';
import {environment} from '../../../environments/environment';
import {TableStatusComponent} from '../components/table/table-status/table-status.component';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss']
})
export class LiveComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'What is Requested Date/Time?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-requested-datetime'
    }, {
      helpText: 'For how much period, the live feed data will be accessible?',
      helpLink: 'https://docs.pepipost.com/docs/for-how-much-period-the-live-feed-data-will-be-accessible'
    }, {
      helpText: 'What is email size? And what should I know about it?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-email-size-and-what-should-i-know-about-it'
    }, {
      helpText: 'What is the difference between a hard and a soft bounce?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-the-difference-between-a-hard-and-a-soft-bounce'
    }, {
      helpText: 'What is Server Remarks?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-server-remarks'
    },
  ];
  // var to show hide no data found window
  nodataFlag = true;
  tableFlag = true;

  dataSourceLivefeed: any;
  redeemed: boolean;
  displayTagWiseValue: boolean;
  tagHeadLine = 'Archive';
  filterValues = [];
  tempDownloadReport = [];
  dateInput = {
    start: moment().subtract(7, 'days'),
    end: moment()
  };
  selectedDateRange = [moment(this.dateInput.start).format('YYYYMMDD'),
    moment(this.dateInput.end).format('YYYYMMDD')];
  // modal
  searchType = 'Recipient Email';
  showShareEmailType = '';
  showShareEmailMsg = '';
  showLiveFeedForm: FormGroup;
  popRef: NgbActiveModal;
  apiData = '';


  mainInput = {
    start: moment().subtract(1, 'month'),
    end: moment()
  };

  // https://akveo.github.io/ng2-smart-table/#/documentation
  settingsLivefeed = {
    attr: {
      class: 'table'
    },
    columns: {
      fromaddress: {
        title: 'Sender Address',
        sort: false
      },
      email: {
        title: 'Recipient Email',
        sort: false
      },
      atime: {
        title: 'Requested Date/Time',
        sort: false
      },
      mtime: {
        title: 'Last Activity',
        sort: false
      },
      // TTD: {
      //   title: 'Time to Deliver',
      //   sort: false,
      //   valuePrepareFunction: (value) => {
      //     return value.replace('00 hr', '').replace('00 m', '').replace('00 Days ', '').trim();
      //   }
      // },
      description: {
        title: 'Status',
        sort: false,
        type: 'custom',
        renderComponent: TableStatusComponent,
        onComponentInitFunction: (instance) => {
          instance.clicked.subscribe(row => {
            // console.log(`${row.description} clicked!`);
          });
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  saveData = (function () {
    const a = document.createElement('a');
    document.body.appendChild(a);
    return function (data, fileName) {
      // let json = JSON.stringify(data), blob = new Blob([json], {type: "octet/stream"}),
      const blob = new Blob([data], {type: 'octet/stream'});
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    };
  }());

  // displayTagWiseValue = false; // Refer documentation of ng2-smart-table for settings
  @ViewChild(DaterangePickerComponent) picker: DaterangePickerComponent;
  @ViewChild('shareLivePopup') shareLivePopup;

  constructor(private http: Http,
              private daterangepickerOptions: DaterangepickerConfig,
              private apiService: ApiService,
              private router: Router,
              private modalService: NgbModal) {
    this.daterangepickerOptions.settings = {
      locale: {
        format: 'YYYY-MM-DD'
      },
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [
          moment().subtract(1, 'month'),
          moment()
        ],
        'Last 3 Months': [
          moment().subtract(4, 'month'),
          moment()
        ],
        'Last 6 Months': [
          moment().subtract(6, 'month'),
          moment()
        ],
        'Last 12 Months': [
          moment().subtract(12, 'month'),
          moment()
        ]
      }
    };

  }

  ngOnInit() {
    this.fetchTagWiseData(this.tagHeadLine);
  }


  // dropdown change
  onChange(deviceValue) {
    if (deviceValue) {
      this.filterValues
        .push({field: 'filter_status', search: deviceValue});
      if (this.selectedDateRange[0]) {
        this.filterValues
          .push({field: 'start_date', search: this.selectedDateRange[0]});
        this.filterValues
          .push({field: 'end_date', search: this.selectedDateRange[1]});
      }
      this.dataSourceLivefeed
        .setFilter(this.filterValues, false);
    } else {
      this.dataSourceLivefeed.setFilter([], false);
    }
  }

  /**
   * To fetch API key on initial load as well as show on lick of "SHOW"
   * @param {string} email
   */
  fetchShareData(email = '*') {
    this.apiService
      .post(SEND_LIVE_FEED, ({'email': email}))
      .subscribe(result => {
        if (result['status'] === 'success') {
          if (email !== '*') {
            this.popRef.close();
          }
          this.apiData = result['data'];
        } else {
          this.showShareEmailType = 'danger';
          this.showShareEmailMsg = result['error_info']['message'];
        }
      });
  }

  // sets to today's date
  public updateDateRange() {
    const displayDate = new Date().toLocaleDateString();
    const dateSplits = displayDate.split('/');
    this.picker
      .datePicker
      .setStartDate(dateSplits[2] + '-' + dateSplits[0] + '-' + dateSplits[1]);
    this.picker
      .datePicker
      .setEndDate(dateSplits[2] + '-' + dateSplits[0] + '-' + dateSplits[1]);
  }

  selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = [
        moment(dateInput.start).format('YYYYMMDD'),
        moment(dateInput.end).format('YYYYMMDD')
      ];
      // clear existing date ranges
      const tempFilterValues = this.filterValues
        .map((obj) => {
          if (obj.field !== 'start_date' && obj.field !== 'end_date') {
            return obj;
          }
        });

      console.log('here' + this.filterValues);

      this.filterValues = tempFilterValues.filter(n => n);

      // Add new date ranges
      this.filterValues
        .push({field: 'start_date', search: this.selectedDateRange[0]});
      this.filterValues
        .push({field: 'end_date', search: this.selectedDateRange[1]});
      this.dataSourceLivefeed
        .setFilter(this.filterValues, false);
    }
  }

  downloadLiveReport() {
    // const options = {   headers: {'Content-Type' : 'text/plain'},
    // responseType: 'text/plain',   withCredentials: false };
    this.tempDownloadReport = [];

    for (let index = 0; index < this.filterValues.length; index++) {
      const element = this.filterValues[index];
      const temp = {};
      temp[element.field] = element.search;
      this.tempDownloadReport.push(temp);
    }

    this.apiService
      .post(DOWNLOAD_LIVE_REPORT, ({'settings': this.tempDownloadReport}))
      .subscribe((result) => {
        this.saveData(result['data']['live_list']['data'], result['data']['live_list']['filename']);
        if (result['status'] === 'success') {
          // console.log('Download started');
        } else {
          alert('Download failed : ' + result['error_info']['message']);
        }
      });
    return false;
  }

  // shareLiveReport() {   this.apiService.post(SHARE_LIVE_REPORT, ({'settings':
  // this.filterValues}))     .subscribe((result) => {       console.log(result);
  //      if (result['status'] === 'success') {         console.log('Report
  // sharing started');       } else {         alert('Report sharing failed : ' +
  // result['error_info']['message']);       }     }, error => {
  // alert(WARNING_MSG);     }); } Angular D3.js refer
  // https://keathmilligan.net/create-a-reusable-chart-component-with-angular-and-

  onSearch(query: string, searchType: string) {
    query = query.trim();
    this.filterValues = [];
    switch (searchType) {
      case 'Sender Address':
        if (query) {
          this.filterValues
            .push({field: 'filter_fromaddress', search: query});
          if (this.selectedDateRange[0]) {
            this.filterValues
              .push({field: 'start_date', search: this.selectedDateRange[0]});
            this.filterValues
              .push({field: 'end_date', search: this.selectedDateRange[1]});
          }
          // this.updateFilter("filter_email", query);
          this.dataSourceLivefeed
            .setFilter(this.filterValues, false);
        } else {
          this.dataSourceLivefeed
            .setFilter([], false);
        }
        break;
      case 'Recipient Email':
        if (query) {
          this.filterValues
            .push({field: 'filter_email', search: query});
          if (this.selectedDateRange[0]) {
            this.filterValues
              .push({field: 'start_date', search: this.selectedDateRange[0]});
            this.filterValues
              .push({field: 'end_date', search: this.selectedDateRange[1]});
            // this.updateFilter("filter_fromaddress", query);
          }
          this.dataSourceLivefeed
            .setFilter(this.filterValues, false);
        } else {
          this.dataSourceLivefeed
            .setFilter([], false);
        }
        break;
      case 'XAPI-Header':
        if (query) {
          this.filterValues
            .push({field: 'XAPI-Header', search: query});
          if (this.selectedDateRange[0]) {
            this.filterValues
              .push({field: 'start_date', search: this.selectedDateRange[0]});
            this.filterValues
              .push({field: 'end_date', search: this.selectedDateRange[1]});
          }
          this.dataSourceLivefeed
            .setFilter(this.filterValues, false);
        } else {
          this.dataSourceLivefeed
            .setFilter([], false);
        }
        break;
        case 'Subject':
        if (query) {
          this.filterValues
            .push({field: 'subject', search: query});
          if (this.selectedDateRange[0]) {
            this.filterValues
              .push({field: 'start_date', search: this.selectedDateRange[0]});
            this.filterValues
              .push({field: 'end_date', search: this.selectedDateRange[1]});
          }
          this.dataSourceLivefeed
            .setFilter(this.filterValues, false);
        } else {
          this.dataSourceLivefeed
            .setFilter([], false);
        }
        break;
      case 'Status':
      // if (query) {
      //   console.log("test");
      //   this.filterValues
      //     .push({field: 'filter_status', search: query});
      //   if (this.selectedDateRange[0]) {
      //     this.filterValues
      //       .push({field: 'start_date', search: this.selectedDateRange[0]});
      //     this.filterValues
      //       .push({field: 'end_date', search: this.selectedDateRange[1]});
      //   }
      //   // this.updateFilter("filter_status", query);
      //   this.dataSourceLivefeed
      //     .setFilter(this.filterValues, false);
      // } else {
      //   this.dataSourceLivefeed.setFilter([], false);
      // }
      // break;
      default:
        this.filterValues = [];
        this.filterValues
          .push({field: 'start_date', search: this.selectedDateRange[0]});
        this.filterValues
          .push({field: 'end_date', search: this.selectedDateRange[1]});
        this.dataSourceLivefeed
          .setFilter([], false);
        break;
    }
  }

  // Fetch Tag Wise Data
  fetchTagWiseData(tagHeadLine) {
    // console.log('aasdasd');
    const displayDate = new Date().toLocaleDateString();
    const dateSplits = displayDate.split('/');

    this.displayTagWiseValue = false;
    this.tagHeadLine = tagHeadLine;
    this.dataSourceLivefeed = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${ GET_LIVEFEED_INFO}`,
      dataKey: 'live_list'
    });
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.dataSourceLivefeed.setFilter(this.filterValues, false);

    // to show hide of no data fond window
    this.dataSourceLivefeed.onGetData.subscribe((res) => {
      this.nodataFlag = +res.data.live_list.total_livefeed_counts > 0;
      this.tableFlag = !this.nodataFlag;
    });
  }

  /**
   * To sahow API key , Open popup to enter password
   */
  showLiveFeedKey() {
    this.open(this.shareLivePopup);
    return false;
  }

  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.showLiveFeedForm = new FormGroup({
      shareEmail: new FormControl('', [
        Validators.required, Validators.min(8),
        Validators.pattern(EMAIL)
      ])
    });
    this.popRef = this
      .modalService
      .open(content);
  }
}
