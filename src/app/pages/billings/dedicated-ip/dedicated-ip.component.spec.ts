import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DedicatedIpComponent } from './dedicated-ip.component';

describe('DedicatedIpComponent', () => {
  let component: DedicatedIpComponent;
  let fixture: ComponentFixture<DedicatedIpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DedicatedIpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DedicatedIpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
