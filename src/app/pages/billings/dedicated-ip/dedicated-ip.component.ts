import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {SEND_REQUEST_FOR_DEDICATED_IP, WARNING_MSG} from '../../../config';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ONLY_DIGIT} from '../../components/forms/form-validation';

@Component({
  selector: 'app-dedicated-ip',
  templateUrl: './dedicated-ip.component.html',
  styleUrls: ['./dedicated-ip.component.scss']
})
export class DedicatedIpComponent implements OnInit {
  type: string;
  msg: string;
  @ViewChild('dedicatedIpPopup') dedicatedIpPopup;
  popRef: NgbActiveModal;
  dedicatedIpRequestFrm: FormGroup;

  constructor(private apiService: ApiService,
              private modalService: NgbModal,
              private fb: FormBuilder) {
  }

  ngOnInit() {
  }

  openDedicatedPopup() {
    this.msg = '';
    this.open(this.dedicatedIpPopup);
  }

  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.dedicatedIpRequestFrm = new FormGroup({
      noOfIp: new FormControl('', [Validators.required, Validators.pattern(ONLY_DIGIT)]),
      emailVolume: new FormControl('', [Validators.required, Validators.pattern(ONLY_DIGIT)]),
      comments: new FormControl(''),
    });

    this.popRef = this.modalService.open(content);
  }

// Send Request for dedicated IP request
  showRequestForDedicatedIp(formData) {
    this.apiService.post(SEND_REQUEST_FOR_DEDICATED_IP, formData)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.type = 'info';
            this.msg = result['message'];
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

}
