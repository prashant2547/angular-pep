import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Http} from '@angular/http';
import {
  API_URL,
  ENABLE_AUTO_REDEEM, FETCH_CREDIT_MANAGEMENT_GRAPH_DATA,
  GET_AVAILABLE_EMAIL_CREDITS,
  GET_EMAIL_CREDITS,
  GET_REDEEM_HISTORY, NO_DATA_FOUND,
  REDEEM_OPEN_CREDITS,
  WARNING_MSG
} from '../../../config';
import {environment} from '../../../../environments/environment';
import {ApiService} from '../../services/api.service';
import {TableDataSource} from '../../services/table.data-source';
import {HttpClient} from '@angular/common/http';
import {DaterangepickerConfig} from 'ng2-daterangepicker';
import * as moment from 'moment';
import {addCommasToNum} from '../../components/utils/number-utils';


@Component({
  selector: 'app-credit-management',
  templateUrl: './credit-management.component.html',
  styleUrls: ['./credit-management.component.scss']
})
export class CreditManagementComponent implements OnInit, AfterViewInit {

  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'What is Earned email credits?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-the-open-free-concept'
    },
  ];

  earn_credit: number;
  credits_available: any;
  redimMsg = '';
  autoRedeem = false;
  redeemedDisp: boolean;
  type = '';
  msg = '';
  total_available: number;
  hideredeemHistory = true;
  hideredeemHistory_hide = true;
  // Refer documentation of ng2-smart-table for settings
  // https://akveo.github.io/ng2-smart-table/#/documentation
  settings = {
    attr: {
      class: 'table'
    },
    columns: {
      date: {
        title: 'Date',
        sort: false
      },
      time: {
        title: 'Time',
        sort: false
      },
      description: {
        title: 'Redeem Method',
        sort: false
      },
      credit_action: {
        title: 'Credits Redeemed',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.credit_action);
        }
      },
      close_balance: {
        title: 'Closing Balance',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.close_balance);
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };
  dataSource: any;
  redeemed: boolean;

  eventLog = '';
  mainInput = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(6, 'month')
  };

  // graph variables
  graphData = [];
  view = [];
  colorSchemeGraph = {
    domain: ['#549fd6']
  };

  @ViewChild('tabBox') tabBox;
  @ViewChild('tabset') tabset;

  constructor(private http: Http,
              private dateRangePickerOptions: DaterangepickerConfig,
              private httpClient: HttpClient,
              private apiService: ApiService) {
    this.dateRangePickerOptions.settings = {
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(4, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
      }
    };

    this.dataSource = new TableDataSource(http,
      {
        endPoint: `${environment.api_url}${GET_REDEEM_HISTORY}`,
        dataKey: 'redeem_history'
      });

    this.dataSource.onGetData.subscribe((res) => {
      // console.log('Here redeem ===>' + res.data.redeem_history.redeem_history_cnt);
      // console.log(res);
       if (+res.data.redeem_history.redeem_history_cnt === 0) {
         // console.log('here inside');
        this.hideredeemHistory = false;
        this.hideredeemHistory_hide = true;
       }else {
         this.hideredeemHistory = true;
         this.hideredeemHistory_hide = false;
       }
    });
  }

  onSearch(query: string = '') {
    this.dataSource.setFilter([
      // fields we want to include in the search
      {
        field: 'description',
        search: query
      },
    ]);
  }

  ngOnInit() {
    this.creditMangement();
    this.redeemHistory();
    this.fetchGraphData();
    // this.fetchCreditManagementGraphData();
  }

  ngAfterViewInit() {
  }

  applyDate(e: any) {
    this.eventLog += '\nEvent Fired: ' + e.event.type;
  }


  // Credit management onload and onclik fetching data
  creditMangement() {
    this.fetchCreditAvailable();
    this.fetchEarnEmailCredit();
  }

  // redeem history onload and onclik fetching data
  redeemHistory() {
    // console.log('in redeem history');
  }

  // To redeem earned email working
  onRedeemClick() {
    this.apiService.post(REDEEM_OPEN_CREDITS,
      ({'available_credits': this.credits_available, 'redeem_credits': this.earn_credit}))
      .subscribe(result => {
        if (result['status'].toLowerCase() === 'success') {
          this.redimMsg = 'Credits redeemed successfully!';
          this.redeemed = true;
          this.fetchCreditAvailable();
          this.fetchEarnEmailCredit();
        } else {
          this.redimMsg = 'Failed to redeem Credits';
          this.redeemed = false;
        }
      });
  }

  // fetch Email credits available working
  fetchCreditAvailable() {
    this.apiService.post(GET_EMAIL_CREDITS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.credits_available = result['data']['credits_available'];
            this.total_available = result['data']['total_available'];
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

  // fetch Earned email credits working
  fetchEarnEmailCredit() {
    this.apiService.post(GET_AVAILABLE_EMAIL_CREDITS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.earn_credit = result['data']['opens_available'];
            // for auto redem checkbox value
            this.autoRedeem = result['data']['auto_reedem_active'] === 0 ? false : true;
            this.redeemedDisp = this.earn_credit === 0 ? true : false;
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

  // Auto redeem status change working
  autoRedeemStatus(e) {
    this.apiService.post(ENABLE_AUTO_REDEEM,
      ({'auto_redeem': e.target.checked === true ? 'ON' : 'OFF'}))
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.type = 'info';
            this.msg = result['message'];
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

  fetchGraphData() {
    // this.httpClient.get('http://localhost:5000/credit-graph-data').subscribe(result => {
    //   // this.graphData = result['data'];
    //   // this.graphData = [{"name":"open" , data:[{"name":"2017-07-03", "value":"0"}]}];

    //       if (result['status'].toLowerCase() === 'success') {
    //         // this.graphData = result['data'];
    //         let data = result['data'];
    //         let formattedResult = [];
    //         formattedResult[0] = {};
    //         formattedResult[1] = {};
    //         formattedResult[0]['name'] = "Open";
    //         formattedResult[1]['name'] = "Save";
    //         console.log("formattedResult ",formattedResult);
    //         formattedResult[0].series = [];
    //         formattedResult[1].series = [];
    //         for (let i = 0; i < data.length; i++) {
    //           const element = data[i];
    //           formattedResult[0].series.push({ "name" : element.adate,  "value" : element.open});
    //           formattedResult[1].series.push({ "name" : element.adate,  "value" : element.save});
    //         }
    //         console.log("formattedResult ",formattedResult);
    //         this.graphData = formattedResult;
    //       } else {
    //         this.type = 'error';
    //         this.msg = result['error_info']['message'];
    //       }
    // });
  }

  fetchCreditManagementGraphData() {
    this.apiService.get(FETCH_CREDIT_MANAGEMENT_GRAPH_DATA, '')
    // this.apiService.get('http://localhost:5000/credit-graph-data')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            console.log(result['data']);
            // this.graphData = result['data'];
            const data = result['data'];
            const formattedResult = [];
            formattedResult[0] = {};
            formattedResult[0].name = 'Open';
            formattedResult[1].name = 'Save';
            formattedResult[0].series = [];
            formattedResult[1].series = [];
            for (let i = 0; i < data.length; i++) {
              const element = data[i];
              formattedResult[0].series.push({'name': element.adate, 'value': element.open});
              formattedResult[1].series.push({'name': element.adate, 'value': element.save});
            }
            this.graphData = formattedResult;
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

  redirectToRedeem() {
    this.tabset.select('redeem');
  }

}
