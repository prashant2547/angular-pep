import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BillingsComponent} from './billings.component';
import {BillingDetailsComponent} from './billing-details/billing-details.component';
import {AddOnsComponent} from './add-ons/add-ons.component';
import {CreditManagementComponent} from './credit-management/credit-management.component';
import {BillingRechargeComponent} from './billing-details/billing-recharge/billing-recharge.component';
import {BillingInformationComponent} from './billing-details/billing-information/billing-information.component';
import {BillingHistoryComponent} from './billing-details/billing-history/billing-history.component';
import {PostpaidBillingComponent} from './postpaid-billing/postpaid-billing.component';
import {PostpaidBillingHistoryComponent} from './postpaid-billing/postpaid-billing-history/postpaid-billing-history.component';
import {PostpaidBillingPaymentComponent} from './postpaid-billing/postpaid-billing-payment/postpaid-billing-payment.component';
import {PostpaidBillingTransactionsComponent} from './postpaid-billing/postpaid-billing-transactions/postpaid-billing-transactions.component';
import {PostpaidCreditManagementComponent} from './postpaid-credit-management/postpaid-credit-management.component';
import {DedicatedIpComponent} from './dedicated-ip/dedicated-ip.component';
import {SubaccountLogComponent} from './subaccount-log/subaccount-log.component';
import {UsageSummaryComponent} from './subaccount-log/usage-summary/usage-summary.component';
import {CreditLogComponent} from './subaccount-log/credit-log/credit-log.component';
import {AccountLogComponent} from './account-log/account-log.component';
import {SubaccountBillingComponent} from './subaccount-billing/subaccount-billing.component';

const routes: Routes = [{
  path: '',
  component: BillingsComponent,
  children: [{
    path: 'details',
    component: BillingDetailsComponent,
    children: [{
      path: 'recharge',
      component: BillingRechargeComponent
    }, {
      path: 'information',
      component: BillingInformationComponent
    }, {
      path: 'history',
      component: BillingHistoryComponent
    }, {
      path: '',
      redirectTo: 'recharge',
      pathMatch: 'full'
    }]
  }, {
    path: 'add-ons',
    component: AddOnsComponent
  }, {
    path: 'credit-management',
    component: CreditManagementComponent
  }, {
    path: 'subaccount-billing',
    component: SubaccountBillingComponent
  }, {
    path: 'account-log',
    component: AccountLogComponent
  }, {
    path: 'subaccount-log',
    component: SubaccountLogComponent,
    children: [{
      path: 'usage-summary',
      component: UsageSummaryComponent
    },
      {
        path: 'credit-log',
        component: CreditLogComponent
      }, {
        path: '',
        redirectTo: 'usage-summary',
        pathMatch: 'full'
      }]
  }, {
    path: 'postpaid-credit-management',
    component: PostpaidCreditManagementComponent
  }, {
    path: 'dedicated-ip',
    component: DedicatedIpComponent
  }, {
    path: 'postpaid-billing',
    component: PostpaidBillingComponent,
    children: [{
      path: 'postpaid-billing',
      component: PostpaidBillingComponent
    }, {
      path: 'payment',
      component: PostpaidBillingPaymentComponent
    }, {
      path: 'transactions',
      component: PostpaidBillingTransactionsComponent
    }, {
      path: 'history',
      component: PostpaidBillingHistoryComponent
    },
      {
        path: '',
        redirectTo: 'payment',
        pathMatch: 'full'
      }]
  }, {
    path: '',
    redirectTo: 'billings',
    pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingsRoutingModule {
}
