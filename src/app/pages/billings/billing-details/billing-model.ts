export class Billing {
  bill_name? = '';
  phone_number? = '';
  company_name? = '';
  bill_address? = '';
  bill_tax_id? = '';
}

export class PromoCode {
  promo_code? = '';
}
