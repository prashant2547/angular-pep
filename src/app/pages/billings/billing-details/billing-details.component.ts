import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {MenuService} from '../../services/menu.service';

@Component({
  selector: 'app-billing-details',
  templateUrl: './billing-details.component.html',
  styleUrls: ['./billing-details.component.scss']
})
export class BillingDetailsComponent implements OnInit {
  menuSubscription: Subscription;
  falconPrepaidClient: boolean;
  falconPrepaid = true;
  subUserClient: boolean;
  subUser = true;

  constructor(private menuService: MenuService) {
    this.menuSubscription = menuService.falconPrepaid$.subscribe(bool => this.falconPrepaid = bool);
    this.menuSubscription = menuService.subUser$.subscribe(bool => this.subUser = bool);
  }

  ngOnInit() {
    this.checkFalconPrepaidClient();
    this.checkSubUser();
  }

  checkSubUser() {
    if (this.subUser === true) {
      this.subUserClient = true;
    }
  }

  checkFalconPrepaidClient() {
    if (this.falconPrepaid === true) {
      this.falconPrepaidClient = true;
    }
  }
}
