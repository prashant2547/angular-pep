import {Component, OnInit, ViewChild} from '@angular/core';
import {
  ADD_NEW_CARD, APPLY_COUPON, GET_APPLIED_COUPON_CODE, GET_AUTORENEW_DETAILS, GET_BILLING_DETAILS, GET_PLAN_TYPE, GET_SPECIAL_PRICING,
  MAKE_PRIMARY_CARD, NO_DATA_FOUND,
  RETRIVE_CREDIT_CARD_DETAILS,
  SAVE_BILLING_DETAILS, UNDEFINED_TEXT,
  UPDATE_AUTO_RENEW,
  UPDATE_AUTO_RENEW_STATUS, WARNING_MSG
} from '../../../../config';
import {Billing} from '../billing-model';
import {ElementsOptions, StripeService} from '../../../components/payments/ngx-stripe/ngx-stripe';
import {StripeCardComponent} from '../../../components/payments/stripe-card/stripe-card.component';
import {Http} from '@angular/http';
import {ApiService} from '../../../services/api.service';
import {environment} from '../../../../../environments/environment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableDataSource} from '../../../services/table.data-source';
import {CouponCodeStateComponent} from '../../../components/table/coupon-code-state/coupon-code-state.component';

@Component({
  selector: 'app-billing-information',
  templateUrl: './billing-information.component.html',
  styleUrls: ['./billing-information.component.scss']
})
export class BillingInformationComponent implements OnInit {
  billingEditForm: FormGroup;
  autoRenewApply: FormGroup;
  promoCodeApply: FormGroup;
  billingInfo: Billing;
  addedCardList: any;
  type = '';
  msg = '';
  editBillingType: string;
  editBillingMsg: string;
  promoCodeType: string;
  promoCodeMsg: string;
  primaryCard: string;
  card_id: any;
  token_id: any;
  makePrimaryCardType: string;
  makePrimaryCardMsg: string;
  addNewCardType: string;
  addNewCardMsg: string;
  autoRenewType: string;
  autoRenewMsg: string;
  autoRenewSaveType: string;
  autoRenewSaveMsg: string;
  autoRenewEditMode = false;
  autoRenew: boolean;
  promoSource: any;
  postpaidPlan: boolean;
  specialPrice: boolean;
  undefinedText = UNDEFINED_TEXT;

  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  // @ViewChild(StripeCardSmallComponent) card1: StripeCardSmallComponent;

  settingsPromo = {
    attr: {
      class: 'table'
    },
    columns: {
      coupon_code: {
        title: 'Promo Code',
        sort: false
      },
      description: {
        title: 'Description',
        sort: false
      },
      start_date: {
        title: 'Start Date',
        sort: false
      },
      end_date_new: {
        title: 'End Date',
        sort: false
      },
      status_new: {
        title: 'Status',
        sort: false,
        type: 'custom',
        renderComponent: CouponCodeStateComponent
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  editDetails: boolean;
  addCard: boolean;

  constructor(private http: Http,
              private apiService: ApiService,
              private fb: FormBuilder,
              private stripeService: StripeService) {
  }

  ngOnInit() {
    // Form validation for profile information on edit
    this.billingEditForm = this.fb.group({
      bill_name: ['', Validators.required],
      phone_number: ['', Validators.required],
      company_name: ['', Validators.required],
      bill_address: ['', Validators.required],
      bill_tax_id: ['', Validators.required]
    });

    this.autoRenewApply = this.fb.group({
      amount: ['', [Validators.required]],
      threshold: ['', [Validators.required]]
    });
    this.getPlanType();
    this.getbillingdetails();
    this.getAddedCards();
    this.fetchAutoRenewDetails();
    this.getPromoCodeDetails();
    this.getSpecialPrice();

    // Form validation for promocode
    this.promoCodeApply = this.fb.group({
      coupon_code: ['', [Validators.required]]
    });
  }

  htmlTagCheck(value) {
    const rgx = new RegExp('<(\\"[^\\"]*\\"|\'[^\']*\'|[^\'\\">])*>');
    if (rgx.test(value)) {
      // your logic goes here.
      console.log('here in' + value);
      return false;
    }
  }

  // Save billing info
  saveBillingInfo() {
    this.billingInfo = Object.assign(this.billingInfo, this.billingEditForm.value);
    const name_validate = this.htmlTagCheck(this.billingInfo.bill_name);
    if (name_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The bill name should not contain HTML tags';
      return;
    }

    const company_name_validate = this.htmlTagCheck(this.billingInfo.company_name);
    if (company_name_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The company name should not contain HTML tags';
      return;
    }

    const bill_address_validate = this.htmlTagCheck(this.billingInfo.bill_address);
    if (bill_address_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The bill address should not contain HTML tags';
      return;
    }

    const bill_tax_id_validate = this.htmlTagCheck(this.billingInfo.bill_tax_id);
    if (bill_tax_id_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The tax id should not contain HTML tags';
      return;
    }

    const phone_number_validate = this.htmlTagCheck(this.billingInfo.phone_number);
    if (phone_number_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The phone number should not contain HTML tags';
      return;
    }


    this.apiService.post(SAVE_BILLING_DETAILS, this.billingInfo)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.editBillingType = 'info';
            this.editBillingMsg = result['message'];
            this.onCancelClick();
          } else {
            this.editBillingType = 'danger';
            this.editBillingMsg = result['error_info']['message'];
          }
        }
      );
    return false;
  }

  editDetailsClick() {
    this.editDetails = true;
    this.billingEditForm.setValue({
      bill_name: this.billingInfo.bill_name,
      phone_number: this.billingInfo.phone_number,
      company_name: this.billingInfo.company_name,
      bill_address: this.billingInfo.bill_address,
      bill_tax_id: this.billingInfo.bill_tax_id
    });
  }

  onCancelClick() {
    this.editDetails = false;
    this.editBillingType = '';
    this.editBillingMsg = '';
    return false;
  }

// fetch billing information
  getbillingdetails() {
    this.apiService.post(GET_BILLING_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.billingInfo = result['data'];
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  // Save auto renew threshold and value
  autoRenewSave(auto_renew_val) {
    this.autoRenewEditMode = false;
    this.apiService.post(UPDATE_AUTO_RENEW, auto_renew_val)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.autoRenewType = 'info';
            this.autoRenewMsg = result['message'];
          } else {
            this.autoRenewType = 'danger';
            this.autoRenewMsg = result['error_info']['message'];
          }
        }
      );

  }

  // Auto renew status update change working
  autoRenewStatus(e) {
    this.apiService.post(UPDATE_AUTO_RENEW_STATUS,
      ({'auto_renew': e ? 'ON' : 'OFF'}))
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.autoRenewType = 'info';
            this.autoRenewMsg = result['message'];
            if (e === false) {
              this.autoRenew = false;
              // console.log('222' + this.autoRenew);
            }

            if (e === true) {
              this.autoRenew = true;
              // console.log('444' + this.autoRenew);
            }

            // this.autoRenew = false;
            // console.log('333' +this.autoRenew);
          } else {
            // this.autoRenew = false;
            // console.log('22==>' + this.autoRenew);
            this.autoRenewType = 'danger';
            this.autoRenewMsg = result['error_info']['message'];
          }
        }
      );
  }

  autoRenewEditClick() {
    this.autoRenewEditMode = true;
    return false;
  }

// fetch recurring details
  // fetch Earned email credits working
  fetchAutoRenewDetails() {
    this.apiService.post(GET_AUTORENEW_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // this.autorenew_active = result['data']['active'];
            // for auto redeem checkbox value
            this.autoRenew = result['data']['active'] === 1 ? true : false;

            this.autoRenewApply.setValue({
              amount: result['data']['amount'],
              threshold: result['data']['threshold']
            });

          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }


  // Get all added credit cards
  getAddedCards() {
    this.apiService.post(RETRIVE_CREDIT_CARD_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            if (result['data']['data'] !== 'null') {
              this.primaryCard = result['data']['default_source'];
              this.addedCardList = result['data']['sources']['data'];
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  // Make primary card
  makePrimaryCard(cardid) {
    this.card_id = {'card_id': cardid};
    this.apiService.post(MAKE_PRIMARY_CARD, this.card_id)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.updatePrimaryCardLocal(this.card_id);
            this.primaryCard = cardid;
            this.makePrimaryCardType = 'info';
            this.makePrimaryCardMsg = result['message'];
          } else {
            this.makePrimaryCardType = 'danger';
            this.makePrimaryCardMsg = result['error_info']['message'];
          }
        }
      );
    return false;
  }

  /**
   * Moves primary card in array to 0 index
   * @param cardObj {card_id: id} primary card id
   */
  updatePrimaryCardLocal(cardObj) {
    const cardList = [null];
    for (const card of this.addedCardList) {
      if (card.id === cardObj.card_id) {
        cardList[0] = card;
      } else {
        cardList.push(card);
      }
    }
    this.addedCardList = cardList;
  }

  download_invoice() {
    alert('hi');
  }

  addNewCardStripe(name) {
    console.log('name ', name);
    this.stripeService
      .createToken(this.card.getCard(), {name})
      .subscribe(result => {
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          this.token_id = {'token_id': result.token.id};
          // Call API for adding the new card
          this.apiService.post(ADD_NEW_CARD, this.token_id)
            .subscribe(result_val => {
                if (result_val['status'] === 'success') {
                  this.addNewCardType = 'info';
                  this.addNewCardMsg = result_val['message'];
                  this.getAddedCards();
                  this.addCard = false;
                } else {
                  this.addNewCardType = 'danger';
                  this.addNewCardMsg = result_val['error_info']['message'];
                }
              }
            );
        } else if (result.error) {
          // Error creating the token
          // this.addNewCardType = 'warning';
          // this.addNewCardMsg = WARNING_MSG;
          // this.addNewCardMsg = result.error.message;
        }
      });
  }

  cancelAddNewCard() {
    this.addCard = false;
    this.addNewCardType = '';
    this.addNewCardMsg = '';
    this.makePrimaryCardType = '';
    this.makePrimaryCardMsg = '';
  }

  addNewCard() {
    this.addCard = true;
    this.makePrimaryCardType = '';
    this.makePrimaryCardMsg = '';
    return false;
  }

  // Apply Coupon
  applyCoupon(promo) {
    console.log(promo);
    console.log(promo.coupon_code);
    // console.log('here in apply coupon');
    this.apiService.get(APPLY_COUPON + '/?coupon_code=' + promo.coupon_code)
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.promoCodeType = 'info';
            this.promoCodeMsg = result['message'];
            this.getPromoCodeDetails();
          } else {
            this.promoCodeType = 'danger';
            this.promoCodeMsg = result['error_info']['message'];
          }
        }, error => {
          this.promoCodeType = 'warning';
          this.promoCodeMsg = WARNING_MSG;
        }
      );
  }

  // get applied promo code
  getPromoCodeDetails() {
    this.promoSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${GET_APPLIED_COUPON_CODE}`,
      dataKey: 'settingsPromo'
    });
  }

  getPlanType() {
    this.apiService.post(GET_PLAN_TYPE, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            console.log('get plan type');
            console.log(result['data']);
            if (result['data'] !== 'null') {
              // this.primaryCard = result['data']['default_source'];
              console.log('Client type ==>' + result['data']['client_type']);
              if (result['data']['client_type'] === 'postpaid') {
                this.postpaidPlan = true;
                // this.specialPrice = true;
                // this.getPostPaidDetails();
              }
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  // Get all added credit cards
  getSpecialPrice() {
    // this.apiService.post(RETRIVE_CREDIT_CARD_DETAILS, '')
    this.apiService.post(GET_SPECIAL_PRICING, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // console.log('getSpecialPrice');
            // console.log(result['data']);
            this.specialPrice = false;
            if (result['data'] !== null) {
              // this.primaryCard = result['data']['default_source'];
              // this.specialPriceList = result['data']['data'];
              this.specialPrice = true;
              console.log('sp ==>' + this.specialPrice);
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }


}
