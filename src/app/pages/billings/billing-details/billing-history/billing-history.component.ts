import {Component, OnInit} from '@angular/core';
import {DownloadIconComponent} from '../../../components/table/download-icon/download-icon.component';
import {TableDataSource} from '../../../services/table.data-source';
import {BILLING_HISTORY, DOWNLOAD_INVOICE, NO_DATA_FOUND} from '../../../../config';
import {environment} from '../../../../../environments/environment';
import {Http} from '@angular/http';
import {ApiService} from '../../../services/api.service';
import {ResponseContentType} from '@angular/http';
import {MenuService} from '../../../services/menu.service';
import {Subscription} from 'rxjs/Subscription';
import {addCommasToNum} from '../../../components/utils/number-utils';

@Component({
  selector: 'app-billing-history',
  templateUrl: './billing-history.component.html',
  styleUrls: ['./billing-history.component.scss']
})
export class BillingHistoryComponent implements OnInit {
  billingHistory = {
    attr: {
      class: 'table'
    },
    columns: {
      invoice1: {
        title: 'Invoice No.',
        sort: false
      },
      entered: {
        title: 'Date',
        sort: false
      },
      amount: {
        title: 'Amount',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.amount);
        }
      },
      description: {
        title: 'Credits Added',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.description);
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };
  billingHistorySource: any;
  type = '';
  msg = '';
  billingHistoryFlag = true;
  billingHistoryFlag_hide = true;
  menuSubscription: Subscription;
  falconPrepaid = true;

  constructor(private http: Http, private menuService: MenuService, private apiService: ApiService) {
    this.menuSubscription = menuService.falconPrepaid$.subscribe(bool => this.falconPrepaid = bool);
  }

  ngOnInit() {
    this.getBillingHistory();
    this.checkFalconPrepaidClient();
  }


  checkFalconPrepaidClient() {
    // console.log('here' + this.falconPrepaid);
    if (this.falconPrepaid === true) {
      console.log('here in' + this.falconPrepaid);
      // this.hideBillingMenu = true;
      // this.hideCreditManagement1 = true;
    }else {
      // console.log('here else' + this.falconPrepaid);
      this.billingHistory.columns['button'] = {
      title: 'Download',
      sort: false,
      type: 'custom',
      renderComponent: DownloadIconComponent,
      onComponentInitFunction: (instance) => {
        instance.clicked.subscribe(row => {
          this.download_invoice_pdf(row.invoice_path);
        });
      }
    };

    }
  }

  // Get billing history
  getBillingHistory() {
    this.billingHistorySource = new TableDataSource(this.http,
      {
        endPoint: `${environment.api_url}${BILLING_HISTORY}`,
        dataKey: 'billing_history'
      });

    this.billingHistorySource.onGetData.subscribe((res) => {
      if (+res.data.billing_history.payment_history === 0) {
        this.billingHistoryFlag = false;
        this.billingHistoryFlag_hide = true;
      } else {
        this.billingHistoryFlag = true;
        this.billingHistoryFlag_hide = false;
      }
    });
  }

  download_invoice_pdf(invoicePath) {
    const invoiceDownloadUrl = `${environment.api_url}${DOWNLOAD_INVOICE}`;
    this.http.post(invoiceDownloadUrl, JSON.stringify({invoice_path: invoicePath}), {responseType: ResponseContentType.Blob})
      .catch((err) => {
        console.log((err.status === 404));
        alert('Failed to download invoice. Please contact support');
        return null;
      })
      .subscribe(data => {
          // gets the filename
          return this.saveData(data, invoicePath.replace(/^.*[\\\/]/, ''));
        },
        error => {
          console.log(JSON.stringify(error.json()));
        }
      );
  }


  saveData = (function () {
    const a = document.createElement('a');
    document.body.appendChild(a);
    return function (data, fileName) {
      const blob = new Blob([data.blob()], {type: 'application/pdf'});
      // const blob = new Blob([data], {type: 'octet/stream'});
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    };
  }());

}
