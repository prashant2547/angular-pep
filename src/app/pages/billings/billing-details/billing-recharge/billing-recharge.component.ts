import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Http} from '@angular/http';
import {
  APPLY_COUPON, CHECK_PRPMO_CODE, CHECK_UNLIMITED_CODE_APPLIED, DOMAIN_APPROVE_CHECK, DOMAIN_LISTING, GET_APPLIED_COUPON_CODE,
  GET_AUTORENEW_DETAILS,
  GET_AVAILABLE_EMAIL_CREDITS,
  GET_BILLING_DETAILS,
  GET_EMAIL_CREDITS, GET_PLAN_TYPE, GET_POSTPAID_BILLING_DETAILS, GET_SPECIAL_PRICING,
  MAKE_PAYMENT,
  MAKE_PAYMENT_EXISTING_CARD,
  REDEEM_OPEN_CREDITS,
  RETRIVE_CREDIT_CARD_DETAILS,
  SAVE_BILLING_DETAILS,
  VALIDATE_AUTO_RENEW,
  WARNING_MSG,
  UNDEFINED_TEXT
} from '../../../../config';
import {
  ElementOptions,
  ElementsOptions,
  StripeService
} from '../../../components/payments/ngx-stripe/ngx-stripe';
import {ApiService} from '../../../services/api.service';
import {Billing} from '../../recharge/recharge-model';
import {TableDataSource} from '../../../services/table.data-source';
import {ActivatedRoute, Router} from '@angular/router';
import {StripeCardComponent} from '../../../components/payments/stripe-card/stripe-card.component';
import {AlertsService} from '../../../services/alerts.service';
import {Subscription} from 'rxjs/Subscription';
import {MenuService} from '../../../services/menu.service';

@Component({
  selector: 'app-billing-recharge',
  templateUrl: './billing-recharge.component.html',
  styleUrls: ['./billing-recharge.component.scss']
})
export class BillingRechargeComponent implements OnInit {

  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'How free email credits gets accumulated?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-the-open-free-concept'
    }, {
      helpText: 'How the billing cycle works ?',
      helpLink: 'https://docs.pepipost.com/docs/how-the-billing-cycle-works'
    }, {
      helpText: 'What if my email credits are exhausted?',
      helpLink: 'https://docs.pepipost.com/docs/what-if-my-email-credits-are-exhausted'
    }, {
      helpText: 'Do I have to pay anything in Forever Free Plan?',
      helpLink: 'https://docs.pepipost.com/docs/do-i-have-to-pay-anything-in-forever-free-plan'
    },
  ];

  key: string;
  event: KeyboardEvent;

  billingEditForm: FormGroup;
  buyClicked = false;
  paymentDetails = false;

  credits_available: any;
  total_available: number;
  earn_credit: number;
  redeemed: boolean;
  autoRedeem = false;
  redeemedDisp: boolean;
  redeemMsg: string;
  redeemMsgType: string;

  editDetails: boolean;
  addedCardList: any;
  primaryCard: string;
  type = '';
  msg = '';

  selectedEntry: any;
  selectedPrice: any;
  editBillingType: string;
  editBillingMsg: string;
  billingInfo: Billing;
  selectedPages: number;
  pagesOptions = [10, 20, 30, 40];
  addCard: boolean;
  // token_id: any;
  makePaymentParam: any;
  charge_param: any;
  addNewCardPaymentType: string;
  addNewCardPaymentMsg: string;
  existingCardPaymentType: string;
  existingCardPaymentMsg: string;
  stripeMakePayment: FormGroup;
  autoRenewApply: FormGroup;
  buyEmailCredits: FormGroup;
  autoRenew = true;
  autoRenewActive = true;
  autoRenewValidateType: string;
  autoRenewValidateMsg: string;
  selectedEmailOption: any;
  custom_buy: boolean;
  // custom_buy: false;
  // selectEmail: boolean;
  selectEmail = true;
  selectEmailWP = true;
  promoCodeType: string;
  promoCodeMsg: string;
  promoSource: any;
  promoCode2x: boolean;
  promoCodeRow2x: boolean;
  promoCode2xOnetime: boolean;
  promoCode3x: boolean;
  promoCodeRow3x: boolean;
  total_emails: any;
  promoCode2xType: string;
  promoCode2xMsg: string;
  promoCode3xType: string;
  promoCode3xMsg: string;
  specialPriceList: any;
  specialPrice: boolean;
  proceedCheck: boolean;
  postPaidClient: boolean;
  postPaidBillingInfo: any;
  postPaidPay: boolean;
  postpaid_payment: any;
  approvedDomain: boolean;
  domainMsg: string;
  domainMsgType: string;
  unlimitedCodeApplied: boolean;
  isStripCardValid: boolean;
  canclePayment: boolean;
  selectedCard: boolean;
  emailSelected: boolean;
  refreshPageSub: Subscription;
  price_wp: any;
  description_wp: any;
  menuSubscription: Subscription;
  falconPrepaidClient: boolean;
  falconPrepaid = true;
  showdiv: boolean;
  subUserClient: boolean;
  subUser = true;

  domainApprovalMsg = `<strong>There is no approved sending domain under your account</strong><br>You won’t be able to recharge until a member of our team has reviewed your domain and approved it for sending.`;
  undefinedText = UNDEFINED_TEXT;

  @ViewChild(StripeCardComponent) card: StripeCardComponent;

  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };


  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  constructor(private http: Http,
              private router: Router,
              private route: ActivatedRoute,
              private apiService: ApiService,
              private alertsService: AlertsService,
              private fb: FormBuilder,
              private menuService: MenuService,
              private stripeService: StripeService) {
    this.selectedPages = this.pagesOptions[1];
    this.menuSubscription = menuService.falconPrepaid$.subscribe(bool => this.falconPrepaid = bool);
    this.menuSubscription = menuService.subUser$.subscribe(bool => this.subUser = bool);
  }

  ngOnInit() {
    this.getPlanType();
    this.checkFalconPrepaidClient();
    this.checkSubUser();

    // Form validation for profile information on edit
    this.billingEditForm = this.fb.group({
      bill_name: ['', Validators.required],
      phone_number: ['', Validators.required],
      company_name: ['', Validators.required],
      bill_address: ['', Validators.required],
      bill_tax_id: ['', Validators.required]
    });
    this.getSpecialPrice();
    this.getbillingdetails();
    this.getAddedCards();
    this.creditMangement();
    this.fetchAutoRenewDetails();
    // this.getPromoCodeDetails();
    this.promoCodeCheck();
    this.getSpecialPrice();
    this.domainCheck();
    this.checkUnlimitedCodeApplied();

    this.autoRenewApply = this.fb.group({
      amount: ['', [Validators.required]],
      threshold: ['', [Validators.required]]
    });


    this.buyEmailCredits = this.fb.group({
      pay_amount: ['', [Validators.required]],
      total_email_credits: ['', [Validators.required]]
    });

  }

  checkFalconPrepaidClient() {
    console.log('here' + this.falconPrepaid);
    if (this.falconPrepaid === true) {
      // console.log('here inside');
      this.falconPrepaidClient = true;
    }
  }

  checkSubUser() {
    if (this.subUser === true) {
      this.subUserClient = true;
    }
  }

  onBuyClick() {
    // console.log('onBuyClick');
    // console.log('domain check ==' + this.approvedDomain);
    if (this.approvedDomain === false) {
      this.domainMsg = this.domainApprovalMsg;
      this.domainMsgType = 'danger';
      return;
    }
    // console.log('this.buyClicked ==>' + this.buyClicked);
    this.buyClicked = true;
    // console.log('this.buyClicked  ==>' + this.buyClicked);
  }

  onRedeemClick() {
    // this.redeemed = true;
    this.apiService.post(REDEEM_OPEN_CREDITS,
      ({'available_credits': this.credits_available, 'redeem_credits': this.earn_credit}))
      .subscribe(result => {
        if (result['status'].toLowerCase() === 'success') {
          this.redeemMsg = 'Credits redeemed successfully!';
          this.redeemMsgType = 'info';
          this.redeemed = true;
          this.fetchCreditAvailable();
          this.fetchEarnEmailCredit();
        } else {
          this.redeemMsg = 'Failed to redeem Credits';
          this.redeemMsgType = 'danger';
          this.redeemed = false;
        }
      });
  }

  onPayClick() {
    // Validate Auto renew amount and threshold
    // console.log(this.buyEmailCredits.get('pay_amount'));
    // console.log('Pay Amount ===>' + JSON.stringify(this.buyEmailCredits));
    // console.log('Function: onPayClick | amount' + this.buyEmailCredits.value.pay_amount);

    // console.log('onBuyClick');
    // console.log('domain check ==' + this.approvedDomain);
    if (this.approvedDomain === false) {
      this.domainMsg = this.domainApprovalMsg;
      this.domainMsgType = 'danger';
      return;
    }
    // console.log('this.buyClicked ==>' + this.buyClicked);
    this.buyClicked = true;
    // console.log('this.buyClicked  ==>' + this.buyClicked);


    if (this.buyEmailCredits.value.pay_amount < 10) {
      this.autoRenewValidateType = 'danger';
      this.autoRenewValidateMsg = 'Recharge amount can not be less then $10';
      return;
    }

    this.total_emails = this.buyEmailCredits.value.total_email_credits;

    // 2x offer applied
    if ((this.promoCode2x === true && this.buyEmailCredits.value.pay_amount >= 100) || this.promoCode2xOnetime) {
      this.total_emails = this.buyEmailCredits.value.total_email_credits * 2;
      this.promoCodeRow2x = true;
    } else {
      this.promoCodeRow2x = false;
    }

    // 3x offer applied
    if (this.promoCode3x === true && this.buyEmailCredits.value.pay_amount >= 100) {
      this.total_emails = this.buyEmailCredits.value.total_email_credits * 3;
      this.promoCodeRow3x = true;
    } else {
      this.promoCodeRow3x = false;
    }


    if (this.autoRenewActive === true && this.autoRenew === false) {
      // console.log('Validate Auto renew' + this.autoRenewActive);
      // console.log(this.autoRenewApply.value);
      // console.log(this.buyEmailCredits.value);
      this.apiService.post(VALIDATE_AUTO_RENEW, this.autoRenewApply.value)
        .subscribe(result => {
            // console.log(result);
            if (result['status'] === 'success') {
              this.autoRenewValidateType = 'info';
              // this.autoRenewValidateMsg = result['message'];
              this.paymentDetails = true;
            } else {
              this.autoRenewValidateType = 'danger';
              this.autoRenewValidateMsg = result['error_info']['message'];
            }
          }, error => {
            this.autoRenewValidateType = 'warning';
            this.autoRenewValidateMsg = WARNING_MSG;
          }
        );
    } else {
      this.paymentDetails = true;
    }
  }

  editAmount() {
    this.paymentDetails = false;
    this.autoRenewValidateMsg = '';
  }

  editDetailsClick() {
    this.editDetails = true;
    this.billingEditForm.setValue({
      bill_name: this.billingInfo.bill_name,
      phone_number: this.billingInfo.phone_number,
      company_name: this.billingInfo.company_name,
      bill_address: this.billingInfo.bill_address,
      bill_tax_id: this.billingInfo.bill_tax_id
    });
  }

  onCancelClick() {
    this.editDetails = false;
    this.editBillingType = '';
    this.editBillingMsg = '';
  }

// fetch billing information
  getbillingdetails() {
    this.apiService.post(GET_BILLING_DETAILS, '')
      .subscribe(result => {
          // console.log(result);
          if (result['status'].toLowerCase() === 'success') {
            this.billingInfo = result['data'];
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

  htmlTagCheck(value) {
    const rgx = new RegExp('<(\\"[^\\"]*\\"|\'[^\']*\'|[^\'\\">])*>');
    if (rgx.test(value)) {
      // your logic goes here.
      // console.log('here in' + value);
      return false;
    }
  }


// Save billing info
  saveBillingInfo() {
    this.billingInfo = Object.assign(this.billingInfo, this.billingEditForm.value);
    // console.log(this.billingInfo);
    const name_validate = this.htmlTagCheck(this.billingInfo.bill_name);
    if (name_validate === false) {
      // console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The bill name should not contain HTML tags';
      return;
    }

    const company_name_validate = this.htmlTagCheck(this.billingInfo.company_name);
    if (company_name_validate === false) {
      // console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The company name should not contain HTML tags';
      return;
    }

    const bill_address_validate = this.htmlTagCheck(this.billingInfo.bill_address);
    if (bill_address_validate === false) {
      // console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The bill address should not contain HTML tags';
      return;
    }

    const bill_tax_id_validate = this.htmlTagCheck(this.billingInfo.bill_tax_id);
    if (bill_tax_id_validate === false) {
      // console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The tax id should not contain HTML tags';
      return;
    }

    const phone_number_validate = this.htmlTagCheck(this.billingInfo.phone_number);
    if (phone_number_validate === false) {
      // console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The phone number should not contain HTML tags';
      return;
    }
    this.apiService.post(SAVE_BILLING_DETAILS, this.billingInfo)
      .subscribe(result => {
          // console.log(result);
          if (result['status'] === 'success') {
            this.editBillingType = 'info';
            this.editBillingMsg = result['message'];
          } else {
            this.editBillingType = 'danger';
            this.editBillingMsg = result['error_info']['message'];
          }
          this.editDetails = false;
          this.editBillingType = '';
          this.editBillingMsg = '';
        }, error => {
          this.editBillingType = 'warning';
          this.editBillingMsg = WARNING_MSG;
        }
      );
  }

// Add new card and do payment
  addNewCard() {
    // console.log('here : Add new card and make payment');
    this.addCard = true;
    this.canclePayment = true;
    // this.makePrimaryCardType = '';
    // this.makePrimaryCardMsg = '';
    return false;
  }

  cancelAddNewCard() {
    this.addCard = false;
    this.selectedCard = false;
    // this.addNewCardType = '';
    // this.addNewCardMsg = '';
  }

  saveNewCardAndPayment() {
    // console.log('here : save new cars and do payment');
  }

  getAddedCards() {
    this.apiService.post(RETRIVE_CREDIT_CARD_DETAILS, '')
      .subscribe(result => {
          // console.log('=======>1' + result['status']);
          if (result['status'].toLowerCase() === 'success') {
            // console.log('data==>' + result['data']['data']);
            if (result['data']['data'] === 'null') {
              // console.log('Here Inside null');
              this.addCard = true;
              this.canclePayment = false;
            }
            this.primaryCard = result['data']['default_source'];
            this.addedCardList = result['data']['sources']['data'];
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

  onSelectionChange(entry) {
    // clone the object for immutability
    this.selectedEntry = Object.assign({}, this.selectedEntry, entry);
    this.selectedCard = true;
    // console.log('selected radio' + entry);
    // console.log('selected radio' + JSON.stringify(this.selectedEntry));
  }

// Credit management onload and onclik fetching data
  creditMangement() {
    this.fetchCreditAvailable();
    this.fetchEarnEmailCredit();
  }

// fetch Email credits available working
  fetchCreditAvailable() {
    this.apiService.post(GET_EMAIL_CREDITS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.credits_available = Number(result['data']['credits_available']).toFixed(0);
            this.total_available = result['data']['total_available'];
            this.showdiv = true;
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

// fetch Earned email credits working
  fetchEarnEmailCredit() {
    this.apiService.post(GET_AVAILABLE_EMAIL_CREDITS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.earn_credit = result['data']['opens_available'];
            // for auto redem checkbox value
            this.autoRedeem = result['data']['auto_reedem_active'] === 0 ? false : true;
            this.redeemedDisp = this.earn_credit === 0 ? true : false;
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

// fetch recurring details
// fetch Earned email credits working
  fetchAutoRenewDetails() {
    this.apiService.post(GET_AUTORENEW_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // this.autorenew_active = result['data']['active'];
            // for auto redeem checkbox value
            this.autoRenew = +result['data']['active'] === 1 ? true : false;

            this.autoRenewApply.setValue({
              amount: result['data']['amount'],
              threshold: result['data']['threshold']
            });

          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }, error => {
          this.type = 'warning';
          this.msg = WARNING_MSG;
        }
      );
  }

// Auto renew status update change working
  autoRenewStatus(e) {
    this.autoRenewActive = e ? true : false;
    // console.log('autoRenewStatus' + this.autoRenewActive);
  }

  selectEmailCreditWP(evt) {
    // console.log('here' + evt.target.value.toLowerCase());
    this.selectEmailWP = false;
    if (evt.target.value.toLowerCase() === 'select') {
      this.proceedCheck = false;
      this.selectEmailWP = true;
      return;
    }

    this.price_wp = evt.target.value.split('||')[0];
    // console.log('here ==' + this.price_wp);

    this.description_wp = evt.target.value.split('||')[1];
    // console.log('here ==' + this.description_wp);

    this.selectedPrice = this.price_wp;
    this.buyEmailCredits.setValue({
      pay_amount: this.price_wp,
      total_email_credits: 'UNLIMITED'
    });
    this.proceedCheck = true;
  }

  selectEmailCredit(evt) {
    // console.log('here' + evt.target.value.toLowerCase());
    this.selectEmail = false;
    this.emailSelected = true;
    this.selectedEmailOption = evt.target.value.toLowerCase();
    // console.log('Selected Option ==> ' + this.selectedEmailOption);
    if (this.selectedEmailOption === 'select') {
      this.custom_buy = false;
      this.selectEmail = true;
      this.emailSelected = false;

      this.buyEmailCredits.setValue({
        pay_amount: '',
        total_email_credits: ''
      });
      return;
    }

    if (this.selectedEmailOption === 'custom') {
      this.custom_buy = true;
      this.buyEmailCredits.setValue({
        pay_amount: '',
        total_email_credits: ''
      });
      this.autoRenewApply.setValue({
        amount: 10,
        threshold: 90
      });
    } else {
      this.custom_buy = false;
      this.buyEmailCredits.setValue({
        pay_amount: 0.0002 * this.selectedEmailOption,
        total_email_credits: this.selectedEmailOption
      });

      this.autoRenewApply.setValue({
        amount: 0.0002 * this.selectedEmailOption,
        threshold: 90
      });

    }

  }

  customAmount() {
    // console.log('here customAmount ==> ' + this.buyEmailCredits.value.pay_amount);
    this.autoRenewApply.setValue({
      amount: this.buyEmailCredits.value.pay_amount,
      threshold: 90
    });

    this.buyEmailCredits.setValue({
      pay_amount: this.buyEmailCredits.value.pay_amount,
      total_email_credits: this.buyEmailCredits.value.pay_amount / 0.0002
    });
  }

  customEmail() {
    // console.log('here customAmount' + this.buyEmailCredits.value.total_email_credits);
    this.autoRenewApply.setValue({
      amount: 0.0002 * this.buyEmailCredits.value.total_email_credits,
      threshold: 90
    });

    this.buyEmailCredits.setValue({
      pay_amount: 0.0002 * this.buyEmailCredits.value.total_email_credits,
      total_email_credits: this.buyEmailCredits.value.total_email_credits
    });

  }

  // fetch recurring details
  // fetch Earned email credits working
  promoCodeCheck() {
    // check 2x code is applied and active or not
    this.apiService.post(CHECK_PRPMO_CODE, {'coupon_id': 47})
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // this.autoRenew = result['data']['active'] === 1 ? true : false;
            // console.log('code_active =======' + result['data']['code_active']);
            // console.log(result['data']);
            if (+result['data']['code_active'] === 1) {
              this.promoCode2x = true;
              this.promoCode2xType = 'secondary';
              this.promoCode2xMsg = 'Recharge with amount $100 or more to get 2x email credits';
            }
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );

    this.apiService.post(CHECK_PRPMO_CODE, {'coupon_id': 55})
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // this.autoRenew = result['data']['active'] === 1 ? true : false;
            // console.log('code_active =======' + result['data']['code_active']);
            // console.log(result['data']);
            if (+result['data']['code_active'] === 1) {
              this.promoCode2x = true;
              this.promoCode2xOnetime = true;
              this.promoCode2xType = 'secondary';
              this.promoCode2xMsg = 'You will get 2X email credit on your first recharge.';
            }
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );


    // check 3x code is applied and active or not
    this.apiService.post(CHECK_PRPMO_CODE, {'coupon_id': 7})
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // this.autoRenew = result['data']['active'] === 1 ? true : false;
            // console.log('code_active =======' + result['data']['code_active']);
            // console.log(result['data']);
            if (+result['data']['code_active'] === 1) {
              this.promoCode3x = true;
              this.promoCode3xType = 'secondary';
              this.promoCode3xMsg = 'Recharge with amount $100 or more to get 3x email credits';
            }
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  // Make payment from added card
  makePayment() {
    // console.log('here : make payment');
    // console.log('selected radio' + JSON.stringify(this.selectedEntry));
    // console.log('selected card ===>' + JSON.stringify(this.selectedEntry.id));

    // this.charge_param = {'card': JSON.stringify(this.selectedEntry.id)};
    this.charge_param = {'card': this.selectedEntry.id, 'amount': this.buyEmailCredits.value.pay_amount};
    if (this.autoRenewActive === true && this.autoRenew === false) {
      this.charge_param = {
        'card': this.selectedEntry.id,
        'amount': this.buyEmailCredits.value.pay_amount,
        'renew_amount': this.autoRenewApply.value.amount,
        'renew_threshold': this.autoRenewApply.value.threshold
      };
    }
    if (this.postpaid_payment === 1) {
      this.charge_param = {
        'card': this.selectedEntry.id,
        'amount': this.buyEmailCredits.value.pay_amount,
        'postpaid_payment': 1
      };
    }
    // console.log('Buy credits');
    // console.log('old auto renewal status' + this.autoRenew);
    // console.log('autoRenewStatus === ' + this.autoRenewActive);
    // console.log(this.autoRenewApply.value);
    // console.log(this.charge_param);

    this.apiService.post(MAKE_PAYMENT_EXISTING_CARD, this.charge_param)
      .subscribe(result_val => {
          // console.log(result_val);
          if (result_val['status'] === 'success') {
            this.existingCardPaymentType = 'info';
            this.existingCardPaymentMsg = result_val['message'];
            this._redirectToHistory();
          } else {
            this.existingCardPaymentType = 'danger';
            this.existingCardPaymentMsg = result_val['error_info']['message'];
          }
        }, error => {
          this.existingCardPaymentType = 'warning';
          this.existingCardPaymentMsg = WARNING_MSG;
        }
      );
  }

  stripeCardStatusChanges(status) {
    this.isStripCardValid = 'VALID' === status;
  }

  // Make payment from new card
  buy() {
    // console.log('Buy Function');
    const name = this.card.getName();
    this.stripeService
      .createToken(this.card.getCard(), {name})
      .subscribe(result => {
        if (result.token) {
          // console.log('Buy Function result');
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          // console.log('Stripe Token => ' + result.token.id);
          // console.log('Stripe Token => ' + result.token.id);
          // renew_amount , renew_amount, renew_threshold


          this.makePaymentParam = {'token_id': result.token.id, 'amount': this.buyEmailCredits.value.pay_amount};
          if (this.autoRenewActive === true && this.autoRenew === false) {
            this.makePaymentParam = {
              'token_id': result.token.id,
              'amount': this.buyEmailCredits.value.pay_amount,
              'renew_amount': this.autoRenewApply.value.amount,
              'renew_threshold': this.autoRenewApply.value.threshold,
              'addCard': 1
            };
          }

          if (this.postpaid_payment === 1) {
            this.makePaymentParam = {
              'token_id': result.token.id,
              'amount': this.buyEmailCredits.value.pay_amount,
              'postpaid_payment': 1
            };
          }
          // console.log('Buy credits');
          // console.log('old auto renewal status' + this.autoRenew);
          // console.log('autoRenewStatus === ' + this.autoRenewActive);
          // console.log(this.autoRenewApply.value);
          // console.log(this.makePaymentParam);
          // Call API for making payment
          this.apiService.post(MAKE_PAYMENT, this.makePaymentParam)
            .subscribe(result_val => {
                // console.log(result_val);
                if (result_val['status'] === 'success') {
                  this.addNewCardPaymentType = 'info';
                  this.addNewCardPaymentMsg = result_val['message'];
                  this._redirectToHistory();
                } else {
                  this.addNewCardPaymentType = 'danger';
                  this.addNewCardPaymentMsg = result_val['error_info']['message'];
                }
              }, error => {
                this.addNewCardPaymentType = 'warning';
                this.addNewCardPaymentMsg = WARNING_MSG;
              }
            );
        } else if (result.error) {
          // Error creating the token
          // console.log('Error' + result.error.message);
          this.addNewCardPaymentType = 'warning';
          this.addNewCardPaymentMsg = result.error.message;
        }
      });
  }

  _redirectToHistory() {
    this.alertsService.refreshPage();

    setTimeout(() => {
      this.router.navigate(['../history'], {relativeTo: this.route});
    }, 3000);
  }


  redirectToLogout(url) {
    // redirect to logout page
    // Fix: https://stackoverflow.com/questions/34338440/how-to-redirect-to-an-external-url-in-angular2/34340883
    this.router.navigate(['/']).then(result => {
      window.location.href = url;
    });
  }


  // redirectToHistory() {
  //   setTimeout(() => {
  //     this.redirectToLogout('/app/billings/details/history');
  //   }, 3000);
  // }


  // Get all added credit cards
  getSpecialPrice() {
    // this.apiService.post(RETRIVE_CREDIT_CARD_DETAILS, '')
    this.apiService.post(GET_SPECIAL_PRICING, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // console.log('getSpecialPrice');
            // console.log(result['data']);
            this.specialPrice = false;
            if (result['data'] !== null) {
              // this.primaryCard = result['data']['default_source'];
              this.specialPriceList = result['data']['data'];
              this.specialPrice = true;
              console.log('sp ==>' + this.specialPrice);
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  onSelectionPrice(entry) {
    this.selectedPrice = Object.assign({}, this.selectedPrice, entry);
    //   console.log('selected radio' + entry);
    //   console.log('selected radio' + JSON.stringify(this.selectedPrice));
    //  console.log('selected radio amount' + JSON.stringify(this.selectedPrice.offer_value));

    this.proceedCheck = true;
  }

  onProceedClick() {
    if (this.approvedDomain === false) {
      this.domainMsg = this.domainApprovalMsg;
      this.domainMsgType = 'danger';
      return;
    }
    // console.log('Function: onProceedClick | amount' + this.buyEmailCredits.value.pay_amount);
    this.autoRenewActive = false;
    this.paymentDetails = true;

    // this.buyEmailCredits.setValue({
    //  pay_amount: this.selectedPrice.offer_value,
    //   total_email_credits: 'UNLIMITED'
    // });

    this.buyEmailCredits.setValue({
      pay_amount: this.selectedPrice,
      total_email_credits: 'UNLIMITED'
    });
    this.total_emails = 'UNLIMITED';
  }

  getPlanType() {
    this.apiService.post(GET_PLAN_TYPE, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // console.log('get plan type');
            // console.log(result['data']);
            if (result['data'] !== 'null') {
              // this.primaryCard = result['data']['default_source'];
              // console.log('Client type ==>' + result['data']['client_type']);
              if (result['data']['client_type'] === 'postpaid') {
                this.postPaidClient = true;
                // this.specialPrice = true;
                this.getPostPaidDetails();
              }
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  getPostPaidDetails() {
    this.apiService.post(GET_POSTPAID_BILLING_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // console.log('getPostPaidDetails');
            // console.log(result['data']);
            if (result['data'] !== 'null') {
              this.postPaidBillingInfo = result['data']['postpaid_data'];
              if (this.postPaidBillingInfo.last_month_usages > 0) {
                this.postPaidPay = true;
              }
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  onProceedPostpaid() {
    // console.log('amount to be paid ' + this.postPaidBillingInfo.price * this.postPaidBillingInfo.last_month_usages);
    this.autoRenewActive = false;
    this.paymentDetails = true;

    this.buyEmailCredits.setValue({
      pay_amount: this.postPaidBillingInfo.price * this.postPaidBillingInfo.last_month_usages,
      total_email_credits: this.postPaidBillingInfo.last_month_usages
    });
    this.total_emails = this.postPaidBillingInfo.last_month_usages;

    this.postpaid_payment = 1;

  }

  domainCheck() {
    this.apiService.post(DOMAIN_APPROVE_CHECK, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // console.log('approved domain' + result['data']['approval']);
            // console.log(result['data']['approval']);
            if (result['data']['approval'] > 0) {
              this.approvedDomain = true;
            } else {
              this.approvedDomain = false;
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  checkUnlimitedCodeApplied() {
    // this.unlimitedCodeApplied = true;
    this.apiService.post(CHECK_UNLIMITED_CODE_APPLIED, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // console.log('Unlimited code applied');
            // console.log(result['data']['unlimited_plan']);
            // console.log(result['data']['approval']);
            if (result['data']['unlimited_plan'] === 1) {
              // console.log('Unlimited code applied');
              this.unlimitedCodeApplied = true;
              this.buyClicked = true;
              this.redeemedDisp = true;
            } else {
              this.unlimitedCodeApplied = false;
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

}
