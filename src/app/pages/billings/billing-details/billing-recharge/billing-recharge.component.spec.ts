import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingRechargeComponent } from './billing-recharge.component';

describe('BillingRechargeComponent', () => {
  let component: BillingRechargeComponent;
  let fixture: ComponentFixture<BillingRechargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingRechargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingRechargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
