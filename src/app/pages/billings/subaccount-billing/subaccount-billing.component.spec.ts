import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubaccountBillingComponent } from './subaccount-billing.component';

describe('SubaccountBillingComponent', () => {
  let component: SubaccountBillingComponent;
  let fixture: ComponentFixture<SubaccountBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubaccountBillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubaccountBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
