import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {ApiService} from '../../services/api.service';
import {GTE_SUBACCOUNT_BILLING_DETAILS} from '../../../config';
import {addCommasToNum} from '../../components/utils/number-utils';

@Component({
  selector: 'app-subaccount-billing',
  templateUrl: './subaccount-billing.component.html',
  styleUrls: ['./subaccount-billing.component.scss']
})
export class SubaccountBillingComponent implements OnInit {
  credit_remaining = 'Unlimited';
  credit_type: any;
  recurringDetails: any;
  showRecurringDetails = false;

  constructor(private http: Http,
              private apiService: ApiService) {
  }

  ngOnInit() {
    this.fetchBillingDetails();
  }

  fetchBillingDetails() {
    this.apiService.get(GTE_SUBACCOUNT_BILLING_DETAILS)
      .subscribe(result => {
        if (result['status'] === 'success') {
          this.credit_remaining = result['data']['credit_type'].toLowerCase() !== 'fixed' ? 'Unlimited' : addCommasToNum(result['data']['credit_remaining']);
          this.credit_type = result['data']['credit_type'].toLowerCase() === 'fixed' ? 'One Time' : 'Unlimited';
        }
        if (result['data']['recurring']['status'] === 1) {
          this.showRecurringDetails = true;
          this.recurringDetails = result['data']['recurring'];
        } else {
          this.showRecurringDetails = false;
        }

      });
  }
}
