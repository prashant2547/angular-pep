import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {addCommasToNum} from '../../components/utils/number-utils';
import {saveData} from '../../components/utils/savedata';
import {ALL_SUBACCOUNT_CREDIT_HISTORY, DOWNLOAD_ACCOUNT_LOG, DOWNLOAD_USAGE_SUMMARY, NO_DATA_FOUND} from '../../../config';
import {Http} from '@angular/http';
import {DaterangepickerConfig} from 'ng2-daterangepicker';
import {ApiService} from '../../services/api.service';
import {TableDataSource} from '../../services/table.data-source';
import {environment} from '../../../../environments/environment';
import {TableSubaccountAmountstatusComponent} from '../../components/table/table-subaccount-amountstatus/table-subaccount-amountstatus.component';


@Component({
  selector: 'app-account-log',
  templateUrl: './account-log.component.html',
  styleUrls: ['./account-log.component.scss']
})
export class AccountLogComponent implements OnInit {

  dateInput = {
    start: moment().subtract(7, 'days'),
    end: moment()
  };
  selectedDateRange = [moment(this.dateInput.start).format('YYYY-MM-DD'),
    moment(this.dateInput.end).format('YYYY-MM-DD')];
  filterValues = [];
  tempDownloadReport = [];
  netCredit = 0;
  accountLogTabelSource: any;
  accountLogSettings = {
    attr: {
      class: 'table'
    },
    columns: {
      entered: {
        title: 'Date',
        sort: false,
      },
      type: {
        title: 'Type of Credit',
        sort: false,

      },
      credit: {
        title: 'Amount Assigned',
        sort: false,
        type: 'custom',
        renderComponent: TableSubaccountAmountstatusComponent
      },
      close_balance: {
        title: 'Total credit',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          if (row.close_balance.toLowerCase() === 'unlimited') {
            return '-';
          } else {
            return addCommasToNum(+row.close_balance);
          }
        }
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
      columnTitle: 'Change Plan'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  constructor(private http: Http,
              private dateRangePickerOptions: DaterangepickerConfig,
              private apiService: ApiService) {
    this.dateRangePickerOptions.settings = {
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(3, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
      }
    };
  }

  ngOnInit() {
    this.fetchcreditActivityLogs();
  }

  selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = [
        moment(dateInput.start).format('YYYY-MM-DD'),
        moment(dateInput.end).format('YYYY-MM-DD')
      ];
      // clear existing date ranges
      const tempFilterValues = this.filterValues
        .map((obj) => {
          if (obj.field !== 'start_date' && obj.field !== 'end_date') {
            return obj;
          }
        });

      this.filterValues = tempFilterValues.filter(n => n);

      // Add new date ranges
      this.filterValues
        .push({field: 'start_date', search: this.selectedDateRange[0]});
      this.filterValues
        .push({field: 'end_date', search: this.selectedDateRange[1]});
      this.accountLogTabelSource
        .setFilter(this.filterValues, false);
    }
  }

  fetchcreditActivityLogs() {
    this.accountLogTabelSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${ALL_SUBACCOUNT_CREDIT_HISTORY}`,
      // endPoint: 'http://localhost:5000/creditActivityLog',
      dataKey: 'creditActivityLog'
    });
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.accountLogTabelSource.setFilter(this.filterValues, false);

    this.accountLogTabelSource.onGetData.subscribe((res) => {
      this.netCredit = res.data.creditActivityLog.netcredit;
    });
  }

  downloaAccountLog() {
    this.tempDownloadReport = [];
    for (let index = 0; index < this.filterValues.length; index++) {
      const element = this.filterValues[index];
      const temp = {};
      temp[element.field] = element.search;
      this.tempDownloadReport.push(temp);
    }

    this.apiService
      .post(DOWNLOAD_ACCOUNT_LOG, ({
        'settings': this.tempDownloadReport,
        'start_date': this.selectedDateRange[0],
        'end_date': this.selectedDateRange[1]
      }))
      .subscribe((result) => {
        saveData(result['data']['creditActivityLog']['data'], result['data']['creditActivityLog']['filename']);
        if (result['status'] === 'success') {
          // console.log('Download started');
        } else {
          alert('Download failed : ' + result['error_info']['message']);
        }
      });
    return false;
  }
}


