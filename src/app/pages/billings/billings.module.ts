import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BillingsRoutingModule} from './billings-routing.module';
import {ComponentsModule} from '../components/components.module';

import {BillingsComponent} from './billings.component';
import {BillingDetailsComponent} from './billing-details/billing-details.component';
import {AddOnsComponent} from './add-ons/add-ons.component';
import {CreditManagementComponent} from './credit-management/credit-management.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from '../components/ng2-smart-table/ng2-smart-table.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Daterangepicker} from 'ng2-daterangepicker';
import {NgxStripeModule} from '../components/payments/ngx-stripe/ngx-stripe';
import {BillingInformationComponent} from './billing-details/billing-information/billing-information.component';
import {BillingHistoryComponent} from './billing-details/billing-history/billing-history.component';
import {BillingRechargeComponent} from './billing-details/billing-recharge/billing-recharge.component';
import {PostpaidBillingComponent} from './postpaid-billing/postpaid-billing.component';
import {PostpaidBillingPaymentComponent} from './postpaid-billing/postpaid-billing-payment/postpaid-billing-payment.component';
import {PostpaidBillingHistoryComponent} from './postpaid-billing/postpaid-billing-history/postpaid-billing-history.component';
import {PostpaidBillingTransactionsComponent} from './postpaid-billing/postpaid-billing-transactions/postpaid-billing-transactions.component';
import {PostpaidCreditManagementComponent} from './postpaid-credit-management/postpaid-credit-management.component';
import {DedicatedIpComponent} from './dedicated-ip/dedicated-ip.component';
import {SubaccountLogComponent} from './subaccount-log/subaccount-log.component';
import {UsageSummaryComponent} from './subaccount-log/usage-summary/usage-summary.component';
import {CreditLogComponent} from './subaccount-log/credit-log/credit-log.component';
import {AccountLogComponent} from './account-log/account-log.component';
import {SubaccountBillingComponent} from './subaccount-billing/subaccount-billing.component';
import {environment} from '../../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    BillingsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    Ng2SmartTableModule,
    NgbModule.forRoot(),
    Daterangepicker,
    NgxStripeModule.forRoot(`${environment.stripe_key}`),
  ],
  declarations: [
    BillingsComponent,
    BillingDetailsComponent,
    AddOnsComponent,
    CreditManagementComponent,
    BillingInformationComponent,
    BillingHistoryComponent,
    BillingRechargeComponent,
    PostpaidBillingComponent,
    PostpaidBillingPaymentComponent,
    PostpaidBillingHistoryComponent,
    PostpaidBillingTransactionsComponent,
    PostpaidCreditManagementComponent,
    DedicatedIpComponent,
    SubaccountLogComponent,
    UsageSummaryComponent,
    CreditLogComponent,
    AccountLogComponent,
    SubaccountBillingComponent,
  ]
})
export class BillingsModule {
}
