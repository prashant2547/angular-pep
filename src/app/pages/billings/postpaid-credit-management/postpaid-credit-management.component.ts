import {Component, OnInit, ViewChild} from '@angular/core';
import {
  GET_AVAILABLE_EMAIL_CREDITS, GET_EMAIL_CREDITS, GET_NEW_SLAB_EMAIL_CREDITS, GET_POSTPAID_REDEEM_HISTORY, GET_REDEEM_HISTORY,
  NO_DATA_FOUND,
  WARNING_MSG
} from '../../../config';
import {addCommasToNum} from '../../components/utils/number-utils';
import * as moment from 'moment';
import {environment} from '../../../../environments/environment';
import {TableDataSource} from '../../services/table.data-source';
import {Http} from '@angular/http';
import {DaterangepickerConfig} from 'ng2-daterangepicker';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-postpaid-credit-management',
  templateUrl: './postpaid-credit-management.component.html',
  styleUrls: ['./postpaid-credit-management.component.scss']
})
export class PostpaidCreditManagementComponent implements OnInit {
  helpSectionData = [
    {
      helpText: 'What is Earned email credits?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-the-open-free-concept'
    },
  ];
  earn_credit: number;
  credits_available: any;
  redimMsg = '';
  autoRedeem = false;
  redeemedDisp = true;
  type = '';
  msg = '';
  total_available: number;
  hideredeemHistory = true;
  hideredeemHistory_hide = true;
  redeemInfo = '';

  settings = {
    attr: {
      class: 'table'
    },
    columns: {
      date: {
        title: 'DateTime',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return (row.date + ' ' + row.time);
        }
      },
      description: {
        title: 'Redeem Method',
        sort: false
      },
      credit_action: {
        title: 'Credits Redeemed',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.credit_action);
        }
      },
      redeem_per_email_cost: {
        title: 'Redeemtation rate/email',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return ('$' + row.redeem_per_email_cost);
        }
      },
      redeem_amount: {
        title: 'Amount',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return ('$' + row.redeem_amount);
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  dataSource: any;
  redeemed: boolean;

  eventLog = '';
  mainInput = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(6, 'month')
  };

  view = [];
  colorSchemeGraph = {
    domain: ['#549fd6']
  };

  @ViewChild('tabBox') tabBox;
  @ViewChild('tabset') tabset;

  constructor(private http: Http,
              private dateRangePickerOptions: DaterangepickerConfig,
              private httpClient: HttpClient,
              private apiService: ApiService) {
    this.dateRangePickerOptions.settings = {
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(4, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
      }
    };

    this.dataSource = new TableDataSource(http,
      {
        endPoint: `${environment.api_url}${GET_POSTPAID_REDEEM_HISTORY}`,
        dataKey: 'redeem_history'
      });

    this.dataSource.onGetData.subscribe((res) => {
      // console.log('Here redeem ===>' + res.data.redeem_history.redeem_history_cnt);
      // console.log(res);
      if (+res.data.redeem_history.redeem_history_cnt === 0) {
        // console.log('here inside');
        this.hideredeemHistory = false;
        this.hideredeemHistory_hide = true;
      } else {
        this.hideredeemHistory = true;
        this.hideredeemHistory_hide = false;
      }
    });
  }

  ngOnInit() {
    this.creditMangement();
    this.redeemHistory();
    // this.fetchCreditManagementGraphData();
  }


  applyDate(e: any) {
    this.eventLog += '\nEvent Fired: ' + e.event.type;
  }


  // Credit management onload and onclik fetching data
  creditMangement() {
    this.fetchNewSlabCredits();
  }

  // redeem history onload and onclik fetching data
  redeemHistory() {
    // console.log('in redeem history');
  }


  fetchNewSlabCredits() {
    this.apiService.post(GET_NEW_SLAB_EMAIL_CREDITS)
      .subscribe(result => {
        if (result['status'] && result['status'].toLowerCase() === 'success') {
          // result['data']['total_email_sent'] = 100250;
          if (+result['data']['total_email_sent'] > +result['data']['allocated_emails']) {
            this.credits_available = 0;
          } else {
            this.credits_available = result['data']['allocated_emails'] - result['data']['total_email_sent'];
          }

          this.total_available = result['data']['allocated_emails'];
          this.earn_credit = result['data']['earned_open_emails'];

          if (result['data']['current_plan_coupon_code'] === 'PLAN-FREE') {
            this.redeemInfo = 'Do you know, you can get cashback on all opened emails in our paid plans? <a href="http://help.pepipost.com/knowledge_base/topics/how-open-free-cashbacks-will-be-calculated" target="_blank">Learn more</a>';
          }else {
            this.redeemInfo = 'Pepipost rewards you for your good sender practices. All the accumulated opened email credits will reflect as cashback in your next invoice.';
          }


        }
      });
  }

  redirectToRedeem() {
    this.tabset.select('redeem');
  }

}
