import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostpaidCreditManagementComponent } from './postpaid-credit-management.component';

describe('PostpaidCreditManagementComponent', () => {
  let component: PostpaidCreditManagementComponent;
  let fixture: ComponentFixture<PostpaidCreditManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostpaidCreditManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostpaidCreditManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
