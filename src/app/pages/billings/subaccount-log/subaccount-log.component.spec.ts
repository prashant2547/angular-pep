import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubaccountLogComponent } from './subaccount-log.component';

describe('SubaccountLogComponent', () => {
  let component: SubaccountLogComponent;
  let fixture: ComponentFixture<SubaccountLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubaccountLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubaccountLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
