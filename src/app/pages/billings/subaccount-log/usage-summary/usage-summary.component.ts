import {Component, OnInit} from '@angular/core';
import {addCommasToNum} from '../../../components/utils/number-utils';
import {Http} from '@angular/http';
import {DaterangepickerConfig} from 'ng2-daterangepicker';
import {ApiService} from '../../../services/api.service';
import {DOWNLOAD_USAGE_SUMMARY, NO_DATA_FOUND, USAGE_SUMMARY} from '../../../../config';
import * as moment from 'moment';
import {TableDataSource} from '../../../services/table.data-source';
import {environment} from '../../../../../environments/environment';
import {saveData} from '../../../components/utils/savedata';

// import {Ng2SmartTableDynamicColumn} from '../../../components/ng2-smart-table/ng2-smart-table-dynamic-column';

@Component({
  selector: 'app-usage-summary',
  templateUrl: './usage-summary.component.html',
  styleUrls: ['./usage-summary.component.scss']
})
export class UsageSummaryComponent implements OnInit {
  searchUsername = '';
  dateInput = {
    start: moment().subtract(7, 'days'),
    end: moment()
  };
  selectedDateRange = [moment(this.dateInput.start).format('YYYY-MM-DD'),
    moment(this.dateInput.end).format('YYYY-MM-DD')];
  filterValues = [];
  tempDownloadReport = [];
  usageSummaryTabelSource: any;
  usageSummarytSettings = {
    attr: {
      class: 'table'
    },
    columns: {
      username: {
        type: 'html',
        title: 'User Name',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return (row.username === 'Grand Total' ? '<b>' + row.username + '</b>' : row.username);
        }
      },
      Requests: {
        title: 'Requests',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Requests);
        }
      },
      Delivered: {
        title: 'Delivered',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Delivered);
        }
      },
      Opened: {
        title: 'Opened',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Opened);
        }
      },
      Clicked: {
        title: 'Clicked',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Clicked);
        }
      },
      Unsubscribes: {
        title: 'Unsubscribed',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Unsubscribes);
        }
      },
      Bounces: {
        title: 'Bounces',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Bounces);
        }
      },
      'Spams Reports': {
        title: 'Spam Report',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row['Spams Reports']);
        }
      },
      Dropped: {
        title: 'Drops',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Dropped);
        }
      },
      Invalid: {
        title: 'Invalids',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.Invalid);
        }
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
      columnTitle: 'Change Plan'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      display : false,
      showSelectPage: false,
    }
  };

  constructor(private http: Http,
              private dateRangePickerOptions: DaterangepickerConfig,
              private apiService: ApiService) {
    this.dateRangePickerOptions.settings = {
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(3, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
      }
    };
  }

  ngOnInit() {
    this.fetchUserSummary();
  }

  selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = [
        moment(dateInput.start).format('YYYY-MM-DD'),
        moment(dateInput.end).format('YYYY-MM-DD')
      ];
      // clear existing date ranges
      const tempFilterValues = this.filterValues
        .map((obj) => {
          if (obj.field !== 'start_date' && obj.field !== 'end_date') {
            return obj;
          }
        });

      this.filterValues = tempFilterValues.filter(n => n);

      // Add new date ranges
      this.filterValues
        .push({field: 'start_date', search: this.selectedDateRange[0]});
      this.filterValues
        .push({field: 'end_date', search: this.selectedDateRange[1]});
      this.usageSummaryTabelSource
        .setFilter(this.filterValues, false);
    }
  }

  onSearch(query: string) {
    this.searchUsername = query;
    this.filterValues = [];
    this.filterValues.push({field: 'username', search: query});
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.usageSummaryTabelSource.setFilter(this.filterValues, false);
  }

  fetchUserSummary() {
    this.usageSummaryTabelSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${USAGE_SUMMARY}`,
      dataKey: 'usage_summary'
    });
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.usageSummaryTabelSource.setFilter(this.filterValues, false);
  }

  downloadusageSummary() {
    this.tempDownloadReport = [];
    for (let index = 0; index < this.filterValues.length; index++) {
      const element = this.filterValues[index];
      const temp = {};
      temp[element.field] = element.search;
      this.tempDownloadReport.push(temp);
    }

    this.apiService
      .post(DOWNLOAD_USAGE_SUMMARY, ({
        'settings': this.tempDownloadReport,
        'start_date': this.selectedDateRange[0],
        'end_date': this.selectedDateRange[1],
        'username': this.searchUsername
      }))
      .subscribe((result) => {
        saveData(result['data']['usage_summary']['data'], result['data']['usage_summary']['filename']);
        if (result['status'] === 'success') {
          // console.log('Download started');
        } else {
          alert('Download failed : ' + result['error_info']['message']);
        }
      });
    return false;
  }
}
