import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostpaidBillingComponent } from './postpaid-billing.component';

describe('PostpaidBillingComponent', () => {
  let component: PostpaidBillingComponent;
  let fixture: ComponentFixture<PostpaidBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostpaidBillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostpaidBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
