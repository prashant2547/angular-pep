import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostpaidBillingPaymentComponent } from './postpaid-billing-payment.component';

describe('PostpaidBillingPaymentComponent', () => {
  let component: PostpaidBillingPaymentComponent;
  let fixture: ComponentFixture<PostpaidBillingPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostpaidBillingPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostpaidBillingPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
