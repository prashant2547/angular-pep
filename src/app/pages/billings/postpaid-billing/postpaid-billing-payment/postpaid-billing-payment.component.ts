import {AfterContentInit, Component, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {
  ADD_NEW_CARD, ADD_NEW_CARD_AND_MAKE_PRIMARY, ADD_NEW_CARD_CHARGE_PENDING_BILL, ADD_NEWCARD_CHARGE_AND_MAKE_PRIMARY, APPLY_COUPON,
  DELETE_CARD, GET_APPLIED_COUPON_CODE,
  GET_AUTORENEW_DETAILS,
  GET_BILLING_DETAILS, GET_CURRENT_PLAN,
  GET_PLAN_TYPE,
  GET_SLAB_PLAN,
  GET_SPECIAL_PRICING, GET_UPGRADE_PLAN_DETAILS,
  MAKE_PRIMARY_CARD, MAKE_PRIMARY_CARD_CHARGE_UNPAID_BILL, NEW_CHARGE_PRIMARY_CARD, NO_DATA_FOUND,
  RETRIVE_CREDIT_CARD_DETAILS,
  SAVE_BILLING_DETAILS, SEND_CUSTOM_PRICE_REQUEST, UNDEFINED_TEXT,
  UPDATE_AUTO_RENEW,
  UPDATE_AUTO_RENEW_STATUS, UPDATE_DOWNGRADE_REQUEST, WARNING_MSG
} from '../../../../config';
import {PostpaidBilling} from '../postpaid-billing-model';
import {ElementsOptions, StripeService} from '../../../components/payments/ngx-stripe/ngx-stripe';
import {StripeCardComponent} from '../../../components/payments/stripe-card/stripe-card.component';
import {Http} from '@angular/http';
import {ApiService} from '../../../services/api.service';
import {environment} from '../../../../../environments/environment';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TableDataSource} from '../../../services/table.data-source';
import {CouponCodeStateComponent} from '../../../components/table/coupon-code-state/coupon-code-state.component';
import {TablePlanStatusComponent} from '../../../components/table/table-plan-status/table-plan-status.component';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TableCurrentPlanChangeComponent} from '../../../components/table/table-current-plan-change/table-current-plan-change.component';
import {abbreviateNumber, addCommasToNum} from '../../../components/utils/number-utils';
import {EMAIL} from '../../../components/forms/form-validation';
import {AlertsService} from '../../../services/alerts.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
  // selector: 'app-billing-information',
  // templateUrl: './billing-information.component.html',
  // styleUrls: ['./billing-information.component.scss']

  selector: 'app-postpaid-billing-payment',
  templateUrl: './postpaid-billing-payment.component.html',
  styleUrls: ['./postpaid-billing-payment.component.scss']

})

// export class BillingInformationComponent implements OnInit {
export class PostpaidBillingPaymentComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'How are credits calculated?',
      helpLink: 'https://docs.pepipost.com/docs/how-are-credits-calculated'
    }, {
      helpText: 'Do unused emails roll over into the next month?',
      helpLink: 'https://docs.pepipost.com/docs/do-unused-emails-roll-over-into-the-next-month'
    }, {
      helpText: 'What happens if I go over my plan’s monthly email limit?',
      helpLink: 'https://docs.pepipost.com/docs/what-happens-if-i-go-over-my-plans-monthly-email-limit'
    }, {
      helpText: 'What is Overage and how it is calculated?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-overage-and-how-it-is-calculated'
    }, {
      helpText: 'How does the refund works?',
      helpLink: 'https://docs.pepipost.com/docs/how-does-the-refund-works'
    }, {
      helpText: 'Why am I getting Billing failed notifications?',
      helpLink: 'https://docs.pepipost.com/docs/why-am-i-getting-billing-failed-notifications'
    }, {
      helpText: 'How to Cancel my paid account?',
      helpLink: 'https://docs.pepipost.com/docs/how-to-cancel-my-paid-account'
    }, {
      helpText: 'More FAQ\'s',
      helpLink: 'https://docs.pepipost.com/docs/billing-and-pricing'
    }
  ];
  // ngAfterContentInit(): void {
  //   console.group('cards');
  //   console.info('this.card', this.card);
  //   console.info('this.card2', this.card1);
  //   console.groupEnd('cards');
  // }
  billingEditForm: FormGroup;
  autoRenewApply: FormGroup;
  promoCodeApply: FormGroup;
  billingInfo: PostpaidBilling;
  addedCardList: any;
  type = '';
  msg = '';
  editBillingType: string;
  editBillingMsg: string;
  promoCodeType: string;
  promoCodeMsg: string;
  primaryCard: string;
  card_id: any;
  token_id: any;
  charge_param: any;
  makePrimaryCardType: string;
  makePrimaryCardMsg: string;
  addNewCardType: string;
  addNewCardMsg: string;
  addNewCardTypeNew1: string;
  addNewCardMsgNew1: string;
  addNewCardTypeNew2: string;
  addNewCardMsgNew2: string;
  addNewCardTypeNew3: string;
  addNewCardMsgNew3: string;
  addNewCardTypeNew4: string;
  addNewCardMsgNew4: string;
  autoRenewType: string;
  autoRenewMsg: string;
  autoRenewSaveType: string;
  autoRenewSaveMsg: string;
  autoRenewEditMode = false;
  autoRenew: boolean;
  promoSource: any;
  planSource: any;
  currentPlanSource: any;
  postpaidPlan: boolean;
  specialPrice: boolean;
  undefinedText = UNDEFINED_TEXT;
  popRef: NgbActiveModal;
  upgradeForm: FormGroup;
  customPriceForm: FormGroup;
  rowData: any;
  upgradePlanInfo: any;
  cardAdded = false;
  showCardAdd = false;
  rowCurrentPlanData: any;
  showAllPlans = false;
  showPlanDeatils = false;
  showCurrentPlan = false;
  isStripCardValid: boolean;
  newPlanInfo = '';
  upgradeSuccessInfo = '';
  pieCredits = [];
  downgrade_planid: any;
  showCardDowngrade = false;
  showDowngradePlanDeatils = false;
  card_status: any;
  downgradeRequestActive  = false;
  unpaidBillInfo = '';
  unpaidBillActive = false;
  upgradeSuccessActive  = false;
  showCard = false;
  upgrade_plan_amount: any;
  chargeAmount: any;
  upgradePlanId: any;
  custom_param: any;
  hideOverageSection = false;
  current_date: any;
  addNewCardAlertType: string;
  addNewCardAlertMsg: string;
  approvedDomain = false;
  domainMsgType: string;
  domainMsg: string;
  usedEmails: any;

  @ViewChild('upgradePopup') upgradePopup;
  @ViewChild('customPricePopup') customPricePopup;
  @ViewChild('card') card: StripeCardComponent;
  @ViewChild('card1') card1: StripeCardComponent;
  @ViewChild('card2') card2: StripeCardComponent;


  slabPlan = {
    attr: {
      class: 'table'
    },
    columns: {
      coupon_code: {
        title: 'Plan',
        sort: false
      },
      plan_price: {
        title: 'Price',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return ('$' + row.plan_price + '/mo');
        }
      },
      email_credits: {
        title: 'Emails',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return (abbreviateNumber(+row.email_credits));
        }
      },
      plan_overage_per_email_cost: {
        title: 'Overage (per email)',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          if (+row.plan_overage_per_email_cost === 0.00000) {
            return '-';
          } else {
            return ('$' + row.plan_overage_per_email_cost);
          }
        }
      },
      status: {
        title: '',
        sort: false,
        type: 'custom',
        renderComponent: TablePlanStatusComponent,
        onComponentInitFunction: (instance) => {
          instance.icon1Click.subscribe(row => {
            this.rowData = row;
            switch (row.status) {
              case 'upgrade' :
                // this.upgrade_plan(row.id);
                // this.getUpgradePlanDetails();
                // this.open(this.upgradePopup);
                this.upgradePlanId = row.id;
                this.getBillingAmount(row.plan_price);
                this.showPlanDetails();
                break;
              case 'downgrade':
                this.downgrade_planid = row.id;
                this.showPlanDowbgradeDetails();
                // this.downgrade_plan(row.id);
                break;
              default:
                break;
            }
          });
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
      columnTitle: 'Change Plan'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };


  settingsPromo = {
    attr: {
      class: 'table'
    },
    columns: {
      coupon_code: {
        title: 'Promo Code',
        sort: false
      },
      description: {
        title: 'Description',
        sort: false
      },
      start_date: {
        title: 'Start Date',
        sort: false
      },
      end_date_new: {
        title: 'End Date',
        sort: false
      },
      status_new: {
        title: 'Status',
        sort: false,
        type: 'custom',
        renderComponent: CouponCodeStateComponent
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };


  currentPlan = {
    attr: {
      class: 'table'
    },
    columns: {
      coupon_code: {
        title: 'Plan',
        sort: false
      },
      plan_price: {
        title: 'Price',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return ('$' + row.plan_price + '/mo');
        }
      },
      email_credits: {
        title: 'Emails',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return (abbreviateNumber(+row.email_credits));
        }
      },
      plan_overage_per_email_cost: {
        title: 'Overage (per email)',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          if (+row.plan_overage_per_email_cost === 0.00000) {
            return '-';
          } else {
            return ('$' + row.plan_overage_per_email_cost);
          }
        }
      },
      status: {
        title: '',
        sort: false,
        type: 'custom',
        renderComponent: TableCurrentPlanChangeComponent,
        onComponentInitFunction: (instance) => {
          instance.buttonClick.subscribe(row1 => {
            // this.rowCurrentPlanData = row1;
            this.showAllPlans = true;
          });
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
      columnTitle: 'Change Plan'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };
  editDetails: boolean;

  addCard: boolean;
  @ViewChild('downgradePopup') downgradePopup;
  alertText = '';
  alertSubscription: Subscription;

  constructor(private http: Http,
              private apiService: ApiService,
              private router: Router,
              private fb: FormBuilder,
              private modalService: NgbModal,
              private route: ActivatedRoute,
              private alertsService: AlertsService,
              private stripeService: StripeService) {
    this.alertSubscription = this.alertsService.p0Alert$
      .subscribe(text => this.alertText = text);
  }

  ngOnInit() {
    // Form validation for profile information on edit
    this.billingEditForm = this.fb.group({
      bill_name: ['', Validators.required],
      phone_number: ['', Validators.required],
      company_name: ['', Validators.required],
      bill_address: ['', Validators.required],
      bill_tax_id: ['', Validators.required]
    });

    this.autoRenewApply = this.fb.group({
      amount: ['', [Validators.required]],
      threshold: ['', [Validators.required]]
    });
    // this.getPlanType();
    this.getSlabPlan();
    this.getCurrentSlabPlan();
    this.getUpgradePlanDetails();
    this.getbillingdetails();
    this.getAddedCards();
    // this.fetchAutoRenewDetails();
    this.getPromoCodeDetails();
    this.getSpecialPrice();

    // Form validation for promocode
    this.promoCodeApply = this.fb.group({
      coupon_code: ['', [Validators.required]]
    });


    this.upgradeForm = new FormGroup({
      password: new FormControl('', [
        Validators.required,
        // Validators.min(8)
        // Validators.pattern(PASSWORD)
      ]),
    });

  }

  htmlTagCheck(value) {
    const rgx = new RegExp('<(\\"[^\\"]*\\"|\'[^\']*\'|[^\'\\">])*>');
    if (rgx.test(value)) {
      // your logic goes here.
      // console.log('here in' + value);
      return false;
    }
  }

  // Save billing info
  saveBillingInfo() {
    this.billingInfo = Object.assign(this.billingInfo, this.billingEditForm.value);
    const name_validate = this.htmlTagCheck(this.billingInfo.bill_name);
    if (name_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The bill name should not contain HTML tags';
      return;
    }

    const company_name_validate = this.htmlTagCheck(this.billingInfo.company_name);
    if (company_name_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The company name should not contain HTML tags';
      return;
    }

    const bill_address_validate = this.htmlTagCheck(this.billingInfo.bill_address);
    if (bill_address_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The bill address should not contain HTML tags';
      return;
    }

    const bill_tax_id_validate = this.htmlTagCheck(this.billingInfo.bill_tax_id);
    if (bill_tax_id_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The tax id should not contain HTML tags';
      return;
    }

    const phone_number_validate = this.htmlTagCheck(this.billingInfo.phone_number);
    if (phone_number_validate === false) {
      console.log('Fail');
      this.editBillingType = 'danger';
      this.editBillingMsg = 'The phone number should not contain HTML tags';
      return;
    }


    this.apiService.post(SAVE_BILLING_DETAILS, this.billingInfo)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.editBillingType = 'info';
            this.editBillingMsg = result['message'];
            this.onCancelClick();
          } else {
            this.editBillingType = 'danger';
            this.editBillingMsg = result['error_info']['message'];
          }
        }
      );
    return false;
  }

  editDetailsClick() {
    this.editDetails = true;
    this.billingEditForm.setValue({
      bill_name: this.billingInfo.bill_name,
      phone_number: this.billingInfo.phone_number,
      company_name: this.billingInfo.company_name,
      bill_address: this.billingInfo.bill_address,
      bill_tax_id: this.billingInfo.bill_tax_id
    });
  }

  onCancelClick() {
    this.editDetails = false;
    this.editBillingType = '';
    this.editBillingMsg = '';
    return false;
  }

// fetch billing information
  getbillingdetails() {
    this.apiService.post(GET_BILLING_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.billingInfo = result['data'];
          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }


// fetch recurring details
  // fetch Earned email credits working
  fetchAutoRenewDetails() {
    this.apiService.post(GET_AUTORENEW_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // this.autorenew_active = result['data']['active'];
            // for auto redeem checkbox value
            this.autoRenew = result['data']['active'] === 1 ? true : false;

            this.autoRenewApply.setValue({
              amount: result['data']['amount'],
              threshold: result['data']['threshold']
            });

          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }


  // Get all added credit cards
  getAddedCards() {
    this.apiService.post(RETRIVE_CREDIT_CARD_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            if (result['data']['data'] !== 'null') {
              this.primaryCard = result['data']['default_source'];
              this.addedCardList = result['data']['sources']['data'];
              this.cardAdded = true;
            } else {
              this.cardAdded = false;
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  // Make primary card
  makePrimaryCard(cardid) {
    this.card_id = {'card_id': cardid};
    this.apiService.post(MAKE_PRIMARY_CARD_CHARGE_UNPAID_BILL, this.card_id)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.updatePrimaryCardLocal(this.card_id);
            this.getUpgradePlanDetails();
            this.getCurrentSlabPlan();
            this.getSlabPlan();
            this.alertsService.hideP0Alert();
            this.primaryCard = cardid;
            this.makePrimaryCardType = 'info';
            this.makePrimaryCardMsg = result['message'];
          } else {
            this.makePrimaryCardType = 'danger';
            this.makePrimaryCardMsg = result['error_info']['message'];
          }
        }
      );
    return false;
  }

  // Delete added card
  deleteCard(cardid) {
    this.card_id = {'card_id': cardid};
    this.apiService.post(DELETE_CARD, this.card_id)
      .subscribe(result => {
          if (result['status'] === 'success') {
            // this.updatePrimaryCardLocal(this.card_id);
            // this.primaryCard = cardid;
            // this.refreshCardLocal(this.card_id);
            this.getAddedCards();
            this.makePrimaryCardType = 'info';
            this.makePrimaryCardMsg = result['message'];
          } else {
            this.makePrimaryCardType = 'danger';
            this.makePrimaryCardMsg = result['error_info']['message'];
          }
        }
      );
    return false;
  }


  /**
   * Moves primary card in array to 0 index
   * @param cardObj {card_id: id} primary card id
   */
  updatePrimaryCardLocal(cardObj) {
    const cardList = [null];
    for (const card of this.addedCardList) {
      if (card.id === cardObj.card_id) {
        cardList[0] = card;
      } else {
        cardList.push(card);
      }
    }
    this.addedCardList = cardList;
  }


  addNewCardStripe(name) {
    // alert('hey hi');
    // console.log('name ', name);
    this.stripeService
      .createToken(this.card.getCard(), {name})
      .subscribe(result => {
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          this.token_id = {'token_id': result.token.id};
          // Call API for adding the new card
          this.apiService.post(ADD_NEW_CARD_CHARGE_PENDING_BILL, this.token_id)
            .subscribe(result_val => {
                if (result_val['status'] === 'success') {
                  this.addNewCardType = 'info';
                  this.addNewCardMsg = result_val['message'];
                  this.getAddedCards();
                  this.addCard = false;
                  this.getCurrentSlabPlan();
                  this.alertsService.hideP0Alert();
                } else {
                  this.addNewCardType = 'danger';
                  this.addNewCardMsg = result_val['error_info']['message'];
                }
              }
            );
        } else if (result.error) {
          // Error creating the token
          // this.addNewCardType = 'warning';
          // this.addNewCardMsg = WARNING_MSG;
          // this.addNewCardMsg = result.error.message;
        }
      });
  }


  addNewCardStripeNew(name) {
    alert('card ==>' + this.card1);
    alert('hey hi ==> ' + name);
    // console.log('name ', name);
    this.stripeService
      .createToken(this.card1.getCard(), {name})
      .subscribe(result => {
        alert(result);
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          this.token_id = {'token_id': result.token.id};
          // Call API for adding the new card
          this.apiService.post(ADD_NEW_CARD, this.token_id)
            .subscribe(result_val => {
                if (result_val['status'] === 'success') {
                  this.addNewCardType = 'info';
                  this.addNewCardMsg = result_val['message'];
                  this.getAddedCards();
                  this.addCard = false;
                  this.alertsService.hideP0Alert();
                } else {
                  this.addNewCardType = 'danger';
                  this.addNewCardMsg = result_val['error_info']['message'];
                }
              }
            );
        } else if (result.error) {
          // alert(result.error);
          // Error creating the token
          // this.addNewCardType = 'warning';
          // this.addNewCardMsg = WARNING_MSG;
          // this.addNewCardMsg = result.error.message;
        }
      });
  }

  addNewCardAndMakePrimary() {
    const name = this.card2.getName();
    // alert('card ==>' + this.card2);
    // alert('hey hi ==> ' + name);
    // console.log('name ', name);
    this.stripeService
      .createToken(this.card2.getCard(), {name})
      .subscribe(result => {
        //  alert(result);
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          this.token_id = {'token_id': result.token.id};
          // Call API for adding the new card
          this.apiService.post(ADD_NEW_CARD_AND_MAKE_PRIMARY, this.token_id)
            .subscribe(result_val => {
                if (result_val['status'] === 'success') {
                  this.addNewCardTypeNew1 = 'info';
                  this.addNewCardMsgNew1 = result_val['message'];
                  this.getAddedCards();
                  this.addCard = false;
                  this.downgradeRequestActive = true;
                  this.showCardDowngrade = false;
                  this.showCurrentPlan = false;
                  this.getUpgradePlanDetails();
                  this.getPromoCodeDetails();
                } else {
                  this.addNewCardTypeNew1 = 'danger';
                  this.addNewCardMsgNew1 = result_val['error_info']['message'];
                }
              }
            );
        } else if (result.error) {
          // alert(result.error);
          // Error creating the token
          // this.addNewCardType = 'warning';
          // this.addNewCardMsg = WARNING_MSG;
          // this.addNewCardMsg = result.error.message;
        }
      });
  }


  cancelAddNewCard() {
    this.addCard = false;
    this.addNewCardType = '';
    this.addNewCardMsg = '';
    this.makePrimaryCardType = '';
    this.makePrimaryCardMsg = '';
    this.showCardAdd = false;
    this.showCurrentPlan = false;
    this.showCard = false;
  }

  cancelAddNewCardUp() {
    this.addCard = false;
    this.addNewCardType = '';
    this.addNewCardMsg = '';
    this.makePrimaryCardType = '';
    this.makePrimaryCardMsg = '';
    this.showCardAdd = false;
    this.showCurrentPlan = true;
    // this.showCard = false;
  }

  addNewCard() {
    this.addCard = true;
    this.makePrimaryCardType = '';
    this.makePrimaryCardMsg = '';
    this.addNewCardType = '';
    this.addNewCardMsg = '';
    return false;
  }

  // Apply Coupon
  applyCoupon(promo) {
    // console.log(promo);
    // console.log(promo.coupon_code);
    // console.log('here in apply coupon');
    this.apiService.get(APPLY_COUPON + '/?coupon_code=' + promo.coupon_code)
      .subscribe(result => {
          console.log(result);
          if (result['status'] === 'success') {
            this.promoCodeType = 'info';
            this.promoCodeMsg = result['message'];
            this.getPromoCodeDetails();
          } else {
            this.promoCodeType = 'danger';
            this.promoCodeMsg = result['error_info']['message'];
          }
        }, error => {
          this.promoCodeType = 'warning';
          this.promoCodeMsg = WARNING_MSG;
        }
      );
  }

  // get applied promo code
  getPromoCodeDetails() {
    this.promoSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${GET_APPLIED_COUPON_CODE}`,
      dataKey: 'settingsPromo'
    });
  }

  // get applied promo code
  getSlabPlan() {
    this.planSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${GET_SLAB_PLAN}`,
      dataKey: 'slabPlan'
    });
  }

  // get applied promo code
  getCurrentSlabPlan() {
    // alert('herre');
    this.currentPlanSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${GET_CURRENT_PLAN}`,
      dataKey: 'currentPlan'
    });
  }

  getPlanType() {
    this.apiService.post(GET_PLAN_TYPE, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            console.log('get plan type');
            console.log(result['data']);
            if (result['data'] !== 'null') {
              // this.primaryCard = result['data']['default_source'];
              console.log('Client type ==>' + result['data']['client_type']);
              if (result['data']['client_type'] === 'postpaid') {
                this.postpaidPlan = true;
                // this.specialPrice = true;
                // this.getPostPaidDetails();
              }
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  // Get all added credit cards
  getSpecialPrice() {
    // this.apiService.post(RETRIVE_CREDIT_CARD_DETAILS, '')
    this.apiService.post(GET_SPECIAL_PRICING, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            // console.log('getSpecialPrice');
            // console.log(result['data']);
            this.specialPrice = false;
            if (result['data'] !== null) {
              // this.primaryCard = result['data']['default_source'];
              // this.specialPriceList = result['data']['data'];
              this.specialPrice = true;
              console.log('sp ==>' + this.specialPrice);
            }
          } else {
            this.type = 'danger';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }


  downgrade_plan() {
    alert('here down' + this.downgrade_planid);
  }

  getUpgradePlanDetails() {
    this.apiService.post(GET_UPGRADE_PLAN_DETAILS, '')
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.upgradePlanInfo = result['data'];
            // alert('current_plan_price' + result['status']['data']);
            // alert('current_plan_price' + result['data']['current_plan_price']);
            // alert('overage_email_sent ==> ' + this.upgradePlanInfo.overage_email_sent);
            if (this.upgradePlanInfo.downgrade_status === 1) {
              this.downgradeRequestActive = true;
              this.newPlanInfo = 'You are scheduled to downgrade to the '
                + abbreviateNumber(+this.upgradePlanInfo.downgrade_plan_credits)  + '' +
                ' plan on ' + this.upgradePlanInfo.downgrade_date + '.';
            }

            if (this.upgradePlanInfo.unpaid_bill === 1) {
              this.unpaidBillActive = true;
              this.unpaidBillInfo = 'You have unpaid bill. Hence you are not allowed to change the plan.';
              this.addNewCardAlertMsg = 'Your card on file is either invalid or rejected ' +
                'by payment gateway. Please enter a valid card below to continue.';
            } else {
              this.unpaidBillActive = false;
            }

            if (this.upgradePlanInfo.domain_approved > 0) {
              this.approvedDomain = true;
              this.domainMsg = '';
            }else {
              this.approvedDomain = false;
              this.domainMsg = '<strong>There is no approved sending domain under your account</strong>' +
                '<br>You won’t be able to change plan until a member of our team has reviewed your domain and approved it for sending.';
              this.domainMsgType = 'danger';
            }



            if (this.upgradePlanInfo.current_plan_coupon_code === 'PLAN-FREE') {
              this.hideOverageSection = true;
            }

            this.card_status = +this.upgradePlanInfo.card_status;

            // if ( +this.upgradePlanInfo.total_email_sent > +this.upgradePlanInfo.allocated_emails) {
            //   this.pieCredits = [0, +this.upgradePlanInfo.total_email_sent];
            // }else {
            //    this.pieCredits = [+this.upgradePlanInfo.allocated_emails, +this.upgradePlanInfo.total_email_sent];
            // }
            // this.upgradePlanInfo.total_email_sent = 50000;

            if ( +this.upgradePlanInfo.total_email_sent > +this.upgradePlanInfo.allocated_emails) {
              this.pieCredits = [0, +this.upgradePlanInfo.total_email_sent];
            }else {
              // this.credits_available = result['data']['allocated_emails'] - result['data']['total_email_sent'];
              this.usedEmails = +this.upgradePlanInfo.allocated_emails - this.upgradePlanInfo.total_email_sent;
              this.pieCredits = [+this.usedEmails, +this.upgradePlanInfo.total_email_sent];
            }


          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }

  getBillingAmount(plan_price) {
    this.upgrade_plan_amount = {'upgrade_plan_amount': plan_price, 'row_data': this.rowData};
    this.apiService.post(GET_UPGRADE_PLAN_DETAILS, this.upgrade_plan_amount)
      .subscribe(result => {
          if (result['status'].toLowerCase() === 'success') {
            this.upgradePlanInfo = result['data'];
            // alert('current_plan_price' + result['status']['data']);
            // alert('current_plan_price' + result['data']['current_plan_price']);
            // alert('overage_email_sent ==> ' + this.upgradePlanInfo.overage_email_sent);
            this.chargeAmount = +this.upgradePlanInfo.total_amount_due_cents;
            this.card_status = +this.upgradePlanInfo.card_status;
            // this.pieCredits = [+this.upgradePlanInfo.allocated_emails, +this.upgradePlanInfo.total_email_sent];

            if ( +this.upgradePlanInfo.total_email_sent > +this.upgradePlanInfo.allocated_emails) {
              this.pieCredits = [0, +this.upgradePlanInfo.total_email_sent];
            }else {
              // this.credits_available = result['data']['allocated_emails'] - result['data']['total_email_sent'];
              this.usedEmails = result['data']['allocated_emails'] - result['data']['total_email_sent'];
              this.pieCredits = [+this.usedEmails, +this.upgradePlanInfo.total_email_sent];
            }

          } else {
            this.type = 'error';
            this.msg = result['error_info']['message'];
          }
        }
      );
  }



  /**
   * To open Bootstrap modal window
   * @param content
   */
  open(content) {
    this.customPriceForm = new FormGroup({
      email_cnt: new FormControl('', [
        Validators.required,
        Validators.pattern(EMAIL)
        // Validators.min(8)
        // Validators.pattern(PASSWORD)
      ]),
      monthly_vol: new FormControl('', [
        Validators.required,
        // Validators.pattern(EMAIL)
        // Validators.min(8)
        // Validators.pattern(PASSWORD)
      ]),
      msg_contactus: new FormControl('', [
        Validators.required,
        // Validators.min(8)
        // Validators.pattern(PASSWORD)
      ]),

    });

    this.popRef = this.modalService.open(content);
  }

  hideAllPlans() {
    this.showAllPlans = false;
    // return false;
  }

  showPlanDetails() {
    // alert(this.showCurrentPlan);
    this.showCurrentPlan = !this.showCurrentPlan;
    this.showAllPlans = true;
    this.showPlanDeatils = true;
    this.addNewCardTypeNew1 = '';
    this.addNewCardMsgNew1 = '';
    this.addNewCardTypeNew2 = '';
    this.addNewCardMsgNew2 = '';
    this.addNewCardTypeNew3 = '';
    this.addNewCardMsgNew3 = '';
    this.addNewCardTypeNew4 = '';
    this.addNewCardMsgNew4 = '';
  }

  cancel_plan_details() {
    // this.showCurrentPlan = !this.showCurrentPlan;
    this.showAllPlans = true; // !this.showAllPlans;
    this.showCurrentPlan = false;
    this.showPlanDeatils = !this.showPlanDeatils;
  }

  addAndCharge() {
    const name_here = this.card2.getName();
    alert('here111' + name_here);
  }


  addNewCardChargeAndMakePrimary() {
    this.addNewCardTypeNew2 = '';
    this.addNewCardMsgNew2 = '';
    const name = this.card1.getName();
    // alert('Amount charge ==> ' + this.chargeAmount);
    // console.log('name ', name);
    this.stripeService
      .createToken(this.card1.getCard(), {name})
      .subscribe(result => {
        //  alert(result);
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          this.token_id = {'token_id': result.token.id, 'amount': this.chargeAmount,  'plan_info': this.upgradePlanInfo};
          // Call API for adding the new card
          this.apiService.post(ADD_NEWCARD_CHARGE_AND_MAKE_PRIMARY, this.token_id)
            .subscribe(result_val => {
                if (result_val['status'] === 'success') {
                  this.addNewCardTypeNew2 = 'info';
                  this.addNewCardMsgNew2 = result_val['message'];
                  this.alertsService.hideP0Alert();
                  this.getCurrentSlabPlan();
                  this.getSlabPlan();
                  this.getAddedCards();
                  this.showCardAdd = false;
                  this.showCurrentPlan = !this.showCurrentPlan;
                  this.showCard = !this.showCard;
                  this.getUpgradePlanDetails();
                  this.upgradeSuccessActive = true;
                  this.upgradeSuccessInfo = 'You are successfully upgrade to the '
                    + abbreviateNumber(+this.rowData.email_credits)  + 'plan.';
                  this.downgradeRequestActive = false;
                  this.getPromoCodeDetails();
                  // this._redirectToHistory();
                  // this.addCard = false;
                  // this.downgradeRequestActive = true;
                  // this.showCardDowngrade = false;
                  // this.showCurrentPlan = false;
                  // this.getUpgradePlanDetails();
                } else {
                  this.addNewCardTypeNew2 = 'danger';
                  this.addNewCardMsgNew2 = result_val['error_info']['message'];
                }
              }
            );
        } else if (result.error) {
          // alert(result.error);
          // Error creating the token
          // this.addNewCardType = 'warning';
          // this.addNewCardMsg = WARNING_MSG;
          // this.addNewCardMsg = result.error.message;
        }
      });
  }

  _redirectToHistory() {
    this.alertsService.refreshPage();

    setTimeout(() => {
      this.router.navigate(['../history'], {relativeTo: this.route});
    }, 3000);
  }

  chargeBillAmountFromExistingCard() {
    this.addNewCardTypeNew3 = '';
    this.addNewCardMsgNew3 = '';
    this.charge_param = {'amount': this.chargeAmount,  'plan_info': this.upgradePlanInfo};
    this.apiService.post(NEW_CHARGE_PRIMARY_CARD, this.charge_param)
      .subscribe(result_val => {
          if (result_val['status'] === 'success') {
            this.addNewCardTypeNew3 = 'info';
            this.addNewCardMsgNew3 = result_val['message'];
            this.getCurrentSlabPlan();
            this.getSlabPlan();
            this.getUpgradePlanDetails();
            this.getPromoCodeDetails();
            this.upgradeSuccessActive = true;
            this.upgradeSuccessInfo = 'You are successfully upgrade to the '
              + abbreviateNumber(+this.rowData.email_credits)  + 'plan.';
            this.showPlanDeatils = !this.showPlanDeatils;
            this.showCardAdd = false;
            this.showCard = false;
            this.showCurrentPlan = false;
            this.showAllPlans = false;
            this.downgradeRequestActive = false;
            this.alertsService.hideP0Alert();
            // this._redirectToHistory();
          } else {
            this.addNewCardTypeNew3 = 'danger';
            this.addNewCardMsgNew3 = result_val['error_info']['message'];
          }
        }
      );
  }


  stripeCardStatusChanges(status) {
    this.isStripCardValid = 'VALID' === status;
  }

  contactUs() {
    this.addNewCardTypeNew4 = '';
    this.addNewCardMsgNew4 = '';
    this.open(this.customPricePopup);
  }

  sendContactUsDetails(formdata) {
    this.custom_param = {'email': formdata.email_cnt, 'monthly_vol': formdata.monthly_vol, 'message': formdata.msg_contactus};
    this.apiService.post(SEND_CUSTOM_PRICE_REQUEST, this.custom_param)
      .subscribe(result_val => {
          if (result_val['status'] === 'success') {
            this.addNewCardTypeNew4 = 'info';
            this.addNewCardMsgNew4 = result_val['message'];
          } else {
            this.addNewCardTypeNew4 = 'danger';
            this.addNewCardMsgNew4 = result_val['error_info']['message'];
          }
        }
      );



  }

  downgradePlan() {
    // alert('here downgrade plan');
    if (this.card_status === 1) {
      this.updateDowngradeRequest();
    }else {
      this.showDowngradePlanDeatils = false;
      this.showCardDowngrade = true;
    }
  }

  // Upgrade plan
  upgrade_plan() {
    if (this.card_status === 1) {
      this.chargeBillAmountFromExistingCard();
    } else {
      this.showCard = !this.showCard;
      this.showPlanDeatils = !this.showPlanDeatils;
      this.showAllPlans = !this.showAllPlans;
    }
  }


  updateDowngradeRequest() {
    this.apiService.get(UPDATE_DOWNGRADE_REQUEST + '/?downgrade_id=' + this.downgrade_planid)
      .subscribe(result => {
          if (result['status'] === 'success') {
            this.downgradeRequestActive = true;
            this.showCardDowngrade = false;
            this.showCurrentPlan = false;
            this.showDowngradePlanDeatils = false;
            // this.showCardDowngrade = true;
            this.getUpgradePlanDetails();
          } else {
            // this.promoCodeType = 'danger';
            // this.promoCodeMsg = result['error_info']['message'];
          }
        }, error => {
          // this.promoCodeType = 'warning';
          // this.promoCodeMsg = WARNING_MSG;
        }
      );
  }

  cancel_downgrade() {
    this.showCardDowngrade = false;
    this.showDowngradePlanDeatils = false;
    this.showAllPlans = true;
    this.showCurrentPlan = false;
  }

  showPlanDowbgradeDetails() {
    this.showDowngradePlanDeatils = true;
    this.showAllPlans = !this.showAllPlans;
    this.showCurrentPlan = !this.showCurrentPlan;
  }

  cancelAddNewCardDowngrade() {
    this.showCardDowngrade = false;
    this.showCurrentPlan = false;
  }

  cancelBilling(){
    alert('Here Cancel billing');
  }

}
