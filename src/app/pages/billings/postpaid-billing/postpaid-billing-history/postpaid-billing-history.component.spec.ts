import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostpaidBillingHistoryComponent } from './postpaid-billing-history.component';

describe('PostpaidBillingHistoryComponent', () => {
  let component: PostpaidBillingHistoryComponent;
  let fixture: ComponentFixture<PostpaidBillingHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostpaidBillingHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostpaidBillingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
