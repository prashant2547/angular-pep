import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {Http, ResponseContentType} from '@angular/http';
import {environment} from '../../../../../environments/environment';
import {TableDataSource} from '../../../services/table.data-source';
import {DOWNLOAD_INVOICE, GET_ALL_INVOICE_DATA, NO_DATA_FOUND} from '../../../../config';
import {DownloadIconComponent} from '../../../components/table/download-icon/download-icon.component';

@Component({
  selector: 'app-postpaid-billing-history',
  templateUrl: './postpaid-billing-history.component.html',
  styleUrls: ['./postpaid-billing-history.component.scss']
})
export class PostpaidBillingHistoryComponent implements OnInit {
  invoiceTabelSource: any;
  billingHistoryFlag = true;
  billingHistoryFlag_hide = true;

  invoiceTabel = {
    attr: {
      class: 'table'
    },
    columns: {
      invoice_number: {
        title: 'Invoice Number',
        sort: false
      },
      billing_type: {
        title: 'Type',
        sort: false
      },
      due_date: {
        title: 'Due Date',
        sort: false
      },
      amount: {
        title: 'Invoice Amount ',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return ('$' + row.amount);
        }
      },
      status: {
        title: 'Satus',
        sort: false
      },
      invoice_path:{
        title: 'Download',
        sort: false,
        type: 'custom',
        renderComponent: DownloadIconComponent,
        onComponentInitFunction: (instance) => {
          instance.clicked.subscribe(row => {
            this.download_invoice_pdf(row.invoice_path);
          });
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
      columnTitle: 'Change Plan'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };


  constructor(private http: Http,
              private apiService: ApiService) { }

  ngOnInit() {
    this.getAllInvoices();
  }

  getAllInvoices() {
    // alert('herre');
    this.invoiceTabelSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${GET_ALL_INVOICE_DATA}`,
      dataKey: 'invoiceData'
    });

    this.invoiceTabelSource.onGetData.subscribe((res) => {
      if (+res.data.invoiceData.payment_history === 0) {
        this.billingHistoryFlag = false;
        this.billingHistoryFlag_hide = true;
      } else {
        this.billingHistoryFlag = true;
        this.billingHistoryFlag_hide = false;
      }
    });


  }

  download_invoice_pdf(invoicePath) {
    const invoiceDownloadUrl = `${environment.api_url}${DOWNLOAD_INVOICE}`;
    this.http.post(invoiceDownloadUrl, JSON.stringify({invoice_path: invoicePath}), {responseType: ResponseContentType.Blob})
      .catch((err) => {
        console.log((err.status === 404));
        alert('Failed to download invoice. Please contact support');
        return null;
      })
      .subscribe(data => {
          // gets the filename
          return this.saveData(data, invoicePath.replace(/^.*[\\\/]/, ''));
        },
        error => {
          console.log(JSON.stringify(error.json()));
        }
      );
  }

  saveData = (function () {
    const a = document.createElement('a');
    document.body.appendChild(a);
    return function (data, fileName) {
      const blob = new Blob([data.blob()], {type: 'application/pdf'});
      // const blob = new Blob([data], {type: 'octet/stream'});
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    };
  }());


}
