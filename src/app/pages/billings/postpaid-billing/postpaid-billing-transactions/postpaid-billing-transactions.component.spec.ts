import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostpaidBillingTransactionsComponent } from './postpaid-billing-transactions.component';

describe('PostpaidBillingTransactionsComponent', () => {
  let component: PostpaidBillingTransactionsComponent;
  let fixture: ComponentFixture<PostpaidBillingTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostpaidBillingTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostpaidBillingTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
