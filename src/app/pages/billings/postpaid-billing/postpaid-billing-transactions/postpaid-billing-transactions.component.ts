import { Component, OnInit } from '@angular/core';
import {abbreviateNumber} from '../../../components/utils/number-utils';
import {GET_TRANSACTION_DATA, NO_DATA_FOUND} from '../../../../config';
import {environment} from '../../../../../environments/environment';
import {TableDataSource} from '../../../services/table.data-source';
import {ApiService} from '../../../services/api.service';
import {Http} from '@angular/http';

@Component({
  selector: 'app-postpaid-billing-transactions',
  templateUrl: './postpaid-billing-transactions.component.html',
  styleUrls: ['./postpaid-billing-transactions.component.scss']
})
export class PostpaidBillingTransactionsComponent implements OnInit {

  transactionTabelSource: any;
  billingHistoryFlag = true;
  billingHistoryFlag_hide = true;

  transactionTabel = {
    attr: {
      class: 'table'
    },
    columns: {
      transaction_date_time: {
        title: 'Transaction Date/Time',
        sort: false
      },
      payment_method: {
        title: 'Payment Method',
        sort: false
      },
      transaction_id: {
        title: 'Transaction ID',
        sort: false
      },
      amount: {
        title: 'Amount',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return ('$' + row.amount);
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
      columnTitle: 'Change Plan'
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  constructor(private http: Http,
              private apiService: ApiService) { }

  ngOnInit() {
    this.getTransactionHistory();
  }


  // get applied promo code
  getTransactionHistory() {
    // alert('herre');
    this.transactionTabelSource = new TableDataSource(this.http, {
      endPoint: `${environment.api_url}${GET_TRANSACTION_DATA}`,
      dataKey: 'transactionData'
    });

    this.transactionTabelSource.onGetData.subscribe((res) => {
      if (+res.data.transactionData.payment_history === 0) {
        this.billingHistoryFlag = false;
        this.billingHistoryFlag_hide = true;
      } else {
        this.billingHistoryFlag = true;
        this.billingHistoryFlag_hide = false;
      }
    });

  }

}
