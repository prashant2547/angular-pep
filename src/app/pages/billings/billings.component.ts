import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-billing',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class BillingsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
