import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {GettingStartedComponent} from './getting-started.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {VerifyComponent} from './verify/verify.component';
import {GettingStartedRoutingModule} from './getting-started-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentsModule} from '../components/components.module';
import {UpgradeAccountComponent} from './upgrade-account/upgrade-account.component';
import { ResendConfirmationComponent } from './resend-confirmation/resend-confirmation.component';

@NgModule({
  declarations: [
    GettingStartedComponent,
    WelcomeComponent,
    VerifyComponent,
    UpgradeAccountComponent,
    ResendConfirmationComponent,
  ],
  imports: [
    CommonModule,
    GettingStartedRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
  ]
})
export class GettingStartedModule {
}
