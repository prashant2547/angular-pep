import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {ADD_DOMAIN, WARNING_MSG} from '../../../config';
import {Router} from '@angular/router';
import {DOMAIN} from '../../components/forms/form-validation';
import {GetStartedService} from '../../services/get-started.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
  // data to display helpful links
  helpSectionData = [
    {
      helpText: 'What is Sender Domain?',
      helpLink: 'https://docs.pepipost.com/docs/what-is-a-sending-domain-how-to-set-up-sending-domains'
    },
    {
      helpText: 'How do I start sending emails?',
      helpLink: 'https://docs.pepipost.com/docs/how-do-i-start-sending-email'
    }, {
      helpText: 'What is dedicated IP address? How can I get one?',
      helpLink: 'https://docs.pepipost.com/docs/does-pepipost-provide-a-dedicated-ip'
    },

  ];
  // Declaration of variables
  domain_name = '';
  msg = '';
  type = '';
  verifyForm: FormGroup;
  greetings = 'Good Morning';

  stepList = [];
  stepCount = 0;

  constructor(private apiService: ApiService,
              private getStartedService: GetStartedService,
              private router: Router) {
    this.updateGreeting();
  }

  ngOnInit() {
    // form validation
    this.verifyForm = new FormGroup({
      domain_name: new FormControl('', [
        Validators.required,
        Validators.pattern(DOMAIN)
      ]),
    });

    this.fetchGetStartSteps();
  }

  // gets data for get started widget in sidebar
  fetchGetStartSteps() {
    this.getStartedService.getSteps().subscribe(result => {
      this.stepList = result['stepList'] || [];
      this.stepCount = result['stepCount'] || 0;
    });
  }

  // Goes to Get Started current step
  onStepClick(evt) {
    this.getStartedService.goToStep(evt);
  }

  updateGreeting() {
    const hours = new Date().getHours();
    if (hours >= 12 && hours < 17) {
      this.greetings = 'Good Afternoon';
    } else if (hours >= 17) {
      this.greetings = 'Good Evening';
    }
  }

  verifyDomain(domainName) {
    this.apiService.post(ADD_DOMAIN, (domainName))
      .subscribe((result) => {
        if (result['status'].toLowerCase() === 'success') {
          // redirect to domain management
          this.router.navigate(['/settings/domain/' + result['data']['domid']], {
            queryParams: {domain: domainName['domain_name']}
          });
        } else {
          this.type = 'danger';
          this.msg = result['error_info']['message'];
        }
      }, error => {
        this.type = 'warning';
        this.msg = WARNING_MSG;
      });
  }
}
