export class GettingStarted {
  message: string;
  plan_info: string;
  plan_description: string;
}
