import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RESEND_CONFIRMATION} from '../../../config';

@Component({
  selector: 'app-resend-confirmation',
  templateUrl: './resend-confirmation.component.html',
  styleUrls: ['./resend-confirmation.component.scss']
})
export class ResendConfirmationComponent implements OnInit {

  @ViewChild('resendConfirmationPopup') resendConfirmationPopup;
  popRef: NgbActiveModal;
  confirmationText = '';

  constructor(private apiService: ApiService,
              private modalService: NgbModal,
              private route: Router) {
  }

  ngOnInit() {
    this.apiService.get(RESEND_CONFIRMATION)
      .subscribe(result => {
        if (result['status'] === 'success') {
          this.confirmationText = result['message'];
        } else {
          this.confirmationText = result['error_info']['message'];
        }
        this.open(this.resendConfirmationPopup);
      });
  }

  open(content) {
    this.popRef = this.modalService.open(content);
  }

  redirectToDashboard() {
    this.popRef.close();
    this.route.navigate(['/dashboard']);
  }
}
