import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';

import {GETTING_STARTED_API_URL, GETTING_STARTED_TIMEZONE_URL} from '../../../config';
import {ApiService} from '../../services/api.service';
import {GettingStarted} from '../getting-started';
import {Timezone} from '../../components/forms/timezone/timezone';
import {MenuService} from '../../services/menu.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})

export class WelcomeComponent implements OnInit, OnDestroy {
  data: any = null;
  timezoneData: Timezone;
  selectedTimeZone = '';
  title = '';
  gettingStarted: GettingStarted;

  constructor(private router: Router,
              private apiService: ApiService,
              private menuService: MenuService) {
    this.menuService.hideMenu();
  }

  ngOnInit() {
    this.selectedTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    // if we haven't recieve any timezone from browser display default us timezone
    // this.title = this.selectedTimeZone.toLowerCase() === 'utc'
    //   ? 'Please select your timezone as all your future email performance reports will be generated as per the timezone.'
    //   : 'It seems you are from ' + this.selectedTimeZone + ', adjust the timezone if needed, as all your future email performance ' +
    //   'reports will be generated as per the timezone.';
    this.title = 'As a first step, confirm your time zone. Your dashboard will be set in your time zone.';
    this.apiService.post(GETTING_STARTED_API_URL, '')
      .subscribe(result => {
          if (result['message'] === 'success') {
            this.gettingStarted = result['data'];
            this.timezoneData = result['data']['timezone'];
            this.selectedTimeZone = this.selectedTimeZone.toLowerCase() === 'utc' ? 'US/Eastern' : this.selectedTimeZone;
          }
        }
      );
  }

  ngOnDestroy(): void {
    this.menuService.showMenu();
  }

  onContinueClick(timezone) {
    // timezone = timezone || this.timezoneData[0]['timezone'];
    timezone = timezone || this.selectedTimeZone;
    console.log(timezone);
    // make post call to save the data and redirect to domain page
    this.apiService.post(GETTING_STARTED_TIMEZONE_URL, {'timezone': timezone})
      .subscribe(result => {
          if (result['status'] === 'success') {
            // route to domain page
            const redirect_url = result['data']['redirect_url'];
            this.router.navigate([redirect_url]);
          } else {
            // stay on the same page with error msg
          }
        },
      );
  }
}
