import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {WelcomeComponent} from './welcome/welcome.component';
import {GettingStartedComponent} from './getting-started.component';
import {VerifyComponent} from './verify/verify.component';
import {UpgradeAccountComponent} from './upgrade-account/upgrade-account.component';
import {ResendConfirmationComponent} from './resend-confirmation/resend-confirmation.component';

const routes: Routes = [{
  path: '',
  component: GettingStartedComponent,
  children: [{
    path: 'welcome',
    component: WelcomeComponent
  }, {
    path: 'verify',
    component: VerifyComponent
  }, {
    path: 'upgrade-account',
    component: UpgradeAccountComponent
  }, {
    path: 'resend-confirmation',
    component: ResendConfirmationComponent
  }, {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class GettingStartedRoutingModule {

}
