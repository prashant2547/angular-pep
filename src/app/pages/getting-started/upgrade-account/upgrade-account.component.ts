import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-upgrade-account',
  templateUrl: './upgrade-account.component.html',
  styleUrls: ['./upgrade-account.component.scss']
})
export class UpgradeAccountComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  // redirect to credit management
  redirectToCreditManagement() {
    this.router.navigate(['/billings/details/recharge']);

  }
}
