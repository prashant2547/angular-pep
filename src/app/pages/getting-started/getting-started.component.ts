import { Component } from '@angular/core';

@Component({
  selector: 'app-getting-started',
  template: `<router-outlet></router-outlet>`
})
export class GettingStartedComponent {
}
