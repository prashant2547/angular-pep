import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubaccountAnalyticsComponent } from './subaccount-analytics.component';

describe('SubaccountAnalyticsComponent', () => {
  let component: SubaccountAnalyticsComponent;
  let fixture: ComponentFixture<SubaccountAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubaccountAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubaccountAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
