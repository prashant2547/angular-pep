import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareSubAnalyticsComponent } from './compare-sub-analytics.component';

describe('CompareSubAnalyticsComponent', () => {
  let component: CompareSubAnalyticsComponent;
  let fixture: ComponentFixture<CompareSubAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompareSubAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareSubAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
