import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {Http} from '@angular/http';
import {TableDataSource} from '../../services/table.data-source';
import {
  NO_DATA_FOUND,
} from '../../../config';
import {Ng2SmartTableDynamicColumn} from '../../components/ng2-smart-table/ng2-smart-table-dynamic-column';
import {addCommasToNum} from '../../components/utils/number-utils';

@Component({
  selector: 'app-compare-sub-analytics',
  templateUrl: './compare-sub-analytics.component.html',
  styleUrls: ['./compare-sub-analytics.component.scss']
})
export class CompareSubAnalyticsComponent implements OnInit {
  requestColumnName = 'bounce_rate';
  togglePercent = true;
  toggleVal = true;
  allData = [];
  tableFlag = 0;
  arrDomainList = [];
  availableText = 'Subaccounts';
  selectedText = 'Selected Subaccounts';
  items = [{
    id: 'anb.com',
    name: 'anb.com'
  }, {
    id: 'andb.com',
    name: 'adnb.com'
  }, {
    id: 'anhb.com',
    name: 'anhb.com'
  }, {
    id: 'anbb.com',
    name: 'anbb.com'
  }, {
    id: 'anbt.com',
    name: 'anbt.com'
  }, {
    id: 'anbu.com',
    name: 'anbu.com'
  }, {
    id: 'anbr.com',
    name: 'anbr.com'
  }];
  subAccountsdataSource: any;
  /**
   *Setting column data of Tag wise smart table
   * @type {
   * {attr: {class: string};
   * columns: {clientid: {
   * title: string; sort: boolean
   * }};
   * actions: {
   * add: boolean; edit: boolean; delete: boolean
   * };
   * hideSubHeader: boolean; noDataMessage: string; pager: {perPage: number}}}
   */
  subAccountsSettings = {
    attr: {
      class: 'table'
    },
    columns: {},
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  test = [{
    'columnTitleName': 'username',
    'columnDataName': 'username',
  }, {
    'columnTitleName': 'email',
    'columnDataName': 'bounce_rate',
  }];
  clonObj: Ng2SmartTableDynamicColumn;

  constructor(private http: Http,
              private apiService: ApiService) {
  }

  ngOnInit() {
  }

  // Function to get Domain list
  getSubAccountList(ev) {
    this.arrDomainList = ev.selected;
    if (this.arrDomainList.length < 0) {
      this.tableFlag = 0;
    } else {
      this.tableFlag = 1;
    }
    this.compareAnalytics();
    //api call dynamic to store values of domain
  }

  numbToPercentToggle(ev) {
    this.toggleVal = ev;
    this.subAccountsdataSource.refresh();
  }

  getRequestType(ev) {
    this.subAccountsdataSource.refresh();
    this.requestColumnName = ev.target.value;
    alert(ev.target.value);
  }

  fetchSubaccountDetails() {
    this.subAccountsdataSource = new TableDataSource(this.http, {
      // endPoint: `${environment.api_url}${LIST_SUBACCOUNT_DETAILS}`,
      endPoint: 'http://localhost:5000/subaccount',
      dataKey: 'subaccount'
    });
    this.clonObj = new Ng2SmartTableDynamicColumn();
    let clone = this.clonObj.clone(this.subAccountsSettings);
    for (let key of this.test) {
      alert(key.columnTitleName);
      clone.columns[this.requestColumnName] = {
        title: key.columnTitleName,
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row[this.requestColumnName]) : row[this.requestColumnName + '_per'];
        }
      };
    }
    this.subAccountsSettings = clone;
  }

  compareAnalytics() {
    this.fetchSubaccountDetails();
  }

}
