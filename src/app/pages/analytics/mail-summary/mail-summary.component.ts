import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {TableDataSource} from '../../services/table.data-source';
import {environment} from '../../../../environments/environment';
import {API_URL, LIST_TAG_NAMES, MAIL_SUMMARY_TAGS, MAIL_SUMMARY_TAGS_CSV, ANALYTICS_GRAPH, NO_DATA_FOUND} from '../../../config';
import {Http} from '@angular/http';
import {DaterangePickerComponent, DaterangepickerConfig} from 'ng2-daterangepicker';
import * as moment from 'moment';
import {ApiService} from '../../services/api.service';
import {HttpClient} from '@angular/common/http';
import {addCommasToNum} from '../../components/utils/number-utils';

@Component({
  selector: 'app-mail-summary',
  templateUrl: './mail-summary.component.html',
  styleUrls: ['./mail-summary.component.scss']
})
export class MailSummaryComponent implements OnInit, AfterViewInit {
  // var to show hide no data found window
  nodataFlag = true;
  tableFlag = true;
  toggleVal = true;
  // graph variables
  allData = [];
  graphData = [];
  analyticsData = [];
// [
//   {
//     "name": "Delivered",
//     "series": []
//   }, {
//     "name": "Open",
//     "series": []
//   }, {
//     "name": "Click",
//     "series": []
//   }, {
//     "name": "Unsubscribe",
//     "series": []
//   }, {
//     "name": "Bounce",
//     "series": []
//   }, {
//     "name": "Abuse",
//     "series": []
//   }, {
//     "name": "Invalid",
//     "series": []
//   }, {
//     "name": "Dropped",
//     "series": []
//   }, {
//     "name": "Requests",
//     "series": []
//   }
// ];

  view = [];
  colorScheme = {
    domain: ['#d4336c', '#ffb141', '#81c7ff', '#fa4956', '#7349fa', '#51d599', '#fa49db', '#fa6f49']
  };
  @ViewChild('flexContent') flexContent;
  // cache all graph elements
  graphElements: any[];

  // https://akveo.github.io/ng2-smart-table/#/documentation
  settings = {
    attr: {
      class: 'table'
    },
    columns: {
      adate: {
        title: 'Date',
        sort: false
      },
      total: {
        title: 'Requests',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return addCommasToNum(row.total);
        }
      },
      delivered: {
        title: 'Delivered',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.delivered) : row.per_delivered;
        }
      },
      open: {
        title: 'Opened',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.open) : row.per_open;
        }
      },
      click: {
        title: 'Clicked',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.click) : row.per_click;
        }
      },
      unsub: {
        title: 'Unsubscribes',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.unsub) : row.per_unsub;
        }
      },
      bounce: {
        title: 'Bounces',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.bounce) : row.per_bounce;
        }
      },
      abuse: {
        title: 'Spam Reports',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.abuse) : row.per_abuse;
        }
      },
      invalid: {
        title: 'Invalids',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.invalid) : row.per_invalid;
        }
      },
      dropped: {
        title: 'Drops',
        sort: false,
        valuePrepareFunction: (cell, row) => {
          return this.toggleVal ? addCommasToNum(row.dropped) : row.per_dropped;
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  dataSource: any;
  filterValues = [];
  selectedTag: any;
  dateInput = {
    start: moment().subtract(7, 'days'),
    end: moment()
  };
  selectedDateRange = [moment(this.dateInput.start).format('YYYYMMDD'),
    moment(this.dateInput.end).format('YYYYMMDD')];
  tags: any;
  // for drop down
  tagItems: Array<string>;
  togglePercent = true;
  resizeInterval: any;

  constructor(private http: Http,
              private httpClient: HttpClient,
              private daterangepickerOptions: DaterangepickerConfig,
              private apiService: ApiService) {
    this.daterangepickerOptions.settings = {
      locale: {format: 'YYYY-MM-DD'},
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(4, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
      }
    };

    this.dataSource = new TableDataSource(http, {
      endPoint: `${environment.api_url}${MAIL_SUMMARY_TAGS}`,
      dataKey: 'tagSummary'
    });
    // to show hide of no data fond window
    this.dataSource.onGetData.subscribe((res) => {
      this.nodataFlag = +res.data.tagSummary.total_report_counts > 0;
      this.tableFlag = !this.nodataFlag;
      this.onResize();

    });
  }

  tagSelected(value: any): void {
    this.filterValues = [];
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.filterValues.push({field: 'tag', search: value.text});
    this.dataSource.setFilter(this.filterValues, false);
    this.selectedTag = value.text;

    this.fetchGraphData(this.selectedDateRange);
  }

  ngOnInit() {
    this.fetchGraphData(this.selectedDateRange);
    this.setGraphSize();
    this.filterValues = [];
    this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
    this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
    this.dataSource.setFilter(this.filterValues, false);

    this.apiService.get(LIST_TAG_NAMES)
      .subscribe((result) => {
          if (result['status'] === 'success') {
            this.tags = result['data']['tag_list']['data'];
            const arr = ['All data'];
            for (const key in this.tags) {
              if (this.tags.hasOwnProperty(key)) {
                arr.push(this.tags[key]['tag']);
              }
            }
            this.tagItems = arr;
          } else {
            alert('No tags');
          }
        }
      );

  }

  ngAfterViewInit(): void {
  }

  cacheGraphElements() {
    this.graphElements = this.flexContent.nativeElement.querySelectorAll('ngx-charts-line-chart');
  }

  fetchGraphData(analyticsDateRange) {
    const tagString = this.selectedTag ? ('&tag_like=' + this.selectedTag) : '';
    this.apiService
      .get(ANALYTICS_GRAPH + '?table=tagSummary&start_date_like=' +
        analyticsDateRange[0] + '&end_date_like=' + analyticsDateRange[1] + tagString)
      .subscribe((result) => {
        if (result['status'] === 'success') {
          this.setGraphSize();
          this.graphData = result['data']['graph']['data'];
        } else {
          console.log('Analytics graph loading failed : ' + result['error_info']['message']);
        }
      });
  }

  numbToPercentToggle(ev) {
    this.toggleVal = ev;
    this.dataSource.refresh();
    this.analyticsData = (ev === false) ? this.allData['plaindata'] : this.allData['perdata'];
  }

  selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    if (dateInput.start && dateInput.end) {
      this.selectedDateRange = [
        moment(dateInput.start).format('YYYYMMDD'),
        moment(dateInput.end).format('YYYYMMDD')
      ];

      // clear existing date ranges
      const tempFilterValues = this.filterValues
        .map((obj) => {
          if (obj.field !== 'start_date' && obj.field !== 'end_date') {
            return obj;
          }
        });
      this.filterValues = tempFilterValues.filter(n => n);

      this.filterValues.push({field: 'start_date', search: this.selectedDateRange[0]});
      this.filterValues.push({field: 'end_date', search: this.selectedDateRange[1]});
      this.dataSource.setFilter(this.filterValues, false);

      this.fetchGraphData(this.selectedDateRange);
    }
  }

  downloadCSV() {
    this.apiService.post(MAIL_SUMMARY_TAGS_CSV, ({
      'start_date_like': this.selectedDateRange[0],
      'end_date_like': this.selectedDateRange[1],
      'tag_like': this.selectedTag
    }))
      .subscribe((result) => {
        // TODO: add download code
      });

    return false;
  }

  setGraphSize() {
    // check if graph containers are available
    if (!this.graphElements || (this.graphElements && this.graphElements.length === 0)) {
      // try to cache graph elements
      this.cacheGraphElements();

      // if graph elements are still not available, try to get after some time
      if (!this.graphElements || (this.graphElements && this.graphElements.length === 0)) {
        this.onResize();
        return;
      }
    }

    // hide all graphs for getting correct width
    this.graphElements.forEach(element => {
      element.classList.add('d-none');
    });
    // 25px padding * 2 sides
    this.view = [this.flexContent.nativeElement.offsetWidth - (25 * 2), 280];
    // after getting correct width show all graphs
    this.graphElements.forEach(element => {
      element.classList.remove('d-none');
    });
  }

  onResize() {
    // calling after 300 ms instead of every ms
    clearInterval(this.resizeInterval);
    this.resizeInterval = setInterval(() => {
      this.setGraphSize();
      clearInterval(this.resizeInterval);
    }, 300);
  }

}

