import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html'})
export class TestComponent implements OnInit {

  multi = [
    {
      'name': 'Norfolk Island',
      'series': [
        {
          'value': 4669,
          'name': '2016-09-17'
        },
        {
          'value': 4577,
          'name': '2016-09-17'
        },
        {
          'value': 4227,
          'name': '2016-09-15'
        },
        {
          'value': 5843,
          'name': '2016-09-21'
        },
        {
          'value': 4189,
          'name': '2016-09-12'
        }
      ]
    },
    {
      'name': 'Jordan',
      'series': [
        {
          'value': 4976,
          'name': '2016-09-17'
        },
        {
          'value': 5067,
          'name': '2016-09-17'
        },
        {
          'value': 5681,
          'name': '2016-09-15'
        },
        {
          'value': 5580,
          'name': '2016-09-21'
        },
        {
          'value': 3932,
          'name': '2016-09-12'
        }
      ]
    },
    {
      'name': 'British Indian Ocean Territory',
      'series': [
        {
          'value': 5536,
          'name': '2016-09-17'
        },
        {
          'value': 6536,
          'name': '2016-09-17'
        },
        {
          'value': 3172,
          'name': '2016-09-15'
        },
        {
          'value': 5169,
          'name': '2016-09-21'
        },
        {
          'value': 4491,
          'name': '2016-09-12'
        }
      ]
    },
    {
      'name': 'Rwanda',
      'series': [
        {
          'value': 3091,
          'name': '2016-09-17'
        },
        {
          'value': 5883,
          'name': '2016-09-17'
        },
        {
          'value': 3018,
          'name': '2016-09-15'
        },
        {
          'value': 3340,
          'name': '2016-09-21'
        },
        {
          'value': 5716,
          'name': '2016-09-12'
        }
      ]
    },
    {
      'name': 'Mali',
      'series': [
        {
          'value': 2636,
          'name': '2016-09-17'
        },
        {
          'value': 4998,
          'name': '2016-09-17'
        },
        {
          'value': 2131,
          'name': '2016-09-15'
        },
        {
          'value': 2773,
          'name': '2016-09-21'
        },
        {
          'value': 6107,
          'name': '2016-09-12'
        }
      ]
    }
  ];

  // lineChart
  public lineChartData: Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40, 59, 80, 81], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90, 59, 80, 81], label: 'Series B'},
    {data: [18, 48, 77, 9, 100, 27, 40, 59, 80, 81], label: 'Series C'}
  ];
  public lineChartLabels: Array<any> = ['Day 1', 'Day 2', 'Day 3', 'Day 4', 'Day 5', 'Day 6', 'Day 7', 'Day 8', 'Day 9', 'Day 10'];
  public lineChartOptions: any = {
    responsive: true
  };

  public lineChartLegend: boolean = false;
  public lineChartType: string = 'doughnut';

  constructor() {
  }

  ngOnInit() {
  }

  public randomize(): void {
    let _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

}
