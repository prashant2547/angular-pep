import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceWiseComponent } from './device-wise.component';

describe('DeviceWiseComponent', () => {
  let component: DeviceWiseComponent;
  let fixture: ComponentFixture<DeviceWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
