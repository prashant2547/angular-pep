import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {TableDataSource} from '../../services/table.data-source';
import {API_URL, NO_DATA_FOUND} from '../../../config';
import {TableStatusComponent} from '../../components/table/table-status/table-status.component';

@Component({
  selector: 'app-isp-report',
  templateUrl: './isp-report.component.html',
  styleUrls: ['./isp-report.component.scss']
})
export class IspReportComponent implements OnInit {

  dataSourceTagWise: any;

  // Refer documentation of ng2-smart-table for settings
  // https://akveo.github.io/ng2-smart-table/#/documentation
  settingsTagWise = {
    attr: {
      class: 'table'
    },
    columns: {
      fromaddress: {
        title: 'Sender Address',
        sort: false
      },
      email: {
        title: 'Recipient Email',
        sort: false
      },
      atime: {
        title: 'Requested Date/Time',
        sort: false
      },
      TTD: {
        title: 'Time to Deliver',
        sort: false
      },
      description: {
        title: 'Status',
        sort: false,
        type: 'custom',
        renderComponent: TableStatusComponent,
        onComponentInitFunction: (instance) => {
          instance.clicked.subscribe(row => {
            console.log(`${row.description} clicked!`);
          });
        }
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    hideSubHeader: true,
    noDataMessage: NO_DATA_FOUND,
    pager: {
      perPage: 10
    }
  };

  constructor(private http: Http) {
    this.fetchTagWiseData();
  }

  ngOnInit() {
  }

  fetchTagWiseData() {
    this.dataSourceTagWise = new TableDataSource(this.http, {
      endPoint: API_URL + 'live',
      dataKey: 'live_list'
    });
  }

}
