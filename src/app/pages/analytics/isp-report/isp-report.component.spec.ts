import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IspReportComponent } from './isp-report.component';

describe('IspReportComponent', () => {
  let component: IspReportComponent;
  let fixture: ComponentFixture<IspReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IspReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IspReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
