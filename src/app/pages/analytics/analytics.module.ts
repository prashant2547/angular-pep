import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AnalyticsRoutingModule} from './analytics-routing.module';
import {ComponentsModule} from '../components/components.module';
import {MailSummaryComponent} from './mail-summary/mail-summary.component';
import {AnalyticsComponent} from './analytics.component';
import {IspReportComponent} from './isp-report/isp-report.component';
import {GeoComponent} from './geo/geo.component';
import {DeviceWiseComponent} from './device-wise/device-wise.component';
import {TimeWiseComponent} from './time-wise/time-wise.component';
import {WebhookComponent} from './webhook/webhook.component';
import {ApiReportComponent} from './api-report/api-report.component';
import {TestComponent} from './test/test.component';
import {ChartsModule} from 'ng2-charts';
import {Daterangepicker} from 'ng2-daterangepicker';
import {FormsModule} from '@angular/forms';
import { CompareSubAnalyticsComponent } from './compare-sub-analytics/compare-sub-analytics.component';
import {DualListBoxModule} from 'ng2-dual-list-box';
import { SubaccountAnalyticsComponent } from './subaccount-analytics/subaccount-analytics.component';

@NgModule({
  imports: [
    CommonModule,
    AnalyticsRoutingModule,
    ComponentsModule,
    ChartsModule,
    Daterangepicker,
    FormsModule,
    DualListBoxModule.forRoot(),
  ],
  declarations: [
    MailSummaryComponent,
    AnalyticsComponent,
    IspReportComponent,
    GeoComponent,
    DeviceWiseComponent,
    TimeWiseComponent,
    WebhookComponent,
    ApiReportComponent,
    TestComponent,
    CompareSubAnalyticsComponent,
    SubaccountAnalyticsComponent,
  ]
})
export class AnalyticsModule {
}
