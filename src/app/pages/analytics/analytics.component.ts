import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-analytics',
  template: `
    <router-outlet></router-outlet>`,
  styles: []
})
export class AnalyticsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
