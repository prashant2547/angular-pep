import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MailSummaryComponent} from './mail-summary/mail-summary.component';
import {CompareSubAnalyticsComponent} from './compare-sub-analytics/compare-sub-analytics.component';
import {AnalyticsComponent} from './analytics.component';
import {SubaccountAnalyticsComponent} from './subaccount-analytics/subaccount-analytics.component';

const routes: Routes = [{
  path: '',
  component: AnalyticsComponent,
  children: [{
    path: 'mail-summary',
    component: MailSummaryComponent
  },  {
    path: 'subaccount-analytics',
    component: SubaccountAnalyticsComponent
  }, {
    path: 'compare-sub-analytics',
    component: CompareSubAnalyticsComponent
  }, {
    path: '',
    redirectTo: 'mail-summary',
    pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalyticsRoutingModule {
}
