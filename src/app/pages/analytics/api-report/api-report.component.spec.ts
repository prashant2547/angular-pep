import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiReportComponent } from './api-report.component';

describe('ApiReportComponent', () => {
  let component: ApiReportComponent;
  let fixture: ComponentFixture<ApiReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
