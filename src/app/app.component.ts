import {Component, OnDestroy, OnInit} from '@angular/core';
import {Spinkit} from 'ng-http-loader/spinkits';
import {MenuService} from './pages/services/menu.service';
import {Subscription} from 'rxjs/Subscription';
import {NavigationEnd, Router} from '@angular/router';

// import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public spinkit = Spinkit;
  currentYear = new Date().getFullYear();
  title = 'app';
  menuSubscription: Subscription;
  showMenu = false;

  constructor(private menuService: MenuService, private _router: Router) {
    this.menuSubscription = menuService.showMenu$.subscribe(bool => this.showMenu = bool);
  }

  ngOnInit() {
    // override the route reuse strategy
    this._router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this._router.events.subscribe((evt) => {
      // trick the Router into believing it's last link wasn't previously loaded
      if (evt instanceof NavigationEnd) {
        this._router.navigated = false;
        // if you need to scroll back to top, here is the right place
        window.scrollTo(0, 0);
      }
    });
  }

  ngOnDestroy(): void {
    this.menuSubscription.unsubscribe();
  }
}
