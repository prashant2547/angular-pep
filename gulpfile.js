var gulp = require('gulp');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');

var fontName = 'netcore-icons';

gulp.task('iconfont', function(){
  gulp.src(['src/assets/icons/icon-font/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
      path: 'src/app/theme/templates/_icons.scss',
      targetPath: '_icons-generated.scss',
      fontPath: 'app/theme/'
    }))
    .pipe(iconfont({
      fontName: fontName
    }))
    .pipe(gulp.dest('src/app/theme/'));
});
